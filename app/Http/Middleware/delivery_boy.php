<?php

namespace App\Http\Middleware;
use Auth;

use Closure;

class delivery_boy
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            
            return redirect()->route('delivery_login');
        }

        // if (Auth::user()->role == 'USER') {
        //     return redirect()->route('user');
        // }

        if (Auth::user()->role == 'DELIVERY') {
            return $next($request);
        }    
    }
}
