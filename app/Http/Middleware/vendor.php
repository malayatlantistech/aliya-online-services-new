<?php

namespace App\Http\Middleware;
use Auth;

use Closure;

class vendor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            
            return redirect()->route('vendor_login');
        }

        // if (Auth::user()->role == 'USER') {
        //     return redirect()->route('user');
        // }
       // dd(Auth::user()->status);
        if (Auth::user()->role == 'VENDOR') {
            
            if(Auth::user()->status=='NO'){
                //dd(Auth::user()->status);
                Auth::logout();
                return redirect()->route('approve');
            }elseif(Auth::user()->block=='YES'){
                //dd(Auth::user()->block);
                //return $next($request);
                    Auth::logout();
                return redirect()->route('block');
            }else{
                //return $next($request);
                return $next($request);
                
            }
            
        }else{
            Auth::logout();
        }    
    }
}
