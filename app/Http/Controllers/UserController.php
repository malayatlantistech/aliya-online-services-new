<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\user_wallet;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use GuzzleHttp\Client;
use Validator;
use Session;
use DB;
class UserController extends Controller
{
    public $successStatus = 200;

    public function login1(Request $request){

      
        Log::info($request);

        if(is_numeric(request('mobile')))
        {
          
                        if(Auth::attempt(['mobile' => request('mobile'), 'password' => request('password')])){
                            return redirect('/');
                        }
                        else{
                            return redirect()->back()->with('error','Invalid credentials!');

                        }


        }else{
                        if(Auth::attempt(['email' => request('mobile'), 'password' => request('password')])){
                            return redirect('/');
                        }
                        else{
                            // return Redirect::back ()->with('Invalid credentials!');
                            return redirect()->back()->with('error','Invalid credentials!');

                        }
        }
    }


    public function smsRequest($phone,$msg)
    {
        $client = new \GuzzleHttp\Client();
              $url = "http://weberleads.in/http-tokenkeyapi.php?authentic-key=".env('WEBERLEADS_SMS_API')."&senderid=".env('WEBERLEADS_SMS_SENDERID')."&route=2&number=91".$phone."&message=".$msg;
        $request = $client->get($url);
        $response = $request->getBody();
    }



    public function loginWithOtp(Request $request)
    {

        
        if(is_numeric(request('mobile')))
        {
            

            Log::info($request);
            $user  = User::where([['mobile','=',request('mobile')],['otp','=',request('otp')]])->first();
            
        }
        else
        {
          
            Log::info($request);
            $user  = User::where([['email','=',request('mobile')],['otp','=',request('otp')]])->first();
            

        }
      
        if( $user){
       
            Auth::login($user, true);
            User::where('mobile','=',$request->mobile)->update(['otp' => null]); 
            return response()->json(['success' => true,'msg'=>'Login Successful'], 200);

        }

    }
    public function loginWithOtp1(){
        return view('auth/OtpLogin');
    }
    public function register1(Request $request)
    {
        return view('auth/mediator_register');
    }
    
    public function register(Request $request)
    {


        $input = $request->all();
        $validator = Validator::make($input, [

            'password' => 'required|string|min:8|max:10',
    
        ]);


        if ($validator->fails()) 
        {
           
            return response()->json(['success' => false, 'errors' => $validator->getMessageBag()->toArray()], 400);
        }
        else
        {
              $input['password'] = Hash::make($request->password);
            $re=User::create($input);
            Log::info($request);
            $user  = User::where([['mobile','=',request('mobile')]])->first();
            Auth::login($user, true);
            $re=new user_wallet;
            $re->wallet_ammount=0;
            $re->user_id=Auth::user()->id;
            $re->save();
            return response()->json(['success' => true,'msg'=>'Register successful'],200);
            // return response()->json(['success' => true, 'message' =>'Otp Send Your Mobile Number.','otp11'=>$otp],200);

        }



      
      
    }


    public function sendOtp(Request $request){

       // $otp = rand(1000,9999);
        $otp =123456;
        Log::info("otp = ".$otp);
        if(is_numeric(request('mobile')))
        {
            $user= User::where('mobile','=',$request->mobile)->first();

            if($user)
            {
            User::where('mobile','=',$request->mobile)->update(['otp' => $otp]);
           // self::smsRequest($request->mobile,"Use This otp for Login".$otp);
            $r=1;
            echo $user."|".$otp."|". $r."|";
            }
            else{
                $user=0;
                $r=0;
                echo $user."|".$otp."|". $r."|";
            }
        }
        else{
           
            $user= User::where('email','=',$request->mobile)->first();
            if($user)
            {
            User::where('email','=',$request->mobile)->update(['otp' => $otp]);
           // self::smsRequest($request->mobile,"Use This otp for Login".$otp);
            $r=2;
            echo $user."|".$otp."|". $r."|";
            }
            else{
                $user=0;
                $r=0;
                echo $user."|".$otp."|". $r."|";
            }

        }
       
      
        
      

       // return response()->json([$user],200);
    }



    /********* Admin Login********** */
       
    public function admin_login(){

        $admin=DB::table('users')->where('role','ADMIN')->first();
        return view('admin.auth.login')->with('admin',$admin);
    }




    public function user_register_otp(Request $req)
    {
        $input = $req->all();
        $validator = Validator::make($input, [
                'mobile' => ['required', 'integer', 'digits:10', 'unique:users'],
            ]);

        if ($validator->fails()) 
        {
           
            return response()->json(['success' => false, 'errors' => $validator->getMessageBag()->toArray()], 400);
        }
        else
        {
            // $otp = rand(100000,999999);
            $otp=123456;
          //  self::smsRequest($req->mobile,"Use This otp for Login".$otp);
            return response()->json(['success' => true, 'message' =>'Otp Send Your Mobile Number.','otp11'=>$otp],200);

        }
    }
}
