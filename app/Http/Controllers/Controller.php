<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
     /**
     * 
     * @OA\Info(
     *      version="1.0.0",
     *      title="Aliya Online Service",
     *      description="Aliya Online Service API for Apps",
     * )
     */

    /**
	 * @OA\SecurityScheme(
	 *     securityScheme="bearerAuth",
	 *     type="http",
	 *     scheme="bearer",
	 *     in="header",
	 *     description="Passport bearer",
	 *     bearerFormat="Passport"
	 * )
	 */
    /**
	 *  @OA\Server(
	 *      url="http://localhost:8000/",
	 *  )
	 */

    /**
     * @OA\ExternalDocumentation(
     *     description="Frontend Link",
     *     url="http://localhost:8000/"
     * )
     */
}
