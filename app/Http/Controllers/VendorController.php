<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Rules\MatchOldPassword;
use DB;
use App\cat;
use App\sub_cat;
use App\sub_sub_cat;
use App\product_image;
use App\brand;
use App\group;
use Image;
use App\user_wallet_transaction;
use App\vendor_wallet_transaction;
use App\user_wallet;
use Illuminate\Support\Facades\Validator;
use App\display_banner;
use App\pincode;
use App\coupon_banner;
use App\product_features;
use App\product_spacification;
use Illuminate\Support\Facades\Mail;
use App\stock_trasanction;
use PDF;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\delivery_boy_pincode;
use App\delivery_wallet_transaction;
use App\vendor_wallet;
use App\addre;
use App\bank_details;
use App\withdradal_request;
use Session;
use Artisan;
use Toastr;
class VendorController extends Controller
{
    public function smsRequest($phone,$msg)
    {
        $client = new \GuzzleHttp\Client();
              $url = "http://weberleads.in/http-tokenkeyapi.php?authentic-key=".env('WEBERLEADS_SMS_API')."&senderid=".env('WEBERLEADS_SMS_SENDERID')."&route=2&number=91".$phone."&message=".$msg;
        $request = $client->get($url);
        $response = $request->getBody();
    }
    public function homepage()
    {
      //  dd(env('VENDOR_APP_URL'));
        return view('vendor.homepage');
    }
    public function vendor_login()
    {
        return view('vendor.auth.login');
    }
    public function vendor_register()
    {
        return view('vendor.auth.register');
    }
    protected function vendor_logout(){

        Auth::logout();
        return redirect()->route('vendor_login');
    }
    public function vendor_register_action(Request $request)
    {
        $validator = request()->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'mobile' => ['required', 'integer', 'min:10', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'companey_logo' => ['required'],
            'vendor_sign' => ['required'],
            'aadhar_image' => ['required'],
            'pan_image' => ['required'],
            'profile_image' => ['required'],
            'aadhar_no' => ['required','unique:users'],
            'pin_no' => ['required'],
            'gst_no' => ['required','unique:users'],
            'pan_no' => ['required','unique:users']
        ]);
          
        $image_array4=$request->file('companey_logo');
        $image_ext4=$image_array4->getClientOriginalExtension();
        $new_image_name4=rand(123456,999999).".".$image_ext4;
        $destination_path4="user_details/";
        $image_array4->move($destination_path4,$new_image_name4);
    
        $image_array5=$request->file('vendor_sign');
        $image_ext5=$image_array5->getClientOriginalExtension();
        $new_image_name5=rand(123456,999999).".".$image_ext5;
        $destination_path5="user_details/";
        $image_array5->move($destination_path5,$new_image_name5);
    


        $image_array1=$request->file('aadhar_image');
        $image_array2=$request->file('pan_image');
        $image_array3=$request->file('profile_image');


        $image_ext1=$image_array1->getClientOriginalExtension();
        $new_image_name1=rand(123456,999999).".".$image_ext1;
        $destination_path1="user_details/";
        $image_array1->move($destination_path1,$new_image_name1);


        $image_ext2=$image_array2->getClientOriginalExtension();
        $new_image_name2=rand(123456,999999).".".$image_ext2;
        $destination_path2="user_details/";
        $image_array2->move($destination_path2,$new_image_name2);

        $image_ext3=$image_array3->getClientOriginalExtension();
        $new_image_name3=rand(123456,999999).".".$image_ext3;
        $destination_path3="user_details/";
        $image_array3->move($destination_path3,$new_image_name3);
        
        $user=User::create([

            'name' => $request['name'],
            'role' => $request['role'],
            'address' => $request['address'],
            'pin_no' => $request['pin_no'],
            'mobile' => $request['mobile'],
            'companey_name' => $request['companey_name'],
            'pan_no' => $request['pan_no'],
            'aadhar_no' => $request['aadhar_no'],
            'gst_no' => $request['gst_no'],
            'email' => $request['email'],
            'aadhar_image'=>$new_image_name1,
            'pan_image'=>$new_image_name2,
            'profile_image'=>$new_image_name3,
            'vendor_sign'=>$new_image_name5,
            'companey_logo'=>$new_image_name4,
            'ref_code' =>'',
            'use_cust_id'=>0,
            'password' => Hash::make($request['password']),
        ]);

      

        $vendorWallet=new vendor_wallet;
        $vendorWallet->vendor_id=$user->id;
        $vendorWallet->wallet_amount=0;
        $vendorWallet->save();

            Toastr::success('You are successfully register.', 'Vendor Registration', ["positionClass" => "toast-top-right"]);
       
        return view('vendor.auth.welcome')->with('name',$request['name']);
    }
   
    public function block(){
        return view('vendor.auth.block');
    }
    public function approve(){
        return view('vendor.auth.approve');
    }
   
    public function dashboard()
    {
        $total_booking=DB::table('bookings')->count();
        $total_product=DB::table('products')->count();
        $normal_user=DB::table('users')->where('role','USER')->count();
        $madiator=DB::table('users')->where('role','USER')->count();
  
        $pending_booking=DB::table('bookings')
        ->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')
        ->where('book_multi_items.order_status',1)
        ->orwhere('book_multi_items.order_status',2)
        ->orwhere('book_multi_items.order_status',3)
        ->count();
  
        $return_booking=DB::table('bookings')
        ->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')
        ->where('book_multi_items.order_status',5)
        ->orwhere('book_multi_items.order_status',6)
  
        ->count();
  
        $cancel_booking=DB::table('bookings')
        ->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')
        ->where('book_multi_items.order_status',8)
        
        ->count();
  
  
          return view('vendor.index')
          ->with('total_booking',$total_booking)
          ->with('total_product',$total_product)
          ->with('normal_user',$normal_user)
          ->with('madiator',$madiator)
          ->with('pending_booking',$pending_booking)
          ->with('return_booking',$return_booking)
          ->with('cancel_booking',$cancel_booking);

    }
    public function category(){

        $cat=DB::table('cats')->get();
        return view('vendor.category')->with('cat',$cat);
    }
    public function view_sub_category(Request $req)
    {
    
    $cat_id=$req->cat_id;
    $cat_name=DB::table('cats')->where('cat_id',$cat_id)->first();
      $sub_cat=DB::table('sub_cats')->where('cat_id',$cat_id)->get();
    
    return view('vendor.view_sub_category')->with('cats',$sub_cat)->with('cat_name',$cat_name);
    
    }
    public function sub_cat_ajax()
   {
       return view('/vendor/sub_cat_ajax');
   }
   public function sub_sub_cat_ajax()
   {
       return view('/vendor/sub_sub_cat_ajax');
   }
   public function all_cats_deail()
   {
      $cats=DB::table('cats')->get();
  
       return view('/vendor/all_cats_details')->with('cat',$cats);
   }
   public function view_sub_sub_category(Request $req)
   {
   
     $sub_cat_id=$req->sub_cat_id;
     $cat_name=DB::table('cats')->join('sub_cats','sub_cats.cat_id','=','cats.cat_id')->where('sub_cat_id',$sub_cat_id)->first();
     $sub_sub_cat=DB::table('sub_sub_cats')->where('sub_cat_id',$sub_cat_id)->get();
      
   return view('vendor.view_sub_sub_category')->with('cats',$sub_sub_cat)->with('cat_name',$cat_name);
   
   }


   public function group(Request $req)
 {
  $brands=DB::table('groups')->get();
  return view('/vendor/group')->with('group',$brands);
 }
 public function add_group(Request $req)
 {
  return view('/vendor/add_group');
 }
 public function add_group_action(Request $req)
 {
  $cat1=$req->input('group_name');
    
  
     for($i=0;$i<count($cat1);$i++)
     {
     
      
     $contact = new group;
     $contact->group_name =$cat1[$i];
     $contact->remember_token = $req->input('_token');
     $contact->save();
     }
 
        Toastr::success('Group add successfully.', 'Product Group', ["positionClass" => "toast-top-right"]);
  
     return redirect('group');
    
 }
 public function update_group(Request $req)
 {
  $groups=DB::table('groups')->where('group_id',$req->id)->first();
  return view('/vendor/update_group')->with('group',$groups);
 }
 public function update_group_action(Request $req)
 {
  $group_name=$req->input('group_name');
  $group_id=$req->input('id');

      $pro=DB::table('groups')->where('group_id',$group_id)->update(['group_name' => $group_name]);
     
      if($pro){
        Toastr::success('Group update successfully.', 'Product Group', ["positionClass" => "toast-top-right"]);
    }else{
        Toastr::error('Somethings Wrong! try again later.', 'Product Group', ["positionClass" => "toast-top-right"]);
    }

     return redirect('group');
 }
 public function delete_group(Request $req)
 {
  $group_id=$req->input('id');
 
 
  $brands=DB::table('groups')->where('group_id',$group_id)->delete();
  if($brands){
    Toastr::success('Group delete successfully.', 'Product Group', ["positionClass" => "toast-top-right"]);
}else{
    Toastr::error('Somethings Wrong! try again later.', 'Product Group', ["positionClass" => "toast-top-right"]);
}
  return redirect('group');
 }


 public function brand(Request $req)
 {
  $brands=DB::table('brands')->get();
  return view('/vendor/brand')->with('brand',$brands);
 }
 public function add_brand(Request $req)
 {
  return view('/vendor/add_brand');
 }
 public function add_brand_action(Request $req)
 {
  $cat1=$req->input('brand_name');
     $cat=$req->file('brand_image');
  
     for($i=0;$i<count($cat1);$i++)
     {
      $image_name = uniqid() . '.' . $cat[$i]->getClientOriginalExtension();
      $destinationPath = 'brand_logo';
      $resize_image = Image::make($cat[$i]->getRealPath());
      $resize_image->resize(200,60, function($constraint){
      })->save($destinationPath . '/' . $image_name);
 
      
     $contact = new brand;
     $contact->brand_name =$cat1[$i];
     $contact->brand_image =$image_name;
     $contact->remember_token = $req->input('_token');
     $contact->save();
     }
 
        Toastr::success('Brand add successfully.', 'Product Brand', ["positionClass" => "toast-top-right"]);
  
     return redirect('brand');
    
 }
 public function update_brand(Request $req)
 {
  $brands=DB::table('brands')->where('brand_id',$req->id)->first();
  return view('/vendor/update_brand')->with('brand',$brands);
 }
 public function update_brand_action(Request $req)
 {
  $brand_name=$req->input('brand_name');
  $brand_id=$req->input('id');
 
 if($req->hasFile('brand_image'))
 {
         $image_array1=$req->file('brand_image');
     



         $image_name = uniqid() . '.' . $image_array1->getClientOriginalExtension();
         $destinationPath = 'brand_logo';
         $resize_image = Image::make($image_array1->getRealPath());
         $resize_image->resize(200,60, function($constraint){
         })->save($destinationPath . '/' . $image_name);
    



try{
         $image2=DB::table('brands')->where('brand_id',$brand_id)->first();
         $link="brand_logo/".$image2->brand_image;
         unlink($link);

        }
        catch (\Exception $e)
       {
            
       }
         $pro=DB::table('brands')->where('brand_id',$brand_id)->update(['brand_name' => $brand_name,'brand_image' => $image_name]);

     }else{
      $pro=DB::table('brands')->where('brand_id',$brand_id)->update(['brand_name' => $brand_name]);
     }

     if($pro){
        Toastr::success('Brand update successfully.', 'Product Brand', ["positionClass" => "toast-top-right"]);
    }else{
        Toastr::error('Somethings Wrong! try again later.', 'Product Brand', ["positionClass" => "toast-top-right"]);
    }
     return redirect('brand');
 }
 public function delete_brand(Request $req)
 {
  $brand_id=$req->input('id');
  $image2=DB::table('brands')->where('brand_id',$brand_id)->first();
  $link="brand_logo/".$image2->brand_image;
  unlink($link);
  $brands=DB::table('brands')->where('brand_id',$brand_id)->delete();
  return redirect('brand');
 }


public function view_details()
{
 $product_id=$_GET['product_id'];


 $feature=DB::table('product_features')->where('product_id',$product_id)->get();
 $specification=DB::table('product_spacifications')->where('product_id',$product_id)->get();
 
// $color2=DB::table('product_colors')->where('product_id',$product_id)->get();
 $image=DB::table('product_images')->where('product_id',$product_id)->get();

 $product2=DB::table('products')->where('product_id',$product_id)->first();
 $cat=DB::table('cats')->where('cat_id',$product2->cat_id)->first();
 $sub_cat=DB::table('sub_cats')->where('sub_cat_id',$product2->sub_cat_id)->first();
 $sub_sub_cat=DB::table('sub_sub_cats')->where('sub_sub_cat_id',$product2->sub_sub_cat_id)->first();

 $brand=DB::table('brands')->where('brand_id',$product2->brand_id)->first();
 $group=DB::table('groups')->where('group_id',$product2->group_id)->first();

 $related_product=DB::table('products')->where('group_id',$product2->group_id)->get();
 
 

return view('vendor.view_details')->with('product_id',$product_id)->with('feature',$feature)->with('product2',$product2)->with('specification',$specification)->with('image',$image)->with('cat',$cat)->with('sub_cat',$sub_cat)->with('sub_sub_cat',$sub_sub_cat)->with('brand',$brand)->with('group',$group)->with('related_product',$related_product);

}
public function add_product()
{
    $product1=DB::table('products')->select('product_code')->orderby('product_code','desc')->first();
    $product_count=DB::table('products')->select('product_code')->orderby('product_code','desc')->count();
    if($product_count==0){
        $product=sprintf("%06d",1);
    }else{
        $product=sprintf("%06d",$product1->product_code+1);
    }
    $cat=DB::table('cats')->get();
    $brand=DB::table('brands')->get();
    $group=DB::table('groups')->get();
    return view('vendor.add_product')->with('cat',$cat)->with('brand',$brand)->with('group',$group)->with('product_code',$product);
}
public function view_product()
{
    $product=DB::table('products')->where('vendor_id',Auth::user()->id)->get();
    return view('vendor.view_product')->with('product',$product);
}


protected function update_product_details(Request $req)
{
   $product_id=$req->product_id;
   $pro=DB::table('products')->where('product_id',$product_id)->first();
   $brand=DB::table('brands')->get();
   $groups=DB::table('groups')->get();
   $cat=DB::table('cats')->get();
   return view('vendor.update_product_details')->with('pro',$pro)->with('cat1',$cat)->with('brand',$brand)->with('group',$groups);
 }


 protected function update_product_details_code(Request $req)
{

    $validatedData = $req->validate([
        'product_name' => 'required',
        'hsn_code' => 'required',
        'brand_id' => 'required',
        'cat_id' => 'required',
        'sub_cat_id' => 'required',
        'sub_sub_cat_id' => 'required',
    ]);

    if(strlen($req->size)==0){
        $size='NO';
    }
    else{
        $size=$req->size;
    }

    if(strlen($req->color)==0){
        $color='NO';
    }
    else{
        $color=$req->color;
    }


   $product_id=$req->product_id;
   $product_name=$req->product_name;
   $hsn_code=$req->hsn_code;
   $brand_id=$req->brand_id;
   $group_id=$req->group_id;
   $cat_id=$req->cat_id;
   $sub_cat_id=$req->sub_cat_id;
   $sub_sub_cat_id=$req->sub_sub_cat_id;
  

   $pro=DB::table('products')->where('product_id',$product_id)->update(['product_name'=>$product_name,'hsn_code'=>$hsn_code,'cat_id'=>$cat_id,'sub_cat_id'=>$sub_cat_id,'sub_sub_cat_id'=>$sub_sub_cat_id,'brand_id'=>$brand_id,'group_id'=>$group_id,'size'=>$size,'color'=>$color]);
   if($pro){
    Toastr::success('Details update successfully.', 'Product Details', ["positionClass" => "toast-top-right"]);
}else{
    Toastr::error('Somethings Wrong! try again later.', 'Product Details', ["positionClass" => "toast-top-right"]);
}
   
   return redirect("view_details?product_id=$product_id");
 }


 protected function update_product_description(Request $req){
   $product_id=$req->product_id;
   $pro=DB::table('products')->where('product_id',$product_id)->first();
   return view('vendor.update_product_description')->with('pro',$pro)->with('product_id',$product_id);
  
 }

 protected function update_product_description_code(Request $req){
   $product_id=$req->product_id;
    $description=$req->editor1;


   $pro=DB::table('products')->where('product_id',$product_id)->update(['description'=>$description]);
   if($pro){
    Toastr::success('Description update successfully.', 'Product Description', ["positionClass" => "toast-top-right"]);
}else{
    Toastr::error('Somethings Wrong! try again later.', 'Product Description', ["positionClass" => "toast-top-right"]);
}
   return redirect("view_details?product_id=$product_id");
  
 }




 protected function add_features(Request $req){
    $product_id=$req->id;


  
   $pro=DB::table('products')->where('product_id',$product_id)->first();
   return view('vendor.add_feature')->with('pro',$pro)->with('product_id',$product_id);
  
 }

 protected function add_features_code(Request $req){
   $product_id=$req->id;

   
   $token=$req->_token;
   $feture=$req->input('feature');

   for($i=0;$i<count($feture);$i++)
   {
       $features= new product_features;
       $feature1=$feture[$i];
       $features->features=$feature1;
       $features->product_id=$product_id;
       $features->remember_token=$token;
       $features->save();
   }

    Toastr::success('Features add successfully.', 'Product Features', ["positionClass" => "toast-top-right"]);

   return redirect("view_details?product_id=$product_id");
  
 }

 function delete_feature(Request $req)
 {
     $product_id=$req->product_id;
     $feature=$req->id;
     $pro=DB::table('product_features')->where('product_feature_id',$feature)->delete();
     if($pro){
        Toastr::success('Features delete successfully.', 'Product Features', ["positionClass" => "toast-top-right"]);
    }else{
        Toastr::error('Somethings Wrong! try again later.', 'Product Features', ["positionClass" => "toast-top-right"]);
    }
     return redirect("view_details?product_id=$product_id");

 }


 protected function add_specification(Request $req)
 {
  
   $product_id=$req->id;
   $pro=DB::table('products')->where('product_id',$product_id)->first();
   return view('vendor.add_specification')->with('pro',$pro)->with('product_id',$product_id);
  
 }


 protected function add_specification_code(Request $req){
   $product_id=$req->id;


   $token=$req->_token;

 $specification_name=$req->input('specification');
 $specification_type=$req->input('specification1');

  
   for($i=0;$i<count($specification_name);$i++)
   {
   
       $specification= new product_spacification;
       $specification_name1=$specification_name[$i];
       $specification_type1=$specification_type[$i];
       $specification->title=$specification_name1;
       $specification->description=$specification_type1;
       $specification->product_id=$product_id;
       $specification->remember_token=$token;
       $specification->save();
   }
 
    Toastr::success('Specification add successfully.', 'Product Specification', ["positionClass" => "toast-top-right"]);


   return redirect("view_details?product_id=$product_id");
  
 }

 

 function delete_specification(Request $req)
 {
     $specification_id=$req->id;

     $product_id=$req->product_id;

     $pro=DB::table('product_spacifications')->where('product_spacifications_id',$specification_id)->delete();
     if($pro){
        Toastr::success('Specification delete successfully.', 'Product Specification', ["positionClass" => "toast-top-right"]);
    }else{
        Toastr::error('Somethings Wrong! try again later.', 'Product Specification', ["positionClass" => "toast-top-right"]);
    }
     return redirect("view_details?product_id=$product_id");

 }
 




 public function add_product_image(Request $req)
{
    $id=$req->input('product_id');
   
    $image_array3=$req->file('image');
  
    for($i=0;$i<count($image_array3);$i++)
    {


 


       $image_name[$i] = uniqid() . '.' . $image_array3[$i]->getClientOriginalExtension();

       $destinationPath ='product_image';
       $destinationPath1 = 'small_product_image';

       $resize_image[$i] = Image::make($image_array3[$i]->getRealPath());

       $resize_image[$i]->resize(1000,1000, function($constraint){
       // $constraint->aspectRatio();
       })->save($destinationPath . '/' . $image_name[$i]);

      $resize_image[$i]->resize(255,255, function($constraint){
           // $constraint->aspectRatio();
           })->save($destinationPath1 . '/' . $image_name[$i]);


       $contact = new product_image;
       $contact->product_id =$id;
     
       $contact->image =$image_name[$i];
       $contact->remember_token = $req->input('_token');
       $contact->save();
    }
    return redirect('/view_details?product_id='.$id.'');
   /* $banner=DB::table('display_banners')->get();
   return view('admin.display_banner')->with('banner',$banner);*/
   
    Toastr::success('Image add successfully.', 'Product Image', ["positionClass" => "toast-top-right"]);

}
public function delete_product_image()
{
    $id=$_GET['id'];
try{
    $image=DB::table('product_images')->where('product_image_id',$id)->first();
    $link="product_image/".$image->image;
    $link1="small_product_image/".$image->image;

    unlink($link);
    unlink($link1);
  }
  catch (\Exception $e)
 {
      
 }

    $delete_query=DB::table('product_images')->where('product_image_id',$id)->delete();
    if($delete_query){
        Toastr::success('Image delete successfully.', 'Product Image', ["positionClass" => "toast-top-right"]);
    }else{
        Toastr::error('Somethings Wrong! try again later.', 'Product Image', ["positionClass" => "toast-top-right"]);
    }
    
    return redirect()->back();
   /* $banner=DB::table('display_banners')->get();
   return view('admin.display_banner')->with('banner',$banner);*/

}
protected function update_price(){
   $price_id=$_GET['product_id'];

   $pro=DB::table('products')->where('product_id',$price_id)->first();
   return view('vendor.update_price')->with('price1',$pro);
  
 }
 protected function update_price_code(Request $req){


   $id=$req->input('product_id');
   $weight=$req->input('weight');
   $mrp=$req->input('mrp');
   $selling_price=$req->input('selling_price');




  $a= DB::table('products')->where('product_id',$id)->update(['weight'=> $weight,'mrp'=> $mrp,'selling_price'=> $selling_price]);

  
   if($a){
    Toastr::success('Details update successfully.', 'Price Details', ["positionClass" => "toast-top-right"]);
}else{
    Toastr::error('Somethings Wrong! try again later.', 'Price Details', ["positionClass" => "toast-top-right"]);
}

   $product_id=$req->input('product_id');
   return redirect('/view_details?product_id='.$product_id.'');
 }

 public function approve1(Request $qq)
 {
     $product_id=$qq->product_id;
     $status=$qq->y;
     if($status=='yes')
 {
    
    $a=DB::table('products')->where('product_id',$product_id)->update(['vendor_activate_status' =>'YES']);
    if($a){
        Toastr::success('Product successfully active.', 'Product Approval', ["positionClass" => "toast-top-right"]);
    }else{
        Toastr::error('Somethings Wrong! try again later.', 'Product Approval', ["positionClass" => "toast-top-right"]);
    }
 }
 else{

    $a=DB::table('products')->where('product_id',$product_id)->update(['vendor_activate_status' =>'NO']);
    if($a){
        Toastr::info('Product successfully Inactive.', 'Product Approval', ["positionClass" => "toast-top-right"]);
    }else{
        Toastr::error('Somethings Wrong! try again later.', 'Product Approval', ["positionClass" => "toast-top-right"]);
    }

 }

 return redirect('/view_product');
}
protected function profile(){
    return view('vendor.profile');
}

protected function change_password(){
    return view('vendor.change_password');
}
public function store_address()
{
  $id=Auth::user()->id;;
  $address=DB::table('addres')->where('user_id', $id)->first();
  $address_count=DB::table('addres')->where('user_id', $id)->count();
  return view('vendor.store_address')->with('address_count',$address_count)->with('address',$address);
}
public function store_address_action(Request $req11)
      {
        $user_id=Auth::user()->id;
        $phone2=Auth::user()->phone_no;
        $email=Auth::user()->email ;
        $token=$req11->input('_token');
        $phone1=$req11->input('phone');
        $name=$req11->input('name');
        $companey_name=$req11->input('companey_name');
        $address=$req11->input('address');
        $state=$req11->input('state');
        $district=$req11->input('district');
        $city=$req11->input('city');
        $area=$req11->input('area');
        $pin_code=$req11->input('pin_code');
        $landmark=$req11->input('landmark');
        $house_no=$req11->input('house_no');
        $address_co=DB::table('addres')->where('user_id', $user_id)->count();
   
                
                    if($address_co==0){
                    
                        $address2=new addre;
                        $address2->flat=$house_no;
                        $address2->landmark=$landmark;
                        $address2->location=$area;        
                        $address2->pincode=$pin_code;
                        $address2->city=$city;
                        $address2->district=$district;
                        $address2->state=$state;
                        $address2->name=$name;
                        $address2->phone_no=$phone1;
                        $address2->optional_phone=$phone2;

                        $address2->address=$address;
                        $address2->gst_no='NONE';
                        $address2->user_id=$user_id;
                        $address2->remember_token=$token;
                        $address2->save();
                
                      

                        Toastr::success('Address Successfully Saved', 'Store Address', ["positionClass" => "toast-top-right"]);
                        return redirect()->back();
                
                
                    }
                    else
                    {   
                
                  
                        $a=DB::table('addres')
                        ->where('user_id', $user_id)
                        ->update(['flat' =>$house_no,'landmark' =>$landmark,'location' =>$area,'pincode' =>$pin_code,'city' =>$city,'district' =>$district,'state' =>$state,'name' =>$name,'phone_no' =>$phone1,'optional_phone' =>$phone2,'address' =>$address,'remember_token'=>$token]);
                  
                 
                        if($a){
                            Toastr::success('Address Successfully Update', 'Store Address', ["positionClass" => "toast-top-right"]);
                        }else{
                            Toastr::error('Address not Updated', 'Store Address', ["positionClass" => "toast-top-right"]);
                        }
                        
                        return redirect()->back();
                    }

   }

   //bank
  public function bank_details()
  {
    $id=Auth::user()->id;
    $bank_count=DB::table('bank_details')->where('vendor_id',$id)->count();
    if($bank_count=='0')
    {
        return view('/vendor/bank_details');  
    }
    else
    {
        $bank=DB::table('bank_details')->where('vendor_id',$id)->get();
        return view('/vendor/bank_details_update')->with('bank', $bank); 
    }
     
  }
  public function bank()
  {
  

    $delivery_id=Auth::user()->id;
    $rte=bank_details::where('delivery_id',$delivery_id)->first();

  return view('vendor.bank_details',['details'=>$rte]);
     
  }

  public function add_bank(Request $req)
  {
    
    $delivery_id=Auth::user()->id;
        $a_h_name=$req->a_h_name;
        $bank_name=$req->bank_name;
        $b_name=$req->b_name;
        $a_no=$req->a_no;
        $ifcs_code=$req->ifcs_code;
        $upi_id=$req->upi_id;

        $validator = request()->validate([
            'a_h_name' => 'required',
            'bank_name' => 'required',
            'b_name' => 'required',
            'a_no' => 'required|integer',
            'ifcs_code' => 'required',
            'upi_id' => 'required',
        ],[
            'a_h_name.required'=>'Please enter Account Holder Name',
            'b_name.required'=>'Please enter Brach Name',
            'bank_name.required'=>'Please enter Bank Name',
            'a_no.required'=>'Please enter Account Number',
            'a_no.integer'=>'Account Number must be integer',
            'ifcs_code.required'=>'Please enter IFSC Code',
            'upi_id.required'=>'Please enter your UPI ID'
        ]);
        $tr=bank_details::where('delivery_id',$delivery_id)->count();
        if($tr==0)
        {
        $b_d=new bank_details; 
        $b_d->a_h_name=$a_h_name;
        $b_d->qr_code= $bank_name;
        $b_d->b_name= $b_name;
        $b_d->a_no=$a_no;
        $b_d->ifcs_code=$ifcs_code;
        $b_d->upi_id=$upi_id;
        $b_d->delivery_id=$delivery_id;
        $re=$b_d->save();
        $dd='Bank Add Successfully';
        
        }
        else
        {
           $re=bank_details::where('delivery_id',$delivery_id)->update(['a_h_name'=>$a_h_name,'qr_code'=>$bank_name,'b_name'=>$b_name,'a_no'=>$a_no,'ifcs_code'=>$ifcs_code,'upi_id'=>$upi_id,]);
           $dd='Bank Update Successfully';
        }
if($re){
    Toastr::success($dd, 'Bank Details', ["positionClass" => "toast-top-right"]);
}else{
    Toastr::error('Bank not Updated', 'Bank Details', ["positionClass" => "toast-top-right"]);
}
            
            $gs=bank_details::where('delivery_id',$delivery_id)->first();
            return redirect()->back();
        
    
  }


  public function update_bank(Request $req)
  {
    $delivery_id=Auth::user()->id;
    $a_h_name=$req->a_h_name;
    $bank_name=$req->bank_name;
    $b_name=$req->b_name;
    $a_no=$req->a_no;
    $ifcs_code=$req->ifcs_code;
    $upi_id=$req->upi_id;

    $validator = request()->validate([
        'a_h_name' => 'required',
        'bank_name' => 'required',
        'b_name' => 'required',
        'a_no' => 'required|integer',
        'ifcs_code' => 'required',
        'upi_id' => 'required',
    ],[
        'a_h_name.required'=>'Please enter Account Holder Name',
        'b_name.required'=>'Please enter Brach Name',
        'bank_name.required'=>'Please enter Bank Name',
        'a_no.required'=>'Please enter Account Number',
        'a_no.integer'=>'Account Number must be integer',
        'ifcs_code.required'=>'Please enter IFSC Code',
        'upi_id.required'=>'Please enter your UPI ID'
    ]);
    $tr=bank_details::where('delivery_id',$delivery_id)->count();
    if($tr==0)
    {
    $b_d=new bank_details; 
    $b_d->a_h_name=$a_h_name;
    $b_d->qr_code= $bank_name;
    $b_d->b_name= $b_name;
    $b_d->a_no=$a_no;
    $b_d->ifcs_code=$ifcs_code;
    $b_d->upi_id=$upi_id;
    $b_d->delivery_id=$delivery_id;
    $re=$b_d->save();
    $dd='Bank Add Successfully';
    }
    else
    {
       $re=bank_details::where('delivery_id',$delivery_id)->update(['a_h_name'=>$a_h_name,'qr_code'=>$bank_name,'b_name'=>$b_name,'a_no'=>$a_no,'ifcs_code'=>$ifcs_code,'upi_id'=>$upi_id,]);
       $dd='Bank Update Successfully';
    }
    return redirect()->back();

   
  }
  protected function edit_profile(Request $details)
  {
      return view('/vendor/edit_profile');
  }
  protected function edit_profile_code(Request $details)
  {
    $validator = request()->validate([
        'companey_name' => 'required',
        'address' => 'required',
        'pin_no' => 'required',
        'aadhar_no' => 'required',
        'pan_no' => 'required',
        'gst_no'=> 'required',
    ],[
        'companey_name.required'=>'Please Enter Your Company Name',
        'mobile.required'=>'Please Enter Your Phone Number',
        'mobile.unique'=>'This Phone Number already exits',
        'address.required'=>'Please Enter Your Address',
        'pin_no.required'=>'Please Enter Your Pin Number',
        'aadhar_no.required' => 'Please Enter Your Aadhar Card Number',
        'pan_no.required'=>'Please Enter Your Pan Card Number',
        'gst_no.required'=>'Please Enter Your GST Number'
    ]);

      $id=Auth::user()->id;
      $name=$details->input('name');
      $companey_name=$details->input('companey_name');
      $email=$details->input('email');
      $phone_no=$details->input('mobile');
      $address=$details->input('address');
      $pin_no=$details->input('pin_no');
      $aadhar_no=$details->input('aadhar_no');
      $pan_no=$details->input('pan_no');
      $gst_no=$details->input('gst_no');

      if($details->hasFile('aadhar_image'))
      {
          $image_array1=$details->file('aadhar_image');
          $image_ext1=$image_array1->getClientOriginalExtension();
          $new_image_name1=rand(123456,999999).".".$image_ext1;
          $destination_path1="user_details/";
          $image_array1->move($destination_path1,$new_image_name1);

      }else{
          $new_image_name1=$details->input('org_aadhar_image');
      }
      if($details->hasFile('pan_image'))
      {
          $image_array2=$details->file('pan_image');
          $image_ext2=$image_array2->getClientOriginalExtension();
          $new_image_name2=rand(123456,999999).".".$image_ext2;
          $destination_path2="user_details/";
          $image_array2->move($destination_path2,$new_image_name2);

      }else{
          $new_image_name2=$details->input('org_pan_image');
      }
      if($details->hasFile('profile_image'))
      {
          $image_array3=$details->file('profile_image');
          $image_ext3=$image_array3->getClientOriginalExtension();
          $new_image_name3=rand(123456,999999).".".$image_ext3;
          $destination_path3="user_details/";
          $image_array3->move($destination_path3,$new_image_name3);

      }else{
          $new_image_name3=$details->input('org_profile_image');
      }
      if($details->hasFile('companey_logo'))
      {
          $image_array4=$details->file('companey_logo');
          $image_ext4=$image_array4->getClientOriginalExtension();
          $new_image_name4=rand(123456,999999).".".$image_ext4;
          $destination_path4="user_details/";
          $image_array4->move($destination_path4,$new_image_name4);

      }else{
          $new_image_name4=$details->input('org_acompaney_logo');
      }
      if($details->hasFile('vendor_sign'))
      {
          $image_array5=$details->file('vendor_sign');
          $image_ext5=$image_array5->getClientOriginalExtension();
          $new_image_name5=rand(123456,999999).".".$image_ext5;
          $destination_path5="user_details/";
          $image_array5->move($destination_path5,$new_image_name5);

      }else{
          $new_image_name5=$details->input('org_vendor_sign');
      }




      $re=DB::table('users')->where('id',$id)->update(['name' =>$name,'companey_name' =>$companey_name,'email' =>$email,'mobile' =>$phone_no,'pin_no' =>$pin_no,'address' =>$address,'aadhar_no' =>$aadhar_no,'pan_no'=> $pan_no,'gst_no'=>$gst_no,'aadhar_image'=>$new_image_name1,'pan_image'=>$new_image_name2,'profile_image'=>$new_image_name3,'companey_logo'=>$new_image_name4,'vendor_sign'=>$new_image_name5]);
    if($re){
        Toastr::success('Profile Update Successfully.', 'Your Profile', ["positionClass" => "toast-top-right"]);
    }else{
        Toastr::error('Somethings wrong! Please try again later.', 'Your Profile', ["positionClass" => "toast-top-right"]);
    }
      return redirect('profile');
  }
  protected function change_password1(Request $request)
  {
     /* $data->validate([
              'password'=>"required|min:6",
            
              
      ]);
         
      $id=Auth::user()->id;
      echo Auth::user()->password."<br>";
      echo Hash::make($data['current_password'])."<br>";
      echo Hash::make(11111111)."<br>";
      echo Hash::make(11111111);
      exit;
      if(Auth::user()->password==(Hash::make($data['current_password']))){
          if(($data->password)==($data->confirm)){

              DB::table('users')->where('id',$id)->update(['password' =>Hash::make($data['current_password'])]);
                  }else{
              return redirect('/vendor/change_password?pas');
          }
      }else{
          return redirect('/vendor/change_password?pas');
      }*/

      $request->validate([

          'current_password' => 'required', new MatchOldPassword,

          'password' => 'required|min:8',

          'confirm' => 'same:password',

      ]);

 

      User::find(auth()->user()->id)->update(['password'=> Hash::make($request->password)]);
      Toastr::success('Password Change Successfully.', 'Password Change', ["positionClass" => "toast-top-right"]);
          return redirect()->back();

    //  dd('Password change successfully.');
  }
  public function inventory_report()
{
  $product1=DB::table('products')->where('vendor_id',Auth::user()->id)->get();
    return view('vendor.inventory_report')->with('product',$product1);
}
public function inventory_details(Request $req)
{
    $product_id=$req->product_id;
    $product1=DB::table('products')->where('product_id',$product_id)->first();
    $stoke_trans=DB::table('stock_trasanctions')->where('product_id','=',$product_id)->get();
    return view('vendor.inventory_details')->with('product',$product1)->with('stoke_trans',$stoke_trans);
}
public function add_stock(Request $req)
{
    $product_id=$req->product_id;
    $product1=DB::table('products')->where('product_id',$product_id)->first();
    $stoke_trans=DB::table('stock_trasanctions')->where('product_id','=',$product_id)->get();
    return view('vendor.add_stock')->with('product',$product1)->with('stoke_trans',$stoke_trans);
}

public function add_stock_code(Request $req)
{
    $product_id=$req->product_id;
    $transfer_id=$req->transfer_id;
    $remarks=$req->remarks;
    $transfer_qty=$req->transfer_qty;

   $product1=DB::table('products')->where('product_id',$product_id)->first();
 
   $total_stock=($product1->total_stock)+$transfer_qty;

   $available_stock=($product1->available_stock)+$transfer_qty;
   DB::table('products')->where('product_id',$product_id)->update(['total_stock'=>$total_stock,'available_stock'=>$available_stock]);

   $stock= new stock_trasanction;
   $stock->product_id=$product_id;

   $stock->quantity=$transfer_qty;
   $stock->type='CREDIT';
   $stock->remarks='ADD STOCK';
   $stock->transfer_id=$transfer_id;
   $stock->total_quantity=$available_stock;

   $stock->save();
   Toastr::success('Stock add Successfully.', 'Inventory Details', ["positionClass" => "toast-top-right"]);
   return redirect('inventory_details?product_id='.$product_id);

}

public function pending_order(Request $req)
{
    $booking_details=DB::table('bookings')
    ->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')
    ->join('users','users.id','=','bookings.customer_id')
    ->join('products','book_multi_items.product_id','=','products.product_id')
    ->where('products.vendor_id',Auth::user()->id)
    ->where(function ($query) {
        $query->where('book_multi_items.order_status',1)
        ->orwhere('book_multi_items.order_status',2)
        ->orwhere('book_multi_items.order_status',3);
    })
    ->groupby('book_multi_items.booking_id')
    ->select(['bookings.*','users.name','users.email'])
    ->get();;

  return view('vendor.pending_order')->with('booking',$booking_details);

}

public function cancel_order(Request $req)
{
    $booking_details=DB::table('bookings')
    ->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')
    ->join('users','users.id','=','bookings.customer_id')
    ->join('products','book_multi_items.product_id','=','products.product_id')
    ->where('products.vendor_id',Auth::user()->id)
    ->where('book_multi_items.order_status',8)
    ->groupby('book_multi_items.booking_id')
    ->select(['bookings.*','users.name','users.email'])
    ->get();
  return view('vendor.cancel_booking')->with('booking',$booking_details);

}


public function return_order(Request $req)
{
    $booking_details=DB::table('bookings')
    ->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')
    ->join('users','users.id','=','bookings.customer_id')
    ->join('products','book_multi_items.product_id','=','products.product_id')
    ->where('products.vendor_id',Auth::user()->id)
    ->where(function ($query) {
        $query->where('book_multi_items.order_status',5)
        ->orwhere('book_multi_items.order_status',6);
    })
    ->groupby('book_multi_items.booking_id')
    ->select(['bookings.*','users.name','users.email'])
    ->get();

  return view('vendor.return_booking')->with('booking',$booking_details);

}


public function complete_return_order(Request $req)
{
    if(!isset($req->complete_order))
    {
    $booking_details=DB::table('bookings')
    ->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')
    ->join('users','users.id','=','bookings.customer_id')
    ->join('products','book_multi_items.product_id','=','products.product_id')
    ->where('products.vendor_id',Auth::user()->id)
    ->where('book_multi_items.order_status',7)
    ->select(['bookings.*','users.name','users.email'])
    ->groupby('book_multi_items.booking_id')
    ->get();
    }
  return view('vendor.complete_return_order')->with('booking',$booking_details);

}

public function complete_order(Request $req)
{
    $booking_details=DB::table('bookings')
    ->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')
    ->join('users','users.id','=','bookings.customer_id')
    ->join('products','book_multi_items.product_id','=','products.product_id')
    ->where('products.vendor_id',Auth::user()->id)
    ->where('book_multi_items.order_status',4)
    ->groupby('book_multi_items.booking_id')
    ->select(['bookings.*','users.name','users.email'])
    ->get();
  

  return view('vendor.complete_order')->with('booking',$booking_details);

}




public function view_booking_details(Request $req)
{
 $booking_id=$_GET['booking_id'];
 $booking_details=DB::table('bookings')->where('booking_id',$booking_id)->get();
 if(isset($req->cancel)){
   $multi_details=DB::table('bookings')
   ->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')
   ->join('products','products.product_id','=','book_multi_items.product_id')
   ->join('users','users.id','=','bookings.customer_id')
   ->where('products.vendor_id',Auth::user()->id)
   ->where('bookings.booking_id',$booking_id)
   ->where('book_multi_items.order_status',8)
   ->get();
 }elseif(isset($req->return)){
   $multi_details=DB::table('bookings')->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')->join('products','products.product_id','=','book_multi_items.product_id')->join('users','users.id','=','bookings.customer_id')->where('bookings.booking_id',$booking_id)
   ->where('products.vendor_id',Auth::user()->id)
   ->where(function($q){ 
   $q->where('book_multi_items.order_status',5)
   ->orwhere('book_multi_items.order_status',6);
    })
   ->get();

 }elseif(isset($req->complete_return)){
   $multi_details=DB::table('bookings')
   ->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')
   ->join('products','products.product_id','=','book_multi_items.product_id')
   ->join('users','users.id','=','bookings.customer_id')
   ->where('products.vendor_id',Auth::user()->id)
   ->where('bookings.booking_id',$booking_id)
   ->where('book_multi_items.order_status',7)
   ->get();

 }elseif(isset($req->pending_order)){
   $multi_details=DB::table('bookings')
   ->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')
   ->join('products','products.product_id','=','book_multi_items.product_id')
   ->join('users','users.id','=','bookings.customer_id')
   ->where('bookings.booking_id',$booking_id)
   ->where('products.vendor_id',Auth::user()->id)
   ->where(function($q){ 
       $q->where('book_multi_items.order_status',1)
       ->orwhere('book_multi_items.order_status',2)
       ->orwhere('book_multi_items.order_status',3);
   })
   ->get();

 }elseif(isset($req->complete_order)){
   $multi_details=DB::table('bookings')->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')->join('products','products.product_id','=','book_multi_items.product_id')->join('users','users.id','=','bookings.customer_id')->where('bookings.booking_id',$booking_id)
   ->where('products.vendor_id',Auth::user()->id)
   ->where('book_multi_items.order_status',4)->get();

 }
 elseif(isset($req->complete_return_order)){
   $multi_details=DB::table('bookings')->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')->join('products','products.product_id','=','book_multi_items.product_id')->join('users','users.id','=','bookings.customer_id')->where('bookings.booking_id',$booking_id)
   ->where('products.vendor_id',Auth::user()->id)
   ->where('book_multi_items.order_status',7)->get();

 }
 else{
   $multi_details=DB::table('bookings')->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')->join('products','products.product_id','=','book_multi_items.product_id')->join('users','users.id','=','bookings.customer_id')
   ->where('products.vendor_id',Auth::user()->id)
   ->where('bookings.booking_id',$booking_id)->get();
 }

 return view('vendor.view_booking_details')->with('booking_details',$booking_details)->with('multi_details',$multi_details);
}

public function order_status(Request $req)
{
echo  $booking_id=$req->booking_id;
echo  $booking_multi_id=$req->book_multi_id;
 $staTUS=DB::table('book_multi_items')->where('booking_id',$booking_id)->where('multi_id',$booking_multi_id)->first();
 $staTUS1=DB::table('bookings')->where('booking_id',$booking_id)->first();
// print_r($staTUS);
 $product_id=$staTUS->product_id;
 $productDetails=DB::table('products')->join('sub_cats','products.sub_cat_id','=','sub_cats.sub_cat_id')->where('products.product_id','=',$staTUS->product_id)->first();
 $selling_price=$productDetails->selling_price;
 $gst=$productDetails->gst;
 $commission=$productDetails->commission;
 $order_price=$staTUS1->price;
 $wallet_price=$staTUS1->wallet_amount;
 $product_price=($staTUS->product_price+$staTUS->gst)*$staTUS->quantity1;
 $user_id=$staTUS1->customer_id;
 $order_id=$staTUS1->order_id;
 //print_r($staTUS);
 if($staTUS->order_status==3){
    DB::table('book_multi_items')->where('booking_id',$booking_id)->where('multi_id',$booking_multi_id)->update(['book_multi_items.order_status'=>$staTUS->order_status+1,'book_multi_items.delivery_date'=>date('Y-m-d h:i:s')]);


    // $product_priceeeee=$selling_price+($selling_price*($gst/100));
    // $refound_amount=$product_priceeeee-($product_priceeeee*($commission/100));
    //    $vendor_id=Auth::user()->id;
    //    $wallet=DB::table('vendor_wallets')->where('vendor_id',$vendor_id)->first();
    //    $wallet_amount1=$wallet->wallet_amount;
    //    $remaining_wallet_amount=$wallet_amount1+$refound_amount;
    //    $user_wallet1=new vendor_wallet_transaction;
    //    $user_wallet1->vendor_id=$vendor_id;
    //    $user_wallet1->transaction_amount=$refound_amount;
    //    $user_wallet1->after_transaction_amount=$remaining_wallet_amount;
    //    $user_wallet1->remarks='Booking Completed';
    //    $user_wallet1->order_id=$order_id;
    //    $user_wallet1->tranasaction_type='Credit';
    //    $user_wallet1->booking_id=$booking_id;
    //    $user_wallet1->transaction_id=strtoupper(uniqid());;
    //    $user_wallet1->save();
    //    DB::table('vendor_wallets')->where('vendor_id',$vendor_id)->update(['wallet_amount' => $remaining_wallet_amount]); 
 }
 elseif($staTUS->order_status==6)
 {
   DB::table('book_multi_items')->where('booking_id',$booking_id)->where('multi_id',$booking_multi_id)->update(['book_multi_items.order_status'=>$staTUS->order_status+1,'book_multi_items.return_date'=>date('Y-m-d h:i:s')]);
        if($wallet_price!=0)
        {
            $persentage=($product_price/$order_price)*100;
            $refound_amount=$wallet_price*($persentage/100);
            $wallet=DB::table('user_wallets')->where('user_id',$user_id)->first();
            $wallet_amount1=$wallet->wallet_ammount;
            $remaining_wallet_amount=$wallet_amount1+$refound_amount;
            $user_wallet1=new user_wallet_transaction;
            $user_wallet1->user_id=$user_id;
            $user_wallet1->transaction_amount=$refound_amount;
            $user_wallet1->after_transaction_amount=$remaining_wallet_amount;
            $user_wallet1->remarks='Return Refund';
            $user_wallet1->order_id=$order_id;
            $user_wallet1->tranasaction_type='Credit';
            $user_wallet1->booking_id=$booking_id;
            // $user_wallet1->remember_token=$token;
            $user_wallet1->save();
            DB::table('user_wallets')->where('user_id',$user_id)->update(['wallet_ammount' => $remaining_wallet_amount]); 
        }
        if($wallet_price==$product_price){
        DB::table('book_multi_items')->where('multi_id',$booking_multi_id)->update(['refund_status' => 'YES']); 
        }
 }
 elseif($staTUS->order_status==1)
 {
   DB::table('book_multi_items')->where('booking_id',$booking_id)->where('multi_id',$booking_multi_id)->update(['order_status'=>$staTUS->order_status+1]);
 }
 elseif($staTUS->order_status==5)
 {
     DB::table('book_multi_items')->where('booking_id',$booking_id)->where('multi_id',$booking_multi_id)->update(['order_status'=>$staTUS->order_status+1]);
 }
 else{
   DB::table('book_multi_items')->where('booking_id',$booking_id)->where('multi_id',$booking_multi_id)->update(['book_multi_items.order_status'=>$staTUS->order_status+1]);
 }
}

public function redeem(Request $request){
    $booking_multi_id=$request->id;
    $staTUS=DB::table('book_multi_items')->where('multi_id',$booking_multi_id)->first();
  
    $product_id=$staTUS->product_id;
    $productDetails=DB::table('products')->join('sub_cats','products.sub_cat_id','=','sub_cats.sub_cat_id')->where('products.product_id','=',$staTUS->product_id)->first();
    $selling_price=$productDetails->selling_price;
    $gst=$productDetails->gst;
    $commission=$productDetails->commission;
   

    $product_priceeeee=$selling_price+($selling_price*($gst/100));
    $refound_amount=$product_priceeeee-($product_priceeeee*($commission/100));
       $vendor_id=Auth::user()->id;
       $wallet=DB::table('vendor_wallets')->where('vendor_id',$vendor_id)->first();
       $wallet_amount1=$wallet->wallet_amount;
       $remaining_wallet_amount=$wallet_amount1+$refound_amount;
       $user_wallet1=new vendor_wallet_transaction;
       $user_wallet1->vendor_id=$vendor_id;
       $user_wallet1->transaction_amount=$refound_amount;
       $user_wallet1->after_transaction_amount=$remaining_wallet_amount;
       $user_wallet1->remarks='Booking Completed';
    //    $user_wallet1->order_id=$order_id;
       $user_wallet1->tranasaction_type='Credit';
    //    $user_wallet1->booking_id=$booking_id;
       $user_wallet1->transaction_id=strtoupper(uniqid());;
       $user_wallet1->save();
       DB::table('vendor_wallets')->where('vendor_id',$vendor_id)->update(['wallet_amount' => $remaining_wallet_amount]); 
       DB::table('book_multi_items')->where('multi_id',$booking_multi_id)->update(['redeem_status' => 'YES']); 
       Toastr::success('Redeem Successfully.', 'Redeem', ["positionClass" => "toast-top-right"]);
       return redirect()->back();
    }

public function wallet(Request $request)
{
    $user_id = Auth::user()->id;
    $request_count = DB::table('withdradal_requests')->where('delivery_id', $user_id)->where('request_status', 'YES')
        ->count();
        $bank_count = DB::table('bank_details')->where('delivery_id', $user_id)
        ->count();
    $wallet_details = DB::table('vendor_wallets')->where('vendor_id', $user_id)->first();
    $wallet_transaction = DB::table('vendor_wallet_transactions')->where('vendor_id', $user_id)->limit(10)
        ->orderBy('vendor_wallet_transaction_id', 'DESC')
        ->get();
    return view('vendor.wallet', ['wallet_details' => $wallet_details, 'request_count' => $request_count, 'wallet_transaction' => $wallet_transaction,'bank_count'=>$bank_count]);

}
public function vendor_withdrawal(Request $request)
{
        $input = $request->all();
        $validator = Validator::make($input, ['withdrawal_amount' => ['required'], ]);
        if ($validator->fails())
        {
            return response()
                ->json(['success' => false, 'errors' => $validator->getMessageBag()
                ->toArray() ], 400);
        }
        $user_id = Auth::user()->id;
        $aa = new withdradal_request;
        $aa->delivery_id = $user_id;
        $aa->request_status = 'YES';
        $aa->withdrawal_amount = $request->withdrawal_amount;
        $gs = $aa->save();
        try{     
            // Helper::sms_for_all(Auth::user()->phone,"Withdrawal: Withdrawal request of Rs ".$request->withdradal_amount." has been initiated. Amount will be Credited to your Bank account ending within 24 hours.");
            // $to_name=Auth::user()->name;
            // $to_email=Auth::user()->email;
            // $data = array('name'=>$to_name,'email'=>$to_email,'body'=>"This is to let you know that we have received a request of Rs ".$request->withdradal_amount." for withdrawal from your Digonic wallet.If you have made a request for withdrawal before 23:59:59 on a calendar day,then your request gets reviewed and the amount is credited to your account within 24 hours.");
           
            // Mail :: send('admin.mail',$data,function($message) use ($to_name,$to_email)
            // {
            // $message->to($to_email)
            // ->subject('Appointment Approval');

            // });
        }
        catch (\Exception $e)
        {
        // echo 0;
        }

        if ($gs)
        {
            return response()->json(['success' => true, 'msg' => 'Withdrawal Request send Successfully'], 200);
        }
}


}
