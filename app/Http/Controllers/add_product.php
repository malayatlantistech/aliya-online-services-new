<?php

namespace App\Http\Controllers;
use App\product_size;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Image;
use Auth;
use App\product;
use App\product_image;
use App\product_features;
use Illuminate\Support\Facades\Mail;
use App\cat;
use App\product_spacification;
use App\product_price;
use App\stock_trasanction;

use Toastr;

class add_product extends Controller
{


    public function formValidation()
    {
      return view('form-validation');
    }

    public function product_action(Request $req)
    {


      $this->validate($req,[
        'hsn' => 'required',
        'product_cat_id' => 'required',
        'product_sub_cat_id' => 'required',
        'product_name' => 'required',
        'brand' => 'required',
        'editor1' => 'required',
        'image_name' => 'required',
        'mrp' => 'required',
        'selling_price' => 'required',
        'weight' => 'required',
        'qty' => 'required',
        'size'=>'required',
        'color'=>'required',
        'origin'=>'required',
      ],[
        'product_cat_id.required' => ' The Category field is Required.',
        'product_sub_cat_id.required' => ' The Sub Category field is Required.',
        'product_name.required' => ' The Product Name field is Required.',
        'brand.required' => ' The brand field is Required.',
        'editor1.required' => ' The Description field is Required.',
        'image_name.mimes' => ' The Image  Must be must be jpeg,png,jpg,gif,svg.',
        'image_name.max' => ' You Can Upload Maximum 2KB Image.',
        'mrp.required' => ' The Product MRP field is Required.',
        'selling_price.required' => ' The Selling Price field is Required.',
        'weight.required' => 'The Product Weight field is Required.',
        'qty.required' => 'The opening stock field is Required.',
        'size.required' => 'The size field is Required.',
        'color.required' => 'The color field is Required.',
        'origin.required' => 'The origin field is Required.',
        
        
      ]);


        
       $vendor_id=Auth::user()->id;

     $product = new product;
     

     $product->product_code=$req->input('product_code');
     $product->hsn_code=$req->input('hsn');
     $product->product_name=$req->input('product_name');
     $product->cat_id=$req->input('product_cat_id');
     $product->sub_cat_id=$req->input('product_sub_cat_id');
     $product->sub_sub_cat_id=$req->input('product_sub_sub_cat_id');
     $product->brand_id=$req->input('brand');
     $product->description=$req->input('editor1');
     $token=$req->input('_token');
     $product->remember_token=$token;


     $product->group_id=$req->input('group');
     $product->weight=$req->input('weight');
     $product->mrp=$req->input('mrp');
     $product->selling_price=$req->input('selling_price');

     $product->available_stock=$req->input('qty');
     $product->total_stock=$req->input('qty');
     $product->size=$req->input('size');
     $product->color=$req->input('color');
     $product->origin=$req->input('origin');
     $product->vendor_id=$vendor_id;


     $product->save();
     $product_id=$product->id;


     $stock= new stock_trasanction;
     $stock->product_id=$product_id;
     $stock->quantity=$req->input('qty');
     $stock->type='CREDIT';
     $stock->remarks='INITIAL ADD';
     $stock->transfer_id=0;
     $stock->total_quantity=$req->input('qty');
   
     $stock->save();

     /*
     DB::table('products')->where('product_code',$code)->get()->first();
     $product_id->product_id;

*/

  $specification_name=$req->input('specification1');
  $specification_type=$req->input('specification');

 
     
if(isset($specification_name)){

for($i=0;$i<count($specification_name);$i++)
{

    $specification= new product_spacification;
    $specification_name1=$specification_name[$i];
    $specification_type1=$specification_type[$i];
    $specification->title=$specification_name1;
    $specification->description=$specification_type1;

    $specification->product_id=$product_id;
    $specification->remember_token=$token;
    $specification->save();
}
}
        
        $feture=$req->input('fetuars');
     
        if(isset($feture)){

        for($i=0;$i<count($feture);$i++)
        {

            $features= new product_features;
            $feature1=$feture[$i];
            $features->features=$feature1;

            $features->product_id=$product_id;
            $features->remember_token=$token;
            $features->save();
        }
    }



     

      //  DB::table('features')->insert(['product_id' =>$product_id,'feature_name'=> $fetuars,'remember_token'=>$token]);



     
    /*  $p_type=$req->input('size');
      if($p_type[0]!=''){
     for($i=0;$i<count($p_type);$i++)
     {
         $for_size= new product_size;
         $for_size->product_id=$product_id;
         $for_size->product_size=$p_type[$i];
         $for_size->rememeber_token=$token;
         $for_size->save();
     }

    
    }*/











     
  if($req->hasFile('image_name'))
	{	
     $image_array3=$req->file('image_name');
     
    
  for($i=0;$i<count($image_array3);$i++)
  {




    
    
      $image_name = uniqid() . '.' . $image_array3[$i]->getClientOriginalExtension();


        $destinationPath = 'product_image';
        $destinationPath1 = 'small_product_image';
        $resize_image = Image::make($image_array3[$i]->getRealPath());
     $resize_image->resize(1000,1000, function($constraint){
     })->save($destinationPath . '/' . $image_name);


    $resize_image->resize(255,255, function($constraint){
    })->save($destinationPath1 . '/' . $image_name);






   


   
     DB::table('product_images')->insert(['product_id' =>$product_id,'image'=>$image_name,'remember_token'=>$token]);
  }
    }


  
        /* for($i=0;$i<$size;$i++)
         {
             $new_size=new product_size;
             $new_size->product_id=$product_id;
             $new_size->product_size=$size[$i];
             $new_size->save();
         }*/
        /* $product=DB::table('products')->join('cats','products.product_cat_id','=','cats.cat_id')->join('sub_cats','products.product_sub_cat_id','=','sub_cats.sub_cat_id')->join('sub_sub_cats','products.product_sub_sub_cat_id','=','sub_sub_cats.sub_sub_cat_id')->where('products.vendor_id',$vendor_id)->get();
         return view('/vendor/view_product')->with('product',$product);*/
         Toastr::success('Product Add successfully.', 'Product Add', ["positionClass" => "toast-top-right"]);
        
         return redirect('/view_product');
         
   
    }

  

}
