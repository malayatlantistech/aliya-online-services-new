<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use App\delivery_wallet;
use App\vendor_wallet;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */


    protected $redirectTo;

    public function redirectTo()
    {
 

    if($_POST["role"]==Auth::user()->role){

        switch(Auth::user()->role){
            case 'ADMIN':
              
                $this->redirectTo = '/';
                return $this->redirectTo;
                break;

            case 'USER':

                $this->redirectTo = '/';
                return $this->redirectTo;
                break;

            case 'DELIVERY':

                $re=new delivery_wallet;
                $re->wallet_ammount=0;
                $re->delivery_id=Auth::user()->id;
                $re->save();

                $this->redirectTo = '/';
                return $this->redirectTo;
                break;

            case 'VENDOR':

                $re=new vendor_wallet;
                $re->wallet_ammount=0;
                $re->vendor_id=Auth::user()->id;
                $re->save();

                $this->redirectTo = '/';
                return $this->redirectTo;
                break;

                    
                
                
         
            default:
                $this->redirectTo = '/';
                return $this->redirectTo;
        }
        
    }else{
      
        Auth::logout();
        $this->redirectTo = '/';
        return $this->redirectTo;
       
      
   }



        // return $next($request);
    } 




    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'mobile' => ['required', 'integer', 'min:10', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {


        $request = request();



        $ref_code=$request->input('ref_code');
        if($ref_code!='')
        {
        $abc=explode('SSJ',$ref_code);
        $use_cust_id=$abc[1];
        $ref_code;
        }
        else{
            $use_cust_id=0;
            $ref_code=0;
        }

        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'mobile' => $data['mobile'],
            'use_cust_id'=>$use_cust_id,
            'role'=> $data['role'],
            'ref_code'=> $ref_code,
            'password' => Hash::make($data['password']),
        ]);
    }
}
