<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use App\user_wallet; 
use App\cart; 
use DB;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Validator;
use  Artisan;
class UserController extends Controller 
{
public $successStatus = 200;

/**
     * Login API
     * @return \Illuminate\Http\Response
     * 
     * @OA\Post(
     *      path="/api/phone_no_check",
     *      operationId="phone no check",
     *      tags={"Auth"},
     *      summary=" Phone no check",
     *      @OA\RequestBody(
     *           @OA\MediaType(
     *                mediaType="application/json",
     *                      @OA\Schema(
     *                        @OA\Property(
     *                          property="mobile",
     *                            type="integer"
     *                           ),
     *                          )
     *                          ),
     *                        ),
     *      @OA\Response(response=200, description="successful operation"),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     * )
     * 
     */
    public function smsRequest($phone,$msg)
    {
        Artisan::call('config:cache');
        $client = new \GuzzleHttp\Client();
        $url = "http://weberleads.in/http-tokenkeyapi.php?authentic-key=".env('WEBERLEADS_SMS_API')."&senderid=".env('WEBERLEADS_SMS_SENDERID')."&route=2&number=91".$phone."&message=".$msg;
        $request = $client->get($url);
        $response = $request->getBody();
    }
        public function phone_no_check(Request $request){ 
              $mobile=$request->mobile;
              $user_count=DB::table('users')->where('mobile',$mobile)->where('role','USER')->count();
                  $user=DB::table('users')->where('mobile',$mobile)->where('role','USER')->first();
                 
                  if($user_count>0)
                  {
                      if($user->block=='NO')
                      {
                        $status=1;
                        return response()->json(['msg'=>"valid mobile no",'status'=>$status], $this-> successStatus);   
                      }
                      else if($user->block=='YES'){
                        $status=-1;
                        return response()->json(['msg'=>"You are Block By admin",'status'=>$status], $this-> successStatus);   
                      }
                    }
                  else{
                    $status=0;
                    return response()->json(['msg'=>"Invalid mobile no",'status'=>$status], $this-> successStatus);   
                  }
        }


 /**
     * Login API
     * @return \Illuminate\Http\Response
     * 
     * @OA\Post(
     *      path="/api/login",
     *      operationId="authenticate",
     *      tags={"Auth"},
     *      summary=" Login",
     *      @OA\RequestBody(
     *           @OA\MediaType(
     *                mediaType="application/json",
     *                      @OA\Schema(
     *                        @OA\Property(
     *                          property="mobile",
     *                            type="integer"
     *                           ),
     *        
     *                                 )
     *                           ),
     *                        ),
     *      @OA\Response(response=200, description="successful operation"),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     * )
     * 
     */

    public function login(Request $request){ 

        Log::info($request);
        $user  = User::where([['mobile','=',request('mobile')]])->first();
        if($user){
            Auth::login($user, true);
             $success['token'] =  $user->createToken('MyApp')-> accessToken; 
            return response()->json(['success' => $success,'user_details'=>$user], $this-> successStatus); 
        } 
        else{ 
            return response()->json(['error'=>'Unauthorised'], 401); 
        } 
    }

     /**
     * Login API
     * @return \Illuminate\Http\Response
     * 
     * @OA\Post(
     *      path="/api/userregisters",
     *      operationId="register",
     *      tags={"Auth"},
     *      summary="register",
     *      @OA\RequestBody(
     *           @OA\MediaType(
     *                mediaType="application/json",
     *                      @OA\Schema(
     *                        @OA\Property(
     *                          property="name",
     *                            type="string"
     *                           ),
     *                         @OA\Property(
     *                          property="email",
     *                          type="string"
     *                            ),
     *                          @OA\Property(
     *                          property="mobile",
     *                          type="string"
     *                            ),
     *                          @OA\Property(
     *                          property="password",
     *                          type="string"
     *                            ),
     *                          @OA\Property(
     *                          property="c_password",
     *                          type="string"
     *                            ),
     *                                 )
     *                           ),
     *                        ),
     *      @OA\Response(response=200, description="successful operation"),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     * )
     * 
     */

    public function register(Request $request) 
    { 
        
          $email_find=User::where('email', $request->email)->count();
        if($email_find>0){
             return response()->json(['error'=>'Email alredy exits'], 401);       
        }
        
          $mobile_find=User::where('mobile', $request->mobile)->count();
        if($mobile_find>0){
             return response()->json(['error'=>'Phone number alredy exits'], 401);       
        }
        
        
        
$input = $request->all(); 
        $input['password'] = bcrypt($input['password']); 
        $user = User::create($input); 
        $success['token'] =  $user->createToken('MyApp')-> accessToken; 
       
        $user_id=$user->id;

        $user_wallet=new user_wallet;
        $user_wallet->user_id=$user_id;
        $user_wallet->wallet_ammount=0;
        $user_wallet->save();
        self::smsRequest($input['mobile'],"Hi ".$input['name'].", Welcome to OHMAGO Delivery Services.");
return response()->json(['success'=>$success,'user_details'=>$user], $this-> successStatus); 
    }
 /**
     * Login API
     * @return \Illuminate\Http\Response
     * 
     * @OA\Post(
     *      path="/api/details",
     *      operationId="User Details",
     *      tags={"User Profile"},
     *      summary="User Details",
     *      @OA\RequestBody(
     *           @OA\MediaType(
     *                mediaType="application/json",
     *                      @OA\Schema(
     *                            )
     *                           ),
     *                        ),
     *      @OA\Response(response=200, description="successful operation"),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *       security={
	 *          {"bearerAuth": {}}
	 *      },
     * )
     * 
     */

    public function details() 
    { 
        $user_details=[];
        // $user_details = Auth::user(); 
        $user_details['name']=Auth::user()->name;
        $user_details['email']=Auth::user()->email;
        $user_details['phone']=Auth::user()->mobile;
        $user_details['profile_image']="/profile_image/".Auth::user()->profile_image;
        
        return response()->json(['success' => $user_details], $this-> successStatus); 
    } 

     /**
     * user Wallet API
     * @return \Illuminate\Http\Response
     * 
     * @OA\Post(
     *      path="/api/user_wallet",
     *      operationId="User wallet",
     *      tags={"User Profile"},
     *      summary="User wallet",
     *      @OA\RequestBody(
     *           @OA\MediaType(
     *                mediaType="application/json",
     *                      @OA\Schema(
     *                            )
     *                           ),
     *                        ),
     *      @OA\Response(response=200, description="successful operation"),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *       security={
	 *          {"bearerAuth": {}}
	 *      },
     * )
     * 
     */


    public function user_wallet() 
    { 
        $trans=[];
        $user_id = Auth::user()->id; 
        $wallet_details=DB::table('user_wallets')->where('user_id',$user_id)->first();
        $wallet_amount=$wallet_details->wallet_ammount;
        $wallet_transaction=DB::table('user_wallet_transactions')->where('user_id',$user_id)->limit(10)->orderBy('wallet_transaction_id','DESC')->get();
        foreach($wallet_transaction as $key=>$abc){
            $trans[$key]['transction_amount']=strval($abc->transaction_amount);
            $trans[$key]['remarks']=$abc->remarks;
            $trans[$key]['type']=$abc->tranasaction_type;
            $trans[$key]['closing_blance']=strval($abc->after_transaction_amount);
            $trans[$key]['date']=date('d M Y, H:i a',strtotime($abc->created_at));
            
        }
        
        
        
        
        
        return response()->json(['wallet_amount'=>$wallet_amount,'wallet_transaction'=>$trans], $this-> successStatus); 
    } 


     /**
     * user cart API
     * @return \Illuminate\Http\Response
     * 
     * @OA\Post(
     *      path="/api/cart",
     *      operationId="User cart",
     *      tags={"Cart & Wishlist"},
     *      summary="User cart",
     *  
     *      @OA\Response(response=200, description="successful operation"),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *       security={
	 *          {"bearerAuth": {}}
	 *      },
     * )
     * 
     */

    public function cart() 
    { 
        $product=[];
        $user_id = Auth::user()->id; 
        $cart_count=DB::table('carts')
        ->join('products','carts.product_id','=','products.product_id')
        ->join('sub_cats','products.sub_cat_id','=','sub_cats.sub_cat_id')
        ->join('brands','products.brand_id','=','brands.brand_id')
        ->where('carts.user_id',$user_id)
        
        ->count();
        $cart_item=DB::table('carts')
        ->join('products','carts.product_id','=','products.product_id')
        ->join('sub_cats','products.sub_cat_id','=','sub_cats.sub_cat_id')
        ->join('brands','products.brand_id','=','brands.brand_id')
        ->where('carts.user_id',$user_id)
        ->get();
      
        $cart_total=0;
        foreach($cart_item as $key=>$pro){
            $product[$key]['product_id']=$pro->product_id;
            $product[$key]['product_name']=$pro->product_name;
            $product[$key]['product_brand']=$pro->brand_name;
         
            if($pro->weight>999)
                $weight=round($pro->weight/1000,2).' Kg';
            else
                $weight=$pro->weight.' Gm';
            $product[$key]['product_weight']=$weight;
            $product[$key]['total_review']=$pro->total_review;
            $product[$key]['avg_review']=$pro->review;
            $product[$key]['mrp']=strval($pro->mrp);
            $product[$key]['selling_price']=strval(round(($pro->selling_price*$pro->gst/100)+$pro->selling_price,2));

            
            $banner=DB::table('product_images')->where('product_id',$pro->product_id)->limit(1)->get();
            foreach($banner as $banner){
                    $product[$key]['product_image']="/product_image/".$banner->image;
            }
            $product[$key]['discount']=strval(round((($pro->mrp-($pro->selling_price+($pro->selling_price*$pro->gst/100)))/$pro->mrp)*100));
           
            $selling_price=$pro->selling_price;
            $gst=$pro->gst;
            $product_price=$selling_price+$selling_price*($gst/100);
            $total_product_price=$product_price*($pro->quantity);
            $cart_total=$cart_total+$total_product_price;
            
            $product[$key]['product_quantity']=$pro->quantity;
            $product[$key]['product_total']= strval(round($total_product_price,2));
        }

        return response()->json(['cart_item'=>$product,'total_cart_item'=>$cart_count,'cart_total'=> strval(round($cart_total,2))], $this-> successStatus); 
    } 

   

    /**
     * user Wish list API
     * @return \Illuminate\Http\Response
     * 
     * @OA\Post(
     *      path="/api/wish_list",
     *      operationId="User wish_list",
     *      tags={"Cart & Wishlist"},
     *      summary="User wish_list",
     *      @OA\Response(response=200, description="successful operation"),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *       security={
	 *          {"bearerAuth": {}}
	 *      },
     * )
     * 
     */

    public function wish_list() 
    { 
        $product=[];
        $user_id = Auth::user()->id; 
        $wish_count=DB::table('wishlists')
        ->join('products','wishlists.product_id','=','products.product_id')
        ->join('sub_cats','products.sub_cat_id','=','sub_cats.sub_cat_id')
        ->join('brands','products.brand_id','=','brands.brand_id')
        ->where('wishlists.user_id',$user_id)
        
        ->count();
        $wish_item=DB::table('wishlists')
        ->join('products','wishlists.product_id','=','products.product_id')
        ->join('sub_cats','products.sub_cat_id','=','sub_cats.sub_cat_id')
        ->join('brands','products.brand_id','=','brands.brand_id')
        ->where('wishlists.user_id',$user_id)
        ->get();
      
        $wish_total=0;
        foreach($wish_item as $key=>$pro){
            $product[$key]['product_id']=$pro->product_id;
            $product[$key]['product_name']=$pro->product_name;
            $product[$key]['product_brand']=$pro->brand_name;
         
            if($pro->weight>999)
                $weight=round($pro->weight/1000,2).' Kg';
            else
                $weight=$pro->weight.' Gm';
            $product[$key]['product_weight']=$weight;
            $product[$key]['total_review']=$pro->total_review;
            $product[$key]['available_stock']=$pro->available_stock;
            $product[$key]['avg_review']=$pro->review;
            $product[$key]['mrp']=strval($pro->mrp);
            $product[$key]['selling_price']=strval(round(($pro->selling_price*$pro->gst/100)+$pro->selling_price,2));

            
            $banner=DB::table('product_images')->where('product_id',$pro->product_id)->limit(1)->get();
            foreach($banner as $banner){
                    $product[$key]['product_image']="/product_image/".$banner->image;
            }
            $product[$key]['discount']=strval(round((($pro->mrp-($pro->selling_price+($pro->selling_price*$pro->gst/100)))/$pro->mrp)*100));
           
            $selling_price=$pro->selling_price;
            $gst=$pro->gst;
            $product_price=$selling_price+$selling_price*($gst/100);
            $total_product_price=$product_price*($pro->quantity);
            $wish_total=$wish_total+$total_product_price;
            
            $product[$key]['product_quantity']=$pro->quantity;
            $product[$key]['product_total']= strval(round($total_product_price,2));
        }

        return response()->json(['wish_item'=>$product,'total_wish_item'=>$wish_count,'wish_total'=> strval(round($wish_total,2))], $this-> successStatus); 
    } 

     /**
     * Change Profile API
     * @return \Illuminate\Http\Response
     * 
     * @OA\Post(
     *      path="/api/update_profile",
     *      operationId="Update Profile",
     *      tags={"User Profile"},
     *      summary="Update Profile",
     * @OA\RequestBody(
     *           @OA\MediaType(
     *                mediaType="application/json",
     *                      @OA\Schema(
     *                        @OA\Property(
     *                          property="name",
     *                            type="string"
     *                           ),
     *                     
     *                          @OA\Property(
     *                          property="phone",
     *                          type="integer"
     *                            ),
     *                          @OA\Property(
     *                          property="profile_image",
     *                          type="string"
     *                            ),
     *                       
     *                                 )
     *                           ),
     *                        ),
     * 
     * 
     *      @OA\Response(response=200, description="successful operation"),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *       security={
	 *          {"bearerAuth": {}}
	 *      },
     * )
     * 
     */
    public function update_profile(Request $details)
    {
        
        $id=Auth::user()->id;
        $name=$details->name;
        $phone_no=$details->phone;
        $details->profile_image;

        if($details->profile_image!="")
		{
                $image = base64_decode($details->profile_image);
                $image_name= rand(000000,999999).'.png';
                $path = public_path() . "/profile_image/" . $image_name;

                file_put_contents($path, $image);


                    DB::table('users')->where('id',$id)->update(['name' =>$name,'mobile' =>$phone_no,'profile_image'=>$image_name]);
                
                }else{

                    DB::table('users')->where('id',$id)->update(['name' =>$name,'mobile' =>$phone_no]);
                }

                $status[0]='Profile update Successful';
                $status[1]="1"; 

                $tr=DB::table('users')->where('id',Auth::user()->id)->first();
        
                $status[2]=$tr->profile_image; 

                return response()->json(['status' => $status], $this-> successStatus);

    }



    /**
     * Coupon Calculation API
     * @return \Illuminate\Http\Response
     * 
     * @OA\Post(
     *      path="/api/coupon_calculation",
     *      operationId="coupon calculation",
     *      tags={"coupon calculation"},
     *      summary="coupon calculation",
     * @OA\RequestBody(
     *           @OA\MediaType(
     *                mediaType="application/json",
     *                      @OA\Schema(
     *                        @OA\Property(
     *                          property="shipping_charge",
     *                            type="integer"
     *                           ),
     *                     
     *                          @OA\Property(
     *                          property="coupon_code",
     *                          type="string"
     *                            ),
     *                          @OA\Property(
     *                          property="totalgst",
     *                          type="integer"
     *                            ),
     *                          @OA\Property(
     *                          property="price1",
     *                          type="integer"
     *                            ),
     *                          @OA\Property(
     *                          property="product_id",
     *                          type="integer"
     *                            ),
     *                       
     *                                 )
     *                           ),
     *                        ),
     * 
     * 
     *      @OA\Response(response=200, description="successful operation"),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *       security={
	 *          {"bearerAuth": {}}
	 *      },
     * )
     * 
     */





    public function  coupon_calculation(Request $crte)
    {   
        $user_id=Auth::user()->id;
        $token="dfgiujojghkijhgk";
        $s_charge=$crte->shipping_charge;
        $coupon_code=$crte->coupon_code;
        $product_id=$crte->product_id;
        $totalgst=$crte->totalgst;
        $price1=$crte->price1;

        $price_details=[];

        if($coupon_code!=''){
            $copo=DB::table('coupon_banners')->where('coupon_code',$coupon_code)->count();
            if($copo!=0){
                $co=DB::table('bookings')->where('coupon_code',$coupon_code)->where('customer_id',$user_id)->count();
                if($co==0){
                    $adc=DB::table('coupon_banners')->where('coupon_code',$coupon_code)->where('active_status','YES')->count();
                    if($adc!=0){
                                 $coupon=DB::table('coupon_banners')->where('coupon_code',$coupon_code)->first();
                                 $cat_id=$coupon->cat_id;
                                 if($price1>$coupon->min_price){
                                        $coupon_value=$coupon->coupon_value;
                                        $coupon_status=$coupon->active_status;
                                        
                                    if( $cat_id==0) 
                                    {
                                    if($coupon->coupon_type=='PERCENTAGE')
                                    {
                                        $coupon_price= round($price1*($coupon_value/100));
                                        $total_amount=$s_charge+$price1-$coupon_price;
                                        $wallet=DB::table('user_wallets')->where('user_id',$user_id)->first();
                                        $wallet_ammount=$wallet->wallet_ammount;
                                        if($wallet_ammount>$total_amount)
                                        {
                                          
                                            $price_details['total_amount']=strval($total_amount);
                                            $price_details['coupon_price']=strval($coupon_price);
                                            $price_details['shipping_charge']=strval($s_charge);
                                            $msg='Coupon Applied';


                                        }
                                        else if($wallet_ammount<=$total_amount)
                                        {
                                            //echo $wallet_ammount."|".$coupon_price."|".$s_charge;
                                            $price_details['total_amount']=strval($total_amount);
                                            $price_details['coupon_price']=strval($coupon_price);
                                            $price_details['shipping_charge']=strval($s_charge);
                                            $msg='Coupon Applied';
                                        }
                                    }
                                    else if($coupon->coupon_type=='FLAT')
                                    {
                                        $coupon_price=$coupon_value;
                                        $total_amount=$s_charge+$price1-$coupon_price;
                                        $wallet=DB::table('user_wallets')->where('user_id',$user_id)->first();
                                        $wallet_ammount=$wallet->wallet_ammount;
                                        if($wallet_ammount>$total_amount)
                                        {
                                            $price_details['total_amount']=strval($total_amount);
                                            $price_details['coupon_price']=strval($coupon_price);
                                            $price_details['shipping_charge']=strval($s_charge);
                                            $msg='Coupon Applied';




                                          //  echo $total_amount."|".$coupon_price."|".$s_charge;
                                        }
                                        else if($wallet_ammount<=$total_amount)
                                        {
                                            //echo $wallet_ammount."|".$coupon_price."|".$s_charge;
                                        
                                            $price_details['total_amount']=strval($total_amount);
                                            $price_details['coupon_price']=strval($coupon_price);
                                            $price_details['shipping_charge']=strval($s_charge);
                                            $msg='Coupon Applied';

                                        }





                                    }
    
                                }
                                else
                                {
                                    if($product_id!=0)
                                    {
                                        $user_id=Auth::user()->id;
                                        $cou=DB::table('none_carts')->where('user_id',$user_id)->get(); 
                                        $i=1;
                                        foreach($cou as $cou1)
                                        {
                                            $product_id=$cou1->product_id;
                                            $cou1=DB::table('products')->where('product_id',$product_id)->first(); 
                                            $cattt_id=$cou1->cat_id;
                                            if($cattt_id==$cat_id)
                                            {
                                                $i=$i+1;
                                            }
                                        }
                                            if($i==2)
                                            {
                                                    if($coupon->coupon_type=='PERCENTAGE')
                                                    {
                                                        $coupon_price= round($price1*($coupon_value/100));
                                                        $total_amount=$s_charge+$price1-$coupon_price;
                                                        $wallet=DB::table('user_wallets')->where('user_id',$user_id)->first();
                                                        $wallet_ammount=$wallet->wallet_ammount;
                                                        if($wallet_ammount>$total_amount)
                                                        {
                                                           //echo $total_amount."|".$coupon_price."|".$s_charge;

                                                            $price_details['total_amount']=strval($total_amount);
                                                            $price_details['coupon_price']=strval($coupon_price);
                                                            $price_details['shipping_charge']=strval($s_charge);
                                                            $msg='Coupon Applied';
                
                                                        }
                                                        else if($wallet_ammount<=$total_amount)
                                                        {
                                                           // echo $wallet_ammount."|".$coupon_price."|".$s_charge;
                                                            $price_details['total_amount']=strval($total_amount);
                                                            $price_details['coupon_price']=strval($coupon_price);
                                                            $price_details['shipping_charge']=strval($s_charge);
                                                            $msg='Coupon Applied';
                
                                                        }
                                                    }
                                                    else if($coupon->coupon_type=='FLAT')
                                                    {
                                                        $coupon_price=$coupon_value;
                                                        $total_amount=$s_charge+$price1-$coupon_price;
                                                        $wallet=DB::table('user_wallets')->where('user_id',$user_id)->first();
                                                        $wallet_ammount=$wallet->wallet_ammount;
                                                        if($wallet_ammount>$total_amount)
                                                        {
                                                           // echo $total_amount."|".$coupon_price."|".$s_charge;
                                                     
                                                            $price_details['total_amount']=strval($total_amount);
                                                            $price_details['coupon_price']=strval($coupon_price);
                                                            $price_details['shipping_charge']=strval($s_charge);
                                                            $msg='Coupon Applied';
                
                                                        }
                                                        else if($wallet_ammount<=$total_amount)
                                                        {
                                                           // echo $wallet_ammount."|".$coupon_price."|".$s_charge;
                                                        
                                                            $price_details['total_amount']=strval($total_amount);
                                                            $price_details['coupon_price']=strval($coupon_price);
                                                            $price_details['shipping_charge']=strval($s_charge);
                                                            $msg='Coupon Applied';
                
                                                        }
                                                    }
                                             }
                                            else
                                            {
                                            //    echo 8; 
                                            //    Coupon not applicable for this category
                                               $msg="Coupon not applicable for this category";//8

                                            }
                                            }
                                            else
                                            {
    
                                             //   $cat_id1=$coupon11->cat_id;
                                                $user_id=Auth::user()->id;
                                              
                                                $cou=DB::table('carts')->where('user_id',$user_id)->get(); 
                                                $i=1;
                                                foreach($cou as $cou1)
                                                {
                                                     $product_id=$cou1->product_id;
                                                   
                                               
                                                    $cou1=DB::table('products')->where('product_id',$product_id)->first(); 
                                                
                                                     $cattt_id=$cou1->cat_id;
                                             
    
    
                                                  if($cattt_id!=$cat_id)
                                                  {
                                                    $i=$i+1;
                                                  }
    
                                                }

                                                    if($i==1)
                                                    {
                                                            if($coupon->coupon_type=='PERCENTAGE')
                                                            {
                                                                $coupon_price= round($price1*($coupon_value/100));
                                                                $total_amount=$s_charge+$price1-$coupon_price;
                                                                $wallet=DB::table('user_wallets')->where('user_id',$user_id)->first();
                                                                $wallet_ammount=$wallet->wallet_ammount;
                                                                if($wallet_ammount>$total_amount)
                                                                {
                                                                  //  echo $total_amount."|".$coupon_price."|".$s_charge;
                                                               
                                                                    $price_details['total_amount']=strval($total_amount);
                                                                    $price_details['coupon_price']=strval($coupon_price);
                                                                    $price_details['shipping_charge']=strval($s_charge);
                                                                    $msg='Coupon Applied';
                        
                                                                }
                                                                else if($wallet_ammount<=$total_amount)
                                                                {
                                                                  //  echo $wallet_ammount."|".$coupon_price."|".$s_charge;
                                                               
                                                                    $price_details['total_amount']=strval($total_amount);
                                                                    $price_details['coupon_price']=strval($coupon_price);
                                                                    $price_details['shipping_charge']=strval($s_charge);
                                                                    $msg='Coupon Applied';
                        
                                                                }
                                                            }
                                                            else if($coupon->coupon_type=='FLAT')
                                                            {
                                                                $coupon_price=$coupon_value;
                                                                $total_amount=$s_charge+$price1-$coupon_price;
                                                                $wallet=DB::table('user_wallets')->where('user_id',$user_id)->first();
                                                                $wallet_ammount=$wallet->wallet_ammount;
                                                                if($wallet_ammount>$total_amount)
                                                                {
                                                                   // echo $total_amount."|".$coupon_price."|".$s_charge;
                                                                    $price_details['total_amount']=strval($total_amount);
                                                                    $price_details['coupon_price']=strval($coupon_price);
                                                                    $price_details['shipping_charge']=strval($s_charge);
                                                                    $msg='Coupon Applied';
                        
                                                               
                                                                }
                                                                else if($wallet_ammount<=$total_amount)
                                                                {

                                                                    // echo $wallet_ammount."|".$coupon_price."|".$s_charge;
                                                                    // $msg='Coupon Applied';
                                                                    $price_details['total_amount']=strval($total_amount);
                                                                    $price_details['coupon_price']=strval($coupon_price);
                                                                    $price_details['shipping_charge']=strval($s_charge);
                                                                    $msg='Coupon Applied';
                        

                                                                }
                                                            }
                                                     }
                                                    else
                                                    {
                                                        $catjs=DB::table('cats')->where('cat_id',$cat_id)->first();
                         
                                                    //    echo "9"."|".$catjs->cat_name; 
                                                    //    Coupon not applicable only " + response2[1] + " category
                                                       $msg="Coupon not applicable only {$catjs->cat_name} category";//9
                                                       
                                                    }
    
                                         }
    
                                }
    
                                }else{
                                    // $a=4;
                                    // echo $a."|".$coupon->min_price;
                                   
                                    $msg="Must be order value grater than {$coupon->min_price}";//4

                                }
    
                        }else{
                           $msg="Coupon Expire!";//2
                        }
    
                }else{
                
                    $msg="You already used this coupon!";//5
                    
                }
    
            }else{
                $msg="Invalid Coupon Code!";//3
            }
    
        }else{
            $msg="Invalid Coupon Code!";//3
        }

        return response()->json(['price_details' => $price_details,'msg'=>$msg], $this-> successStatus);

    
    
    }
    
      







        /**
     * @OA\Get(
     *     path="/",
     *     description="Home page",
     *     @OA\Response(response="default", description="Welcome page")
     * )
     */
    public function index(){

    }
    
    /**
     * Coupon Calculation API
     * @return \Illuminate\Http\Response
     * 
     * @OA\get(
     *      path="/api/search",
     *      operationId="Product Search",
     *      tags={"Product Search"},
     *      summary="Product Search",
     * @OA\RequestBody(
     *           @OA\MediaType(
     *                mediaType="application/json",
     *                      @OA\Schema(
     *                       
     *                     
     *                          @OA\Property(
     *                          property="name",
     *                          type="string"
     *                            ),
     *                        
     *                       
     *                                 )
     *                           ),
     *                        ),
     * 
     * 
     *      @OA\Response(response=200, description="successful operation"),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *       security={
	 *          {"bearerAuth": {}}
	 *      },
     * )
     * 
     */

    protected function search(Request $request)
 {
     $name=$request->name;
     
     $products=DB::table('products')->select('product_id','product_name','brand_id')->where('product_name','LIKE','%'.$name.'%')->where('active_status','=','YES')->get();
      $product_count=DB::table('products')->select('product_id','product_name')->where('product_name','LIKE','%'.$name.'%')->where('active_status','=','YES')->count();

      foreach($products as $key=>$productssss){
          $product[$key]['product_id']=$productssss->product_id;
          $product[$key]['product_name']=$productssss->product_name;
        
          
                   $banner=DB::table('product_images')->where('product_id',$productssss->product_id)->limit(1)->get();
                    foreach($banner as $banner){
                          $product[$key]['product_image']=$banner->image;
                    }
                    $banner1=DB::table('brands')->where('brand_id',$productssss->brand_id)->limit(1)->get();
                    foreach($banner1 as $banner1){
                          $product[$key]['product_brand']=$banner1->brand_name;
                    }
                      
       }
     
     
     
     
   
     if($product_count==0){

          $product=[];
     }

    
     return response()->json(['product_result' => $product], $this-> successStatus);
 }




}
