<?php

namespace App\Http\Controllers\API;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use App\cart;
use App\wishlist;
use App\billing_add;
use App\addre;
use App\none_cart;
use App\book_multi_item;
use App\booking;
use App\user_wallet_transaction;
use App\stock_trasanction;
use Carbon;
use App\cancel_reasons;
use Razorpay\Api\Api;
use App\review;
use  Artisan;

class ApiController extends Controller
{

    public function smsRequest($phone,$msg)
    {
        Artisan::call('config:cache');
        $client = new \GuzzleHttp\Client();
        $url = "http://weberleads.in/http-tokenkeyapi.php?authentic-key=".env('WEBERLEADS_SMS_API')."&senderid=".env('WEBERLEADS_SENDERID')."&route=2&number=91".$phone."&message=".$msg;
        $request = $client->get($url);
        $response = $request->getBody();
    }
      /**
     * @OA\Get(
     *      path="/api/cat",
     *      operationId="category",
     *      tags={"Category"},
     *      summary="View all Category",
     *      @OA\Response(response=200, description="successful operation"),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function cat() 
    { 
        $cat1=[];
        $cat=DB::table('cats')->get();
        foreach($cat as $key=> $cat11)
        {
           
            $cat1[$key]['cat_id'] = $cat11->cat_id;
            $cat1[$key]['cat_name'] = $cat11->cat_name;
            $cat1[$key]['cat_icon'] = "/cat_icon/{$cat11->cat_icon}";
        }
      
        return response()->json(['cat' => $cat1], 200); 
     
    }
  /**
     * Document API
     * @return \Illuminate\Http\Response
     * 
     * @OA\Get(
     *      path="/api/sub_cat?cat_id={cat_id}",
     *      operationId="subCategory",
     *      tags={"Category"},
     *      summary="View all Sub Category",
     *      @OA\Parameter(
     *          name="cat_id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(response=200, description="successful operation"),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     * )
     * 
     */
    public function sub_cat(Request $req) 
    { 
        $cat1=[];
        $cat_id=$req->cat_id;
        $sub_cat=DB::table('sub_cats')->where('cat_id',$cat_id)->get();

        foreach($sub_cat as $key=> $cat11)
        {
            $cat1[$key]['cat_id'] = $cat11->cat_id ;
            $cat1[$key]['sub_cat_id'] = $cat11->sub_cat_id ;
            $cat1[$key]['sub_cat_name'] = $cat11->sub_cat_name;
            $cat1[$key]['sub_cat_icon'] = "/sub_cat_icon/{$cat11->sub_cat_icon}";
        }
        
        return response()->json(['sub_cat' => $cat1], 200); 
     
    }
    /**
     * Document API
     * @return \Illuminate\Http\Response
     * 
     * @OA\Get(
     *      path="/api/sub_sub_cat?sub_cat_id={sub_cat_id}",
     *      operationId="subSubCategory",
     *      tags={"Category"},
     *      summary="View all Sub Sub Category",
     *      @OA\Parameter(
     *          name="sub_cat_id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(response=200, description="successful operation"),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     * )
     * 
     */
    public function sub_sub_cat(Request $req) 
    { 
        $cat1=[];
        $sub_cat_id=$req->sub_cat_id;
        $sub_sub_cat=DB::table('sub_sub_cats')->where('sub_cat_id',$sub_cat_id)->get();


        foreach($sub_sub_cat as $key=> $cat11)
        {
            $cat1[$key]['sub_cat_id'] = $cat11->sub_cat_id ;
            $cat1[$key]['sub_sub_cat_id'] = $cat11->sub_sub_cat_id ;
            $cat1[$key]['sub_sub_cat_name'] = $cat11->sub_sub_cat_name;
            $cat1[$key]['sub_sub_cat_icon'] = "/sub_sub_cat_icon/{$cat11->sub_sub_cat_icon}";
        }
        
       
        return response()->json(['sub_sub_cat' => $cat1], 200); 
     
    }
    /**
     * Document API
     * @return \Illuminate\Http\Response
     * 
     * @OA\Get(
     *      path="/api/product?sub_sub_id={sub_sub_id}",
     *      operationId="product",
     *      tags={"Product"},
     *      summary="View all Product sub sub Category Wise",
     *      @OA\Parameter(
     *          name="sub_sub_id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(response=200, description="successful operation"),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     * )
     * 
     */
    public function product(Request $req) 
    { 
        $sub_sub_id=$req->sub_sub_id;
        $pro1=DB::table('products')
        ->join('brands','products.brand_id','=','brands.brand_id')
        ->join('sub_cats','products.sub_cat_id','=','sub_cats.sub_cat_id')
        ->where('sub_sub_cat_id',$sub_sub_id)
        ->where('active_status','YES')
        ->get();
        $product=array();
        foreach($pro1 as $key=>$pro){
            $product[$key]['product_id']=$pro->product_id;
            $product[$key]['product_name']=$pro->product_name;
            $product[$key]['product_brand']=$pro->brand_name;
            if($pro->weight>999)
                $weight=round($pro->weight/1000,2).' Kg';
            else
                $weight=$pro->weight.' Gm';
            $product[$key]['product_weight']=$weight;
            $product[$key]['total_review']=$pro->total_review;
            $product[$key]['avg_review']=$pro->review;
            $product[$key]['mrp']=$pro->mrp;
            $product[$key]['selling_price']=round(($pro->selling_price*$pro->gst/100)+$pro->selling_price);

            
            $banner=DB::table('product_images')->where('product_id',$pro->product_id)->limit(1)->get();
            foreach($banner as $banner){
                    $product[$key]['product_image']="/product_image/".$banner->image;
            }
            $product[$key]['discount']=round((($pro->mrp-($pro->selling_price+($pro->selling_price*$pro->gst/100)))/$pro->mrp)*100);
        }
        return response()->json(['product' => $product], 200); 
     
    }
        /**
     * Document API
     * @return \Illuminate\Http\Response
     * 
     * @OA\Get(
     *      path="/api/product_details?product_id={product_id}&user_id={user_id}",
     *      operationId="product_details",
     *      tags={"Product"},
     *      summary="View Product Details",
     *      @OA\Parameter(
     *          name="product_id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="user_id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(response=200, description="successful operation"),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     * )
     * 
     */
    public function product_details(Request $req) 
    { 
        $product_id=$req->product_id;
        $pro1=DB::table('products')
        ->join('brands','products.brand_id','=','brands.brand_id')
        ->join('sub_cats','products.sub_cat_id','=','sub_cats.sub_cat_id')
        ->where('products.product_id',$product_id)
        ->where('active_status','YES')
        ->get();
        $product=array();
        foreach($pro1 as $pro){
            $product['product_id']=$pro->product_id;
            $product['product_name']=$pro->product_name;
            $product['product_brand']=$pro->brand_name;
            if($pro->weight==0){
                 $weight='0';
            }else{
                 if($pro->weight>999)
                $weight=round($pro->weight/1000,2).' Kg';
            else
            $weight=$pro->weight.' Gm';
            }
           
            
            $product['product_weight']=$weight;
            $product['total_review']=$pro->total_review;
            $product['avg_review']=$pro->review;
            $product['mrp']=$pro->mrp;
            $product['available_product']=$pro->available_stock;
            $product['selling_price']=round(($pro->selling_price*$pro->gst/100)+$pro->selling_price);
            $product['discount']=round((($pro->mrp-($pro->selling_price+($pro->selling_price*$pro->gst/100)))/$pro->mrp)*100);
            
            $banner=DB::table('product_images')->where('product_id',$pro->product_id)->get();
            foreach($banner as $key=>$banner){
                $product['product_image'][$key]="/product_image/".$banner->image;
            }
            $banner=DB::table('product_features')->where('product_id',$pro->product_id)->get();
            foreach($banner as $key=>$banner){
                $product['features'][$key]=$banner->features;
            }
            $banner=DB::table('product_spacifications')->where('product_id',$pro->product_id)->get();
            foreach($banner as $key=>$banner){
                $product['specifications'][$key]['title']=$banner->title;
                $product['specifications'][$key]['description']=$banner->description;
            }
            $product['description']=$pro->description;
            if($req->user_id!=0){
                $cart=DB::table('carts')->where('product_id',$product_id)->where('user_id',$req->user_id)->count();
                if($cart!=0)
                    $product['cart']="1";
                else
                    $product['cart']="0";
                
                $wishlist=DB::table('wishlists')->where('product_id',$product_id)->where('user_id',$req->user_id)->count();
                if($wishlist!=0)
                    $product['wishlist']="1";
                else
                    $product['wishlist']="0";
            }else{
                $product['cart']="0";
                $product['wishlist']="0";
            }
            if($pro->group_id!=0){
                $group_product=DB::table('products')
                ->join('brands','products.brand_id','=','brands.brand_id')
                ->join('sub_cats','products.sub_cat_id','=','sub_cats.sub_cat_id')
                ->where('group_id',$pro->group_id)
                ->where('active_status','YES')
                ->get();
                $product['same_group_product']=array();
                $i=0;
                foreach($group_product as $key1=>$group_product){
                    if($group_product->product_id!=$pro->product_id){
                        $product['same_group_product'][$i]['product_id']=$group_product->product_id;
                        $product['same_group_product'][$i]['name']=$group_product->product_name;
                        if($group_product->weight>999)
                            $weight=round($group_product->weight/1000,2).' Kg';
                        else
                            $weight=$group_product->weight.' Gm';
                        $product['same_group_product'][$i]['weight']=$weight;

                   
                      $banner1=DB::table('product_images')->where('product_id',$group_product->product_id)->limit(1)->get();
                        foreach($banner1 as $key=>$banner1){
                             $product['same_group_product'][$i]['image']="/product_image/".$banner1->image;
                        }
                        $i++;
                     }
                }
            }else{
                $product['same_group_product']=[];
            }

        }
        $repro1=DB::table('products')
        ->join('brands','products.brand_id','=','brands.brand_id')
        ->join('sub_cats','products.sub_cat_id','=','sub_cats.sub_cat_id')
        ->where('sub_sub_cat_id',$pro->sub_sub_cat_id)
        ->where('product_id','!=',$pro->product_id)
        ->where('active_status','YES')
        ->get();
        $reProduct=array();
        foreach($repro1 as $key=>$repro1){
            $reProduct[$key]['product_id']=$repro1->product_id;
            $reProduct[$key]['product_name']=$repro1->product_name;
            $reProduct[$key]['product_brand']=$repro1->brand_name;
            if($repro1->weight>999)
                $weight=round($pro->weight/1000,2).' Kg';
            else
            $weight=$pro->weight.' Gm';
            $reProduct[$key]['product_weight']=$weight;
            $reProduct[$key]['total_review']=$repro1->total_review;
            $reProduct[$key]['avg_review']=$repro1->review;
            $reProduct[$key]['mrp']=$repro1->mrp;
            $reProduct[$key]['selling_price']=round(($repro1->selling_price*$repro1->gst/100)+$repro1->selling_price);

            
            $banner=DB::table('product_images')->where('product_id',$repro1->product_id)->limit(1)->get();
            foreach($banner as $banner){
                $reProduct[$key]['product_image']="/product_image/".$banner->image;
            }
        
            $reProduct[$key]['discount']=round((($repro1->mrp-($repro1->selling_price+($repro1->selling_price*$repro1->gst/100)))/$repro1->mrp)*100);
        }
        return response()->json(['product' => $product,'related_product' => $reProduct], 200); 
                
    }
     /**
     * Document API
     * @return \Illuminate\Http\Response
     * 
     * @OA\Get(
     *      path="/api/new_arrival",
     *      operationId="new_arrival",
     *      tags={"Product"},
     *      summary="View last 10 new arrival Product",
     *      @OA\Response(response=200, description="successful operation"),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     * )
     * 
     */
    public function new_arrival(Request $req) 
    { 
        $sub_sub_id=$req->sub_sub_id;
        $pro1=DB::table('products')
        ->join('brands','products.brand_id','=','brands.brand_id')
        ->join('sub_cats','products.sub_cat_id','=','sub_cats.sub_cat_id')
        ->orderBy('products.product_id','desc')
        ->where('active_status','YES')
        ->limit(10)
        ->get();
        $product=array();
        foreach($pro1 as $key=>$pro){
            $product[$key]['product_id']=$pro->product_id;
            $product[$key]['product_name']=$pro->product_name;
            $product[$key]['product_brand']=$pro->brand_name;
            if($pro->weight>999)
                $weight=round($pro->weight/1000,2).' Kg';
            else
                $weight=$pro->weight.' Gm';
            $product[$key]['product_weight']=$weight;
            $product[$key]['total_review']=$pro->total_review;
            $product[$key]['avg_review']=$pro->review;
            $product[$key]['mrp']=$pro->mrp;
            $product[$key]['selling_price']=round(($pro->selling_price*$pro->gst/100)+$pro->selling_price);

            
            $banner=DB::table('product_images')->where('product_id',$pro->product_id)->limit(1)->get();
            foreach($banner as $banner){
                    $product[$key]['product_image']="/product_image/".$banner->image;
            }
            $product[$key]['discount']=round((($pro->mrp-($pro->selling_price+($pro->selling_price*$pro->gst/100)))/$pro->mrp)*100);
        }
        return response()->json(['product' => $product], 200); 
     
    }
     /**
     * Document API
     * @return \Illuminate\Http\Response
     * 
     * @OA\Get(
     *      path="/api/best_seller",
     *      operationId="best_seller",
     *      tags={"Product"},
     *      summary="View last 10 best seller Product",
     *      @OA\Response(response=200, description="successful operation"),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     * )
     * 
     */
    public function best_seller(Request $req) 
    { 
        $sub_sub_id=$req->sub_sub_id;
        $pro1=DB::table('products')
        ->join('brands','products.brand_id','=','brands.brand_id')
        ->join('sub_cats','products.sub_cat_id','=','sub_cats.sub_cat_id')
        ->join('book_multi_items','products.product_id','=','book_multi_items.product_id')
        ->orderBy('products.product_id','desc')
        ->where('active_status','YES')
        ->groupBy('products.product_id')
        ->select('products.*','brands.*','sub_cats.*')
        ->limit(10)
        ->get();
        $product=array();
        foreach($pro1 as $key=>$pro){
            $product[$key]['product_id']=$pro->product_id;
            $product[$key]['product_name']=$pro->product_name;
            $product[$key]['product_brand']=$pro->brand_name;
            if($pro->weight>999)
                $weight=round($pro->weight/1000,2).' Kg';
            else
                $weight=$pro->weight.' Gm';
            $product[$key]['product_weight']=$weight;
            $product[$key]['total_review']=$pro->total_review;
            $product[$key]['avg_review']=$pro->review;
            $product[$key]['mrp']=$pro->mrp;
            $product[$key]['selling_price']=round(($pro->selling_price*$pro->gst/100)+$pro->selling_price);

            
            $banner=DB::table('product_images')->where('product_id',$pro->product_id)->limit(1)->get();
            foreach($banner as $banner){
                    $product[$key]['product_image']="/product_image/".$banner->image;
            }
            $product[$key]['discount']=round((($pro->mrp-($pro->selling_price+($pro->selling_price*$pro->gst/100)))/$pro->mrp)*100);
        }
        return response()->json(['product' => $product], 200); 
     
    }
    /**
     * Document API
     * @return \Illuminate\Http\Response
     * 
     * @OA\Get(
     *      path="/api/mostViewedProduct",
     *      operationId="mostViewedProduct",
     *      tags={"Product"},
     *      summary="View last 10 most viewed Product",
     *      @OA\Response(response=200, description="successful operation"),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     * )
     * 
     */
    public function mostViewedProduct(Request $req) 
    { 
        $sub_sub_id=$req->sub_sub_id;
        $pro1=DB::table('products')
        ->join('brands','products.brand_id','=','brands.brand_id')
        ->join('sub_cats','products.sub_cat_id','=','sub_cats.sub_cat_id')
        ->orderBy('products.view_count','desc')
        ->where('active_status','YES')
        ->limit(10)
        ->get();
        $product=array();
        foreach($pro1 as $key=>$pro){
            $product[$key]['product_id']=$pro->product_id;
            $product[$key]['product_name']=$pro->product_name;
            $product[$key]['product_brand']=$pro->brand_name;
            if($pro->weight>999)
                $weight=round($pro->weight/1000,2).' Kg';
            else
                $weight=$pro->weight.' Gm';
            $product[$key]['product_weight']=$weight;
            $product[$key]['total_review']=$pro->total_review;
            $product[$key]['avg_review']=$pro->review;
            $product[$key]['mrp']=$pro->mrp;
            $product[$key]['selling_price']=round(($pro->selling_price*$pro->gst/100)+$pro->selling_price);

            
            $banner=DB::table('product_images')->where('product_id',$pro->product_id)->limit(1)->get();
            foreach($banner as $banner){
                    $product[$key]['product_image']="/product_image/".$banner->image;
            }
            $product[$key]['discount']=round((($pro->mrp-($pro->selling_price+($pro->selling_price*$pro->gst/100)))/$pro->mrp)*100);
        }
        return response()->json(['product' => $product], 200); 
     
    }

    /**
     * Document API
     * @return \Illuminate\Http\Response
     * 
     * @OA\Post(
     *      path="/api/pincheck",
     *      operationId="DeliveryPinCheck",
     *      tags={"Delivery"},
     *      summary="Delivery Pin Check for product details page",
     *      @OA\RequestBody(
     *           @OA\MediaType(
     *                mediaType="application/json",
     *                      @OA\Schema(
     *                        @OA\Property(
     *                          property="pin",
     *                            type="integer"
     *                           ),
     *                          )
     *                          ),
     *                        ),
     *      @OA\Response(response=200, description="successful operation"),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     * )
     * 
     */
   
    public function pincheck(Request $req)
    {    
        $pin=$req->pin;
        $store=DB::table('pincodes')->where('active_status','YES')->where('pincode',$pin)->count();
        if($store!=0){
            $status['message']='pincode serviceable';
            $status['status']=1; 
        }else{
            $status['message']='pincode not serviceable';
            $status['status']=0; 
        }
        return response()->json(['pincode' => $status], 200); 
    }

     /**
     * Document API
     * @return \Illuminate\Http\Response
     * 
     * @OA\Post(
     *      path="/api/find_delivery_charge",
     *      operationId="find_delivery_charge",
     *      tags={"Delivery"},
     *      summary="Get Delivery Charge against pincode",
     *      @OA\RequestBody(
     *           @OA\MediaType(
     *                mediaType="application/json",
     *                      @OA\Schema(
     *                        @OA\Property(
     *                          property="pin",
     *                            type="integer"
     *                           ),
     *                        @OA\Property(
     *                          property="product_total",
     *                            type="integer"
     *                           ),
     *                          )
     *                          ),
     *                        ),
     * 
     *      @OA\Response(response=200, description="successful operation"),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     * )
     * 
     */
   
    public function find_delivery_charge(Request $req)
    {
        $pin=$req->pin;
        $payble_amount=$req->product_total;
        $store=DB::table('pincodes')->where('active_status','YES')->where('pincode',$pin)->count();
        if($store!=0){
            $pin=DB::table('pincodes')->where('active_status','YES')->where('pincode',$pin)->first();
            $minamount=$pin->min_price;
            $delivary_charge=$pin->delivery_charge;
            if($payble_amount>$minamount){
                $dc=0;
            }else{
                $dc=$pin->delivery_charge;
            }
            $status['message']='Pincode Serviceable';
            $status['status']=1; 
            $details['pincode']=$pin->pincode;
            $details['payble_amount']=$payble_amount;
            $details['delivary_charge']=(int)$dc;
        
        }else{
            $status['message']='Pincode not Serviceable';
            $status['status']=0; 
            $details=array();
        }
    return response()->json([ 'status'=>$status,
        'details'=>$details], 200);

    }
       /**
     * Document API
     * @return \Illuminate\Http\Response
     * 
     * @OA\Get(
     *      path="/api/coupon_banner",
     *      operationId="coupon_banner",
     *      tags={"Banner & Offer"},
     *      summary="all coupon banner with discount",
     *      @OA\Response(response=200, description="successful operation"),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     * )
     * 
     */
    public function coupon_banner(Request $req)
    {    
        $store=DB::table('coupon_banners')->where('active_status','YES')->get();
        $banner=array();
        foreach($store as $key=>$store){
            $banner[$key]['coupon_image']=$store->coupon_image;
            $banner[$key]['title']=$store->title;
            $banner[$key]['coupon_value']=strval($store->coupon_value);
            $banner[$key]['coupon_code']=$store->coupon_code;
            $banner[$key]['coupon_validity']=$store->coupon_validity;
            $banner[$key]['min_price']=strval($store->min_price);
            $banner[$key]['coupon_type']=$store->coupon_type;
            
        }
       
    
        return response()->json(['coupon' => $banner], 200); 
    }
   /**
     * Document API
     * @return \Illuminate\Http\Response
     * 
     * @OA\Get(
     *      path="/api/display_banner",
     *      operationId="display_banner",
     *      tags={"Banner & Offer"},
     *      summary="all display banner",
     *      @OA\Response(response=200, description="successful operation"),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     * )
     * 
     */
    public function display_banner(Request $req)
    {    
        $store=DB::table('display_banners')->get();
       
    
        return response()->json(['banner' => $store], 200); 
    }
    
     /**
     * Document API
     * @return \Illuminate\Http\Response
     * 
     * @OA\Post(
     *      path="/api/addtocart",
     *      operationId="addtocart",
     *      tags={"Cart & Wishlist"},
     *      summary="Add product to cart",
     *      @OA\RequestBody(
     *           @OA\MediaType(
     *                mediaType="application/json",
     *                      @OA\Schema(
     *                        @OA\Property(
     *                          property="product_id",
     *                            type="integer"
     *                           ),
     *                        @OA\Property(
     *                          property="qty",
     *                            type="integer"
     *                           ),
     *                          )
     *                          ),
     *                        ),
     * 
     *      @OA\Response(response=200, description="successful operation"),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *  security={
     *          {"bearerAuth": {}}
     *           },
     * )
     * 
     */
    public function addtocart(Request $req)
    {
        
        $user=Auth::user()->id;
        $product=$req->product_id;
        $pro=DB::table('carts')->where('product_id',$product)->where('user_id',$user)->count();
        $status['user']=Auth::user()->id;
        if($pro<1){
            $pro11=DB::table('products')->where('product_id',$product)->first();
            if($pro11->available_stock>=$req->qty){
                $item = new cart;
            $item->user_id=$user;
            $item->product_id=$req->product_id;
            $item->quantity=$req->qty;
        //  $item->size=$req->size;
            $item->remember_token='yY1A5EBh3jP7jXwDQvqi847TWVMpMolNS8HK8rCH';
            $item->save();
            //return view('layouts.wishlist_ajax');
            $status['msg']='Item add successfully';
            $status['status']=1;
            }else{
                $status['msg']='Quantity not available';
                $status['status']=0;
            }
            
        }else{
            $status['msg']='Already exits';
            $status['status']=0;
        }

    return response()->json(['status' => $status], 200); 
    
    }

     /**
     * Document API
     * @return \Illuminate\Http\Response
     * 
     * @OA\Post(
     *      path="/api/addtowishlist",
     *      operationId="addtowishlist",
     *      tags={"Cart & Wishlist"},
     *      summary="Add product to wishlist",
     *      @OA\RequestBody(
     *           @OA\MediaType(
     *                mediaType="application/json",
     *                      @OA\Schema(
     *                        @OA\Property(
     *                          property="product_id",
     *                            type="integer"
     *                           ),
     *                        @OA\Property(
     *                          property="qty",
     *                            type="integer"
     *                           ),
     *                          )
     *                          ),
     *                        ),
     * 
     *      @OA\Response(response=200, description="successful operation"),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *  security={
     *          {"bearerAuth": {}}
     *           },
     * )
     * 
     */
    public function addtowishlist(Request $req)
    {
        
        $user=Auth::user()->id;
        $product=$req->product_id;
        $pro=DB::table('wishlists')->where('product_id',$product)->where('user_id',$user)->count();

        if($pro<1){
            $pro11=DB::table('products')->where('product_id',$product)->first();
           
                $item = new wishlist;
            $item->user_id=$user;
            $item->product_id=$req->product_id;
            $item->quantity=$req->qty;
        //  $item->size=$req->size;
            $item->remember_token='yY1A5EBh3jP7jXwDQvqi847TWVMpMolNS8HK8rCH';
            $item->save();
            //return view('layouts.wishlist_ajax');
            $status['msg']='Item add successfully';
            $status['status']=1;
            
            
        }else{
            $status['msg']='Already exits';
            $status['status']=0;
        }

    return response()->json(['status' => $status], 200); 
    
    }
   
        /**
     * Document API
     * @return \Illuminate\Http\Response
     * 
     * @OA\Post(
     *      path="/api/removetocart",
     *      operationId="removetocart",
     *      tags={"Cart & Wishlist"},
     *      summary="remove product to cart",
     *      @OA\RequestBody(
     *           @OA\MediaType(
     *                mediaType="application/json",
     *                      @OA\Schema(
     *                        @OA\Property(
     *                          property="product_id",
     *                            type="integer"
     *                           ),
     *                          )
     *                          ),
     *                        ),
     * 
     *      @OA\Response(response=200, description="successful operation"),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *  security={
     *          {"bearerAuth": {}}
     *           },
     * )
     * 
     */
    public function removetocart(Request $req)
    {
        
        $user=Auth::user()->id;
        $product=$req->product_id;
        $pro=DB::table('carts')->where('product_id',$product)->where('user_id',$user)->delete();

            $status['msg']='Item remove successfully';
            $status['status']=1;
         
        return response()->json(['status' => $status], 200);
    }


      /**
     * Document API
     * @return \Illuminate\Http\Response
     * 
     * @OA\Post(
     *      path="/api/removetowishlist",
     *      operationId="removetowishlist",
     *      tags={"Cart & Wishlist"},
     *      summary="remove product to wishlist",
     *      @OA\RequestBody(
     *           @OA\MediaType(
     *                mediaType="application/json",
     *                      @OA\Schema(
     *                        @OA\Property(
     *                          property="product_id",
     *                            type="integer"
     *                           ),
     *                          )
     *                          ),
     *                        ),
     * 
     *      @OA\Response(response=200, description="successful operation"),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *  security={
     *          {"bearerAuth": {}}
     *           },
     * )
     * 
     */
    public function removetowishlist(Request $req)
    {
        
        $user=Auth::user()->id;
        $product=$req->product_id;
        $pro=DB::table('wishlists')->where('product_id',$product)->where('user_id',$user)->delete();

            $status['msg']='Item remove successfully';
            $status['status']=1;
         
        return response()->json(['status' => $status], 200);
    }

   /**
     * Document API
     * @return \Illuminate\Http\Response
     * 
     * @OA\POST(
     *      path="/api/address",
     *      operationId="address",
     *      tags={"Address"},
     *      summary="Get all shipping address",
     *      @OA\Response(response=200, description="successful operation"),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *  security={
     *          {"bearerAuth": {}}
     *           },
     * )
     * 
     */
    protected function address(Request $request)
    {
     if (Auth::check()) {
        $id=Auth::user()->id;
        $address=DB::table('addres')->where('user_id',$id)->get();
        $count=DB::table('addres')->where('user_id',$id)->count();
        if($count==0){
            $address=0;
            $status['msg']='Address not found';
            $status['status']=0;
        }else{
            $status['msg']='Address found';
            $status['status']=1;

        }

    }
    else{
        $address=0;
        $status['msg']='Please Login';
        $status['status']=0;
    }
   // echo $status;
        return response()->json(['address' => $address,'status'=>$status],200);
    }



    /**
     * Document API
     * @return \Illuminate\Http\Response
     * 
     * @OA\Post(
     *      path="/api/add_update_address",
     *      operationId="add_update_address",
     *      tags={"Address"},
     *      summary="Add or update Shipping address",
     *      @OA\RequestBody(
     *           @OA\MediaType(
     *                mediaType="application/json",
     *                      @OA\Schema(
     *                        @OA\Property(
     *                          property="id",
     *                            type="integer"
     *                           ),
     *                        @OA\Property(
     *                          property="flat",
     *                            type="string"
     *                           ),
     *                        @OA\Property(
     *                          property="landmark",
     *                            type="string"
     *                           ),
     *                        @OA\Property(
     *                          property="location",
     *                            type="string"
     *                           ),
     *                        @OA\Property(
     *                          property="pincode",
     *                            type="string"
     *                           ),
     *                        @OA\Property(
     *                          property="city",
     *                            type="string"
     *                           ),
     *                      
     *   @OA\Property(
     *                          property="district",
     *                            type="string"
     *                           ),
     *                        @OA\Property(
     *                          property="state",
     *                            type="string"
     *                           ),
     *                        @OA\Property(
     *                          property="address",
     *                            type="string"
     *                           ),
     *                       
     * 
     *                        @OA\Property(
     *                          property="name",
     *                            type="string"
     *                           ),
     *                        @OA\Property(
     *                          property="phone_no",
     *                            type="string"
     *                           ),
     *                        @OA\Property(
     *                          property="optional_phone",
     *                            type="string"
     *                           ),
     *                        @OA\Property(
     *                          property="gst_no",
     *                            type="string"
     *                           ),
     *                          )
     *                          ),
     *                        ),
     * 
     *      @OA\Response(response=200, description="successful operation"),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *  security={
     *          {"bearerAuth": {}}
     *           },
     * )
     * 
     */
    protected function add_update_address(Request $req11)
    {
     if (Auth::check()) {
      
        $id=$req11->id;
        $user_id=Auth::user()->id;
        $flat=$req11->input('flat');
        $address=$req11->input('address');
        $landmark=$req11->input('landmark');
        $location=$req11->input('location');
        $pin=$req11->input('pincode');
        $city=$req11->input('city');
        $district=$req11->input('district');
        $state=$req11->input('state');
        $name=$req11->input('name');
        $phone=$req11->input('phone_no');
        $optional_phone=$req11->input('optional_phone');
        $gst=$req11->input('gst_no');
        $token='yY1A5EBh3jP7jXwDQvqi847TWVMpMolNS8HK8rCH';
       
       

        $store1=DB::table('billing_adds')->where('user_id',$user_id)->count();
        $store=DB::table('addres')->where('id',$id)->where('user_id',$user_id)->count();

        if($store1==0)
        {

            $address2=new billing_add;
      
            $address2->pincode=$pin;
            $address2->landmark=$landmark;
            $address2->location=$location;
            $address2->address=$address;
            $address2->city=$city;
            $address2->district=$district;
            $address2->state=$state;
           
            $address2->user_id=$user_id;
            $address2->remember_token=$token;
          
            $address2->save();
         
        }

         if($store==0)
        {
            $address1=new addre;
            $address1->pincode=$pin;
            $address1->flat=$flat;
            $address1->landmark=$landmark;
            $address1->location=$location;
            $address1->address=$address;
            $address1->city=$city;
            $address1->district=$district;
            $address1->state=$state;

            $address1->name=$name;
            $address1->phone_no=$phone;
            $address1->optional_phone=$optional_phone;
            $address1->gst_no= $gst;
            $address1->user_id=$user_id;
            $address1->remember_token=$token;
      
        $address1->save();
        $status['msg']='Address add Successfully';
        $status['status']=1;
        }
        else{
            DB::table('addres')
            ->where('id', $id)
            ->update(['pincode' => $pin,'flat' => $flat,'landmark' => $landmark,'location' => $location,'address' => $address,'city' => $city,'state' => $state,'name' => $name,'phone_no' => $phone,'optional_phone' => $optional_phone,'gst_no'=>$gst]);
            $status['msg']='Address update Successfully';
            $status['status']=1;
        }
        
      
    }
    else{
        $address=0;
        $status['msg']='Please Login';
        $status['status']=0;
    }
   // echo $status;
        return response()->json(['status'=>$status], 200);
    }

/**
     * Document API
     * @return \Illuminate\Http\Response
     * 
     * @OA\Post(
     *      path="/api/checkout_item",
     *      operationId="checkout_item",
     *      tags={"Checkout"},
     *      summary="Product details for checkout Item",
     *      @OA\RequestBody(
     *           @OA\MediaType(
     *                mediaType="application/json",
     *                      @OA\Schema(
     *                        @OA\Property(
     *                          property="product_id",
     *                            type="integer"
     *                           ),
     *                        @OA\Property(
     *                          property="qty",
     *                            type="integer"
     *                           ),
     *                        @OA\Property(
     *                          property="type",
     *                            type="integer"
     *                           ),
     *                          )
     *                          ),
     *                        ),
     * 
     *      @OA\Response(response=200, description="successful operation"),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *  security={
     *          {"bearerAuth": {}}
     *           },
     * )
     * 
     */
    public function checkout_item(Request $ss1)
    
    {
        $tp=0;
        $tp2=0;
        $tdc=0;
        $totalgst=0;
        $gst=0;
        if($ss1->type==0){
            


            $user_id=Auth::user()->id;
            $product_id=$ss1->product_id;
            $quantity=$ss1->qty;
          // $size1=$ss1->size;
    
            $token='LZpRmq6kc0Yvpwdv6dFlLE7kv4ud2TiDVBkkuRrE';
           
           
            $store=DB::table('none_carts')->where('product_id',$product_id)->where('user_id',$user_id)->count();
    
            if($store==0)
        {
    
            DB::table('none_carts')->where('user_id', $user_id)->delete();
            $non_cart1= new none_cart;
            $non_cart1->user_id=$user_id;
            $non_cart1->product_id=$product_id;
            $non_cart1->quantity=$quantity;
           // $non_cart1->size=$size1;
            $non_cart1->remember_token=$token;
            $non_cart1->save();
        }
        else
        {
            DB::table('none_carts')
            ->where('user_id', $user_id)->where('product_id',$product_id)
            ->update(['quantity' => $quantity]);
    
    
        }
            
           
        

            $product_id=$ss1->product_id;
            $qty=$ss1->qty;

            $product1=DB::table('products')
            ->join('brands','products.brand_id','=','brands.brand_id')
            ->join('sub_cats','products.sub_cat_id','=','sub_cats.sub_cat_id')->where('products.product_id',$product_id)->limit(1)->get();
                 
            foreach($product1 as $key=>$product_item){
    
                $product[$key]['product_id']=$product_item->product_id;
                $product[$key]['product_name']=$product_item->product_name;
            
                if($product_item->weight>999)
                $weight=round($product_item->weight/1000,2).' Kg';
            else
                $weight=$product_item->weight.' Gm';
            $product[$key]['product_weight']=$weight;
                $product[$key]['product_brand']=$product_item->brand_name;
                $product[$key]['product_review']=$product_item->review;
                $product[$key]['qty']=$qty;
                $banner=DB::table('product_images')->where('product_id',$product_item->product_id)->limit(1)->get();
                foreach($banner as $banner){
                        $product[$key]['product_image']="/product_image/".$banner->image;
                }
                

                $g=round($product_item->selling_price*$product_item->gst/100,2);
                $price1=$g+$product_item->selling_price;
                    $price2=$product_item->selling_price;
                $tp=$tp+($price1*$qty);
                    $tp2=$tp2+($price2*$qty);

                    $tdc=($price1)*$qty;
                    $product[$key]['selling_price']=strval(round($price1,2));
                   
                $product[$key]['product_mrp']=strval($product_item->mrp);
                $product[$key]['gst']=strval(round($g*$qty,2));
                $gst=round($g*$qty,2);
               $product[$key]['total']=strval(round($tdc,2));

                         }
           $totalgst=$totalgst+$gst;
        }else{

            $user_id=Auth::user()->id;
            $for_cart_view=DB::table('carts')->where('user_id',$user_id)->get();
            foreach($for_cart_view as $key=>$item){
                
                $product_id=$item->product_id;
                $qty=$item->quantity;

                $product1=DB::table('products')
                ->join('brands','products.brand_id','=','brands.brand_id')
                ->join('sub_cats','products.sub_cat_id','=','sub_cats.sub_cat_id')->where('products.product_id',$product_id)->limit(1)->get();
                    
                foreach($product1 as $product_item){
        
                    $product[$key]['product_id']=$product_item->product_id;
                    $product[$key]['product_name']=$product_item->product_name;
                    if($product_item->weight>999)
                    $weight=round($product_item->weight/1000,2).' Kg';
                else
                    $weight=$product_item->weight.' Gm';
                $product[$key]['product_weight']=$weight;
                    $product[$key]['product_brand']=$product_item->brand_name;
                    $product[$key]['product_review']=$product_item->review;
                    $product[$key]['qty']=strval($qty);
                    $banner=DB::table('product_images')->where('product_id',$product_item->product_id)->limit(1)->get();
                    foreach($banner as $banner){
                            $product[$key]['product_image']="/product_image/".$banner->image;
                    }
                    $g=round($product_item->selling_price*$product_item->gst/100,2);
                    $price1=$g+$product_item->selling_price;
                    $price2=$product_item->selling_price;

                    $tp=$tp+($price1*$qty);
                    $tp2=$tp2+($price2*$qty);

                    $tdc=($price1)*$qty;
                    $product[$key]['selling_price']=strval(round($price1,2));
                 

                    $product[$key]['product_mrp']=strval($product_item->mrp);
                    $product[$key]['gst']=strval(round($g*$qty,2));
                    $gst=round($g*$qty,2);
                   $product[$key]['total']=strval(round($tdc,2));
                }
                $totalgst=$totalgst+$gst;

           }


        }

        $user_id=Auth::user()->id;
        $wallet=DB::table('user_wallets')->where('user_id',$user_id)->first();
        $wallet_amount=round($wallet->wallet_ammount);

        $payble_price=$tp;

 
        return response()->json(['product' => $product,
       // 'wallet_ammount'=>$wallet_amount,
       // 'cart_total'=>$tp,
       // 'total_delivary_charge'=>$tdc,
        'total_payble_price'=>strval(round($payble_price,2)),'sub_total'=>strval(round($tp2,2)),'wallet_amount'=>strval(round($wallet_amount)),'totalgst'=>strval(round($totalgst))], 200);
    }


/**
     * Document API
     * @return \Illuminate\Http\Response
     * 
     * @OA\Post(
     *      path="/api/pay",
     *      operationId="pay",
     *      tags={"Checkout"},
     *      summary="Payment Capture",
     *      @OA\RequestBody(
     *           @OA\MediaType(
     *                mediaType="application/json",
     *                      @OA\Schema(
     *                        @OA\Property(
     *                          property="amt_val",
     *                            type="integer"
     *                           ),
     *                  
     *                          )
     *                          ),
     *                        ),
     * 
     *      @OA\Response(response=200, description="successful operation"),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *  security={
     *          {"bearerAuth": {}}
     *           },
     * )
     * 
     */

    public function pay(Request $req){
Artisan::call('config:cache');
        $api_key = env("REZORPAY_API");
        $api_secret = env("REZORPAY_API_SECRET");
    
        $api = new Api($api_key, $api_secret);
    
        $order = $api->order->create(array(  'receipt' => '1234456',  'amount' => $req->amt_val*100,  'payment_capture' => 1,  'currency' => 'INR'  ));
        $returnData = [
            'order_id'=> $order->id,
            'amount'=> $order->amount_due 
        ];
        return $returnData;
    
    }

/**
     * Document API
     * @return \Illuminate\Http\Response
     * 
     * @OA\Post(
     *      path="/api/order_submit",
     *      operationId="order_submit",
     *      tags={"Checkout"},
     *      summary="Order submit api",
     *      @OA\RequestBody(
     *           @OA\MediaType(
     *                mediaType="application/json",
     *                      @OA\Schema(
     *                        @OA\Property(
     *                          property="product_id",
     *                            type="integer"
     *                           ),
     *                        @OA\Property(
     *                          property="total_payble_value",
     *                            type="integer"
     *                           ),
     *                        @OA\Property(
     *                          property="totalgst",
     *                            type="integer"
     *                           ),
     *                        @OA\Property(
     *                          property="sub_total",
     *                            type="integer"
     *                           ),
     *                        @OA\Property(
     *                          property="address_id",
     *                            type="integer"
     *                           ),
     *                        @OA\Property(
     *                          property="razorpay_payment_id",
     *                            type="integer"
     *                           ),
     *                        @OA\Property(
     *                          property="payment_type",
     *                            type="integer"
     *                           ),
     *                        @OA\Property(
     *                          property="wallet_amount",
     *                            type="integer"
     *                           ),
     *                        @OA\Property(
     *                          property="shipping_charge",
     *                            type="integer"
     *                           ),
     *                        @OA\Property(
     *                          property="coupon_discount",
     *                            type="integer"
     *                           ),
     *                        @OA\Property(
     *                          property="coupon_code",
     *                            type="integer"
     *                           ),
     *                  
     *                          )
     *                          ),
     *                        ),
     * 
     *      @OA\Response(response=200, description="successful operation"),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *  security={
     *          {"bearerAuth": {}}
     *           },
     * )
     * 
     */

    public function order_submit(Request $ss1)
    {
        $product_id1=$ss1->input('product_id');
        $grand_total=$ss1->input('total_payble_value');
        $totalgst=$ss1->input('totalgst');
        $sub_total=$ss1->input('sub_total');
        $address_id=$ss1->input('address_id');
        $razorpay_payment_id=$ss1->razorpay_payment_id;
        $payment_type=$ss1->payment_type;
        $wallet_amount=$ss1->input('wallet_amount');
        $delivery_charge=$ss1->input('shipping_charge');
        $coupon_discount=$ss1->input('coupon_discount');
        if($ss1->input('coupon_code')==0){
            $coupon_code='';
        }else{
            $coupon_code=$ss1->input('coupon_code');
        }
        $coupon_code=$ss1->input('coupon_code');
        $token='LZpRmq6kc0Yvpwdv6dFlLE7kv4ud2TiDVBkkuRrE';
        $addressdetails=DB::table('addres')->where('id',$address_id)->first();
        $user_id=Auth::user()->id;
        $name=$addressdetails->name;
        $to_email=Auth::user()->email;
        $phone=$addressdetails->phone_no;
        $order_id1=uniqid();
        $order_id=strtoupper($order_id1);
        if($product_id1==0)
        {
            $items_count=DB::table('carts')->where('user_id',$user_id)->count();
            $smaaaa="cart ".$items_count." product_id".$product_id1;
   
        }else{
            $items_count=DB::table('none_carts')->where('user_id',$user_id)->count();
            $smaaaa="Non-cart ".$items_count." product_id".$product_id1;
        }

        if($items_count>0){
            
       
    
            $booking1=new booking; 
            $booking1->customer_id=$user_id;
            $booking1->coupon_code=$coupon_code;
            $booking1->price=$sub_total;
            $booking1->total_price=$grand_total;
            $booking1->totalgst=$totalgst;
            $booking1->wallet_amount=$wallet_amount;
            $booking1->delivery_charge=$delivery_charge;
            $booking1->coupon_discount=$coupon_discount;
            $booking1->order_id=$order_id;
            $booking1->razorpay_payment_id=$razorpay_payment_id;
            $booking1->payment_type=$payment_type;
            $booking1->address_id=$address_id;
            $booking1->remember_token=$token;
            $booking1->save();
            $booking_id=$booking1->id;
            if($product_id1==0)
            {
                $cart=DB::table('carts')->where('user_id',$user_id)->get();
                foreach($cart as $cats)
                {
                    $qty=$cats->quantity;
                    $product_id=$cats->product_id;
                    $product_price=DB::table('products')->join('sub_cats','products.sub_cat_id','=','sub_cats.sub_cat_id')->where('products.product_id',$product_id)->first();
                    $size=$product_price->weight;
                    $price1=$product_price->selling_price;
                    $gst=$product_price->selling_price*$product_price->gst/100;
                
                    $booking2=new book_multi_item;
                    $booking2->product_id=$product_id;
                    $booking2->booking_id=$booking_id;
                    $booking2->quantity1=$qty;
                    $booking2->size=$size;
                    $booking2->product_prices_id=0;
                    $booking2->product_price=$price1;
                    $booking2->gst=$gst;
                    $booking2->remember_token=$token;
                    $booking2->save();
                }
                $wallet=DB::table('user_wallets')->where('user_id',$user_id)->first();
                $wallet_amount1=$wallet->wallet_ammount;
                if($wallet_amount!=0)
                {
                    $remaining_wallet_amount=$wallet_amount1-$wallet_amount;
                    $user_wallet1=new user_wallet_transaction;
                    $user_wallet1->user_id=$user_id;
                    $user_wallet1->transaction_amount=$wallet_amount;
                    $user_wallet1->after_transaction_amount=$remaining_wallet_amount;
                    $user_wallet1->remarks='Booking';
                    $user_wallet1->order_id=$order_id;
                    $user_wallet1->tranasaction_type='Debit';
                    $user_wallet1->booking_id=$booking_id;
                    $user_wallet1->remember_token=$token;
                    $user_wallet1->save();
                    DB::table('user_wallets')->where('user_id',$user_id)->update(['wallet_ammount' => $remaining_wallet_amount]); 
                }
               
       
                try{
                    self::smsRequest($phone,"".$name." your order have been placed. It will be delivered to you after 2-3 working days");      
                    // $data = array('name'=>$name,'email'=>$to_email,'body'=>"Hii ".$name." your order have been placed. It will be delivered to you after 7-10 working days");
                    //     Mail :: send('mail',$data,function($message) use ($name,$to_email)
                    // {
                    // $message->to($to_email)
                    // ->subject('Booking');
                    // });
                }
                catch (\Exception $e)
                {
                   
                }
                $user_id=Auth::user()->id;
                $cart=DB::table('carts')->where('user_id',$user_id)->get();
                foreach($cart as $cats)
                {
                    $qty=$cats->quantity;
                    $product_id=$cats->product_id;
                //   $product_price_id=$cats->product_price_id;
    
    
    
                $tta=DB::table('products')->where('product_id',$product_id)->first();
                $available_stock=$tta->available_stock; 
                $remaining_stack=$available_stock-$qty;
                    
    
                $stock= new stock_trasanction;
                    $stock->product_id=$product_id;
                //  $stock->product_price_id=0; 
                    $stock->quantity=$qty;
                    $stock->type='DEBIT';
                    $stock->remarks='BOOKING';
                    $stock->transfer_id=$order_id;
                    $stock->total_quantity=$remaining_stack;
                    $stock->save();
                    DB::table('products')->where('product_id',$product_id)->update(['available_stock' => $remaining_stack]);
                
                
                }
                DB::table('carts')->where('user_id',$user_id)->delete();
            }
            else{
                $cart=DB::table('none_carts')->where('user_id',$user_id)->where('product_id',$product_id1)->get();
                foreach($cart as $cats)
                {
                    $qty=$cats->quantity;
                    $product_id=$cats->product_id;
                    //$product_price_id=$cats->product_price_id;
                    $product_price=DB::table('products')->join('sub_cats','products.sub_cat_id','=','sub_cats.sub_cat_id')->where('products.product_id',$product_id)->first();
                    $size=$product_price->weight;
                
                    $price1=$product_price->selling_price;
                
                    $gst=$product_price->selling_price*$product_price->gst/100;
                    $booking2=new book_multi_item;
                    $booking2->product_id=$product_id1;
                    $booking2->booking_id=$booking_id;
                    $booking2->quantity1=$qty;
                    $booking2->size=$size;
                    $booking2->product_prices_id=0;
                    $booking2->product_price=$price1;
                    $booking2->gst=$gst;
                    $booking2->remember_token=$token;
                    $booking2->save();
                }
                $wallet=DB::table('user_wallets')->where('user_id',$user_id)->first();
                $wallet_amount1=$wallet->wallet_ammount;
                if($wallet_amount!=0)
                {
                    $remaining_wallet_amount=$wallet_amount1-$wallet_amount;
                    $user_wallet1=new user_wallet_transaction;
                    $user_wallet1->user_id=$user_id;
                    $user_wallet1->transaction_amount=$wallet_amount;
                    $user_wallet1->after_transaction_amount=$remaining_wallet_amount;
                    $user_wallet1->remarks='Booking';
                    $user_wallet1->order_id=$order_id;
                    $user_wallet1->tranasaction_type='Debit';
                    $user_wallet1->booking_id=$booking_id;
                    $user_wallet1->remember_token=$token;
                    $user_wallet1->save();
                    DB::table('user_wallets')->where('user_id',$user_id)->update(['wallet_ammount' => $remaining_wallet_amount]);
                } 
                
            try{
                self::smsRequest($phone,"".$name." your order have been placed. It will be delivered to you after 2-3 working days");      
        
                // $data = array('name'=>$name,'email'=>$to_email,'body'=>"Hii ".$name." your order have been placed. It will be delivered to you after 7-10 working days");
                //     Mail :: send('mail',$data,function($message) use ($name,$to_email)
                // {
                // $message->to($to_email)
                // ->subject('Booking');
                // });
                }
                catch (\Exception $e)
                {
                    
    
                }
                $user_id=Auth::user()->id;
                $cart=DB::table('none_carts')->where('user_id',$user_id)->where('product_id',$product_id1)->get();
                foreach($cart as $cats)
                {
                    $qty=$cats->quantity;
                    $product_id=$cats->product_id;
                    // $product_price_id=$cats->product_price_id;
                    $tta=DB::table('products')->where('product_id',$product_id)->first();
                    $available_stock=$tta->available_stock; 
                    $remaining_stack=$available_stock-$qty;
                    
    
                    $stock= new stock_trasanction;
                    $stock->product_id=$product_id;
                    // $stock->product_price_id=0; 
                    $stock->quantity=$qty;
                    $stock->type='DEBIT';
                    $stock->remarks='BOOKING';
                    $stock->transfer_id=$order_id;
                    $stock->total_quantity=$remaining_stack;
                    $stock->save();
                    DB::table('products')->where('product_id',$product_id)->update(['available_stock' => $remaining_stack]);
                }
                DB::table('none_carts')->where('user_id',$user_id)->delete();
            }
         
            $status['msg']='Order Submit Successfully';
            $status['status']=1;
        }else{
            $status['msg']='Checkout Item not found';
            $status['status']=0;
         }
        
        return response()->json(['status'=>$status], 200);
    }


    /**
     * Document API
     * @return \Illuminate\Http\Response
     * 
     * @OA\POST(
     *      path="/api/wishlisttocart",
     *      operationId="wishlisttocart",
     *      tags={"Cart & Wishlist"},
     *      summary="add product to cart from wishlist",
     *   @OA\RequestBody(
     *           @OA\MediaType(
     *                mediaType="application/json",
     *                      @OA\Schema(
     *                        @OA\Property(
     *                          property="product_id",
     *                            type="integer"
     *                           ),
     *                  
     *                          )
     *                          ),
     *                        ),
     *      @OA\Response(response=200, description="successful operation"),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *  security={
     *          {"bearerAuth": {}}
     *           },
     * )
     * 
     */
    public function wishlisttocart(Request $req)
{
    $user=Auth::user()->id;
     $product=$req->product_id;
     $pro=DB::table('wishlists')->where('product_id',$product)->where('user_id',$user)->first();
        $pro11=DB::table('carts')->where('product_id',$product)->where('user_id',$user)->count();
     if($pro11==0){
           $item = new cart;
            $item->user_id=$user;
            $item->product_id=$req->product_id;
            $item->quantity=$pro->quantity;
         //   $item->size=$pro->size;
            $item->remember_token='LZpRmq6kc0Yvpwdv6dFlLE7kv4ud2TiDVBkkuRrE';
            $item->save();
     }
          
            //return view('layouts.wishlist_ajax');

            $pro=DB::table('wishlists')->where('product_id',$product)->where('user_id',$user)->delete();

            $status['msg']='Item add successfully';
            $status['status']=1;
      
return response()->json(['status' => $status], 200); 




}

/**
     * Document API
     * @return \Illuminate\Http\Response
     * 
     * @OA\Get(
     *      path="/api/booking_history",
     *      operationId="booking_history",
     *      tags={"Order"},
     *      summary="Get all booking",
     *      @OA\Response(response=200, description="successful operation"),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *      security={
     *          {"bearerAuth": {}}
     *           },
     * )
     * 
     */

public function booking_history(Request $ss1)
{
    $user_id=Auth::user()->id;
    $item=[];
    $booking_count=DB::table('bookings')->where('customer_id',$user_id)->orderBy('booking_id', 'desc')->count();
    if($booking_count==0){
        $booking=0; 
    }else{
   
        $booking=DB::table('bookings')->where('customer_id',$user_id)->orderBy('booking_id', 'desc')->get();
        foreach($booking as $key1=>$booking){
           

            $book_multi_item=DB::table('book_multi_items')
             ->join('bookings','bookings.booking_id','=','book_multi_items.booking_id')
            ->join('products','products.product_id','=','book_multi_items.product_id')
            ->join('brands','products.brand_id','=','brands.brand_id')
            ->where('customer_id',$user_id)->orderBy('bookings.booking_id', 'desc')
            ->get();
            foreach($book_multi_item as $abc=>$book_multi_item){
                $item[$abc]['book_multi_id']=$book_multi_item->multi_id;
                $item[$abc]['product_id']=$book_multi_item->product_id;
                $item[$abc]['product_name']=$book_multi_item->product_name;
                $item[$abc]['product_brand']=$book_multi_item->brand_name;
                $item[$abc]['order_id']=$booking->order_id;
                $item[$abc]['date']=$booking->created_at;
                if($book_multi_item->weight>999)
                    $weight=round($book_multi_item->weight/1000,2).' Kg';
                else
                    $weight=$book_multi_item->weight.' Gm';
                $item[$abc]['product_weight']=$weight;
                $banner=DB::table('product_images')->where('product_id',$book_multi_item->product_id)->limit(1)->get();
                foreach($banner as $banner){
                    $item[$abc]['product_image']="/product_image/".$banner->image;
                }
                $item[$abc]['qty']=strval($book_multi_item->quantity1);
                $item[$abc]['product_value']=strval(($book_multi_item->product_price+$book_multi_item->gst)*$book_multi_item->quantity1);
                $item[$abc]['status_code']=strval($book_multi_item->order_status);
                if($book_multi_item->order_status==1){
                    $msg='Order Under Process';
                }elseif($book_multi_item->order_status==2){
                    $msg='Order In Transit';
                }elseif($book_multi_item->order_status==3){
                    $msg='Out For Delivery';
                }elseif($book_multi_item->order_status==4){
                    $msg='Delivered';
                }elseif($book_multi_item->order_status==5){
                    $msg='Return Under process';
                }elseif($book_multi_item->order_status==6){
                    $msg='Out For Pick Up';
                }elseif($book_multi_item->order_status==7){
                    $msg='Returned';
                }elseif($book_multi_item->order_status==8){
                    $msg='Cancelled';
                }
                $item[$abc]['status_msg']=$msg;
                $item[$abc]['review_status']=$book_multi_item->review_status;
                if($book_multi_item->delivery_date==null){
                                    $item[$abc]['delivary_date']='';
                }else{
                    
                }                $item[$abc]['delivary_date']=date('d m, Y',strtotime($book_multi_item->delivery_date));


                if($book_multi_item->delivery_date==null){
                    $item[$abc]['delivary_different']="-1";
               }else{
                   
               
                 $delivery_date=$book_multi_item->delivery_date;                                            
                $redeemdate=date('Y-m-d', strtotime($delivery_date. ' + 3 days'));
                $mytime = Carbon\Carbon::now();
                $date1=date_create($redeemdate);
                $date2=date_create($mytime);      
                $diff=date_diff($date2,$date1);
                $deffrant=$diff->format("%R%a");
               
                $item[$abc]['delivary_different']=strval($deffrant);
                }  

            }



        }

    }



    return response()->json(['booking' => $item], 200);
}

    /**
     * Document API
     * @return \Illuminate\Http\Response
     * 
     * @OA\POST(
     *      path="/api/cancel_order",
     *      operationId="cancel_order",
     *      tags={"Order"},
     *      summary="Cancel a particular order",
     *   @OA\RequestBody(
     *           @OA\MediaType(
     *                mediaType="application/json",
     *                      @OA\Schema(
     *                        @OA\Property(
     *                          property="book_multi_id",
     *                            type="integer"
     *                           ),
     *                        @OA\Property(
     *                          property="option",
     *                            type="string"
     *                           ),
     *                      @OA\Property(
     *                          property="message",
     *                            type="string"
     *                           ),
     *                  
     *                          )
     *                          ),
     *                        ),
     *      @OA\Response(response=200, description="successful operation"),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *  security={
     *          {"bearerAuth": {}}
     *           },
     * )
     * 
     */
public function cancel_order(Request $tr)
 {
      $multi_id=$tr->book_multi_id;
      $cancel_option=$tr->option;
      $message=$tr->message;
      $token='Fz0ZNAb5jPXfvoJ9vSwst3wBWuOef4RMbvJ4XY2n';
      $product= DB::table('book_multi_items')->where('multi_id',$multi_id)->first();
       $product_price=($product->product_price+$product->gst)*$product->quantity1;


      $booking_id=$product->booking_id;

      $boooking= DB::table('bookings')->where('booking_id',$booking_id)->first();

      $total_booking_price=$boooking->price;
      $parsentage=((($product_price)/$total_booking_price)*100);
      $delivary_charge=$boooking->delivery_charge;
      $coupon_discount=$boooking->coupon_discount;
      $wallet_amount1=$boooking->wallet_amount;
      $order_id=$boooking->order_id;
      $token=$boooking->remember_token;

      $product= DB::table('book_multi_items')->where('booking_id',$booking_id)->count();




      if($delivary_charge!=0){
          $product_delivary_charge=$delivary_charge*($parsentage/100);
      }else{
        $product_delivary_charge=0;
      }
        if($coupon_discount!=0){
        $product_coupon_discount=$coupon_discount*($parsentage/100);
        }else{
        $product_coupon_discount=0;
        }


        if($wallet_amount1!=0){
        $product_wallet_amount=$wallet_amount1*($parsentage/100);
        }else{
        $product_wallet_amount=0;
        }

    $amount=round(($product_price+$product_delivary_charge)-$product_coupon_discount-$product_wallet_amount);

    $user_id=Auth::user()->id;
    $wallet= DB::table('user_wallets')->where('user_id',$user_id)->first();
     if($wallet_amount1!=0){
     $wallet_id=$wallet->wallet_id;
     $wallet_amount=$wallet->wallet_ammount;
     $wallet_updated_amount=$wallet_amount+$product_wallet_amount;

      DB::table('user_wallets')
      ->where('wallet_id',$wallet_id)
      ->update(['wallet_ammount' => $wallet_updated_amount]);
      $non_cart1 = new user_wallet_transaction;
      $non_cart1->user_id=$user_id;
      $non_cart1->transaction_amount=$product_wallet_amount;
      $non_cart1->after_transaction_amount=$wallet_updated_amount;
      $non_cart1->remarks='Order Canceled';
      $non_cart1->order_id=$order_id;
      $non_cart1->tranasaction_type='Credit';
      $non_cart1->booking_id=$booking_id;
      $non_cart1->remember_token=$token;
      $non_cart1->save();
 }


        $cancel_status = new cancel_reasons;
        $cancel_status->book_muti_item_id=$multi_id;
        $cancel_status->cancel_option=$cancel_option;
        $cancel_status->message=$message;
        $cancel_status->remember_token=$token;
        $cancel_status->status='Cancel';
        $cancel_status->save();
      DB::table('book_multi_items')
      ->where('multi_id',$multi_id)
      ->update(['order_status' => 8]);
      $status['msg']='Order Cancel Successfully';
      $status['status']=1;
      return response()->json(['status' => $status], 200);
 }
 

    /**
     * Document API
     * @return \Illuminate\Http\Response
     * 
     * @OA\POST(
     *      path="/api/return_order",
     *      operationId="return_order",
     *      tags={"Order"},
     *      summary="Return a particular order",
     *   @OA\RequestBody(
     *           @OA\MediaType(
     *                mediaType="application/json",
     *                      @OA\Schema(
     *                        @OA\Property(
     *                          property="book_multi_id",
     *                            type="integer"
     *                           ),
     *                        @OA\Property(
     *                          property="option",
     *                            type="string"
     *                           ),
     *                      @OA\Property(
     *                          property="message",
     *                            type="string"
     *                           ),
     *                  @OA\Property(
     *                          property="refund_option",
     *                            type="string"
     *                           ),
     *                   @OA\Property(
     *                          property="account_holder_name",
     *                            type="string"
     *                           ),
     *                      @OA\Property(
     *                          property="account_number",
     *                            type="string"
     *                           ),
     *                       @OA\Property(
     *                          property="ifsc_code",
     *                            type="string"
     *                           ),
     *                          )
     *                          ),
     *                        ),
     *      @OA\Response(response=200, description="successful operation"),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *  security={
     *          {"bearerAuth": {}}
     *           },
     * )
     * 
     */
    public function return_order(Request $tr)
    {
        $multi_id=$tr->book_multi_id;
        $cancel_option=$tr->option;
        $message=$tr->message;

        $refund_option=$tr->refund_option;
        $acc_name=$tr->account_holder_name;
        $acc_no=$tr->account_number;
        $acc_ifsc=$tr->ifsc_code;
        
       
    $token='Fz0ZNAb5jPXfvoJ9vSwst3wBWuOef4RMbvJ4XY2n';
   
    $user=Auth::user()->id;
    $cancel_status = new cancel_reasons;
    $cancel_status->book_muti_item_id=$multi_id;
    $cancel_status->cancel_option=$cancel_option;
    $cancel_status->message=$message;
    $cancel_status->refund_type=$refund_option;
    $cancel_status->acc_name=$acc_name;
    $cancel_status->acc_no=$acc_no;
    $cancel_status->acc_ifsc=$acc_ifsc;
    $cancel_status->remember_token=$token;
    $cancel_status->status='Return';
    $cancel_status->save();
   
    DB::table('book_multi_items')
    ->where('multi_id',$multi_id)
    ->update(['order_status' => 5]);
    $as=DB::table('book_multi_items')
    ->where('multi_id',$multi_id)
    ->first();
   
    $product_price=$as->product_price*$as->quantity1;
   
   
   
    $booking_id=$as->booking_id;
    $ass=DB::table('bookings')
    ->where('booking_id',$booking_id)
    ->first();
   
   
    $address_id=$ass->address_id;
    $order_id=$ass->order_id;
   
    $asss=DB::table('addres')
    ->where('id',$address_id)
    ->first();
   
   
    $to_name=$asss->phone_no;
    $to_name=$asss->name;
    $to_email=Auth::user()->email;
   
   
   //    try{
   //              $data = array('name'=>$to_name,'email'=>$to_email,'body'=>"Hi ".$to_name.", We have Received Your Return Request for Order Id ".$order_id.". Your request is under process. You will be notified shortly on further progress in this regard. ");
   //             Mail :: send('mail1',$data,function($message) use ($to_name,$to_email)
   //             {
   //                 $message->to($to_email)
   //                 ->subject('Return Requested');
   //             });
   //          }
   //          catch(\Exception $e) {
    
   //          }
   //             $phone=Auth::user()->phone;
              // self::smsRequest($phone,"Thank you for your Order with Drishtikart. Your order with Order ID ".$order_id." for ".$product_name.", is successfully placed.<br>Total price: ".$price1.", Payment mode: ".$payment_type.", Shipping Address: ".$address->flat.",".$address->location." <br> ".$address->city.", ".$address->state."<br> ".$address->pincode."");
 $status['msg']='Order Return Successfully';
      $status['status']=1;
      return response()->json(['status' => $status], 200);
    }
   
   /**
     * Document API
     * @return \Illuminate\Http\Response
     * 
     * @OA\Get(
     *      path="/api/option",
     *      operationId="option",
     *      tags={"Order"},
     *      summary="Option for Cancel Order and Return Order",
     *      @OA\Response(response=200, description="successful operation"),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *      
     * )
     * 
     */


   
    public function option(Request $tr)
    {
        $status=array();
        $status[0]='Order placed by mistake';
        $status[1]='Item price too high';
        $status[2]='Shipping cost too high';
        $status[3]='Others';
 

        $status1=array();
        $status1[0]='Item Damaged but shipping box Ok';
        $status1[1]='Both product and shipping box damaged';
        $status1[2]='Defective product';
        $status1[3]='Wrong product sent';
        $status1[4]='Unsatisfactory product quality';
        $status1[5]='Others';


        $status2=array();
        $status2[0]='Original Payment Method';
        $status2[1]='Wallet';
       

        return response()->json(['cancel_option' => $status,'return_option' => $status1,'refund_option' => $status2], 200);

    }
    
    
    public function shop(Request $req)
    {   
   
        $query=DB::table('products')->join('brands','brands.brand_id','=','products.brand_id')->where('products.active_status','=','YES')->where('products.sub_sub_cat_id','=',$req->sub_sub_cat_id);
        $query2=DB::table('products')->where('products.active_status','YES')->where('products.sub_sub_cat_id','=',$req->sub_sub_cat_id);
        
        $brand=$query->groupBy('products.brand_id')->select('brands.brand_id','brands.brand_name')->inRandomOrder()->get();
        $product_size=$query2->groupBy('products.weight')->select('products.weight')->orderby('products.weight','desc')->get();

        return response()->json(['brand'=>$brand,'product_size'=>$product_size], 200);
       
    }




    public function fetch_data(Request $req)
    {


    $body['brand']=$req->brand;
     $body['size']=$req->size;
      $body['brand_length']=$req->brand_length;
       $body['size_length']=$req->size_length;
        $body['status']=$req->status;
         $body['review']=$req->review;
          $body['maximum_price']=$req->maximum_price;
           $body['minimum_price']=$req->minimum_price;
            $body['sub_sub_cat_id']=$req->sub_sub_cat_id;
     

        $price=0;
        $query=DB::table('products')
        ->join('brands','products.brand_id','=','brands.brand_id')
        ->join('sub_cats','products.sub_cat_id','=','sub_cats.sub_cat_id')
        ->where('products.active_status','YES')
        ->where('products.sub_sub_cat_id','=',$req->sub_sub_cat_id);
      
       $brand=explode(",",$req->brand);
       $size=explode(",",$req->size);
       //return Response()->json(explode(",",$req->brand));
     //return Response()->json(count($brand));
      
        if($req->brand_length!=0){
             if(count($brand))
            {
                 $query=$query->whereIn('brands.brand_name', $brand);
            }
        }
       
        if(($req->minimum_price!=0 || $req->maximum_price!=0))
        {
            $query =$query->whereBetween('products.mrp',[$req->minimum_price,$req->maximum_price]);
     
       }
        if(($req->review)!=0)
        {
            $query=$query->where('products.review','>=', ($req->review));
        
        }
          if($req->size_length!=0){
              if(count($size))
                {
                     $query=$query->whereIn('products.weight', $size);
                }
          }
        
      
      
         
            if($req->status=='latest')
            {
                $query =$query->orderby('products.product_id','desc');
            }
            if($req->status=='popular')
            {
                $query =$query->orderby('products.View_count','desc');
               
            }
            if($req->status=='low')
            {
               $query =$query->orderby('products.selling_price','asc');
            }
            if($req->status=='high')
            {
                $query =$query->orderby('products.selling_price','desc');
            }
            if($req->status=='highretting')
            {
                $query =$query->orderby('products.total_review','asc');
            }
            if($req->status=='lowretting')
            {
                $query =$query->orderby('products.total_review','desc');
            }
        

        $pro=$query->inRandomOrder()->get();
           
          $product=array();
            foreach($pro as $key=>$pro){
                $product[$key]['product_id']=$pro->product_id;
                $product[$key]['product_name']=$pro->product_name;
                $product[$key]['product_brand']=$pro->brand_name;
                if($pro->weight>999)
                    $weight=round($pro->weight/1000,2).' Kg';
                else
                    $weight=$pro->weight.' Gm';
                $product[$key]['product_weight']=$weight;
                $product[$key]['total_review']=$pro->total_review;
                $product[$key]['avg_review']=$pro->review;
                $product[$key]['mrp']=$pro->mrp;
                $product[$key]['selling_price']=round(($pro->selling_price*$pro->gst/100)+$pro->selling_price);
    
                
                $banner=DB::table('product_images')->where('product_id',$pro->product_id)->limit(1)->get();
                foreach($banner as $banner){
                        $product[$key]['product_image']="/product_image/".$banner->image;
                }
                $product[$key]['discount']=round((($pro->mrp-($pro->selling_price+($pro->selling_price*$pro->gst/100)))/$pro->mrp)*100);
            }
            return response()->json(['product' => $product,'body'=>$body], 200); 
          
        }

 public function sendReview(Request $req)
 { 
     $multi_id=$req->multi_id;
    $user_id=Auth::user()->id;
    $product_id=$req->product_id;
    $reviews=$req->review;
    $review_title=$req->title;
    $review_description=$req->message;
    $multi_id=$req->multi_id;


    $rev_count=DB::table('reviews')->where('multi_id',$multi_id)->where('user_id',$user_id)->count();



        $image=null;
    



   $review1 = new review;
   $review1->user_id=$user_id;
   $review1->product_id=$product_id;
   $review1->review=$reviews;
   $review1->review_title=$review_title;
   $review1->review_details=$review_description;
   $review1->multi_id=$multi_id;
  
   $review1->save();


   $product_review=DB::table('products')->where('product_id',$product_id)->first();
   $rating=$product_review->review;

   $review_count=$product_review->total_review;

   $previous_total_review=$rating*$review_count;

   $recent_total_review=$previous_total_review+$reviews;

   $recent_person=$review_count+1;

   $recent_avg_review=round($recent_total_review/$recent_person,1);
   DB::table('products')->where('product_id',$product_id)->update(['review'=>$recent_avg_review,'total_review'=>$recent_person]);
   DB::table('book_multi_items')->where('book_multi_items.multi_id',$multi_id)->update(['review_status'=>'YES']);

  return response()->json(['product' => 'Review submit successfully'], 200); 
  

}


}

