<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\cart;
use App\none_cart;
use App\subscriber;
use App\wishlist;
use App\display_banner;
use App\coupon_banner;
use App\product;
use carbon;
use App\product_image;
use App\product_features;
use Illuminate\Support\Facades\Mail;
use App\user_wallet;
use Artisan;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    // public function __construct()
    // {
    //     $this->middleware('verified');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function smsRequest($phone,$msg)
    {
        $client = new \GuzzleHttp\Client();
              $url = "http://weberleads.in/http-tokenkeyapi.php?authentic-key=".env('WEBERLEADS_SMS_API')."&senderid=".env('WEBERLEADS_SMS_SENDERID')."&route=2&number=91".$phone."&message=".$msg;
        $request = $client->get($url);
        $response = $request->getBody();
    }
    public function index()
    {

        $banner=display_banner::all(); 
        $banner22=DB::table('coupon_banners')->where('active_status','YES')->get();
        foreach($banner22 as $banners12)
        {
            $coupon_id=$banners12->id;
            $valedity=$banners12->coupon_validity;
            $redeemdate=$valedity;
            $mytime = Carbon\Carbon::now();
            $date1=date_create($redeemdate);
            $date2=date_create($mytime);
            $diff=date_diff($date2,$date1);
            $deffrant=$diff->format("%R%a");
            if($deffrant<=0)
            {
                DB::table('coupon_banners')
                ->where('id', $coupon_id)
                ->update(['active_status' => 'NO']);
            }
        }

        $coupon_banner=coupon_banner::where('active_status','YES')->get();   
        $new_arival=DB::table('products')->where('active_status','YES')->limit(5)->orderBy('product_id','DESC')->get();  
        $most_view_product=DB::table('products')->where('active_status','YES')->limit(5)->orderBy('view_count','DESC')->get();  

        return view('index',['banner'=>$banner,'coupon_banner'=>$coupon_banner,'new_arival'=>$new_arival,'most_view_product'=>$most_view_product]);
    }
    public function shop(Request $req)
    {   
   
        $query=DB::table('products')->join('brands','brands.brand_id','=','products.brand_id')->where('products.active_status','=','YES');
        $query2=DB::table('products')->where('products.active_status','YES');
        $query3=DB::table('products')->where('products.active_status','YES');


        if(isset($_GET['cat_id'])){
            $query=$query->where('products.cat_id','=',$req->cat_id);
            $query2=$query2->where('products.cat_id','=',$req->cat_id);
            $query3=$query3->where('products.cat_id','=',$req->cat_id);
        }
        if(isset($_GET['sub_cat_id'])){
            $query=$query->where('products.sub_cat_id','=',$req->sub_cat_id);
            $query2=$query2->where('products.sub_cat_id','=',$req->sub_cat_id);
            $query3=$query3->where('products.sub_cat_id','=',$req->sub_cat_id);

        }
        if(isset($_GET['sub_sub_cat_id'])){
            $query=$query->where('products.sub_sub_cat_id','=',$req->sub_sub_cat_id);
            $query2=$query2->where('products.sub_sub_cat_id','=',$req->sub_sub_cat_id);
            $query3=$query3->where('products.sub_sub_cat_id','=',$req->sub_sub_cat_id);

        }

        $brand=$query->groupBy('products.brand_id')->inRandomOrder()->paginate(5);
        $count_brand_product=$query->count();

        $product_size=$query2->where('products.size','!=','NO')->groupBy('products.size')->get();
        $product_size_count=count($product_size);
        
        $product_color=$query3->where('products.color','!=','NO')->groupBy('products.color')->get();
        $product_color_count=count($product_color);

       
        $brand1=$query->groupBy('products.brand_id')->inRandomOrder()->paginate(5);
        $count_brand_product1=$query->groupBy('products.brand_id')->count();
        $product_size1=$query2->inRandomOrder()->paginate(5);
        $count_brand_product1=$query->count();
        return view('shop')
        ->with('brand',$brand)
        ->with('product_size',$product_size)
        ->with('count_brand_product',$count_brand_product)
        ->with('product_size_count',$product_size_count)
        ->with('product_color_count',$product_color_count)
        ->with('brand1',$brand)
        ->with('product_size1',$product_size)
        ->with('count_brand_product1',$count_brand_product)
        ->with('product_color', $product_color)
        ->with('product_size_count1',$product_size_count);
   
    }




    public function fetch_data(Request $req)
    {


        
        $user_type=$req->user_type;
        $price=0;
        $query=DB::table('products')->where('active_status','YES');
        if($req->cat_id!=0){
            $query=$query->where('products.cat_id','=',$req->cat_id);
        }
        if($req->sub_cat_id!=0){
            $query=$query->where('products.sub_cat_id','=',$req->sub_cat_id);
        }
        if($req->sub_sub_cat_id!=0){
            $query=$query->where('products.sub_sub_cat_id','=',$req->sub_sub_cat_id);
        }
       
        if(!empty($req->Product_name)){
           
            $query=$query->where('products.product_name', 'LIKE', '%' . $req->Product_name . '%');
        }
        if(isset($req->brand))
        {
      
                $query=$query->join('brands','brands.brand_id','=','products.brand_id')->whereIn('products.brand_id', $req->brand);
              
         
        
        }
        if(($req->minimum_price!=0 || $req->maximum_price!=0))
        {
            $query =$query->where('products.selling_price','>=',$req->minimum_price)
            ->where('products.selling_price','<=',$req->maximum_price);
           
         
           
          
        }
        if(isset($req->review))
        {
           

            $query=$query->where('products.review','>=', min($req->review));
        
        }
        if(isset($req->size))
        {
           
             $query=$query->whereIn('products.size', $req->size);
             
        }
        if(isset($req->color))
        {
           
             $query=$query->whereIn('products.color', $req->color);
             
        }
      
      
         
            if($req->status=='latest')
            {
                $query =$query->orderby('products.product_id','desc');
                 
               
            }
            if($req->status=='popular')
            {
                $query =$query->orderby('products.View_count','desc');
               
            }
            if($req->status=='low')
            {
                

             
                $query =$query->orderby('products.selling_price','asc');
               
               
            }
            if($req->status=='high')
            {
                $query =$query->orderby('products.selling_price','desc');
               
            }
            if($req->status=='highretting')
            {
                $query =$query->orderby('products.total_review','asc');
                
               
            }
            if($req->status=='lowretting')
            {
                $query =$query->orderby('products.total_review','desc');
               
            }
        

        $pro=$query->inRandomOrder()->paginate(50);
           
       $pro_count=$pro->count();
    //    $pro_count=2;

        return view('product_ajax')->with('pro',$pro)->with('pro_count',$pro_count)->with('price',$price);
        //return view('shop_view')->with('product',$pro)->with('count',$pro1);
    }

    public function product_details(Request $req)
    {
       $product_id=$req->id;
       $product_details=DB::table('products')
       ->join('brands','products.brand_id','=','brands.brand_id')
       ->join('users','products.vendor_id','=','users.id')
       ->where('product_id',$product_id)->first();

       $view_count=$product_details->view_count;
       $view_count=$product_details->view_count;

       $view_count1=$view_count+1;
       DB::table('products')->where('product_id',$product_id)->update(['view_count'=>$view_count1]);


       $product_image=DB::table('product_images')->where('product_id',$product_id)->get();
       $product_feature=DB::table('product_features')->where('product_id',$product_id)->get();
       $product_specification=DB::table('product_spacifications')->where('product_id',$product_id)->get();
      // $product_price=DB::table('product_prices')->where('product_id',$product_id)->get();
       $category_id= $product_details->cat_id;
       $related_product_details=DB::table('products')->where('cat_id',$category_id)->where('active_status','YES')->get();
       $group_product=DB::table('products')->where('group_id',$product_details->group_id)->where('group_id','!=',0)->get();
       $size=DB::table('products')->where('size','!=','NO')->where('color','=',$product_details->color)->where('group_id',$product_details->group_id)->groupBy('size')->get();
       $color=DB::table('products')->where('color','!=','NO')->where('size',$product_details->size)->where('group_id',$product_details->group_id)->groupBy('color')->get();
    
           $cat=DB::table('cats')->where('cat_id',$product_details->cat_id)->first();
           $sub_cat=DB::table('sub_cats')->where('sub_cat_id',$product_details->sub_cat_id)->first();
           $sub_sub_cat=DB::table('sub_sub_cats')->where('sub_sub_cat_id',$product_details->sub_sub_cat_id)->first();
  
       if(auth::check())
       {
       $user_id=auth::user()->id;
       $review_submit=DB::table('bookings')->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')->where('bookings.customer_id',$user_id)->where('book_multi_items.product_id',$product_id)->where('book_multi_items.order_status',4)->orWhere('book_multi_items.order_status', '=', 5)->orWhere('book_multi_items.order_status', '=', 6)->orWhere('book_multi_items.order_status', '=', 7)->count();
       $review_update=DB::table('reviews')->where('user_id',$user_id)->where('product_id',$product_id)->first();
       $review_update1=DB::table('reviews')->where('user_id',$user_id)->where('product_id',$product_id)->count();
     
       }
       else
       {
        $review_submit=0;
        $review_update=0;
        $review_update1=0;
       }
       
       $review_star1=DB::table('reviews')->where('product_id',$product_id)->where('review',1)->count();
       $review_star2=DB::table('reviews')->where('product_id',$product_id)->where('review',2)->count();
       $review_star3=DB::table('reviews')->where('product_id',$product_id)->where('review',3)->count();
       $review_star4=DB::table('reviews')->where('product_id',$product_id)->where('review',4)->count();
       $review_star5=DB::table('reviews')->where('product_id',$product_id)->where('review',5)->count();
       $revie=DB::table('reviews')->where('product_id',$product_id)->limit(10)->orderBy('review', 'DESC')->get();
       $review_count=DB::table('reviews')->where('product_id',$product_id)->count();
       if($review_star1!=0){
        $reviewPercentage1=($review_star1/$review_count)*100;
     }else{
         $reviewPercentage1=0;
     }
     if($review_star2!=0){
         $reviewPercentage2=($review_star2/$review_count)*100;
     }else{
         $reviewPercentage2=0;
     }
     if($review_star3!=0){
         $reviewPercentage3=($review_star3/$review_count)*100;
     }else{
         $reviewPercentage3=0;
     }
     if($review_star4!=0){
         $reviewPercentage4=($review_star4/$review_count)*100;
     }else{
         $reviewPercentage4=0;
     }
     if($review_star5!=0){
         $reviewPercentage5=($review_star5/$review_count)*100;
     }else{
         $reviewPercentage5=0;
     }

    //  dd($product_details);

       return view('product_details',[
        'product_details'=>$product_details,
        'group_product'=>$group_product,
        'cat'=>$cat,
        'sub_cat'=>$sub_cat,
        'sub_sub_cat'=>$sub_sub_cat,
        'product_image'=>$product_image,
        'product_feature'=>$product_feature,
        'product_specification'=>$product_specification,
        'related_product_details'=>$related_product_details,
        'review_submit'=>$review_submit,
        'review_update'=>$review_update,
        'review_update1'=>$review_update1,
        'review_star1'=>$review_star1,
        'review_star2'=>$review_star2,
        'review_star3'=>$review_star3,
        'review_star4'=>$review_star4,
        'review_star5'=>$review_star5,
        'revie'=>$revie,
        'size'=>$size,
        'color'=>$color,
        'reviewPercentage5'=>$reviewPercentage5,
        'reviewPercentage4'=>$reviewPercentage4,
        'reviewPercentage3'=>$reviewPercentage3,
        'reviewPercentage2'=>$reviewPercentage2,
        'reviewPercentage1'=>$reviewPercentage1,
    ]);

    }

    function price(Request $req)
    {
        $token=$req->_token;
        $price_id=$req->price_id;
        $product_price1=DB::table('product_prices')->where('product_prices_id',$price_id)->first();
        return view('price',['product_price'=>$product_price1]);

    }
    public function product_details1(Request $req)
    {

        $product_details22=DB::table('products')
        ->where('group_id',$req->group)
        ->where('size',$req->size)
        ->where('color',$req->color)
        ->first();
       
        $product_details2211=DB::table('products')
        ->where('group_id',$req->group)
        ->where('size',$req->size)
        ->where('color',$req->color)
        ->count();
        if($product_details2211!=0){
              $product_id=$product_details22->product_id;
     
       $product_details=DB::table('products')
       ->join('brands','products.brand_id','=','brands.brand_id')
       ->join('users','products.vendor_id','=','users.id')
       ->where('product_id',$product_id)->first();

       $view_count=$product_details->view_count;
       $view_count1=$view_count+1;
       DB::table('products')->where('product_id',$product_id)->update(['view_count'=>$view_count1]);


       $product_image=DB::table('product_images')->where('product_id',$product_id)->get();
       $product_feature=DB::table('product_features')->where('product_id',$product_id)->get();
       $product_specification=DB::table('product_spacifications')->where('product_id',$product_id)->get();
      // $product_price=DB::table('product_prices')->where('product_id',$product_id)->get();
       $category_id= $product_details->cat_id;
       $related_product_details=DB::table('products')->where('cat_id',$category_id)->where('active_status','YES')->get();
       $group_product=DB::table('products')->where('group_id',$product_details->group_id)->where('group_id','!=',0)->get();
       $size=DB::table('products')->where('size','!=','NO')->where('color','=',$product_details->color)->where('group_id',$product_details->group_id)->groupBy('size')->get();
       $color=DB::table('products')->where('color','!=','NO')->where('size',$product_details->size)->where('group_id',$product_details->group_id)->groupBy('color')->get();
    
    
           $cat=DB::table('cats')->where('cat_id',$product_details->cat_id)->first();
           $sub_cat=DB::table('sub_cats')->where('sub_cat_id',$product_details->sub_cat_id)->first();
           $sub_sub_cat=DB::table('sub_sub_cats')->where('sub_sub_cat_id',$product_details->sub_sub_cat_id)->first();
  
       if(auth::check())
       {
       $user_id=auth::user()->id;
       $review_submit=DB::table('bookings')->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')->where('bookings.customer_id',$user_id)->where('book_multi_items.product_id',$product_id)->where('book_multi_items.order_status',4)->orWhere('book_multi_items.order_status', '=', 5)->orWhere('book_multi_items.order_status', '=', 6)->orWhere('book_multi_items.order_status', '=', 7)->count();
       $review_update=DB::table('reviews')->where('user_id',$user_id)->where('product_id',$product_id)->first();
       $review_update1=DB::table('reviews')->where('user_id',$user_id)->where('product_id',$product_id)->count();
     
       }
       else
       {
        $review_submit=0;
        $review_update=0;
        $review_update1=0;
       }
       
       $review_star1=DB::table('reviews')->where('product_id',$product_id)->where('review',1)->count();
       $review_star2=DB::table('reviews')->where('product_id',$product_id)->where('review',2)->count();
       $review_star3=DB::table('reviews')->where('product_id',$product_id)->where('review',3)->count();
       $review_star4=DB::table('reviews')->where('product_id',$product_id)->where('review',4)->count();
       $review_star5=DB::table('reviews')->where('product_id',$product_id)->where('review',5)->count();
       $revie=DB::table('reviews')->where('product_id',$product_id)->limit(10)->orderBy('review', 'DESC')->get();

       $review_count=DB::table('reviews')->where('product_id',$product_id)->count();
        if($review_star1!=0){
           $reviewPercentage1=($review_star1/$review_count)*100;
        }else{
            $reviewPercentage1=0;
        }
        if($review_star2!=0){
            $reviewPercentage2=($review_star2/$review_count)*100;
        }else{
            $reviewPercentage2=0;
        }
        if($review_star3!=0){
            $reviewPercentage3=($review_star3/$review_count)*100;
        }else{
            $reviewPercentage3=0;
        }
        if($review_star4!=0){
            $reviewPercentage4=($review_star4/$review_count)*100;
        }else{
            $reviewPercentage4=0;
        }
        if($review_star5!=0){
            $reviewPercentage5=($review_star5/$review_count)*100;
        }else{
            $reviewPercentage5=0;
        }







     return view('product_details',[
            'product_details'=>$product_details,
            'group_product'=>$group_product,
            'cat'=>$cat,
            'sub_cat'=>$sub_cat,
            'sub_sub_cat'=>$sub_sub_cat,
            'product_image'=>$product_image,
            'product_feature'=>$product_feature,
            'product_specification'=>$product_specification,
            'related_product_details'=>$related_product_details,
            'review_submit'=>$review_submit,
            'review_update'=>$review_update,
            'review_update1'=>$review_update1,
            'review_star1'=>$review_star1,
            'review_star2'=>$review_star2,
            'review_star3'=>$review_star3,
            'review_star4'=>$review_star4,
            'review_star5'=>$review_star5,
            'revie'=>$revie,
            'size'=>$size,
            'color'=>$color,
            'reviewPercentage5'=>$reviewPercentage5,
            'reviewPercentage4'=>$reviewPercentage4,
            'reviewPercentage3'=>$reviewPercentage3,
            'reviewPercentage2'=>$reviewPercentage2,
            'reviewPercentage1'=>$reviewPercentage1,
        ]);

        }else{
            return 'sdfesa';
        }
     
    }

   
    public function check_out()
    {
        return view('check_out');
    }
    public function cart()
    {
        return view('cart');
    }
    public function wishlist()
    {
        return view('wishlist');
    }

    public function sendOtp1(Request $request){

         //$otp = rand(1000,9999);
         $otp=1234;
       
         echo  $otp;
    }

       public function sendOtp11(Request $request){

        //$otp = rand(1000,9999);
        $otp=1234;
      
        echo  $otp;
   }


   public function cart_ajax()
    {
        
    if (Auth::check()) {
    $user_id=Auth::user()->id;
    $cart_count=DB::table('carts')->where('user_id',$user_id)->count();
    $cart_item=DB::table('carts')->where('user_id',$user_id)->join('products','carts.product_id','=','products.product_id')->orderBy('carts.id', 'desc')->limit(3)->get();

    return view('layouts.cart_ajax')->with('cart_count',$cart_count)->with('cart_items',$cart_item)->with('cart_item1',$cart_item);
        }


        else{
            $user_id=0;
            return view('layouts.cart_ajax');

        }  
    }
    public function wishlist_ajax()
    {
        if (Auth::check()) {
            $user_id=Auth::user()->id;
            $wish_count=DB::table('wishlists')->where('user_id',$user_id)->count();
           // $cart_item=DB::table('carts')->where('user_id',$user_id)->join('products','carts.product_id','=','products.product_id')->join('users','products.vendor_id','=','users.id')->get();
        
            return view('layouts.wishlist_ajax')->with('wish_count',$wish_count);
                }
                else{
                    $user_id=0;
                    return view('layouts.wishlist_ajax');
        
                }
                
                
        
         
    }

    public function addtocart_ajax(Request $req)
    {
        
            $user=Auth::user()->id;
            $product=$req->product_id;
           // $product_price_id=$req->product_price_id;
            $pro=DB::table('carts')->where('product_id',$product)->where('user_id',$user)->count();

            if($pro<1){
                    $item = new cart;
                    $item->user_id=Auth::user()->id;
                    $item->product_id=$req->product_id;
                    $item->product_price_id=0;
                    $item->quantity=1;
                    $item->remember_token=$req->input('_token');
                    $item->save();
                    //return view('layouts.wishlist_ajax');
                }else{
                    echo 2;
                }
                }


    public function add_to_cart_ajax1(Request $req)
    {
                $user=Auth::user()->id;
                $product=$req->product_id;
                $quantity=$req->quantity;
                $pro=DB::table('carts')->where('product_id',$product)->where('user_id',$user)->count();
                if($pro<1){
                    $item = new cart;
                    $item->user_id=Auth::user()->id;
                    $item->product_id=$req->product_id;
                    $item->product_price_id=0;
                    $item->quantity=$quantity;
                    $item->remember_token=$req->input('_token');
                    $item->save();
                    //return view('layouts.wishlist_ajax');
                }else{
                    echo 2;
                }
    }


    public function removetocart_ajax(Request $req)
    {
        
        $user=Auth::user()->id;
        $product=$req->product_id;

        $pro=DB::table('carts')->where('product_id',$product)->where('user_id',$user)->delete();


    }
    public function addtowishlist_ajax(Request $req)
    {
        
        $user=Auth::user()->id;
        $product=$req->product_id;

        $pro=DB::table('wishlists')->where('product_id',$product)->where('user_id',$user)->count();

  if($pro<1){
        $item = new wishlist;
        $item->user_id=Auth::user()->id;
        $item->product_id=$req->product_id;
        $item->product_price_id=0;
        $item->quantity=1;
        $item->remember_token=$req->input('_token');
        $item->save();
    }else{
        echo 2;
    }
    }
     
    public function addtowishlist_ajax1(Request $req)
    {
        
        $user=Auth::user()->id;
        $product=$req->product_id;
    
        $qty=$req->qty;
        $pro=DB::table('wishlists')->where('product_id',$product)->where('user_id',$user)->count();
  if($pro<1){
        $item = new wishlist;
        $item->user_id=Auth::user()->id;
        $item->product_id=$req->product_id;
        $item->product_price_id=0;
        $item->quantity=$qty;
        $item->remember_token=$req->input('_token');
        $item->save();
    }else{
        echo 2;
    }
    }





    public function removetowishlist_ajax(Request $req)
    {
        
        $user=Auth::user()->id;
        $product=$req->product_id;

        $pro=DB::table('wishlists')->where('product_id',$product)->where('user_id',$user)->delete();


    }


    public function toggle( Request $qp)
    {
            $product_id=$qp->product_id;
         
            $token=$qp->input('_token');
            if (Auth::check()) {
            $user_id=Auth::user()->id;
            $store=DB::table('carts')->where('product_id',$product_id)->where('user_id',$user_id)->count();
            }
            else{
                $store=DB::table('carts')->where('product_id',$product_id)->count();
       

            }
                if($store>0)
                {
                $abc=1;
               
                }else{
                    $abc=2;
                 
                }
                return $abc;
    }
    public function toggle_wish( Request $qp)
  { 
    $product_id=$qp->product_id;
    
    $token=$qp->input('_token');
    if (Auth::check()) {
    $user_id=Auth::user()->id;
    $store=DB::table('wishlists')->where('product_id',$product_id)->where('user_id',$user_id)->count();
    }
    else
    {
        $store=DB::table('wishlists')->where('product_id',$product_id)->count();
    
    }
    if($store>0)
    {
     $abc=1;
     
    }else{
        $abc=2;
     
    }
    return $abc;
  }

    
public function subscribe(Request $req)
{   if (Auth::check()) { 
    $id=Auth::user()->id;
    $email=$req->input('email');
    $customer=DB::table('users')->where('email',$email)->first();
    $customer_count=DB::table('users')->where('id',$id)->count();
    if($customer_count=='0')
    {
        echo 1;  
    }
    else
    {
        echo 2;
      
       $subs=new subscriber;
        $subs->user_id=$id;
      $subs->remember_token=$req->input('_token');
        $subs->save();
      
    }
}else{
    echo 3;  
}
} 

public function unsubscribe(Request $req)
{    
    $id=Auth::user()->id;
    $email=$req->input('email');
    $customer=DB::table('users')->where('email',$email)->first();
    $customer_count=DB::table('users')->where('id',$id)->count();
    if($customer_count=='0')
    {
        echo 1;  
    }
    else
    {  

        
        Db::table('subscribers')->where('user_id',$id)->delete();
       echo 2; 
    }
    
} 



public function add_to_noe_cart(Request $req)
{
            $user=Auth::user()->id;
            $product=$req->product_id;
            
            // $product_price_id=$req->product_price_id;
            $quantity=$req->quantity;
            $pro=DB::table('none_carts')->where('product_id',$product)->where('user_id',$user)->count();

        if($pro<1){
            $item = new none_cart;
            $item->user_id=Auth::user()->id;
            $item->product_id=$req->product_id;
           
            $item->quantity=$quantity;
            $item->remember_token=$req->input('_token');
            $item->save();
        }else{
            DB::table('none_carts')
            ->where('user_id', $user)->where('product_id',$req->product_id)
            ->update(['quantity' => $quantity]);
        }
}
public function pinckeck(Request $req)
{

   $pin=$req->pin;
$store=DB::table('pincodes')->where('active_status','YES')->where('pincode',$pin)->count();
if($store==0){
    return response()->json(['success' => false, 'msg' => ''], 200);

}else{
    $store1=DB::table('pincodes')->where('active_status','YES')->where('pincode',$pin)->first();
    if($store1->delivery_charge>0){
        $msg="Free Delivery above &#8377;".$store1->min_price." order.";
    }

else{
    $msg="Free Delivery for all Order.";
}
return response()->json(['success' => true, 'msg' =>$msg], 200);
}
    
}


public function wallet(Request $req)
{
    if(Auth::check()){

    
   $token=$req->_token;
   $user=Auth::user()->id;
   $store=DB::table('user_wallets')->where('user_id',$user)->count();
   if($store==0)
   {
    $user_wallet=new user_wallet;
    $user_wallet->user_id=Auth::user()->id;
    $user_wallet->wallet_ammount=0;
    $user_wallet->remember_token=$token;
    $user_wallet->save();

   }
 
}
}


public function fafasend(Request $req)
{ 
    $review=$req->review1;
 
    $token=$req->_token;
   
   return view('fafa')->with('review',$review);
}
public function contact(Request $req)
{ 
     return view('contact');
}

public function about(Request $req)
{ 
     return view('about');
}

public function contactmail(Request $d)
{
    $name=$d->name;
    $user_email=$d->email;
    $email='support@ssjewelers.com';
    $subject=$d->subject;
    $message=$d->message;
    try{
        $data = array('name'=>$name,'email'=>$email,'body'=>"Hii admin, A user send a Customar Support request. User Name : $name, Email : $user_email, Subject : $subject, Message : $message");


        Mail :: send('mail',$data,function($message) use ($name,$email)
        {
        $message->to($email)
        ->subject('Customar Support Request');
        
        });
        echo '1';
    } catch (\Exception $e) {
    echo 0;
}


    
}

public function faq(Request $req)
{ 
     return view('faq');
}
public function privacy(Request $req)
{ 
     return view('privacy');
}
public function trams(Request $req)
{ 
     return view('trams');
}
public function return(Request $req)
{ 
     return view('return');
}

public function offer(Request $req)
{ 
    
    
    $pq=DB::table('coupon_banners')->where('active_status','YES')->get();

    return view('offer')->with('coupon',$pq)->with('coupon11',$pq);
  
}


public function abc(Request $req)
{ 
    
    Artisan::call('config:clear');
    Artisan::call('route:clear');
    Artisan::call('route:cache');
    Artisan::call('config:cache');
    return "route_cache";
 

    // return view('delivery_boy.abc');
  
}
public function htmlPDF58()
{

    $id=$_GET['id'];
    $book_multi=DB::table('book_multi_items')->join('bookings','book_multi_items.booking_id','=','bookings.booking_id')->join('users','bookings.customer_id','=','users.id')->where('multi_id',$id)->first();
    return view('admin.invoice')->with('book',$book_multi);
    
}
}
