<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAccNameToCancelReasonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cancel_reasons', function (Blueprint $table) {
            $table->string('acc_name')->default('NO');
            $table->string('acc_no')->default('NO');
            $table->string('acc_ifsc')->default('NO');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cancel_reasons', function (Blueprint $table) {
            $table->dropColumn('acc_name');
            $table->dropColumn('acc_no');
            $table->dropColumn('acc_ifsc');
        });
    }
}
