<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCatIdToCouponBannersTrasanctionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('coupon_banners', function (Blueprint $table) {
            $table->integer('cat_id')->default(0);
            $table->string('coupon_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('coupon_banners', function (Blueprint $table) {
            $table->dropColumn('cat_id');
            $table->dropColumn('coupon_type');
        });
    }
}
