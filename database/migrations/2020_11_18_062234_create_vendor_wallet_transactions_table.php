<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorWalletTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_wallet_transactions', function (Blueprint $table) {
            $table->bigIncrements('vendor_wallet_transaction_id');
            $table->bigInteger('vendor_id')->nullable();
            $table->bigInteger('transaction_amount')->nullable();
            $table->bigInteger('after_transaction_amount')->nullable();
            $table->String('remarks')->nullable();
            $table->String('order_id')->nullable();
            $table->String('tranasaction_type')->nullable();
            $table->bigInteger('booking_id')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_wallet_transactions');
    }
}
