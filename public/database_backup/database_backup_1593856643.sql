

CREATE TABLE `addres` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `flat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `landmark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pincode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `district` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `optional_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gst_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO addres VALUES("1","3","Webel IT PARK","Beside Mosjit","Barghasipur B.O","721657","Haldia Municipality","East Midnapore","WEST BENGAL","Webel IT Park, Module-104","Malay Kr. Bera","9733119408","6294008510","","V0i2pITVHxtsC6BJK9MRBH96AHnEcudGVEJzrIWj","2020-06-26 11:00:51","2020-06-26 11:00:51"),
("2","2","Webel IT PARK","Beside Mosjit","Barghasipur B.O","721657","Haldia Municipality","East Midnapore","WEST BENGAL","Webel IT Park, Module-104","Malay Kr. Bera","9733119408","6294008510","","V0i2pITVHxtsC6BJK9MRBH96AHnEcudGVEJzrIWj","2020-06-26 11:00:51","2020-06-26 11:00:51");





CREATE TABLE `billing_adds` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `landmark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pincode` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `district` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO billing_adds VALUES("1","3","Webel IT Park, Module-104","Beside Mosjit","Barghasipur B.O","721657","Haldia Municipality","East Midnapore","WEST BENGAL","V0i2pITVHxtsC6BJK9MRBH96AHnEcudGVEJzrIWj","2020-06-26 11:00:51","2020-06-26 11:00:51"),
("2","2","Webel IT Park, Module-104","Beside Mosjit","Barghasipur B.O","721657","Haldia Municipality","East Midnapore","WEST BENGAL","V0i2pITVHxtsC6BJK9MRBH96AHnEcudGVEJzrIWj","2020-06-26 11:00:51","2020-06-26 11:00:51");





CREATE TABLE `book_multi_items` (
  `multi_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `booking_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_prices_id` int(11) NOT NULL,
  `quantity1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'NO',
  `order_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `delivery_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redeem_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'NO',
  `payment_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `return_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `size` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`multi_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO book_multi_items VALUES("6","7","11","12","1","NO","6","","","NO","","","122000","mbQWDXtuEIJuwOf1VcSrLVSCDpZCiXomMo2nvRcz","2020-07-03 08:00:55","2020-07-03 08:00:55","19.460gm"),
("7","7","5","7","1","NO","4","","","NO","","","69000","mbQWDXtuEIJuwOf1VcSrLVSCDpZCiXomMo2nvRcz","2020-07-03 08:00:55","2020-07-03 08:00:55","16.80mm"),
("8","7","6","8","1","NO","8","","","NO","","","70000","mbQWDXtuEIJuwOf1VcSrLVSCDpZCiXomMo2nvRcz","2020-07-03 08:00:55","2020-07-03 08:00:55","12.370gm"),
("9","7","8","10","1","NO","2","","","NO","","","79500","mbQWDXtuEIJuwOf1VcSrLVSCDpZCiXomMo2nvRcz","2020-07-03 08:00:55","2020-07-03 08:00:55","14.506gm");





CREATE TABLE `bookings` (
  `booking_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL DEFAULT 0,
  `coupon_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `total_price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `cart_total` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `delivery_charge` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `wallet_amount` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `coupon_discount` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `order_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `razorpay_payment_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'NO',
  `notify_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'unseen',
  `wallet_payment_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'NO',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`booking_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO bookings VALUES("7","2","","340500","340500","0","0","0","0","5EFEE5B7B3C7C","online","pay_F9wv1JsDySGSiq","NO","unseen","NO","mbQWDXtuEIJuwOf1VcSrLVSCDpZCiXomMo2nvRcz","2020-07-03 08:00:55","2020-07-03 08:00:55","2");





CREATE TABLE `brands` (
  `brand_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`brand_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO brands VALUES("2","SSJ Jewellery","5ef983366de3a.png","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:59:18","2020-06-29 05:59:18"),
("3","SSJ Jewellery","5ef98376e3b1b.png","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:00:23","2020-06-29 06:00:23"),
("4","SSJ Jewellery","5ef9837720655.png","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:00:23","2020-06-29 06:00:23"),
("5","SSJ Jewellery","5ef98377623bf.png","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:00:23","2020-06-29 06:00:23"),
("6","SSJ Jewellery","5ef983778e99a.png","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:00:23","2020-06-29 06:00:23"),
("7","SSJ Jewellery","5ef98377b9558.png","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:00:23","2020-06-29 06:00:23"),
("8","SSJ Jewellery","5ef98377e82e5.png","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:00:24","2020-06-29 06:00:24"),
("9","SSJ Jewellery","5ef9837822bc6.png","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:00:24","2020-06-29 06:00:24"),
("10","SSJ Jewellery","5ef98378596bc.png","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:00:24","2020-06-29 06:00:24"),
("11","SSJ Jewellery","5ef9837890536.png","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:00:24","2020-06-29 06:00:24");





CREATE TABLE `cancel_reasons` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `book_muti_item_id` int(11) DEFAULT NULL,
  `cancel_option` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `refund_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO cancel_reasons VALUES("1","8","Item price too high","none","Cancel","","mbQWDXtuEIJuwOf1VcSrLVSCDpZCiXomMo2nvRcz","2020-07-03 08:07:03","2020-07-03 08:07:03"),
("2","6","Both product and shipping box damaged","None","Return","Original Payment Method","mbQWDXtuEIJuwOf1VcSrLVSCDpZCiXomMo2nvRcz","2020-07-03 09:11:07","2020-07-03 09:11:07");





CREATE TABLE `carts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `quantity` int(11) NOT NULL DEFAULT 1,
  `product_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `product_price_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;






CREATE TABLE `cats` (
  `cat_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `cat_background` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO cats VALUES("7","Woman Jewellery","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:33:42","2020-06-29 05:33:42","5ef97d362534e.png"),
("8","Man Jewellery","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:33:42","2020-06-29 05:33:42","5ef97d368fe51.png"),
("9","Bangles","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:33:42","2020-06-29 05:33:42","5ef97d36a22d9.png"),
("10","Rings","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:33:42","2020-06-29 05:33:42","5ef97d36bc953.png"),
("11","Jewellery","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:33:42","2020-06-29 05:33:42","5ef97d36ce816.png"),
("12","Brooch","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:33:42","2020-06-29 05:33:42","5ef97d36df269.png"),
("13","Chain","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:33:43","2020-06-29 05:33:43","5ef97d36ef5e7.png"),
("14","Necklace","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:33:43","2020-06-29 05:33:43","5ef97d370ba2a.png"),
("15","Earrings","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:33:43","2020-06-29 05:33:43","5ef97d371bd9b.png"),
("16","Cufflinks","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:37:11","2020-06-29 05:37:11","5ef97e0789861.png"),
("17","Buckle","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:37:11","2020-06-29 05:37:11","5ef97e079d8be.png"),
("18","Bracelet & Kada","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:37:48","2020-06-29 05:37:48","5ef97e2c1981e.png");





CREATE TABLE `coupon_banners` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `coupon_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coupon_value` int(11) NOT NULL DEFAULT 0,
  `coupon_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coupon_validity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `min_price` int(11) NOT NULL DEFAULT 0,
  `active_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'YES',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `cat_id` int(11) NOT NULL DEFAULT 0,
  `coupon_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO coupon_banners VALUES("3","5ef43248dec02.png","THE NEW STANDARD","20","SDERDGTHYUIYTREW","2020-06-25","500","YES","pAxi0a6Z5Nw45NBRvn2tDD8hbjBxNB6uT82pOvnt","2020-06-24 11:26:55","2020-06-24 11:26:55","10","FLAT"),
("4","5ef4308a0cefb.png","THIS MONSOON OFFER","10","MONSOON2020","2020-08-28","500","YES","63B0x7wJLIDdOg9KO13bVIptWEH6hPwMDJU3MXWC","2020-06-25 04:58:41","2020-06-25 04:58:41","9","PERCENTAGE"),
("6","5ef4338595337.png","SSJ WELCOME OFFER","299","WELCOME2020","2020-06-27","999","YES","63B0x7wJLIDdOg9KO13bVIptWEH6hPwMDJU3MXWC","2020-06-25 05:17:57","2020-06-25 05:17:57","0","FLAT");





CREATE TABLE `display_banners` (
  `dis_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `display_banner` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`dis_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO display_banners VALUES("1","1592982089.jpg","rD6z4B5QFWh4lhZ1ruoJUrb3CUuwp2Ma9dhhh3G8","2020-06-24 07:01:29","2020-06-24 07:01:29"),
("2","1592982099.jpg","rD6z4B5QFWh4lhZ1ruoJUrb3CUuwp2Ma9dhhh3G8","2020-06-24 07:01:39","2020-06-24 07:01:39");





CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;






CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO migrations VALUES("7","2014_10_12_000000_create_users_table","1"),
("8","2014_10_12_100000_create_password_resets_table","1"),
("9","2019_08_19_000000_create_failed_jobs_table","1"),
("10","2020_06_12_173230_create_display_banners_table","2"),
("15","2020_06_13_071436_create_coupon_banners_table","3"),
("16","2020_06_13_072538_create_pincodes_table","3"),
("27","2020_06_13_153405_create_cats_table","4"),
("28","2020_06_13_173716_create_sub_cats_table","4"),
("29","2020_06_16_055012_create_sub_sub_cats_table","4"),
("30","2020_06_16_075705_create_brands_table","4"),
("31","2020_06_16_101813_create_products_table","4"),
("32","2020_06_16_104329_create_stock_trasanctions_table","4"),
("33","2020_06_16_104940_create_product_prices_table","4"),
("34","2020_06_16_105348_create_product_images_table","4"),
("35","2020_06_16_105513_create_product_features_table","4"),
("36","2020_06_16_105559_create_product_spacifications_table","4"),
("37","2020_06_16_161912_add_product_price_id_to_stock_trasanctions_table","4"),
("40","2020_06_23_104235_add_transfer_id_to_stock_trasanctions_table","5"),
("41","2020_06_24_093008_add_cat_background_to_cats_trasanctions_table","6"),
("42","2020_06_24_111121_add_cat_id_to_coupon_banners_trasanctions_table","7"),
("43","2020_06_25_052914_create_promotion_banners_table","8"),
("44","2020_06_25_101954_create_carts_table","9"),
("54","2020_06_25_102246_create_wishlists_table","10"),
("55","2020_06_26_092740_create_billing_adds_table","10"),
("56","2020_06_26_105130_create_addres_table","10"),
("57","2020_06_26_121359_create_subscribers_table","11"),
("58","2020_06_28_063211_create_none_carts_table","12"),
("59","2020_06_29_050522_create_bookings_table","12"),
("60","2020_06_29_050827_create_book_multi_items_table","12"),
("61","2020_06_29_064846_create_user_wallets_table","12"),
("62","2020_06_29_064916_create_user_wallet_transactions_table","12"),
("63","2020_06_29_074759_add_address_id_to_bookings_table","12"),
("68","2020_06_29_125447_add_cod_to_pincodes_table","13"),
("69","2020_07_01_111805_create_refer_codes_table","13"),
("70","2020_07_01_075057_add_size_to_book_multi_items_table","14"),
("71","2020_07_01_111554_create_cancel_reasons_table","14");





CREATE TABLE `none_carts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `quantity` int(11) NOT NULL DEFAULT 1,
  `product_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `product_price_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO none_carts VALUES("1","2","1","6","8","5T4ZGIpHyngicCzJJcqfPy4Kz5bfJaeeNCubDR7T","2020-07-01 13:43:03","2020-07-01 13:43:03");





CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;






CREATE TABLE `pincodes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `pincode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `min_price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `delivery_charge` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `active_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'YES',
  `banner_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'YES',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `cod` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO pincodes VALUES("1","721657","500","50","YES","YES","SI4vtYedzcCdbrTOGjHUSptz7uJcMo5hBKoyMldz","2020-06-13 09:21:06","2020-06-13 09:21:06","");





CREATE TABLE `product_features` (
  `product_feature_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `features` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`product_feature_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO product_features VALUES("5","3","Jewellery with Gemstones","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:11:49","2020-06-29 06:11:49"),
("6","3","Earrings","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:11:49","2020-06-29 06:11:49"),
("7","3","Daily Wear","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:11:49","2020-06-29 06:11:49"),
("8","3","Gold Metal","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:11:49","2020-06-29 06:11:49"),
("9","3","Bombay Screw","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:11:49","2020-06-29 06:11:49"),
("10","4","Plain Jewellery with Stones","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:21:31","2020-06-29 06:21:31"),
("11","4","Mangalam COLLECTION","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:21:31","2020-06-29 06:21:31"),
("12","4","Engagement Rings","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:21:31","2020-06-29 06:21:31"),
("13","5","Plain Gold Jewellery","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:29:50","2020-06-29 06:29:50"),
("14","5","Finger Ring","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:29:50","2020-06-29 06:29:50"),
("15","5","Mangalam","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:29:50","2020-06-29 06:29:50"),
("16","6","Plain Jewellery with Stones","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:41:21","2020-06-29 06:41:21"),
("17","6","Bestsellers COLLECTION","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:41:21","2020-06-29 06:41:21"),
("18","6","Party Wear","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:41:21","2020-06-29 06:41:21"),
("19","6","22.00 PURITY","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:41:21","2020-06-29 06:41:21"),
("20","6","Women Jewellery","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:41:22","2020-06-29 06:41:22"),
("21","7","Traditional Wear","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:53:13","2020-06-29 06:53:13"),
("22","7","Swayahm COLLECTION","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:53:13","2020-06-29 06:53:13"),
("23","7","Plain Gold Jewellery","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:53:13","2020-06-29 06:53:13"),
("24","7","60mm HEIGHT","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:53:13","2020-06-29 06:53:13"),
("25","7","60mm WIDTH","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:53:13","2020-06-29 06:53:13"),
("26","8","Ruby|103|Prong|Marquise,Round","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:59:49","2020-06-29 06:59:49"),
("27","8","Jewellery with Gemstones","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:59:49","2020-06-29 06:59:49"),
("28","8","Pendant and Earrings Set","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:59:49","2020-06-29 06:59:49"),
("29","8","Bestsellers Products","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:59:49","2020-06-29 06:59:49"),
("30","8","Traditional Wear","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:59:49","2020-06-29 06:59:49"),
("31","10","Diamond Jewellery","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 07:06:54","2020-06-29 07:06:54"),
("32","10","Pendant and Earrings Set","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 07:06:54","2020-06-29 07:06:54"),
("33","10","Ruby|19|Prong|Marquise","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 07:06:54","2020-06-29 07:06:54"),
("34","10","Bestsellers Products","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 07:06:54","2020-06-29 07:06:54"),
("35","10","Party Wear","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 07:06:54","2020-06-29 07:06:54"),
("36","11","Plain Jewellery with Stones","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 07:13:47","2020-06-29 07:13:47"),
("37","11","Traditional Wear","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 07:13:47","2020-06-29 07:13:47"),
("38","11","Bestsellers Products","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 07:13:47","2020-06-29 07:13:47"),
("39","11","Woman Earrings","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 07:13:47","2020-06-29 07:13:47"),
("40","11","Yellow Gold ,Metal","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 07:13:47","2020-06-29 07:13:47");





CREATE TABLE `product_images` (
  `product_image_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`product_image_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO product_images VALUES("7","3","5ef98625e6097.jpg","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","",""),
("8","3","5ef986262efed.jpg","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","",""),
("9","4","5ef9886ba5fc5.jpg","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","",""),
("10","4","5ef9886bdec6a.jpg","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","",""),
("11","4","5ef9886c25222.jpg","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","",""),
("12","5","5ef98a5e7b36d.jpg","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","",""),
("13","5","5ef98a5eb7fa3.jpg","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","",""),
("14","5","5ef98a5eea787.jpg","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","",""),
("15","6","5ef98d121b294.jpg","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","",""),
("16","6","5ef98d1255d91.jpg","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","",""),
("17","7","5ef98fd9a8c9a.jpg","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","",""),
("18","7","5ef98fd9df9ba.jpg","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","",""),
("19","8","5ef99165ba703.jpg","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","",""),
("20","8","5ef991d96295e.jpg","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 07:01:45","2020-06-29 07:01:45"),
("21","8","5ef991d99e63a.jpg","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 07:01:45","2020-06-29 07:01:45"),
("22","8","5ef991d9d3412.jpg","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 07:01:46","2020-06-29 07:01:46"),
("23","10","5ef9930e73bc1.jpg","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","",""),
("24","10","5ef993266ff3b.jpg","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 07:07:18","2020-06-29 07:07:18"),
("25","10","5ef99326a82a1.jpg","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 07:07:18","2020-06-29 07:07:18"),
("26","10","5ef99326e969f.jpg","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 07:07:19","2020-06-29 07:07:19"),
("27","11","5ef994ab5c9fd.jpg","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","",""),
("28","11","5ef994ab9aabc.jpg","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","","");





CREATE TABLE `product_prices` (
  `product_prices_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `size` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `mrp` int(11) NOT NULL DEFAULT 0,
  `selling_price` int(11) NOT NULL DEFAULT 0,
  `mediator_price` int(11) NOT NULL DEFAULT 0,
  `total_stock` int(11) NOT NULL DEFAULT 0,
  `available_stock` int(11) NOT NULL DEFAULT 0,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`product_prices_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO product_prices VALUES("5","3","2.672gm","18137","18000","17900","10","10","","2020-06-29 06:11:50","2020-06-29 06:11:50"),
("6","4","17.20mm","58407","58400","58000","10","10","","2020-06-29 06:21:32","2020-06-29 06:21:32"),
("7","5","16.80mm","69670","69400","69000","20","19","","2020-06-29 06:29:51","2020-06-29 06:29:51"),
("8","6","12.370gm","79004","78600","70000","10","9","","2020-06-29 06:41:22","2020-06-29 06:41:22"),
("9","7","60mm","85100","84500","84000","10","10","","2020-06-29 06:53:14","2020-06-29 06:53:14"),
("10","8","14.506gm","80763","80000","79500","10","9","","2020-06-29 06:59:49","2020-06-29 06:59:49"),
("11","10","5.485gm","79134","79000","78500","10","10","","2020-06-29 07:06:54","2020-06-29 07:06:54"),
("12","11","19.460gm","122702","122702","122000","10","9","","2020-06-29 07:13:47","2020-06-29 07:13:47");





CREATE TABLE `product_spacifications` (
  `product_spacifications_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`product_spacifications_id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO product_spacifications VALUES("4","3","Sapphire|14|Prong|Round","GEM STONE 1","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:11:49","2020-06-29 06:11:49"),
("5","3","18.00","PURITY","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:11:49","2020-06-29 06:11:49"),
("6","3","Women","GENDER","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:11:49","2020-06-29 06:11:49"),
("7","3","Bestsellers","COLLECTION","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:11:49","2020-06-29 06:11:49"),
("8","3","Gold","METAL","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:11:49","2020-06-29 06:11:49"),
("9","3","Yellow","METAL COLOR","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:11:49","2020-06-29 06:11:49"),
("10","4","22.00","PURITY","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:21:31","2020-06-29 06:21:31"),
("11","4","Finger Ring","PRODUCT","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:21:31","2020-06-29 06:21:31"),
("12","4","16.4 mm","WIDTH","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:21:31","2020-06-29 06:21:31"),
("13","4","Mangalam","COLLECTION","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:21:31","2020-06-29 06:21:31"),
("14","4","Engagement","OCCASION","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:21:31","2020-06-29 06:21:31"),
("15","5","22.00","PURITY","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:29:50","2020-06-29 06:29:50"),
("16","5","Finger Ring","PRODUCT","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:29:50","2020-06-29 06:29:50"),
("17","5","16.4 mm","WIDTH","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:29:50","2020-06-29 06:29:50"),
("18","5","Engagement","OCCASION","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:29:50","2020-06-29 06:29:50"),
("19","5","Mangalam","OLLECTION","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:29:50","2020-06-29 06:29:50"),
("20","6","22.00","PURITY","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:41:21","2020-06-29 06:41:21"),
("21","6","Women","GENDER","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:41:21","2020-06-29 06:41:21"),
("22","6","Bestsellers","COLLECTION","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:41:21","2020-06-29 06:41:21"),
("23","7","22.00","PURITY","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:53:13","2020-06-29 06:53:13"),
("24","7","Swayahm","COLLECTION","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:53:13","2020-06-29 06:53:13"),
("25","7","Yellow","METAL COLOR","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:53:13","2020-06-29 06:53:13"),
("26","7","60 mm","HEIGHT","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:53:13","2020-06-29 06:53:13"),
("27","7","60 mm","WIDTH","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:53:13","2020-06-29 06:53:13"),
("28","8","18.00","PURITY","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:59:49","2020-06-29 06:59:49"),
("29","8","Traditional Wear","OCCASION","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:59:49","2020-06-29 06:59:49"),
("30","8","Bestsellers","COLLECTION","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:59:49","2020-06-29 06:59:49"),
("31","10","18.00","PURITY","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 07:06:54","2020-06-29 07:06:54"),
("32","10","Party Wear","OCCASION","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 07:06:54","2020-06-29 07:06:54"),
("33","10","SI2","DIAMOND CLARITY","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 07:06:54","2020-06-29 07:06:54"),
("34","11","22.00","PURITY","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 07:13:47","2020-06-29 07:13:47"),
("35","11","Bestsellers","COLLECTION","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 07:13:47","2020-06-29 07:13:47"),
("36","11","Jhumka","TYPE","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 07:13:47","2020-06-29 07:13:47");





CREATE TABLE `products` (
  `product_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cat_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `sub_cat_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `sub_sub_cat_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `hsn_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `view_count` int(11) NOT NULL DEFAULT 0,
  `total_review` int(11) NOT NULL DEFAULT 0,
  `review` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0.0',
  `active_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'YES',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO products VALUES("3","000001","INDI SAPPHIRE STUD EARRINGS","7","4","6","<p><img alt=\"\" src=\"https://staticimg.titan.co.in/Tanishq/Banners/BlackBand/purity.png\" /></p>\n\n<h3>THE PURITY GUARANTEE</h3>\n\n<h1>---------------</h1>\n\n<p>The state-of-the-art Karatmeter present in every Tanishq store is a very accurate way of measuring the purity of gold thus making our gold as pure as we say it is.</p>\n\n<p><img alt=\"\" src=\"https://staticimg.titan.co.in/Tanishq/Banners/BlackBand/stone.png\" /></p>\n\n<h3>YOU NEVER PAY FOR STONES AT THE RATE OF GOLD</h3>\n\n<h1>---------------</h1>\n\n<p>We charge only for the actual weight of gold, after subtracting the weight of stones from the total weight of the piece. You are never charged the price of gold for the weight of stones.</p>\n\n<p><img alt=\"\" src=\"https://staticimg.titan.co.in/Tanishq/Banners/BlackBand/exchange.png\" /></p>\n\n<h3>100% EXCHANGE VALUE FOR DIAMONDS, POLKI, RUBIES, EMERALDS</h3>\n\n<h1>---------------</h1>\n\n<p>All our precious stones are carefully selected to deliver best-in-class quality. So much so, we offer 100% buyback on them at current prices. No wonder we are India&acirc;s leading diamond jeweler.</p>","2","500067SYAABA","0","0","0.0","YES","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:11:49","2020-06-29 06:11:49"),
("4","000002","2KT GOLD FINGER RING","7","5","9","<p><img alt=\"\" src=\"https://staticimg.titan.co.in/Tanishq/Banners/BlackBand/tq.png\" /></p>\n\n<h3>A TATA PRODUCT</h3>\n\n<h1>---------------</h1>\n\n<p>The TATA Trust</p>\n\n<p><img alt=\"\" src=\"https://staticimg.titan.co.in/Tanishq/Banners/BlackBand/purity.png\" /></p>\n\n<h3>THE PURITY GUARANTEE</h3>\n\n<h1>---------------</h1>\n\n<p>The state-of-the-art Karatmeter present in every Tanishq store is a very accurate way of measuring the purity of gold thus making our gold as pure as we say it is.</p>\n\n<p><img alt=\"\" src=\"https://staticimg.titan.co.in/Tanishq/Banners/BlackBand/exchange.png\" /></p>\n\n<h3>100% EXCHANGE VALUE FOR DIAMONDS, POLKI, RUBIES, EMERALDS</h3>\n\n<h1>---------------</h1>\n\n<p>All our precious stones are carefully selected to deliver best-in-class quality. So much so, we offer 100% buyback on them at current prices. No wonder we are India&rsquo;s leading diamond jeweler.</p>","2","513217FCWNA","0","0","0.0","YES","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:21:31","2020-06-29 06:21:31"),
("5","000003","22KT GOLD FINGER RING","7","3","3","<p><img alt=\"\" src=\"https://staticimg.titan.co.in/Tanishq/Banners/BlackBand/tq.png\" /></p>\n\n<h3>A TATA PRODUCT</h3>\n\n<h1>---------------</h1>\n\n<p>The TATA Trust</p>\n\n<p><img alt=\"\" src=\"https://staticimg.titan.co.in/Tanishq/Banners/BlackBand/purity.png\" /></p>\n\n<h3>THE PURITY GUARANTEE</h3>\n\n<h1>---------------</h1>\n\n<p>The state-of-the-art Karatmeter present in every Tanishq store is a very accurate way of measuring the purity of gold thus making our gold as pure as we say it is.</p>\n\n<p><img alt=\"\" src=\"https://staticimg.titan.co.in/Tanishq/Banners/BlackBand/exchange.png\" /></p>\n\n<h3>100% EXCHANGE VALUE FOR DIAMONDS, POLKI, RUBIES, EMERALDS</h3>\n\n<h1>---------------</h1>\n\n<p>All our precious stones are carefully selected to deliver best-in-class quality. So much so, we offer 100% buyback on them at current prices. No wonder we are India&rsquo;s leading diamond jeweler.</p>","2","513017FBWMAA0","0","0","0.0","YES","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:29:50","2020-06-29 06:29:50"),
("6","000004","22KT GOLD AND SYNTHETIC STONE PENDANT","7","7","15","<h3>THE PURITY GUARANTEE</h3>\n\n<h1>---------------</h1>\n\n<p>The state-of-the-art Karatmeter present in every Tanishq store is a very accurate way of measuring the purity of gold thus making our gold as pure as we say it is.</p>\n\n<h3>YOU NEVER PAY FOR STONES AT THE RATE OF GOLD</h3>\n\n<h1>---------------</h1>\n\n<p>We charge only for the actual weight of gold, after subtracting the weight of stones from the total weight of the piece. You are never charged the price of gold for the weight of stones.</p>\n\n<h3>100% EXCHANGE VALUE FOR DIAMONDS, POLKI, RUBIES, EMERALDS</h3>\n\n<h1>---------------</h1>\n\n<p>All our precious stones are carefully selected to deliver best-in-class quality. So much so, we offer 100% buyback on them at current prices. No wonder we are India&Acirc;s leading diamond jeweler.</p>","2","512314PMCAA","0","0","0.0","YES","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:41:21","2020-06-29 06:41:21"),
("7","000005","22KT GOLD BANGLE","7","7","25","<p><img alt=\"\" src=\"https://staticimg.titan.co.in/Tanishq/Banners/BlackBand/purity.png\" /></p>\n\n<h3>THE PURITY GUARANTEE</h3>\n\n<h1>---------------</h1>\n\n<p>The state-of-the-art Karatmeter present in every Tanishq store is a very accurate way of measuring the purity of gold thus making our gold as pure as we say it is.</p>\n\n<p><img alt=\"\" src=\"https://staticimg.titan.co.in/Tanishq/Banners/BlackBand/tq.png\" /></p>\n\n<h3>A TATA PRODUCT</h3>\n\n<h1>---------------</h1>\n\n<p>The TATA Trust</p>\n\n<p><img alt=\"\" src=\"https://staticimg.titan.co.in/Tanishq/Banners/BlackBand/exchange.png\" /></p>\n\n<h3>100% EXCHANGE VALUE FOR DIAMONDS, POLKI, RUBIES, EMERALDS</h3>\n\n<h1>---------------</h1>\n\n<p>All our precious stones are carefully selected to deliver best-in-class quality. So much so, we offer 100% buyback on them at current prices. No wonder we are India&acirc;s leading diamond jeweler.</p>","2","513018VQBR1A","0","0","0.0","YES","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:53:13","2020-06-29 06:53:13"),
("8","000006","NOORA RUBY PENDANT EARRINGS SET","7","3","3","<p><img alt=\"\" src=\"https://staticimg.titan.co.in/Tanishq/Banners/BlackBand/purity.png\" /></p>\n\n<h3>THE PURITY GUARANTEE</h3>\n\n<h1>---------------</h1>\n\n<p>The state-of-the-art Karatmeter present in every Tanishq store is a very accurate way of measuring the purity of gold thus making our gold as pure as we say it is.</p>\n\n<p><img alt=\"\" src=\"https://staticimg.titan.co.in/Tanishq/Banners/BlackBand/stone.png\" /></p>\n\n<h3>YOU NEVER PAY FOR STONES AT THE RATE OF GOLD</h3>\n\n<h1>---------------</h1>\n\n<p>We charge only for the actual weight of gold, after subtracting the weight of stones from the total weight of the piece. You are never charged the price of gold for the weight of stones.</p>\n\n<p><img alt=\"\" src=\"https://staticimg.titan.co.in/Tanishq/Banners/BlackBand/exchange.png\" /></p>\n\n<h3>100% EXCHANGE VALUE FOR DIAMONDS, POLKI, RUBIES, EMERALDS</h3>\n\n<h1>---------------</h1>\n\n<p>All our precious stones are carefully selected to deliver best-in-class quality. So much so, we offer 100% buyback on them at current prices. No wonder we are India&acirc;s leading diamond jeweler.</p>","2","5020111ETABA","0","0","0.0","YES","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:59:49","2020-06-29 06:59:49"),
("10","000007","LIZANNE DIAMOND AND RUBY PENDANT EARRINGS SET","7","3","3","<p><img alt=\"\" src=\"https://staticimg.titan.co.in/Tanishq/Banners/BlackBand/purity.png\" /></p>\n\n<h3>THE PURITY GUARANTEE</h3>\n\n<h1>---------------</h1>\n\n<p>The state-of-the-art Karatmeter present in every Tanishq store is a very accurate way of measuring the purity of gold thus making our gold as pure as we say it is.</p>\n\n<p><img alt=\"\" src=\"https://staticimg.titan.co.in/Tanishq/Banners/BlackBand/stone.png\" /></p>\n\n<h3>YOU NEVER PAY FOR STONES AT THE RATE OF GOLD</h3>\n\n<h1>---------------</h1>\n\n<p>We charge only for the actual weight of gold, after subtracting the weight of stones from the total weight of the piece. You are never charged the price of gold for the weight of stones.</p>\n\n<p><img alt=\"\" src=\"https://staticimg.titan.co.in/Tanishq/Banners/BlackBand/exchange.png\" /></p>\n\n<h3>100% EXCHANGE VALUE FOR DIAMONDS, POLKI, RUBIES, EMERALDS</h3>\n\n<h1>---------------</h1>\n\n<p>All our precious stones are carefully selected to deliver best-in-class quality. So much so, we offer 100% buyback on them at current prices. No wonder we are India&Atilde;&cent;&Acirc;&Acirc;s leading diamond jeweler.</p>","2","5024151HAABA","0","0","0.0","YES","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 07:06:54","2020-06-29 07:06:54"),
("11","000008","22KT GOLD JHUMKAS","7","4","6","<p><img alt=\"\" src=\"https://staticimg.titan.co.in/Tanishq/Banners/BlackBand/purity.png\" /></p>\n\n<h3>THE PURITY GUARANTEE</h3>\n\n<h1>---------------</h1>\n\n<p>The state-of-the-art Karatmeter present in every Tanishq store is a very accurate way of measuring the purity of gold thus making our gold as pure as we say it is.</p>\n\n<p><img alt=\"\" src=\"https://staticimg.titan.co.in/Tanishq/Banners/BlackBand/stone.png\" /></p>\n\n<h3>YOU NEVER PAY FOR STONES AT THE RATE OF GOLD</h3>\n\n<h1>---------------</h1>\n\n<p>We charge only for the actual weight of gold, after subtracting the weight of stones from the total weight of the piece. You are never charged the price of gold for the weight of stones.</p>\n\n<p><img alt=\"\" src=\"https://staticimg.titan.co.in/Tanishq/Banners/BlackBand/exchange.png\" /></p>\n\n<h3>100% EXCHANGE VALUE FOR DIAMONDS, POLKI, RUBIES, EMERALDS</h3>\n\n<h1>---------------</h1>\n\n<p>All our precious stones are carefully selected to deliver best-in-class quality. So much so, we offer 100% buyback on them at current prices. No wonder we are India&acirc;s leading diamond jeweler.</p>","2","512515JENABAP1","0","0","0.0","YES","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 07:13:47","2020-06-29 07:13:47"),
("12","000009","xasdasd","8","8","17","","0","13121","0","0","0.0","YES","VmfagG06GM4teq9Khs1nubFs6DnMLMDZWvZJa2Lb","2020-07-04 07:36:27","2020-07-04 07:36:27");





CREATE TABLE `promotion_banners` (
  `dis_id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(100) DEFAULT NULL,
  `remember_token` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`dis_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

INSERT INTO promotion_banners VALUES("1","1593064070.jpg","jW46J8uj620psoZ3OBwSiUwe1v2yoR0yoF4iD92N","2020-05-09 06:08:42","2020-05-09 06:08:42"),
("2","1593063615.jpg","jW46J8uj620psoZ3OBwSiUwe1v2yoR0yoF4iD92N","2020-05-09 06:09:32","2020-05-09 06:09:32"),
("3","1593063623.jpg","jW46J8uj620psoZ3OBwSiUwe1v2yoR0yoF4iD92N","2020-05-09 06:09:44","2020-05-09 06:09:44"),
("4","1593063639.jpg","jW46J8uj620psoZ3OBwSiUwe1v2yoR0yoF4iD92N","2020-05-09 06:09:56","2020-05-09 06:09:56"),
("5","1589029440.jpg","jW46J8uj620psoZ3OBwSiUwe1v2yoR0yoF4iD92N","2020-05-09 06:09:56","2020-05-09 06:09:56"),
("9","1589029440.jpg","jW46J8uj620psoZ3OBwSiUwe1v2yoR0yoF4iD92N","2020-05-09 06:09:56","2020-05-09 06:09:56");





CREATE TABLE `refer_codes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `refer_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO refer_codes VALUES("1","5EFC77F1B89D8SSJ2","2","2020-07-01 11:48:01","2020-07-01 11:48:01");





CREATE TABLE `stock_trasanctions` (
  `stock_trasanctions_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `quantity` int(11) NOT NULL DEFAULT 0,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_quantity` int(11) NOT NULL DEFAULT 0,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `product_price_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transfer_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`stock_trasanctions_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO stock_trasanctions VALUES("1","1","50","CREDIT","50","","2020-06-17 04:56:11","2020-06-17 04:56:11","1","INITIAL ADD","0"),
("2","1","50","CREDIT","50","","2020-06-17 04:56:11","2020-06-17 04:56:11","2","INITIAL ADD","0"),
("3","1","10","CREDIT","60","","2020-06-24 06:34:10","2020-06-24 06:34:10","1","ADD STOCK","123456"),
("4","1","20","CREDIT","80","","2020-06-24 06:43:20","2020-06-24 06:43:20","1","ADD STOCK","545495462"),
("5","1","22","CREDIT","102","","2020-06-24 06:45:14","2020-06-24 06:45:14","1","ADD STOCK","123456"),
("6","3","10","CREDIT","10","","2020-06-29 06:11:50","2020-06-29 06:11:50","5","INITIAL ADD","0"),
("7","4","10","CREDIT","10","","2020-06-29 06:21:32","2020-06-29 06:21:32","6","INITIAL ADD","0"),
("8","5","10","CREDIT","10","","2020-06-29 06:29:51","2020-06-29 06:29:51","7","INITIAL ADD","0"),
("9","6","10","CREDIT","10","","2020-06-29 06:41:22","2020-06-29 06:41:22","8","INITIAL ADD","0"),
("10","7","10","CREDIT","10","","2020-06-29 06:53:14","2020-06-29 06:53:14","9","INITIAL ADD","0"),
("11","8","10","CREDIT","10","","2020-06-29 06:59:50","2020-06-29 06:59:50","10","INITIAL ADD","0"),
("12","10","10","CREDIT","10","","2020-06-29 07:06:54","2020-06-29 07:06:54","11","INITIAL ADD","0"),
("13","11","10","CREDIT","10","","2020-06-29 07:13:47","2020-06-29 07:13:47","12","INITIAL ADD","0"),
("14","11","1","DEBIT","9","","2020-07-03 08:00:58","2020-07-03 08:00:58","12","BOOKING","5EFEE5B7B3C7C"),
("15","5","1","DEBIT","9","","2020-07-03 08:00:58","2020-07-03 08:00:58","7","BOOKING","5EFEE5B7B3C7C"),
("16","6","1","DEBIT","9","","2020-07-03 08:00:58","2020-07-03 08:00:58","8","BOOKING","5EFEE5B7B3C7C"),
("17","8","1","DEBIT","9","","2020-07-03 08:00:58","2020-07-03 08:00:58","10","BOOKING","5EFEE5B7B3C7C"),
("18","5","10","CREDIT","19","","2020-07-03 14:26:59","2020-07-03 14:26:59","7","ADD STOCK","123456");





CREATE TABLE `sub_cats` (
  `sub_cat_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `sub_cat_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cat_id` int(11) NOT NULL DEFAULT 0,
  `gst` int(11) NOT NULL DEFAULT 0,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sub_cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO sub_cats VALUES("3","Necklace","7","5","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:39:05","2020-06-29 05:39:05"),
("4","Earrings","7","5","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:39:05","2020-06-29 05:39:05"),
("5","Rings","7","5","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:39:05","2020-06-29 05:39:05"),
("7","Others Accessories","7","5","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:39:06","2020-06-29 05:39:06"),
("8","Rings","8","5","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:45:15","2020-06-29 05:45:15"),
("9","Brooch","8","5","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:45:15","2020-06-29 05:45:15"),
("11","Watch","8","5","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:45:15","2020-06-29 05:45:15");





CREATE TABLE `sub_sub_cats` (
  `sub_sub_cat_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `sub_sub_cat_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_cat_id` int(11) NOT NULL DEFAULT 0,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sub_sub_cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO sub_sub_cats VALUES("3","Antique Necklace","3","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:40:12","2020-06-29 05:40:12"),
("4","Zircon Necklace","3","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:40:12","2020-06-29 05:40:12"),
("5","Kundan Nacklace","3","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:40:12","2020-06-29 05:40:12"),
("6","Antique Earrings","4","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:41:13","2020-06-29 05:41:13"),
("7","Zicon Earrings","4","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:41:13","2020-06-29 05:41:13"),
("8","Kundan Errings","4","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:41:14","2020-06-29 05:41:14"),
("9","Antique Ring","5","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:42:05","2020-06-29 05:42:05"),
("10","Kundan Ring","5","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:42:05","2020-06-29 05:42:05"),
("14","Chain","7","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:43:58","2020-06-29 05:43:58"),
("15","Pendant","7","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:43:58","2020-06-29 05:43:58"),
("16","Antique Ring","8","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:48:37","2020-06-29 05:48:37"),
("17","Kundan Ring","8","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:48:37","2020-06-29 05:48:37"),
("18","Zicon Ring","8","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:49:22","2020-06-29 05:49:22"),
("19","Gold Watch","11","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:51:15","2020-06-29 05:51:15"),
("20","Silver Watch","11","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:51:15","2020-06-29 05:51:15"),
("21","Party Rings","8","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:53:21","2020-06-29 05:53:21"),
("22","Engagement Ring","8","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:53:21","2020-06-29 05:53:21"),
("23","Work Ware","8","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:54:22","2020-06-29 05:54:22"),
("24","Daily Ware","8","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 05:54:22","2020-06-29 05:54:22"),
("25","BANGLE","7","HJEsRohO3fFIh2dLiHf0FNCWb3iebBWL8t3wnne2","2020-06-29 06:49:33","2020-06-29 06:49:33");





CREATE TABLE `subscribers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO subscribers VALUES("6","2","mbQWDXtuEIJuwOf1VcSrLVSCDpZCiXomMo2nvRcz","2020-07-03 12:28:00","2020-07-03 12:28:00");





CREATE TABLE `user_wallet_transactions` (
  `wallet_transaction_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `transaction_amount` bigint(20) DEFAULT NULL,
  `after_transaction_amount` bigint(20) DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tranasaction_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `booking_id` bigint(20) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`wallet_transaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;






CREATE TABLE `user_wallets` (
  `wallet_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `wallet_ammount` bigint(20) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`wallet_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO user_wallets VALUES("1","2","0","mbQWDXtuEIJuwOf1VcSrLVSCDpZCiXomMo2nvRcz","2020-07-03 07:43:42","2020-07-03 07:43:42");





CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `otp` int(11) DEFAULT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'USER',
  `usertype` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'USER',
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'YES',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subscribe` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'NO',
  `block` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'NO',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_mobile_unique` (`mobile`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO users VALUES("1","gouri","admin@gmail.com","","1234567890","","ADMIN","USER","YES","$2y$10$tyIzVbpVRmg0VFMFAP9zj.RxSKy86eV.B.v7jQ4ab9CafGoYYE4TS","NO","NO","qOiogcNJ16UdoKVyUhy2IqAZvgBpOP63FIFPh6M688TvuYu2pTaj0Fr1Te9W","2020-06-12 09:07:10","2020-06-12 09:42:23"),
("2","Malay Kumar Bera","malay@quantex.co.in","","6294008510","1234","USER","MEDIATOR","YES","$2y$10$iOF2wplYF6vef87vRDHpqe2owmYXOWxKStKkRJ8H0/8AH.0Ti7vsO","NO","NO","","2020-06-29 09:25:30","2020-06-29 09:25:30"),
("3","Malay Kr. Bera","gouriatlantistech@gmail.com","","9635371980","7383","USER","MEDIATOR","YES","$2y$10$tyIzVbpVRmg0VFMFAP9zj.RxSKy86eV.B.v7jQ4ab9CafGoYYE4TS","NO","YES","UoGDoKhbkimgE4Bs9tzR786SvPuNDb3ksDNWEmE9zVRb9m7S93Wg4Jj0n24W","2020-06-12 09:07:10","2020-07-01 09:41:12"),
("4","Malay Kr. Bera","gouriatlantistechjhfgh@gmail.com","","9733118409","","USER","USER","NO","$2y$10$nVudY1Rd2C9HHWGyXQ7is.hYDjIA7WA/rWjsFBlcnNFHVxp01RFpe","NO","NO","lEoaRVi8srmDGAWZIjc22ksdwddJWbd3qsygjb9bWKD2iCvcahuQYKDjoCbu","2020-06-12 09:09:26","2020-06-26 10:29:46");





CREATE TABLE `wishlists` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `quantity` int(11) NOT NULL DEFAULT 1,
  `product_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `product_price_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




