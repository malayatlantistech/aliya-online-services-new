@extends('layouts.menu') @section('title') Booking History : E-Kirana @endsection @section('content')
<style>
    .btn-sm {
        font-size: 12px;
        padding: 8px 15px;
        border-radius: 0px;
        color: inherit;
        text-decoration: none;
        margin: 2px;
    }
    .navbar {
        /* font-weight: 300; */
        -webkit-box-shadow: 0 0px 0px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
        box-shadow: 0 0px 0px 0 rgba(0, 0, 0, 0), 0 2px 10px 0 rgba(0, 0, 0, 0);
    }
    .md-form {
        position: relative;
        margin-top: 1rem;
        margin-bottom: 1rem;
    }
    .stamp {
        transform: rotate(12deg);
        color: #555;
        font-size: 3rem;
        font-weight: 700;
        border: 0.25rem solid #555;
        display: inline-block;
        padding: 0.25rem 1rem;
        text-transform: uppercase;
        border-radius: 1rem;
        font-family: "Courier";
        -webkit-mask-image: url("https://s3-us-west-2.amazonaws.com/s.cdpn.io/8399/grunge.png");
        -webkit-mask-size: 944px 604px;
        mix-blend-mode: multiply;
    }

    .is-nope {
        color: #d23;
        border: 0.5rem double #d23;
        transform: rotate(3deg);
        -webkit-mask-position: 2rem 3rem;
        font-size: 2rem;
    }
    footer .subscribe-section .subscribe-content .subscribe-form .btn-solid {
        color: white;
    }

    @media (max-width: 767.98px) {
        .badge {
            font-weight: 300;
        }
    }
</style>

<!-- ========== MAIN CONTENT ========== -->
<main id="content" role="main" class="cart-page">
    <!-- breadcrumb -->
    <div class="bg-gray-13 bg-md-transparent">
        <div class="container">
            <!-- breadcrumb -->
            <div class="my-md-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="/">Home</a></li>
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">My Order</li>
                    </ol>
                </nav>
            </div>
            <!-- End breadcrumb -->
        </div>
    </div>
    <!-- End breadcrumb -->

    <div class="container">
        <div class="my-6">
            <h1 class="text-center">My Order</h1>
        </div>
        <div class="mb-16 wishlist-table">
            <!-- <form class="mb-4" action="#" method="post"> -->
            <div class="table-responsive">
                <table class="table" cellspacing="0">
                    <thead>
                        <tr>
                            <th class="product-remove">&nbsp;</th>
                            <th class="product-thumbnail">&nbsp;</th>
                            <th class="product-name">Product</th>
                            <th class="product-price">Product Price</th>
                            <th class="product-Stock">Quantity</th>
                            <th class="product-subtotal min-width-200-md-lg">Status</th>
                            <th class="product-subtotal min-width-200-md-lg">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($booking as $booking) @php($rr=DB::table('product_images')->where('product_id',$booking->product_id)->limit(1)->get())
                        <tr class="">
                            <td class="text-center">
                                <a href="#" class="text-gray-90"> </a>
                            </td>
                            <td class="d-none d-md-table-cell">
                                @foreach($rr as $aas)
                                <a href="product_details?id={{$booking->product_id}}"><img class="img-fluid max-width-100 p-1 border border-color-1" src="small_product_image/{{$aas->image}}" alt="Image Description" /></a>
                                @endforeach
                            </td>

                            <td data-title="Product">
                                @php($rrr=DB::table('book_multi_items')->where('product_id',$booking->product_id)->first())
                                <a href="#" class="text-gray-90">
                                    Date: {{date('d-m-Y', strtotime($rrr->created_at))}}<br />
                                    Order Id: <b style="color: black;">{{$booking->order_id}} </b><br />
                                    {{$booking->product_name}}<br />
                                    {{$booking->size}}
                                </a>
                            </td>

                            <td data-title="Unit Price">
                                <span class="">₹ {{($booking->product_price)*($booking->quantity1)}}</span>
                            </td>

                            <td data-title="Quantity">
                                <!-- Stock Status -->
                                <span> {{$booking->quantity1}}</span>
                                <!-- End Stock Status -->
                            </td>

                            <td data-title="Order Status">
                                @if($booking->order_status == 1)

                                <span class="badge badge-primary"> Order Under Process</span>
                                @elseif($booking->order_status == 2)

                                <span class="badge badge-primary"> Order In Transit</span>
                                @elseif($booking->order_status == 3)

                                <span class="badge badge-primary"> Out For Delivery</span>
                                @elseif($booking->order_status == 4)

                                <span class="badge badge-primary"> Delivered </span>
                                @elseif($booking->order_status == 5)

                                <span class="badge badge-warning"> Return Under process </span>
                                @elseif($booking->order_status == 6)
                                <span class="badge badge-warning"> Out For Pic Up </span>
                                @elseif($booking->order_status == 7 )

                                <span class="badge badge-secondary"> Returned </span>
                                @if($booking->refund_status == 'YES')
                                <span class="badge badge-success" style="margin: 3px;"> Refund Completed</span>
                                @endif @elseif($booking->order_status == 8)
                                <span class="badge badge-danger">Canceled</span>

                                @endif

                                <!-- <a type="button" class="btn btn-soft-secondary mb-3 mb-md-0 font-weight-normal px-5 px-md-4 px-lg-5 w-100 w-md-auto">Add to Cart</a> -->
                            </td>
                            <td data-title="Action">
                                @if($booking->order_status == 4)

                                <a href="product_details?id={{$booking->product_id}}#Reviews" id="dssfcsdfc1" class="btn btn-info btn-sm" style="color: black;">Review</a>
                                @endif @if($booking->order_status==4 && $booking->return_status!='Return Policy is Over')

                                <a data-toggle="modal" data-target="#modalreturn{{$booking->multi_id}}" class="btn btn-warning btn-sm" style="color: black; margin-top: 3px;">Return</a>

                                <div class="text-center">
                                    <!--Modal: Login / Register Form-->
                                    <div class="modal fade" id="modalreturn{{$booking->multi_id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog cascading-modal" role="document">
                                            <!--Content-->
                                            <div class="modal-content">
                                                <!--Modal cascading tabs-->
                                                <div class="modal-c-tabs">
                                                    <!-- Tab panels -->
                                                    <div class="tab-content" style="padding: 0 0 0 0;">
                                                        <!--Panel 7-->
                                                        <div class="tab-pane fade in show active" id="panel7" role="tabpanel">
                                                            <br />
                                                            <center>
                                                                <h2><b>RETURN ORDER</b></h2>
                                                            </center>
                                                            <!--Body-->
                                                            <div class="modal-body mb-1">
                                                                <div class="md-form form-sm mb-4" style="margin-top: 0;">
                                                                    @foreach($rr as $aas)
                                                                    <img src="small_product_image/{{$aas->image}}" alt="" style="height: 75px;" /><br />
                                                                    @endforeach Order ID : {{$booking->order_id}}<br />
                                                                    {{$booking->product_name}}
                                                                </div>
                                                                <form action="return_status" method="POST">
                                                                    <input type="hidden" name="product_id" value="{{$booking->multi_id}}" />
                                                                    <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}" />
                                                                    <div class="md-form form-sm" style="text-align: left;">
                                                                        Reason for Return
                                                                        <select name="cancel_option" class="form-control option" required>
                                                                            <option value="" disable selected>-- Select Option -- </option>
                                                                            <option>Item Damaged but shipping box Ok</option>
                                                                            <option>Both product and shipping box damaged</option>
                                                                            <option>Defective product</option>
                                                                            <option>Wrong product sent</option>
                                                                            <option>Unsatisfactory product quality</option>
                                                                            <option value="1">Others</option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="md-form form-sm" style="text-align: left;">
                                                                        Message
                                                                        <textarea name="message" class="form-control message" placeholder="Enter Your Message" required></textarea>
                                                                    </div>

                                                                    <div class="md-form form-sm" style="text-align: left;">
                                                                        Refund Amount Type

                                                                        <select name="refund_option" class="form-control option" required onChange="account(this.value,'{{$booking->payment_type}}','{{csrf_token()}}');">
                                                                            <option value="" disable selected>-- Select Option -- </option>
                                                                            <option value="Original Payment Method">Original Payment Method </option>
                                                                            <option value="Wallet">Wallet</option>
                                                                        </select>
                                                                    </div>

                                                                    <div id="ads"></div>

                                                                    <div class="text-center mt-2">
                                                                        <button class="btn btn-info" style="background-color: #025f97 !important;">Place Request</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            <!--Footer-->
                                                            <!--  <div class="modal-footer">
                                  <div class="options text-center text-md-right mt-1">
                                    <p>Not a member? <a href="#" class="blue-text">Sign Up</a></p>
                                    <p>Forgot <a href="#" class="blue-text">Password?</a></p>
                                  </div>
                                  <button type="button" class="btn btn-outline-info waves-effect ml-auto" data-dismiss="modal">Close</button>
                                </div>
                            -->
                                                        </div>
                                                        <!--/.Panel 7-->
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/.Content-->
                                        </div>
                                    </div>
                                </div>
                                <!--Modal: Login / Register Form-->

                                <!------------return modal end-------------->

                                @endif @if($booking->order_status==1 || $booking->order_status==2)

                                <a data-toggle="modal" data-target="#modalLRForm{{$booking->multi_id}}" class="btn btn-danger btn-sm" style="color: black;">Cancel</a>

                                <div class="text-center">
                                    <!--Modal: Login / Register Form-->
                                    <div class="modal fade" id="modalLRForm{{$booking->multi_id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog cascading-modal" role="document">
                                            <!--Content-->
                                            <div class="modal-content">
                                                <!--Modal cascading tabs-->
                                                <div class="modal-c-tabs">
                                                    <!-- Nav tabs -->

                                                    <!-- Tab panels -->
                                                    <div class="tab-content" style="padding: 0 0 0 0;">
                                                        <!--Panel 7-->
                                                        <div class="tab-pane fade in show active" id="panel7" role="tabpanel">
                                                            <br />
                                                            <center>
                                                                <h2><b>CANCEL ORDER</b></h2>
                                                            </center>

                                                            <!--Body-->
                                                            <div class="modal-body mb-1">
                                                                <div class="md-form form-sm mb-4" style="margin-top: 0;">
                                                                    @foreach($rr as $aas)
                                                                    <img src="small_product_image/{{$aas->image}}" alt="" style="height: 75px;" /><br />
                                                                    @endforeach<br />
                                                                    Order ID : {{$booking->order_id}}<br />
                                                                    {{$booking->product_name}}
                                                                </div>
                                                                <form action="cencel_status" method="POST">
                                                                    <input type="hidden" name="product_id" value="{{$booking->multi_id}}" />
                                                                    <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}" />
                                                                    <div class="md-form form-sm" style="text-align: left;">
                                                                        Reason for Cancel
                                                                        <select name="cancel_option" class="form-control option" required>
                                                                            <option>Order placed by mistake</option>
                                                                            <option>Item price too high</option>
                                                                            <option>Shipping cost too high</option>
                                                                            <option value="1">Others</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="md-form form-sm" style="text-align: left;">
                                                                        Message
                                                                        <textarea name="message" class="form-control message" placeholder="Enter Your Message"></textarea>
                                                                    </div>

                                                                    <div class="text-center mt-2">
                                                                        <button class="btn btn-info" style="background-color: #025f97 !important;">Place Request</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            <!--Footer-->
                                                            <!--  <div class="modal-footer">
                                  <div class="options text-center text-md-right mt-1">
                                    <p>Not a member? <a href="#" class="blue-text">Sign Up</a></p>
                                    <p>Forgot <a href="#" class="blue-text">Password?</a></p>
                                  </div>
                                  <button type="button" class="btn btn-outline-info waves-effect ml-auto" data-dismiss="modal">Close</button>
                                </div>
                    -->
                                                        </div>
                                                        <!--/.Panel 7-->
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/.Content-->
                                        </div>
                                    </div>
                                </div>
                                <!--Modal: Login / Register Form-->

                                @endif

                                <a data-toggle="modal" data-target="#viewdetails{{$booking->multi_id}}" id="dssfcsdfc" class="btn btn-success btn-sm" style="color: black;">Order Details</a>

                                <!--Modal: Login / Register Form-->
                                <div class="modal fade" id="viewdetails{{$booking->multi_id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog cascading-modal aaaaaa" role="document" style="">
                                        <!--Content-->
                                        <div class="modal-content">
                                            <!--Modal cascading tabs-->
                                            <div class="modal-c-tabs">
                                                <!-- Nav tabs -->
                                                <!-- <ul class="nav nav-tabs md-tabs tabs-2 light-blue darken-3" role="tablist" style="padding: 10px;border-radius: 7px;">
                                   <li class="nav-item">
                                     <span class="nav-link active" data-toggle="tab" href="#panel7" role="tab" style="background-color: #025f97;color: white;border-color: #025f97;">
                                     Order Details</span>
                                   </li>
                                  
                                 </ul>-->
                                                <br />
                                                <!-- Tab panels -->
                                                <div class="tab-content" style="padding: 0 0 0 0;">
                                                    <!--Panel 7-->
                                                    <div class="tab-pane fade in show active" id="panel7" role="tabpanel">
                                                        <!--Body-->
                                                        <div class="modal-body mb-1">
                                                            <div class="md-form form-sm mb-4" style="margin-top: 0;">
                                                                @foreach($rr as $aas)
                                                                <img src="small_product_image/{{$aas->image}}" alt="" style="height: 75px;" /><br />
                                                                @endforeach<br />
                                                                Order ID : {{$booking->order_id}}<br />

                                                                {{$booking->product_name}}
                                                            </div>
                                                            @php($addressdetails=DB::table('addres')->where('id',$booking->address_id)->first())
                                                            <b style="font-size: 16px; font-weight: bold;"> Delivery Address </b> <br />
                                                            <b>{{$addressdetails->name}}</b>, {{$addressdetails->phone_no }}, @if(strlen($addressdetails->flat)>0) {{$addressdetails->flat }},@endif
                                                            @if(strlen($addressdetails->location)>0){{$addressdetails->location }},@endif @if(strlen($addressdetails->landmark)>0){{$addressdetails->landmark }},@endif {{$addressdetails->city
                                                            }}, {{$addressdetails->district }}, {{$addressdetails->state }}-{{$addressdetails->pincode }} <br />
                                                            <br />
                                                            <b style="font-size: 16px; font-weight: bold;">
                                                                Payment Type : @if($booking->wallet_amount!=0 && $booking->payment_type!='wallet') Wallet and @endif {{$booking->payment_type}} @if($booking->payment_type=='online')
                                                                <span style="font-size: 13px;">({{$booking->razorpay_payment_id}})</span>@endif
                                                            </b>
                                                            <br />
                                                            <a target="_blank" href="invoice?id={{$booking->booking_id}}" class="btn btn-primary btn-sm">Download Invoice</a>
                                                            <br />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>
<!-- ========== END MAIN CONTENT ========== -->

<script>
    function account(aa, bb, cc) {
        $("#overlay").fadeIn(300);
        $.ajax({
            type: "post",
            url: "account",
            data: { _token: cc, bb: bb, aa: aa },
            success: function (date) {
                $("#ads").html(date);
                $("#overlay").fadeOut(300);
            },
        });
    }
</script>

@endsection
