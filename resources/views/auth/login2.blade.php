
@extends('layouts.menu')
@section('title')
User Login : E-Kirana
@endsection

@section('content')
<style>
  .partitioned {
    margin: 10px;
    padding-left: 15px;
    letter-spacing: 29px;
    border: 0;
    background-image: linear-gradient(to left, black 70%, rgba(255, 255, 255, 0) 0%);
    background-position: bottom;
    background-size: 36px 1px;
    background-repeat: repeat-x;
    background-position-x: 35px;
    width: 220px;
}
.newClass { 
    margin: 10px;
    padding-left: 15px;
    letter-spacing: 29px;
    border: 0;
    background-image: linear-gradient(to left, black 70%, rgba(255, 255, 255, 0) 0%);
    background-position: bottom;
    background-size: 36px 1px;
    background-repeat: repeat-x;
    background-position-x: 35px;
    width: 250px;
} 
</style>
<style>
    .login-btn{
    
      background-color: #3f8dda !important;
        color: white !important;
        font-size: large !important;
        font-weight: bold !important;
    
    }
    .login-btn:hover{
      background-color: #da251d !important;
        color: white !important;
        font-size: large !important;
        font-weight: bold !important;
    
    }
    </style>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<div class="breadcrumb section pt-60 pb-60">
    <div class="container">
      <h1 class="uppercase">Login</h1>
      <ul>
        <li><a href="/"><i class="fa fa-home"></i></a></li>
        <li class="active">Login</li>
      </ul>
    </div>
  </div>
  <!-- =====  BREADCRUMB END===== -->
  <div class="page-login section">
  <!-- =====  CONTAINER START  ===== -->
  <div class="container">
    <div class="row ">
      <div class="col-12 my-3">
        <div class="row">
          
          <!-- Login -->
          <div class="col-md-6 col-12 d-flex">

              <div class="login">
                  
                  <h3>Login to your account</h3>                   
                  <!-- Login Form -->
                  <form class="my-4" method="POST" action="{{ route('loginWithOtp') }}">
                    <div class="form-group" id="mobile1">

                      <input type="text" class="form-control" id="mobile" name="mobile" aria-describedby="exampleInputEmail1" placeholder="Mobile Number (10-digit) / Email Address">
                    </div>
                    <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}" >
                    <input type="hidden" name="role" value="USER">
                    <input type="hidden" value="" id="otp_value">

                    <div class="form-group" id="otp2">
                        <input class="partitioned" type="text" ng-minlength="6" maxlength="6" autocomplete="off"  onkeyup="matchotp(this.value);" onchange="matchotp(this.value);" id="otp" name="otp"   />
                    </div>
                    <span id="ps" style="color:red"></span>
                    <div class="" id="send-otp">
                        <a type="submit" class="btn btn-default btn-lg form-control login-btn" onClick="sendOtp()">Send OTP</a>
                      </div>
                     
                    <div class="row">
                        <div class="col-6">
                            <button type="submit" class="btn btn-default btn-lg form-control login-btn" id="as" >Login</button>
                        </div>
                        <div class="col-6">
                            <button type="submit" class="btn btn-default btn-lg form-control login-btn"  id="resend-otp" onClick="sendOtp()" style="">Resend OTP</button>
                        </div>
                    </div>
                  </form>

                  <h4>Don’t have account? <a href="/register">Register now</a></h4>
                  
              </div>
          </div>

          
          <!-- Login With Social -->
          <div class="col-md-6 col-12 d-flex">
              
              <div class="social-login">
                  <h3>Also you can login with...</h3>
                  
                  <a href="#" class="facebook-login">Login with <i class="fab fa-facebook-f"></i></a>
                  <a href="#" class="twitter-login">Login with <i class="fab fa-twitter"></i></a>
                  <a href="#" class="google-plus-login">Login with <i class="fab fa-google-plus"></i></a>
                  
              </div>
              
          </div>
          
      </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
 $(document).ready(function(){
    document.getElementById("as").style.display = "none";
    document.getElementById("otp2").style.display = "none";
    document.getElementById("resend-otp").style.display = "none";
});

function sendOtp() {
       
       $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           }
       });

       // alert($('#mobile').val());
       $.ajax( {
           url:'sendOtp',
           type:'post',
           data: {'mobile': $('#mobile').val()},
           success:function(data) {
               var data1 = data.split("|");
               if(data1[0] != 0){
                   document.getElementById("send-otp").style.display = "none";
                   document.getElementById("resend-otp").style.display = "block";
                   document.getElementById("otp2").style.display = "block";
                   document.getElementById("otp_value").value=data1[1];
                   document.getElementById("as").style.display = "block";
                   document.getElementById("mobile1").style.display = "none";
                   document.getElementById("as").disabled = true;
                  
                  
                  
               }else{
                   if(data1[2] != 1)
                   {
                    swal("Mobile No not found!");
                   }
                   else if(data1[2] != 2){
                    swal("Email Id not found!");
                   }
                 
                  
               }

           },
           error:function () {
               console.log('error');
           }
       });
   }


function matchotp(otp){ 
    var len=otp.length;
    var div = document.getElementById('otp'); 
if(len==6)
{
                 div.classList.add("newClass"); 
                 div.classList.remove("partitioned"); 
                 document.body.addEventListener('click', fn, true);
               
                  
             
}
 }

 
 function matchotp(otp){ 
            var orotp=document.getElementById("otp_value").value
            var len=otp.length;
            var div = document.getElementById('otp'); 
            if(len==6)
            {
            if(otp==orotp)
            {
                document.getElementById("as").disabled = false;
                document.getElementById("ps").innerHTML = "";
                div.classList.add("newClass"); 
                div.classList.remove("partitioned"); 
               
            }else
            {
                document.getElementById("ps").innerHTML = "Please enter valid OTP!";
                document.getElementById("as").disabled = true;
              
                div.classList.add("newClass"); 
                div.classList.remove("partitioned"); 
                
                
            }
            }
            else if(len>6)
            {
                document.getElementById("ps").innerHTML = "Please enter valid OTP!";  
                document.getElementById("as").disabled = true;  
              
                document.getElementById('mobile').readOnly = false;  
              
            }
            else
            {
                document.getElementById("ps").innerHTML = "Please enter valid OTP!";  
                document.getElementById("as").disabled = true; 
               
                div.classList.remove("newClass"); 
                 div.classList.add("partitioned"); 
              
              
            }
      }




</script>