@extends('layouts.menu')
@section('title')
User Registration : E-Kirana
@endsection
@section('content')
<style>
.login-btn{

  background-color: #3f8dda !important;
    color: white !important;
    font-size: large !important;
    font-weight: bold !important;

}
.login-btn:hover{
  background-color: #da251d !important;
    color: white !important;
    font-size: large !important;
    font-weight: bold !important;

}
</style>

<div class="breadcrumb section pt-60 pb-60 mb-30">
    <div class="container">
      <h1 class="uppercase">Register</h1>
      <ul>
        <li><a href="/"><i class="fa fa-home"></i></a></li>
        <li class="active">Register</li>
      </ul>
    </div>
  </div>
  <!-- =====  BREADCRUMB END===== -->
  <div class="page-login section">
  <!-- =====  CONTAINER START  ===== -->
  <div class="container">
    <div class="row ">
      <div class="col-12 my-4">
        <div class="row">
          
          <!-- Login -->
          <div class="col-md-6 col-12 d-flex">

              <div class="register">
                  
                  <h3>Registration to your account</h3> 

                  <!-- Login Form -->
                  <form class="my-4" method="POST" action="{{ route('register11') }}">
                    <div class="form-group">
                      <input type="text" class="form-control" aria-describedby="exampleInputtext" id="name" name="name" placeholder="Enter your name">
                      <div class="text-danger"><strong class="error" id="name_error"></strong></div>
                    </div>
                    <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}" >
                    <input type="hidden" name="role" value="USER">
                   
                    <div class="form-group">
                      <input type="email" class="form-control" aria-describedby="exampleInputemail" id="email" name="email" placeholder="Enter your email">
                      <div class="text-danger"><strong class="error" id="email_error"></strong></div>
                    </div>
               
                      <div class="form-group">
                        <input type="number" class="form-control" aria-describedby="exampleInputemail" id="mobile" name="mobile" placeholder="Enter your Phone No">
                        <div class="text-danger"><strong class="error" id="mobile_error"></strong></div>
                    </div>
                   

                      <input type="hidden" value="" id="otp_value"> 
                      <div class="form-group" id="otp2">
                        <input type="number" class="form-control" aria-describedby="exampleInputemail" id="otp" name="otp" onkeyup="matchotp(this.value);" onchange="matchotp(this.value);" placeholder="Enter OTP">
                      </div>
                      <span id="ps" style="color:red"></span>
                      <div class="text-danger"><strong  id="otp_error"></strong></div>
                      <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="terms">
                        <label class="form-check-label" for="terms">Terms & conditions</label>
                      </div>
                      <div class="text-danger"><strong class="error" id="terms_error"></strong></div>
                      <div class="form-group input-group-btn" id="send-otp">
                        <a type="submit" class="btn btn-default btn-lg form-control login-btn" onClick="sendOtp()">Sendotp</a>
                      </div>
                     
                    <div class="row">
                        <div class="col-6">
                            <button type="submit" class="btn btn-default btn-lg form-control login-btn" id="as" >Register</button>
                        </div>
                        <div class="col-6">
                            <a type="submit" class="btn btn-default btn-lg form-control login-btn"  id="resend-otp">Resend OTP</a>
                        </div>
                    </div>
                  </form>

                  <h4>You have account? <a href="/login">Login Now</a></h4>
                  
              </div>
          </div>

          
          <!-- Login With Social -->
          <div class="col-md-6 col-12 d-flex">
              
            <div class="account-image">
                  <h3>Upload your Image</h3>
                  
                  <img src="/use_assets/images/account-image-placeholder.png" alt="Account Image Placeholder" class="image-placeholder">
                  
                  <div class="account-image-upload">
                      <input type="file" name="chooseFile" id="account-image-upload">
                      <label class="account-image-label" for="account-image-upload">Choose your image</label>
                  </div>
                  
                  <p>jpEG 250x250</p>
                  
              </div>
              
          </div>
          
      </div>

      </div>
    </div>
  </div>
</div>
</div>
@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
  $(document).ready(function(){
    
  
            document.getElementById("as").style.display = "none";
            document.getElementById("otp2").style.display = "none";
            document.getElementById("resend-otp").style.display = "none";

  });



    function sendOtp()
        {
        
          var name =$('#name').val();
          var email =$('#email').val();
          var mobile =$('#mobile').val();
       
          var terms =document.getElementById('terms').checked;
          var token =$('#_token').val();
      
         
            $.ajax({
            url: "{{ route('user_register_otp') }}",
            type: 'POST',
            data: {name:name,_token:token,email:email,mobile:mobile,terms:terms},
            success: function (response) {  
              
                if(response.success == true){
                  $('.error').html('');
                   document.getElementById("send-otp").style.display = "none";
                   document.getElementById("resend-otp").style.display = "block";
                   document.getElementById("otp2").style.display = "block";
                   document.getElementById("otp_value").value=response.otp11;
                   document.getElementById("as").style.display = "block";
                   document.getElementById("as").disabled = true; 

                    mdtoast(response.msg, { 
                        type: 'ewrty',
                        duration: 3000
                        });
                }

            },error: function (jqXHR) {
                $("#update_bttn").prop('disabled', false);
              
                var errormsg = jQuery.parseJSON(jqXHR.responseText);
                $.each(errormsg.errors,function(key,value) {
                    $('#'+key+'_error').html(value);
                });
            }
        });
       
        }
          
   


  function matchotp(otp){ 
            var orotp=document.getElementById("otp_value").value
            var len=otp.length;
            if(len==6)
            {
            if(otp==orotp)
            {
                document.getElementById("as").disabled = false;
                document.getElementById("ps").innerHTML = "";
                document.getElementById('name').readOnly = true;
                document.getElementById('email').readOnly = true;
                document.getElementById('mobile').readOnly = true; 
                document.getElementById('terms').disabled=true; 
            }else
            {
                document.getElementById("ps").innerHTML = "Please enter valid OTP!";
                document.getElementById("as").disabled = true;
                document.getElementById('name').readOnly = false;
                document.getElementById('email').readOnly = false;
                document.getElementById('mobile').readOnly = false; 
                document.getElementById('terms').disabled=false;
            }
            }
            else if(len>6)
            {
                document.getElementById("ps").innerHTML = "Please enter valid OTP!";  
                document.getElementById("as").disabled = true;  
                document.getElementById('name').readOnly = false;
                document.getElementById('email').readOnly = false;
                document.getElementById('mobile').readOnly = false;  
                document.getElementById('terms').disabled=false;
            }
            else
            {
                document.getElementById("ps").innerHTML = "Please enter valid OTP!";  
                document.getElementById("as").disabled = true; 
                document.getElementById('name').readOnly = false;
                document.getElementById('email').readOnly = false;
                document.getElementById('mobile').readOnly = false;  
                document.getElementById('terms').disabled=false;
              
            }
      }


</script>