    
@extends('layouts.menu')
@section('title')
User Register : ALIYA Online Services
@endsection
@section('content')
    <!-- ========== MAIN CONTENT ========== -->
    <main id="content" role="main">
        <!-- breadcrumb -->
        <div class="bg-gray-13 bg-md-transparent">
            <div class="container">
                <!-- breadcrumb -->
                <div class="my-md-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                            <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="/">Home</a></li>
                            <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">My Account</li>
                        </ol>
                    </nav>
                </div>
                <!-- End breadcrumb -->
            </div>
        </div>
        <!-- End breadcrumb -->

        <div class="container">
            {{-- <div class="mb-4" card>
                <h1 class="text-center">My Account</h1>
            </div> --}}
            <div class="my-4 my-xl-8">
                <div class="row">
                    <div class="col-md-4 ">
                    </div>
                    <div class="col-md-4  card">
                     
                        <div class="border-bottom border-color-1 mb-6">
                            <h3 class="d-inline-block section-title mb-0 pb-2 font-size-26"> <b>Signup</b></h3>
                        </div>
                        {{-- <p class="text-gray-90 mb-4">Create new account today to reap the benefits of a personalized shopping experience.</p> --}}
                        <form id="register_button_form" action="{{ route('register11') }}">
                            <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}" >
                            <div class="js-form-message form-group mb-5">
                                <label class="form-label" for="RegisterSrEmailExample3">Enter Mobile Number
                                    <span class="text-danger">*</span>
                                </label> 
                                <input type="tel" maxlength="10"  class="form-control" name="mobile" id="mobile" placeholder="Mobile Number" aria-label="Email address" required autocomplete="off"
                                data-msg="Please enter a valid email address."
                                data-error-class="u-has-error"
                                data-success-class="u-has-success">
                                <div class="text-danger"><strong class="error" id="mobile_error"></strong></div>

                            </div>

                            <div class="js-form-message form-group mb-5" id="set_password">
                                <label class="form-label" for="RegisterSrEmailExample3">Set Password
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="password" class="form-control" name="password" id="password" placeholder="Set Password" aria-label="Email address" 
                                data-msg="Please enter a valid email address."
                                data-error-class="u-has-error"
                                data-success-class="u-has-success">
                                <div class="text-danger"><strong class="error" id="password_error1"></strong></div>

                            </div>

                            <input type="hidden" value="" id="otp_value"> 
                            <input type="hidden" name="role" value="USER">
                            <div class="js-form-message form-group mb-5" id="send_otp">
                                <label class="form-label" for="RegisterSrEmailExample3">Enter OTP
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="text" class="form-control"  placeholder="Enter OTP" aria-label="Email address" required
                                data-msg="Please enter a valid email address."
                                data-error-class="u-has-error"
                                data-success-class="u-has-success" id="otp" name="otp" onkeyup="matchotp(this.value);" onchange="matchotp(this.value);">
                            </div>

                            <span id="ps" style="color:red"></span>

                            <div class="mb-6" id="register">
                                <div class="mb-3">
                                    {{-- <button  type="submit" class="btn btn-primary-dark-w px-5 form-control" id="register_button">Register</button> --}}
                                   
                                    <div class="row"><div class="col-md-6">  <input type="submit" class="btn btn-primary-dark-w px-5 form-control" id="register_button" value="Register"></div><div class="col-md-6">  <a  class="btn btn-primary form-control" onClick="sendOtp();" >Resend OTP</a></div></div>
                                    <center><b> OR <b></center>
                                    <center> Existing User?<a href="/login"> Login</a></center>
                                </div>
                            </div>

                       
                           
                          
                        </form>

                        <div class="mb-6" id="send_otp_button">
                            <div class="mb-3">
                                <div class="row"><div class="col-md-12"> <a class="btn btn-primary form-control" onClick="sendOtp();" >Send OTP</a></div></div>
                                <center><b> OR <b></center>
                                <center> Existing User?<a href="/login"> Login</a></center>
                          
                            </div>
                        </div>
                        {{-- <h3 class="font-size-18 mb-3">New to Aliya? <a href="#">Create an account</a></h3> --}}
                        {{-- <ul class="list-group list-group-borderless">
                            <li class="list-group-item px-0"><i class="fas fa-check mr-2 text-green font-size-16"></i> Speed your way through checkout</li>
                            <li class="list-group-item px-0"><i class="fas fa-check mr-2 text-green font-size-16"></i> Track your orders easily</li>
                            <li class="list-group-item px-0"><i class="fas fa-check mr-2 text-green font-size-16"></i> Keep a record of all your purchases</li>
                        </ul> --}}
                    </div> 
                    <div class="col-md-4 ">
                    </div>
                </div>
            </div>
        </div>
    </main>
    @endsection
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        $(document).ready(function(){
            document.getElementById("send_otp").style.display = "none";
            document.getElementById("set_password").style.display = "none";
            document.getElementById("register").style.display = "none";
        });

            $(document).ready(function(){
                $('#register_button_form').submit(function(event){
                    event.preventDefault();
                    var formdata = new FormData($(this)[0]);
                    $.ajax({
                      url: $(this).attr('action'),
                      type: 'POST',
                      dataType: 'json',
                      processData: false,
                      contentType: false,
                      cache:false,
                      data: formdata,
                      success: function (response) {   // success callback functiondelete_logo
                        if(response.success == true){
                            // $('#myModal').modal('hide');
                            mdtoast(response.msg, { 
                            type: 'ewrty',
                            duration: 3000
                            });

                            setTimeout(function(){ window.location.href = "/" }, 2000);
                        }
                      },error: function (jqXHR) {
                        // $('#myModal').modal('show');
                        // buttonEnabled(btnid);
                          var errormsg = jQuery.parseJSON(jqXHR.responseText);
                          $.each(errormsg.errors,function(key,value) {
                              $('#'+key+'_error1').html(value);
                          });
                      }
                    });
                });
            });


        function sendOtp()
        {
          var mobile =$('#mobile').val();
          var token =$('#_token').val();
            $.ajax({
            url: "{{ route('user_register_otp') }}",
            type: 'POST',
            data: {_token:token,mobile:mobile},
            success: function (response) {  
                if(response.success == true){
                  $('.error').html('');
                   document.getElementById("send_otp").style.display = "block";
                   document.getElementById("set_password").style.display = "block";
                   document.getElementById("register").style.display = "block";
                   document.getElementById("otp_value").value=response.otp11;
                   document.getElementById("send_otp_button").style.display = "none";
                   document.getElementById("register_button").disabled = true; 
                   document.getElementById('mobile').readOnly = false; 
                    mdtoast(response.message, { 
                        type: 'ewrty',
                        duration: 3000
                        });
                }

            },error: function (jqXHR) {
                $("#update_bttn").prop('disabled', false);
              
                var errormsg = jQuery.parseJSON(jqXHR.responseText);
                $.each(errormsg.errors,function(key,value) {
                    $('#'+key+'_error').html(value);
                });
            }
        });
       
        }



        function matchotp(otp){ 
            var orotp=document.getElementById("otp_value").value
            var len=otp.length;
            if(len==6)
            {
            if(otp==orotp)
            {
                document.getElementById("register_button").disabled = false;
                document.getElementById("ps").innerHTML = "";
                document.getElementById('mobile').readOnly = true; 
            }else
            {
                document.getElementById("ps").innerHTML = "Please enter valid OTP!";
                document.getElementById("register_button").disabled = true;
                document.getElementById('mobile').readOnly = false; 
              
            }
            }
            else if(len>6)
            {
                document.getElementById("ps").innerHTML = "Please enter valid OTP!";  
                document.getElementById("register_button").disabled = true;  
                document.getElementById('mobile').readOnly = false;  
              
            }
            else
            {
                document.getElementById("ps").innerHTML = "Please enter valid OTP!";  
                document.getElementById("register_button").disabled = true; 
                document.getElementById('mobile').readOnly = false; 
            }
      }
     
    </script>
