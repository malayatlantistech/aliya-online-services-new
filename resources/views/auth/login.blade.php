    
@extends('layouts.menu')
@section('title')
User Login : ALIYA Online Services
@endsection
@section('content')
    <!-- ========== MAIN CONTENT ========== -->
    <main id="content" role="main">
        <!-- breadcrumb -->
        <div class="bg-gray-13 bg-md-transparent">
            <div class="container">
                <!-- breadcrumb -->
                <div class="my-md-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                            <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="/">Home</a></li>
                            <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">My Account</li>
                        </ol>
                    </nav>
                </div>
                <!-- End breadcrumb -->
            </div>
        </div>
        <!-- End breadcrumb -->

        <div class="container">
            {{-- <div class="mb-4" card>
                <h1 class="text-center">My Account</h1>
            </div> --}}
            <div class="my-4 my-xl-8">
                <div class="row">
                    <div class="col-md-4 ">
                    </div>
                    <div class="col-md-4  card">
                     
                        <div class="border-bottom border-color-1 mb-6">
                            <h3 class="d-inline-block section-title mb-0 pb-2 font-size-26"><b>Login</b> </h3>
                        </div>
                        @if ($message = Session::get('error'))
                        <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                                <strong>{{ $message }}</strong>
                        </div>
                        @endif
                        <form  method="POST" action="{{ route('login1') }}">
                            <div class="js-form-message form-group mb-5">
                                <label class="form-label" for="RegisterSrEmailExample3">Enter Email/Mobile Number
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="text"  class="form-control" name="mobile" id="mobile" placeholder="Enter Email/Mobile Number" aria-label="Email address" required autocomplete="off"
                                data-msg="Please enter a valid email address."
                                data-error-class="u-has-error"
                                data-success-class="u-has-success">
                            </div>
                            <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}" >

                            <div class="js-form-message form-group mb-5" id="password-div">
                                <label class="form-label" for="RegisterSrEmailExample3">Enter Password
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="password" class="form-control" name="password" id="password" placeholder="Enter Password" aria-label="Email address" required
                                data-msg="Please enter a valid email address."
                                data-error-class="u-has-error"
                                data-success-class="u-has-success">
                            </div>

                            <div class="js-form-message form-group mb-5" id="otp-div">
                                <label class="form-label" for="RegisterSrEmailExample3">Enter OTP
                                    <span class="text-danger">*</span>
                                </label>
                                <input class="form-control" type="text" ng-minlength="6" maxlength="6" autocomplete="off" placeholder="Enter OTP"  onkeyup="matchotp(this.value);" onchange="matchotp(this.value);" id="otp" name="otp"   />
                                <span id="ps" style="color:red"></span>
                            </div>
                            <input type="hidden" value="" id="otp_value">
                            <div class="js-form-message mb-3">
                                <div class="custom-control custom-checkbox d-flex align-items-center">
                                    <input type="checkbox" class="custom-control-input" id="checkbox" name="checkbox" onchange='handleChange(this);'>
                                    <label class="custom-control-label form-label" for="checkbox">
                                       Login With OTP
                                    </label>
                                </div>
                            </div>
                            {{-- <p class="text-gray-90 mb-4">Your personal data will be used to support your experience throughout this website, to manage your account, and for other purposes described in our <a href="#" class="text-blue">privacy policy.</a></p> --}}
                            <div class="mb-6">
                                <div class="mb-3">
                                    <button type="submit" class="btn btn-primary px-5 form-control" id="login">Login</button>
                              
                                    <center><b>    OR <b></center>
                                    <button type="button" class="btn btn-primary px-5 form-control" id="send_otp_button" onClick="sendOtp()">Request OTP</button>
                                    <div id="resend_otp_button">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <button type="button" class="btn btn-primary px-5 form-control"  onClick="loginOtp()">Login</button>
                                        </div>
                                        <div class="col-md-6">
                                            <button type="button" class="btn btn-primary px-5 form-control"  onClick="sendOtp()">Resend</button>
                                        </div>
                                    </div>
                                    <div>

                                    
                              
                                </div>
                            </div>
                          
                        </form>
                      
                        <h5 >  <br>   New to Aliya? <a href="/register" class="text-success">Create an account</a></h5>
                        {{-- <ul class="list-group list-group-borderless">
                            <li class="list-group-item px-0"><i class="fas fa-check mr-2 text-green font-size-16"></i> Speed your way through checkout</li>
                            <li class="list-group-item px-0"><i class="fas fa-check mr-2 text-green font-size-16"></i> Track your orders easily</li>
                            <li class="list-group-item px-0"><i class="fas fa-check mr-2 text-green font-size-16"></i> Keep a record of all your purchases</li>
                        </ul> --}}
                    </div> 
                    <div class="col-md-4 ">
                    </div>
                </div>
            </div>
        </div>
    </main>
    @endsection

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        $(document).ready(function(){
            document.getElementById("send_otp_button").disabled = true; 
            document.getElementById("otp-div"). style.display = "none"; 
            document.getElementById("resend_otp_button"). style.display = "none"; 

            
        });

        function handleChange(checkbox) {
            if(checkbox.checked == true){
                document.getElementById('password').readOnly = true; 
                document.getElementById("send_otp_button").disabled = false;
                document.getElementById("login").disabled = true;
            }else{
                document.getElementById('password').readOnly = false; 
                document.getElementById("send_otp_button").disabled = true;
                document.getElementById("login").disabled = false;
                document.getElementById("otp-div"). style.display = "none"; 
                document.getElementById("password-div"). style.display = "block"; 
                document.getElementById("send_otp_button"). style.display = "block"; 
                document.getElementById("resend_otp_button"). style.display = "none"; 


            }
        }

    function sendOtp() {
       $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           }
       });


       $.ajax( {
           url:'sendOtp',
           type:'post',
           data: {'mobile': $('#mobile').val()},
           success:function(data) {
               var data1 = data.split("|");
               if(data1[0] != 0){
                      document.getElementById("otp-div"). style.display = "block"; 
                      document.getElementById("password-div"). style.display = "none"; 
                      document.getElementById("send_otp_button"). style.display = "none"; 
                      document.getElementById("resend_otp_button"). style.display = "block"; 

                      document.getElementById("otp_value").value=data1[1];
               
               }else{
                   if(data1[2] != 1)
                   {
                    // swal("Mobile No not found!");
                    mdtoast('Mobile Number not found!', { 
                            type: 'ewrty',
                            duration: 3000
                            });
                   }
                   else if(data1[2] != 2){
                    
                    mdtoast('Email Id not found!', { 
                            type: 'ewrty',
                            duration: 3000
                            });
                   }
               }

           },
           error:function () {
               console.log('error');
           }
       });
   }


   function matchotp(otp){ 
            var orotp=document.getElementById("otp_value").value
            var len=otp.length;

            
            if(len==6)
            {
            if(otp==orotp)
            {
               
                document.getElementById("ps").innerHTML = "";
              
            }else
            {
                document.getElementById("ps").innerHTML = "Please enter valid OTP!";
         
            }
            }
            else if(len>6)
            {
                document.getElementById("ps").innerHTML = "Please enter valid OTP!";  
                 
            }
            else
            {
                document.getElementById("ps").innerHTML = "Please enter valid OTP!";  
               
            }
      }


      
      function loginOtp() {
       
       $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           }
       });

     var mo=$('#mobile').val();
       if(mo!=''){
       $.ajax( {
           url:'loginWithOtp',
           type:'post',
           data: {'mobile': $('#mobile').val(),'otp':$('#otp').val()},
           success:function(response) {
            if(response.success == true){
                mdtoast(response.msg, { 
                            type: 'ewrty',
                            duration: 3000
                            });

                setTimeout(function(){ window.location.href = "/" }, 2000);
            }

           },
           error:function () {
               console.log('error');
           }
       });
       }else{
                mdtoast('Email Id/Phone Number is required', { 
                            type: 'ewrty',
                            duration: 3000
                            });
                   }

       }

    </script>