@extends('layouts.menu')

@section('title')
{{$product_details->product_name}} : E-Kirana
@endsection


@section('content')
<style>
.group-product:hover{
    border: solid 1px;
    border-radius: 0;
}
.pincheck button{
    font-weight: 500;
    border: none;
    background: #3f8dda;
    color: #fff;
    font-size: 16px;
    padding: 7px 40px;
 
    transition: 0.5s;
}
.pincheck button:hover{
    font-weight: 500;
    border: none;
    background: #da251d;
    color: #fff;
    font-size: 16px;
    padding: 7px 40px;
 
    transition: 0.5s;
}
.prodetail.caption #product .button-group .wishlist span, .prodetail.caption #product .button-group .compare span {

    padding: 10px !important;
}
.message_btn{

}
.qty_value:hover{
color:black;
}
.carousel-opacity:hover{
    background-color: #818686ab;
}
.review_span{
  background-color: #3f8dda;
    padding: 6px 7px 3px 7px;
    border-radius: 6px;
    font-size: 13px;
    color: white;
    font-weight: 900;
}
@media (min-width: 576px){
  .modal-dialog {
    max-width: fit-content !important;
    margin: 1.75rem auto;
}
}

</style>

    <!-- =====  BREADCRUMB STRAT  ===== -->
    <div class="breadcrumb section pt-60 pb-60 mb-30">
        <div class="container">
          <h1 class="uppercase">{{$product_details->product_name}}</h1>
          <ul>
            <li><a href="/"><i class="fa fa-home"></i></a></li>
            <li class="active">{{$product_details->product_name}}</li>
          </ul>
        </div>
      </div>
  
      <!-- =====  BREADCRUMB END===== -->
  
      <div class="product-section section">
      <!-- =====  CONTAINER START  ===== -->
        <div class="container">
          <div class="row">
            <div class="col-12 mb-20">
              <div class="row mt_10 ">
                <div class="col-md-5">
                  <div>
                      
                   {{-- <a class="thumbnails"> <img data-name="product_image" src="/use_assets/images/pro/001.jpg" alt="" /></a> --}}
                
                   <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">

                        @foreach($product_image as $key=>$product_image1)
                 
                      
                          <div class="carousel-item @if($key==0){{'active'}}@endif">
                            <img class="d-block w-100" src="/product_image/{{$product_image1->image}}" alt="First slide">
                          </div>
                       @endforeach

                    </div>
                    <a class="carousel-control-prev carousel-opacity" href="#carouselExampleControls" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next carousel-opacity" href="#carouselExampleControls" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
                  </div> 
                
                </div>
                  <div id="product-thumbnail" class="owl-carousel">
                    @foreach($product_image as $product_image1)
                 
                    <div class="item">
                        <div class="image-additional"><a class="thumbnail" href="/product_image/{{$product_image1->image}}" data-fancybox="group1"> <img src="/product_image/{{$product_image1->image}}" alt="" /></a></div>
                      </div>
                   @endforeach
                   
                    
                  </div>
                </div>
                <div class="col-md-7 prodetail caption">
                  <h4 data-name="product_name" class="product-name"><a href="#" title="Casual Shirt With Ruffle Hem">{{$product_details->product_name}}@if($product_details->weight!=0)(@if($product_details->weight<999){{$product_details->weight}}gm) @else{{$product_details->weight/1000}}kg) @endif @endif</a></h4>
                    <div class="rating mt-5 mb-5"> Brand : <a href="/shop?brand={{$product_details->brand_id}}">{{$product_details->brand_name}}</a> | 
                    @php($a=(int)$product_details->review) {{$a}}
                        @for($i=0;$i<$a;$i++)
                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i></span>
                        @endfor
                        @for($i=0;$i<5-$a;$i++)
                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-x"></i></span> 
                        @endfor
                        

                        ({{$product_details->total_review}} reviews)
                    </div>
                
                  </span>
                  <hr>
                  @php($progst=$product_details->selling_price*$sub_cat->gst/100)
                  <span class="price mb-20"><span class="amount" style="font-size: 24px;color:black;font-weight: 700;">
                    <span class="currencySymbol">₹</span>{{round($product_details->selling_price+$progst)}} </span> 
                     <span style="font-size: 16px;"><s><span class="currencySymbol">₹</span>{{$product_details->mrp}}</span></s>
                     @php($dis=(($product_details->mrp-$product_details->selling_price+$progst)/$product_details->mrp)*100)
                     <span style="font-size: 16px;color: #388e3c;    font-weight: 700;"><span class="currencySymbol">{{round($dis)}}</span>% off</span>
                     {{-- <a data-toggle="popover" title="Popover Header" data-content="Some content inside the popover"><i class="fa fa-info-circle"></i></a> --}}
                     <div>

                     </div>

                     @if($product_details->group_id!=0)
                      
                            <div style="    padding-top: 17px;">
                                <label>Others Pack Size</label>
                                <div class="row" >
                                    @foreach($group_product as $group_product)
                                        @if($group_product->product_id!=$product_details->product_id)
                                        @php($image=DB::table('product_images')->where('product_id',$group_product->product_id)->first())
                                        
                                            <div class="product-thumb group-product" data-toggle="tooltip" title="" data-original-title="@if($group_product->weight<999){{$group_product->weight}}gm @else {{$group_product->weight/1000}}kg @endif" style="width: 100px; margin:5px">
                                                <div class="item text-center"> 
                                                <a href="/product/{{$group_product->product_id}}"><img src="/product_image/{{$image->image}}" alt="{{$group_product->weight}}" class="img-responsive" />
                                                  
                                                    </a> 
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                   
                            
                                </div>
                            </div>
                       
                    @endif


                  <ul class="list-unstyled product_info my-3">
                   
                    <li><label>Product Code:</label>
                        <span> {{$product_details->product_code}}</span></li>
                        <li><label>HSN Code:</label>
                            <span> {{$product_details->hsn_code}}</span></li>
                    <li>
                      <label>Tags:</label>
                      <span> <a href="/shop?cat_id={{$cat->cat_id}}">{{$cat->cat_name}}</a> -> <a href="/shop?cat_id={{$cat->cat_id}}&sub_cat_id={{$sub_cat->sub_cat_id}}">{{$sub_cat->sub_cat_name}}</a> @if($product_details->sub_sub_cat_id!=0) -> <a href="/shop?cat_id={{$cat->cat_id}}&sub_cat_id={{$sub_cat->sub_cat_id}}&sub_sub_cat_id={{$sub_sub_cat->sub_sub_cat_id}}">{{$sub_sub_cat->sub_sub_cat_name}}</a> @endif</span>
                    
                    </li>
                  </ul>
                  <hr>
                <div class="pincheck ">

                    <label>Delivery</label><br>
                    <div class="row">
                        <div class="col-md-6">
                             <input name="pin" id="pin" min="1" Placeholder="Pincode" type="number" style="">
                    <button type="submit" class="btn btn-default btn-md" id="pincheck" data-toggle="tooltip" title="" data-original-title="Delivey Pincode Check">Check</button>
                        </div>
                        <div class="col-md-6" style="line-height:15px;padding-top: 3px;padding-left:0">
                            <span id="delivery_msg" style="font-weight: 700;font-size: 14px;"></span><br>
                            <span id="delivery_msg1" style="font-size: 12px;"></span>
                        </div>
                    </div>
                      
                        
                </div>
              
                    
                   <div id="product" style="margin-top:2%">
                       
                    <label>Qty</label><br>
                    <div class="qty form-group add-to-cart">
                      
                        
                            <button type="button" class="btn btn-primary" style="padding: 0.17rem 0.75rem;border-color: #c1bcbc;" onClick="down();">-</button>
                            <input type="text" id="quantity" readonly class="btn qty_value" value="1" style="padding: 7px 12px;color:black;border-color: #c1bcbc;background-color:white;    width: 60px;">
                            <button type="button" class="btn btn-primary" style="padding: 0.17rem 0.75rem;border-color: #c1bcbc;"  onClick="up();">+</button>
                       
                    </div>
                    <div class="button-group">
                     
                    <div id="addto_CartButton" class="add-to-cart" data-toggle="tooltip" title="" data-original-title="Add to Basket" >
                          <a href="javascript:void(0)" onclick="add_to_cart();"><span>Add</span></a>
                    </div>
                    <div id="goto_CartButton" class="add-to-cart" data-toggle="tooltip" title="" data-original-title="Go to Basket"  style="display:none;">
                        <a href="/cart"><span>Go</span></a>
                    </div>
                    
                    <div id="addto_WishButton" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wishlist">
                          <a  href="javascript:void(0)" onclick="add_to_wishlist();"><span><i class="fa fa-heart"></i></span></a>
                    </div>
                    <div id="removeto_WishButton" class="wishlist" data-toggle="tooltip" title="" data-original-title="Remove to Wishlist" style="display:none;">
                 
                            <a  href="javascript:void(0)" onclick="remove_to_wishlist();" ><span ><i class="fa fa-heart" style="color:red;"></i></span></a>
                    </div>
                  
                    </div>
                    <div class="prodbottominfo">
                      <ul class="list-unstyled">                    
                        <li data-toggle="tooltip" title="" data-original-title="Worldwide Shipping">
                          <img src="/use_assets/images/world.png" alt=""> 
                        </li>
                        <li data-toggle="tooltip" title="" data-original-title="100% Original Product">
                          <img src="/use_assets/images/original.png" alt=""> 
                        </li>
                        <li data-toggle="tooltip" title="" data-original-title="Best Price Guaranteed">
                          <img src="/use_assets/images/inquire.png" alt=""> 
                        </li>
                         <li data-toggle="tooltip" title="" data-original-title="COD Available in India">
                           <img src="/use_assets/images/cod.png" alt=""> 
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-12 my-5">
                  <ul class="nav nav-tabs mb-30" id="myTab" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link active" id="overview-tab" data-toggle="tab" href="#Overview" role="tab" aria-controls="Overview" aria-selected="true">Overview</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " id="Specification-tab" data-toggle="tab" href="#Specification" role="tab" aria-controls="Specification" aria-selected="true">Specification</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " id="Features-tab" data-toggle="tab" href="#Features" role="tab" aria-controls="Features" aria-selected="true">Features</a>
                    </li>
                  
                    <li class="nav-item">
                      <a class="nav-link" id="reviews-tab" data-toggle="tab" href="#Reviews" role="tab" aria-controls="reviews-tab" aria-selected="false">Reviews ({{$product_details->total_review}})</a>
                    </li>
            
                  </ul>
                  <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="Overview" role="tabpanel" aria-labelledby="Overview"><p>   <?php  echo $product_details->description; ?></p></div>
                    <div class="tab-pane fade show " id="Specification" role="tabpanel" aria-labelledby="Overview"><p>  
                      <table class="table table-hover">
                        <tbody>
                       @foreach($product_specification as $product_specification1)
                            <tr>
                                <th class="px-4 px-xl-5 border-top-0" style="width:30%">{{$product_specification1->title}}</th>
                                <td class="border-top-0">{{$product_specification1->description}}</td>
                            </tr>
                         @endforeach

                            
                        </tbody>
                    </table>
                      
                      </p></div>
                    <div class="tab-pane fade show " id="Features" role="tabpanel" aria-labelledby="Overview"><p> 
                      <ul style="list-style: circle;">
                        @foreach($product_feature as $product_feature)
                        <li>{{$product_feature->features}}</li>
                          
                        @endforeach

                      </ul>
                    
                      </p></div>


                    <div class="tab-pane fade" id="Reviews" role="tabpanel" aria-labelledby="Reviews">
                      @if($product_details->total_review>0)
               
                      @foreach($revie as $revie1)
                        <div id="review"></div>
                        <h4 class="mt-5 mb-10"><span class="review_span">{{$revie1->review}} <i class="fa fa-star" style="font-size:12px "></i></span> {{$revie1->review_title}}</h4>
                        <div class="form-group required">
                          @if($revie1->image!=null)
                           <img src="/review_image/{{$revie1->image}}" style="height:80px;cursor: pointer;" data-toggle="modal" data-target="#exampleModalCenterwww{{$revie1->review_id}}">
                              <div class="modal fade" id="exampleModalCenterwww{{$revie1->review_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                  <div class="modal-content">
                                    
                                    <div class="modal-body row" style="">
                                      <div class="">

                                      <img src="/review_image/{{$revie1->image}}" style="max-height: 600px;">
                                      </div>
                                      <div class="" style="    max-width: 320px;padding: 30px 25px 0 10px;">
                                        <h4 class="mt-5 mb-10"><span class="review_span">{{$revie1->review}} <i class="fa fa-star" style="font-size:12px "></i></span> {{$revie1->review_title}}</h4>
                                        <div class="form-group required">
                                          <p>{{$revie1->review_details}}</p>
                                        </div>
                                      </div>
                                    </div>
                                   
                                  </div>
                                </div>
                              </div>
                          @endif
                         <p>{{$revie1->review_details}}</p>
                        </div>
                        @php($dde=DB::table('users')->where('id',$revie1->user_id)->first())
                      <span style="    color: #8a8987;font-weight:800"><i class="fa fa-user"></i> {{$dde->name}}, {{date('d M Y', strtotime($revie1->updated_at))}}</span>
                        <hr style="margin-top:0px">
                      @endforeach


                      @else
                      No review Found
                      @endif
  
                    </div>
                        </div>
  
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="heading-part text-center mb-10">
                    <h3 class="section_title mt-50">Related Products</h3>
                  </div>
                  <div class="related_pro">
                    <div class="product-layout related-pro  owl-carousel mb-50 ">

                      @foreach($related_product_details as $related_product_details1) 
                      @php($product_id=$related_product_details1->product_id)
                      @php($image1=DB::table('product_images')->where('product_id',$product_id)->limit(1)->first())
                      @php($image2=DB::table('product_images')->where('product_id',$product_id)->limit(1)->orderby('product_image_id','desc')->first())
                      @php($brand=DB::table('brands')->where('brand_id',$related_product_details1->brand_id)->first())
                      @php($sub_cat=DB::table('sub_cats')->where('sub_cat_id',$related_product_details1->sub_cat_id)->first())
  
                      <div class="product-grid">
                        <div class="item">
                           <div class="product-thumb transition">
                              <div class="image">

                                <div class="first_image"> <a href="/product/{{$product_id}}"> <img src="/product_image/{{$image1->image}}" alt="pure-spice-3" title="{{$related_product_details1->product_name}}" class="img-responsive"> </a> </div>
                                <div class="swap_image"> <a href="/product/{{$product_id}}"> <img src="/product_image/{{$image2->image}}" alt="pure-spice-3" title="{{$related_product_details1->product_name}}" class="img-responsive"> </a></div>
                             
                              </div>
                              <div class="product-details">
                                <div class="caption">
                                  <h4><a href="/shop/brand={{$brand->brand_id}}">{{$brand->brand_name}}</a></h4>
                                  @php($progst=$related_product_details1->selling_price*$sub_cat->gst/100)
                       
                                  <p class="price">₹{{$related_product_details1->selling_price+$progst}}</p>
                               
                                  <div class="product_option">
                                    <div class="form-group required ">
                                      <a href="/product/{{$product_id}}"><?php echo substr($related_product_details1->product_name,0,20); ?>@if(strlen($related_product_details1->product_name)>20){{'...'}} @endif</a>
                                    </div>
                                    <div class="input-group button-group">
                                      <span style="font-size: 11px;">
                                      @php($a=(int)$related_product_details1->review)
                                      @for($i=0;$i<$a;$i++)
                                      <span class="" style="    width:  max-contant;"><i class="fa fa-star" style="color:#fa4251"></i></span>
                                      @endfor
                                      @for($i=0;$i<5-$a;$i++)
                                      <span class="" style="    width: max-contant;"><i class="fa fa-star"     style="color: #79797975;"></i></span> 
                                      @endfor
                                      
              
                                      ({{$related_product_details1->review}})
                                      </span>
                                      
                                      <button type="button" class="addtocart pull-right" onclick="cart({{$product_id}});">Add</button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                          </div>
                        </div>
                      </div>

                      @endforeach

                   
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
  
        <!-- =====  Brand start ===== -->
        <div id="brand_carouse" class="section text-center mt-30 pb-15">
          <div class="row">
            <div class="col-12">
              <div class="section_title">Our Popular Brands</div>
            </div>
            <div class="col-sm-12">
              <div class="brand owl-carousel">
                @php($brands=DB::table('brands')
                ->get())
@foreach($brands as $brands)
                <div class="product-thumb"><div class="item text-center"> <a href="/shop?brand={{$brands->brand_id}}"><img src="/brand_logo/{{$brands->brand_image}}" title="{{$brands->brand_name}}" alt="Disney" class="img-responsive" /></a> </div></div>
              
                @endforeach         
              </div>
            </div>
          </div>
        </div>
        <!-- =====  Brand end ===== -->
  
        </div>
      <!-- =====  CONTAINER END  ===== -->
      </div>
      @auth
      <input type="hidden" id="user_id" value="{{ Auth::user()->name }}">
      @else
      <input type="hidden" id="user_id" value="0">
      @endauth
      <input type="hidden" id="_token" value="<?php echo csrf_token();?>">
      <input type="hidden" id="product_id" value="{{$product_details->product_id}}">
@endsection
<script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous">
        </script>

      
<script>
       
$(document).ready(function(){

    var product_id=$("#product_id").val();
    carttoggle(product_id);
    wishtoggle(product_id);


$("#pincheck").click(function(){

var pin=$("#pin").val();

var token=$("#_token").val();
var product_id=$("#product_id").val();

if(pin.length==6){
    
    $.ajax({
        url:'/pinckeck',
        type:'POST',
        data:{pin:pin,_token:token,product_id:product_id},
        success:function(response)
        {
            if(response.success==false){
                $("#delivery_msg").html('Delivery Unavailable').css('color','red');
                $("#delivery_msg1").html('Change the Delivery Pncode').css('color','red');
               // swal("Delivary Unavailable!", "Please try another Pincode", "warning");
            //  $("#pinstatus").css("color", "red").css("weight", "bold").html("Not available in this location");
            }else{
              //  swal("Delivary available", "Delivary available in your Pincode!", "success");
                $("#pincheck").html('Change');
                $("#delivery_msg").html('Delivery Available').css('color','green');
                $("#delivery_msg1").html(response.msg).css('color','green');
            //  $("#pinstatus").css("color", "green").css("weight", "bold").html("Available in this location");
            }
        }
    });
}else if(pin.length==0){
// swal('Not a valid Pincode');
                $("#delivery_msg").html('Invalid Pincode').css('color','red');
                $("#delivery_msg1").html('Please Enter Delivery Pincode').css('color','red');
    

}else{
                    $("#delivery_msg").html('Invalid Pincode').css('color','red');
                $("#delivery_msg1").html('Please Check Delivery Pincode').css('color','red');
}
$("[data-toggle='tooltip']").tooltip('hide');
});
});


function  up()
        {
                var value1=$("#quantity").val();
                var value2=Number(value1)+1;
                $('#quantity').val(value2);        
        }

        function down()
        {
        // alert();
            var value1=$("#quantity").val();
            var value2=Number(value1)-1;
            if(value2<=1)
            {
                $('#quantity').val(1);
            }
            else{
                $('#quantity').val(value2);

            }
        }

function  add_to_cart()
{
    var quantity=$("#quantity").val();
    var user_id=$("#user_id").val();
    var product_id=$("#product_id").val();
    
    var token=$("#_token").val();
    if(user_id!=0){
        $.ajax({
            url:'/add_to_cart_ajax1',
            type:'POST',
            data:{_token:$("#_token").val(),product_id:product_id,quantity:quantity},
            success:function(response)
            {
                if(response==2)
                {
                    swal("Item Already Present In The Basket!");
                }
                else 
                {
                    carttoggle(product_id);
                    cart_ajax();
                    mdtoast('Add to Busket Sucessfully.', { 
                    interaction: true, 
                    duration: 2000,
                    actionText: 'View Cart',
                    action: function(){
                    this.hide(); 
                    window.location.href="{{route('cart')}}"
                        },
                        });
                }
            }
        });

    }else{
        mdtoast('Please login first then item added to Busket.', { 
        type: 'warning',
        duration: 3000
        });
    }
    $("[data-toggle='tooltip']").tooltip('hide');
}


function add_to_wishlist()
{

            var qty=$("#quantity").val();
          //  var user_id=$("#user_id").val();
            var product_id=$("#product_id").val();
 
            var token=$("#_token").val();
            if(user_id!=0){
            $.ajax({
            url:'/addtowishlist_ajax1',
            type:'POST',
            data:{_token:$("#_token").val(),product_id:product_id,qty:qty},
            success:function(response)
            {
             
            if(response==2)
            {
           swal("Item Already Present In The Wishlist!");
            }
            else
            {
                wishtoggle(product_id); 
                
                    mdtoast('Add to Wishlist sucessfully.', { 
                    interaction: true, 
                    duration: 2000,
                    actionText: 'View Wishlist',
                    action: function(){
                    this.hide(); 
                     window.location.href="wishlist"
                     },
                    });
                    // wishlist_ajax();
            }
            }


            });
        }else{
            mdtoast('Please login first then item added to Wishlist.', { 
        type: 'warning',
        duration: 3000
        });
        }
        $("[data-toggle='tooltip']").tooltip('hide');
}

function carttoggle(id){
 var token=$("#_token").val();
$.ajax({

 url:'/toggle',

 type:'POST',

 data:{_token:token,product_id:id},

success:function(response)
 {
if(response==2){
    $('#addto_CartButton').css('display','inline-block');
    $('#goto_CartButton').css('display','none');
}else{
    $('#addto_CartButton').css('display','none');
    $('#goto_CartButton').css('display','inline-block');
}
 
 

 }

 });
}

function wishtoggle(id){
 var token=$("#_token").val();
$.ajax({

 url:'/toggle_wish',

 type:'POST',

 data:{_token:token,product_id:id},

success:function(response)
 {
if(response==2){
    $('#addto_WishButton').css('display','inline-block');
    $('#removeto_WishButton').css('display','none');
}else{
    $('#addto_WishButton').css('display','none');
    $('#removeto_WishButton').css('display','inline-block');
}
 
 

 }

 });
}

function remove_to_wishlist(){
  //  alert(id);
  var product_id=$("#product_id").val();
   var token = $("#_token").val();
   
  
            var user_id=$("#user_id").val();

 
          
 //  alert();
$.ajax({

    url:'/removetowishlist_ajax',
    type:'POST',
    data:{_token:token,product_id:product_id},
   
  success:function(response)
    {
mdtoast('Remove  to Wishlist sucessfully.', { 
  type: 'info',
  duration: 3000
});
wishtoggle(product_id);
        cart_ajax();
       // wishlist_ajax();
      //  wishlist_table();
        
        
    }   
 });
  
  

   
}
</script>
