@extends('layouts.menu') @section('title') Booking History : E-Kirana @endsection @section('content')
<style>
    .modal {
        /* top: 13% !important; */
    }
    @media only screen and (max-width: 600px) {
        .mobile_hide {
            display: none;
        }
    }
    @media only screen and (min-width: 600px) {
        .desktop_hide {
            display: none;
        }
    }
    .badge-primary {
        background-color: #dad3b0;
        color: black;
        padding: 5px 9px !important;
    }
    .badge-danger {
        padding: 5px 9px !important;
    }
    .badge-warning {
        padding: 5px 9px !important;
    }
    .badge-secondary {
        padding: 5px 9px !important;
    }
    .badge-success {
        padding: 5px 9px !important;
    }
    .button {
        color: white !important;
        background: #3f8dda !important;
        border: none;
        padding: 6px 10px;
        margin: 2px;
    }

    .button:hover {
        color: white !important;
        background: #dc3545 !important;
        border: none;
    }
    .button2 {
        color: white !important;
        background: #dc3545 !important;
        padding: 6px 10px;
        margin: 2px;
        border: none;
    }

    .button2:hover {
        color: white !important;
        background: #3f8dda !important;
        border: none;
    }
    .button3 {
        color: white !important;
        background: #333333 !important;
        padding: 6px 10px;
        margin: 2px;
        border: none;
    }

    .button3:hover {
        color: white !important;
        background: #dc3545 !important;
        border: none;
    }
</style>
<style>
.rate {
     float: left; 
    height: 46px;
    padding: 0px 0% 0 4%;
}
.rate:not(:checked) > input {
    position:absolute;
    top:-9999px;
}
.rate:not(:checked) > label {
    float:right;
    width:1em;
    overflow:hidden;
    white-space:nowrap;
    cursor:pointer;
    font-size:40px;
    color:#ccc;
}
.rate:not(:checked) > label:before {
    content: '★ ';
}
.rate > input:checked ~ label {
    color: #ffc700;    
}
.rate:not(:checked) > label:hover,
.rate:not(:checked) > label:hover ~ label {
    color: #deb217;  
}
.rate > input:checked + label:hover,
.rate > input:checked + label:hover ~ label,
.rate > input:checked ~ label:hover,
.rate > input:checked ~ label:hover ~ label,
.rate > label:hover ~ input:checked ~ label {
    color: #c59b08;
}

</style>

<div class="bg-gray-13 bg-md-transparent">
    <div class="container">
        <!-- breadcrumb -->
        <div class="my-md-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                    <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="/">Home</a></li>
                    <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">Order Details</li>
                </ol>
            </nav>
        </div>
        <!-- End breadcrumb -->
    </div>
</div>
<!-- =====  BREADCRUMB END===== -->
<div class="page-checkout section">
    <!-- =====  CONTAINER START  ===== -->
    <div class="container">
        <div class="mb-16 wishlist-table">
            <!-- <form class="mb-4" action="#" method="post"> -->
            <div class="table-responsive">
                <table class="table" cellspacing="0">
                    <thead>
                        <tr>
                            <th class="product-remove mobile_hide">&nbsp;</th>
                            <th class="product-thumbnail mobile_hide">&nbsp;</th>
                            <th class="product-name">Product</th>
                            <th class="product-price mobile_hide">Product Price</th>
                            <th class="product-Stock mobile_hide">Quantity</th>
                            <th class="product-subtotal min-width-200-md-lg mobile_hide">Status</th>
                            <th class="product-subtotal min-width-200-md-lg">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($booking3333 as $booking) @php($rr=DB::table('product_images')->where('product_id',$booking->product_id)->limit(1)->get())
                        <tr class="">
                            <td class="text-center mobile_hide">
                                <a href="#" class="text-gray-90"> </a>
                            </td>
                            <td class="d-none d-md-table-cell mobile_hide">
                                @foreach($rr as $aas)
                                <a href="/product/{{$booking->product_id}}">
                                    <img class="img-fluid max-width-100 p-1 border border-color-1" src="small_product_image/{{$aas->image}}" alt="Image Description" style="height: 100px;" />
                                </a>
                                @endforeach
                            </td>

                            <td data-title="Product" class="mobile_hide">
                                @php($rrr=DB::table('book_multi_items')->where('product_id',$booking->product_id)->first())
                                <a href="/product/{{$booking->product_id}}" class="text-gray-90">
                                    Date: {{date('d-m-Y', strtotime($rrr->created_at))}}<br />
                                    Order Id: <b style="color: black;">{{$booking->order_id}}-{{$booking->multi_id}} </b><br />
                                    {{$booking->product_name}} @if($booking->weight>0) @if($booking->weight>999)({{$booking->weight/1000}}kg) @else {{'('}}{{$booking->weight}}gm) @endif @endif
                                </a>
                            </td>
                            <td data-title="Product" class="desktop_hide">
                                @php($rrr=DB::table('book_multi_items')->where('product_id',$booking->product_id)->first())
                                <a href="/product/{{$booking->product_id}}" class="text-gray-90">
                                    <span style="font-size: 10px; xolor: black;">{{date('d-m-Y', strtotime($rrr->created_at))}}</span><br />
                                    <span style="font-size: 14px;"> Order Id: <b style="color: black;">{{$booking->order_id}} </b></span><br />
                                    <span style="font-size: 14px;"> {{$booking->product_name}} @if($booking->weight>0) @if($booking->weight>999)({{$booking->weight/1000}}kg) @else {{'('}}{{$booking->weight}}gm) @endif @endif </span><br />
                                    <span style="font-size: 14px;"> Quantity: <b style="color: black;"> {{$booking->quantity1}} </b></span><br />
                                    <span style="font-size: 14px;"> Total Price: <b style="color: black;">₹ {{($booking->product_price+$booking->gst)*($booking->quantity1)}} </b></span><br />
                                </a>
                            </td>

                            <td data-title="Unit Price" class="mobile_hide">
                                <span class="">₹ {{($booking->product_price+$booking->gst)*($booking->quantity1)}}</span>
                            </td>

                            <td data-title="Quantity" class="mobile_hide">
                                <!-- Stock Status -->
                                <span> {{$booking->quantity1}}</span>
                                <!-- End Stock Status -->
                            </td>

                            <td data-title="Order Status" class="mobile_hide">
                                @if($booking->order_status == 1)

                                <span class="badge badge-primary"> Order Under Process</span>
                                @elseif($booking->order_status == 2)

                                <span class="badge badge-primary"> Order In Transit</span>
                                @elseif($booking->order_status == 3)

                                <span class="badge badge-primary"> Out For Delivery</span>
                                @elseif($booking->order_status == 4)

                                <span class="badge badge-primary"> Delivered </span>
                                @elseif($booking->order_status == 5)

                                <span class="badge badge-warning"> Return Under process </span>
                                @elseif($booking->order_status == 6)
                                <span class="badge badge-warning"> Out For Pic Up </span>
                                @elseif($booking->order_status == 7 )

                                <span class="badge badge-secondary"> Returned </span>
                                @if($booking->refund_status == 'YES')
                                <span class="badge badge-success" style="margin: 3px;"> Refund Completed</span>
                                @endif @elseif($booking->order_status == 8)
                                <span class="badge badge-danger">Canceled</span>

                                @endif

                                <!-- <a type="button" class="btn btn-soft-secondary mb-3 mb-md-0 font-weight-normal px-5 px-md-4 px-lg-5 w-100 w-md-auto">Add to Cart</a> -->
                            </td>
                            <td data-title="Action">
                                <span class="desktop_hide">
                                    @if($booking->order_status == 1)

                                    <span class="badge badge-primary"> Order Under Process</span>
                                    @elseif($booking->order_status == 2)

                                    <span class="badge badge-primary"> Order In Transit</span>
                                    @elseif($booking->order_status == 3)

                                    <span class="badge badge-primary"> Out For Delivery</span>
                                    @elseif($booking->order_status == 4)

                                    <span class="badge badge-primary"> Delivered </span>
                                    @elseif($booking->order_status == 5)

                                    <span class="badge badge-warning"> Return Under process </span>
                                    @elseif($booking->order_status == 6)
                                    <span class="badge badge-warning"> Out For Pick Up </span>
                                    @elseif($booking->order_status == 7 )

                                    <span class="badge badge-secondary"> Returned </span>
                                    @if($booking->refund_status == 'YES')
                                    <span class="badge badge-success" style="margin: 3px;"> Refund Completed</span>
                                    @endif @elseif($booking->order_status == 8)
                                    <span class="badge badge-danger">Canceled</span>

                                    @endif
                                    <br />
                                </span>

                                @if($booking->order_status == 4)

                                <a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModalCenterreview{{$booking->multi_id}}" id="dssfcsdfc1" class="btn button">Review</a>

                                @php($countdedcf=DB::table('reviews')->where('multi_id', $booking->multi_id)->count())
                                @php($revire_xdsfs=DB::table('reviews')->where('multi_id', $booking->multi_id)->first())


                                <div class="modal fade" id="exampleModalCenterreview{{$booking->multi_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                           
                                            <div class="modal-body">
                                                <br />
                                                <center>
                                                    <h2><b>Give a Review</b></h2>
                                                </center>
                                                <div class="modal-body mb-1">
                                                    <form id="review_form{{$booking->multi_id}}" action="sendReview" enctype="multipart/form-data" method="POST">
                                                        @csrf
                                                        <input type="hidden" name="product_id" value="{{$booking->product_id}}">
                                                        <input type="hidden" name="multi_id" value="{{$booking->multi_id}}">
                                                     
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                        </div>
                                                        <div class="col-md-6">
                                                                <div class="rate ">
                                                                <input type="radio" id="star5{{$booking->multi_id}}" name="review" value="5" required @if($countdedcf!=0) @if($revire_xdsfs->review==5){{'checked'}}@endif @endif/>
                                                                    <label for="star5{{$booking->multi_id}}" title="text">5 stars</label>
                                                                    <input type="radio" id="star4{{$booking->multi_id}}" name="review" value="4" required @if($countdedcf!=0) @if($revire_xdsfs->review==4){{'checked'}} @endif @endif/>
                                                                    <label for="star4{{$booking->multi_id}}" title="text">4 stars</label>
                                                                    <input type="radio" id="star3{{$booking->multi_id}}" name="review" value="3" required @if($countdedcf!=0) @if($revire_xdsfs->review==3){{'checked'}} @endif @endif/>
                                                                    <label for="star3{{$booking->multi_id}}" title="text">3 stars</label>
                                                                    <input type="radio" id="star2{{$booking->multi_id}}" name="review" value="2" required @if($countdedcf!=0) @if($revire_xdsfs->review==2){{'checked'}} @endif @endif/>
                                                                    <label for="star2{{$booking->multi_id}}" title="text">2 stars</label>
                                                                    <input type="radio" id="star1{{$booking->multi_id}}" name="review" value="1" required @if($countdedcf!=0)  @if($revire_xdsfs->review==1){{'checked'}} @endif @endif/>
                                                                    <label for="star1{{$booking->multi_id}}" title="text">1 star</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="js-form-message col-md-12">
                                                                <label class="form-label">
                                                                    Subject
                                                                </label>
                                                                <input type="text" name="title" id="title" class="form-control" value="@if($countdedcf!=0){{$revire_xdsfs->review_title}} @endif">    
                                                             </div>
                                                             <div class="js-form-message  col-md-12">
                                                                <label class="form-label">
                                                                    Message
                                                                </label>
                                                                <textarea name="message" class="form-control"> @if($countdedcf!=0){{$revire_xdsfs->review_details}} @endif</textarea>
                                                             </div>
                                                           
                                                             <div class="js-form-message col-md-12">
                                                                <label class="form-label">
                                                                    Image(if any)
                                                                </label>
                                                                <input type="file" name="image" id="image" class="form-control">    
                                                             </div>
                                                           
                                                           
                                                       
                                                      
                                                           
                                                        </div>  
                                                       
                                                        <br><br>
                                                         <center>
                                                             <a target="_blank" href="" onclick="review({{$booking->multi_id}})" class="btn button3" id="review_submit_btn{{$booking->multi_id}}">Submit Review</a>
                                                            </center>
                                                         
                                                   
                                                </div>
                                            </form>
                                             
                                            </div>
                                           
                                        </div>
                                    </div>
                                </div>

                                @endif @if($booking->order_status==4 && $booking->return_status!='Return Policy is Over')

                                <a data-toggle="modal" data-target="#modalreturn{{$booking->multi_id}}" class="btn button2" style="">Return</a>

                                <div class="text-center">
                                    <!--Modal: Login / Register Form-->
                                    <div class="modal fade" id="modalreturn{{$booking->multi_id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog cascading-modal" role="document">
                                            <!--Content-->
                                            <div class="modal-content">
                                                <!--Modal cascading tabs-->
                                                <div class="modal-c-tabs">
                                                    <!-- Tab panels -->
                                                    <div class="tab-content" style="padding: 0 0 0 0;">
                                                        <!--Panel 7-->
                                                        <div class="tab-pane fade in show active" id="panel7" role="tabpanel">
                                                            <br />
                                                            <center>
                                                                <h2><b>RETURN ORDER</b></h2>
                                                            </center>
                                                            <!--Body-->
                                                            <div class="modal-body mb-1">
                                                                <div class="md-form form-sm mb-4" style="margin-top: 0;">
                                                                    @foreach($rr as $aas)
                                                                    <img src="small_product_image/{{$aas->image}}" alt="" style="height: 75px;" /><br />
                                                                    @endforeach Order ID : {{$booking->order_id}}<br />
                                                                    {{$booking->product_name}}@if($booking->weight>0) @if($booking->weight>999)({{$booking->weight/1000}}kg) @else {{'('}}{{$booking->weight}}gm) @endif @endif
                                                                </div>
                                                                <form action="return_status" method="POST">
                                                                    <input type="hidden" name="product_id" value="{{$booking->multi_id}}" />
                                                                    <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}" />
                                                                    <div class="md-form form-sm" style="text-align: left;">
                                                                        Reason for Return
                                                                        <select name="cancel_option" class="form-control option" required>
                                                                            <option value="" disable selected>-- Select Option -- </option>
                                                                            <option>Item Damaged but shipping box Ok</option>
                                                                            <option>Both product and shipping box damaged</option>
                                                                            <option>Defective product</option>
                                                                            <option>Wrong product sent</option>
                                                                            <option>Unsatisfactory product quality</option>
                                                                            <option value="1">Others</option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="md-form form-sm" style="text-align: left;">
                                                                        Message
                                                                        <input name="message" rows="2" class="form-control message" placeholder="Enter Your Message" required />
                                                                    </div>

                                                                    <div class="md-form form-sm" style="text-align: left;">
                                                                        Refund Amount Type

                                                                        <select name="refund_option" class="form-control option" required onChange="account(this.value,'{{$booking->payment_type}}','{{csrf_token()}}');">
                                                                            <option value="" disable selected>-- Select Option -- </option>
                                                                            <option value="Original Payment Method">Original Payment Method </option>
                                                                            <option value="Wallet">Wallet</option>
                                                                        </select>
                                                                    </div>

                                                                    <div id="ads"></div>

                                                                    <div class="text-center mt-2">
                                                                        <button class="btn button3" style="background-color: #025f97 !important;">Place Request</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            <!--Footer-->
                                                            <!--  <div class="modal-footer">
                                  <div class="options text-center text-md-right mt-1">
                                    <p>Not a member? <a href="#" class="blue-text">Sign Up</a></p>
                                    <p>Forgot <a href="#" class="blue-text">Password?</a></p>
                                  </div>
                                  <button type="button" class="btn btn-outline-info waves-effect ml-auto" data-dismiss="modal">Close</button>
                                </div>
                            -->
                                                        </div>
                                                        <!--/.Panel 7-->
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/.Content-->
                                        </div>
                                    </div>
                                </div>
                                <!--Modal: Login / Register Form-->

                                <!------------return modal end-------------->

                                @endif @if($booking->order_status==1 || $booking->order_status==2)

                                <a data-toggle="modal" data-target="#modalLRForm{{$booking->multi_id}}" class="btn button2" style="">Cancel</a>

                                <div class="text-center">
                                    <!--Modal: Login / Register Form-->
                                    <div class="modal fade" id="modalLRForm{{$booking->multi_id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog cascading-modal" role="document">
                                            <!--Content-->
                                            <div class="modal-content">
                                                <!--Modal cascading tabs-->
                                                <div class="modal-c-tabs">
                                                    <!-- Nav tabs -->

                                                    <!-- Tab panels -->
                                                    <div class="tab-content" style="padding: 0 0 0 0;">
                                                        <!--Panel 7-->
                                                        <div class="tab-pane fade in show active" id="panel7" role="tabpanel">
                                                            <br />
                                                            <center>
                                                                <h2><b>CANCEL ORDER</b></h2>
                                                            </center>

                                                            <!--Body-->
                                                            <div class="modal-body mb-1">
                                                                <div class="md-form form-sm mb-4" style="margin-top: 0;">
                                                                    @foreach($rr as $aas)
                                                                    <img src="small_product_image/{{$aas->image}}" alt="" style="height: 75px;" /><br />
                                                                    @endforeach<br />
                                                                    Order ID : {{$booking->order_id}}<br />
                                                                    {{$booking->product_name}}@if($booking->weight>0) @if($booking->weight>999)({{$booking->weight/1000}}kg) @else {{'('}}{{$booking->weight}}gm) @endif @endif
                                                                </div>
                                                                <form action="cencel_status" method="POST">
                                                                    <input type="hidden" name="product_id" value="{{$booking->multi_id}}" />
                                                                    <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}" />
                                                                    <div class="md-form form-sm" style="text-align: left;">
                                                                        Reason for Cancel
                                                                        <select name="cancel_option" class="form-control option" required>
                                                                            <option selected disabled >-- Select Option --</option>
                                                                            <option>Order placed by mistake</option>
                                                                            <option>Item price too high</option>
                                                                            <option>Shipping cost too high</option>
                                                                            <option value="1">Others</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="md-form form-sm" style="text-align: left;">
                                                                        Message
                                                                        <input name="message" class="form-control message" placeholder="Enter Your Message" />
                                                                    </div>

                                                                    <div class="text-center mt-2">
                                                                        <button class="btn button3" style="background-color: #025f97 !important;">Place Request</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            <!--Footer-->
                                                            <!--  <div class="modal-footer">
                                  <div class="options text-center text-md-right mt-1">
                                    <p>Not a member? <a href="#" class="blue-text">Sign Up</a></p>
                                    <p>Forgot <a href="#" class="blue-text">Password?</a></p>
                                  </div>
                                  <button type="button" class="btn btn-outline-info waves-effect ml-auto" data-dismiss="modal">Close</button>
                                </div>
                    -->
                                                        </div>
                                                        <!--/.Panel 7-->
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/.Content-->
                                        </div>
                                    </div>
                                </div>
                                <!--Modal: Login / Register Form-->

                                @endif

                                <a data-toggle="modal" data-target="#viewdetails{{$booking->multi_id}}" id="dssfcsdfc" class="btn button3" style="">Order Details</a>

                                <!--Modal: Login / Register Form-->
                                <div class="modal fade" id="viewdetails{{$booking->multi_id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog cascading-modal aaaaaa" role="document" style="">
                                        <!--Content-->
                                        <div class="modal-content">
                                            <!--Modal cascading tabs-->
                                            <div class="modal-c-tabs">
                                                <!-- Nav tabs -->
                                                <!-- <ul class="nav nav-tabs md-tabs tabs-2 light-blue darken-3" role="tablist" style="padding: 10px;border-radius: 7px;">
                                   <li class="nav-item">
                                     <span class="nav-link active" data-toggle="tab" href="#panel7" role="tab" style="background-color: #025f97;color: white;border-color: #025f97;">
                                     Order Details</span>
                                   </li>
                                  
                                 </ul>-->
                                                <br />
                                                <!-- Tab panels -->
                                                <div class="tab-content" style="padding: 0 0 0 0;">
                                                    <!--Panel 7-->
                                                    <div class="tab-pane fade in show active" id="panel7" role="tabpanel">
                                                        <!--Body-->
                                                        <br />
                                                        <center>
                                                            <h2><b>ORDER DETAILS</b></h2>
                                                        </center>
                                                        <div class="modal-body mb-1">
                                                            <div class="md-form form-sm mb-4" style="margin-top: 0;">
                                                                @foreach($rr as $aas)
                                                                <center>
                                                                    <img src="small_product_image/{{$aas->image}}" alt="" style="height: 75px;" /><br />
                                                                    @endforeach<br />
                                                                    Order ID : {{$booking->order_id}}-{{$booking->multi_id}}<br />

                                                                    {{$booking->product_name}} @if($booking->weight>0) @if($booking->weight>999)({{$booking->weight/1000}}kg) @else {{'('}}{{$booking->weight}}gm) @endif @endif
                                                                </center>
                                                            </div>
                                                            @php($addressdetails=DB::table('addres')->where('id',$booking->address_id)->first())
                                                            <b style="font-size: 16px; font-weight: bold;"> Delivery Address </b> <br />
                                                            <b>{{$addressdetails->name}}</b>, {{$addressdetails->phone_no }}, @if(strlen($addressdetails->flat)>0) {{$addressdetails->flat }},@endif
                                                            @if(strlen($addressdetails->location)>0){{$addressdetails->location }},@endif @if(strlen($addressdetails->landmark)>0){{$addressdetails->landmark }},@endif {{$addressdetails->city
                                                            }}, {{$addressdetails->district }}, {{$addressdetails->state }}-{{$addressdetails->pincode }} <br />
                                                            <br />
                                                            <b style="font-size: 16px; font-weight: bold;">
                                                                Payment Type : @if($booking->wallet_amount!=0 && $booking->payment_type!='wallet') Wallet and @endif {{$booking->payment_type}} @if($booking->payment_type=='online')
                                                                <span style="font-size: 13px;">({{$booking->razorpay_payment_id}})</span>@endif
                                                            </b>
                                                            <br />
                                                            <br />
                                                            <center><a target="_blank" href="invoice?id={{$booking->multi_id}}" class="btn button3">Download Invoice</a></center>
                                                            <br />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div style="text-align: center; margin-left: 43%;">
                <center style="">{{ $booking3333->links() }}</center>
                <br />
                <br />
            </div>
        </div>
    </div>
    <hr />
    <!-- =====  CONTAINER END  ===== -->
</div>

<script>
    function account(aa, bb, cc) {
        $("#overlay").fadeIn(300);
        $.ajax({
            type: "post",
            url: "account",
            data: { _token: cc, bb: bb, aa: aa },
            success: function (date) {
                $("#ads").html(date);
                $("#overlay").fadeOut(300);
            },
        });
    }

    function review(id){
       
        event.preventDefault();
            var btnid = $('#review_submit_btn'+id).attr('id');
         //   alert(btnid);
            $('#review_submit_btn'+id).prop('disabled', false);
            // alert(btnid);
            var formdata = new FormData($("#review_form"+id)[0]);
           // var formdata['multiid']=id;
            $.ajax({
              url: $("#review_form"+id).attr('action'),
              type: 'POST',
              dataType: 'json',
              processData: false,
              contentType: false,
              cache:false,
              data: formdata,
              success: function (response) {   // success callback functiondelete_logo
                if(response == 1){
                  
                    mdtoast('Review Submit Sucessfully.', { 
                    type: 'black',
                    duration: 3000
                    });
                }else if(response == 2){
                  
                  mdtoast('Review Update Sucessfully.', { 
                  type: 'black',
                  duration: 3000
                  });
              
                }else{
                    mdtoast('Somthings wrong!! Please try again later.', { 
                    type: 'error',
                    duration: 3000
                });
                }
              },error: function (jqXHR) {
               
                buttonEnabled(btnid);
                  var errormsg = jQuery.parseJSON(jqXHR.responseText);
                  $.each(errormsg.errors,function(key,value) {
                      $('#'+key+'_error').html(value);
                  });
              }
            });

    }
</script>
@endsection
