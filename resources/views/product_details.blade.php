@extends('layouts.menu') @section('title') {{$product_details->product_name}} : ALIYA Online Services @endsection @section('content')
<style>
    p {
        margin: 0px;
    }
    @media only screen and (min-width: 1650px) {
        .productIcon{
            font-size: 1vw;
        }
        .productButton{
            font-size: 0.8vw;
        }
    }
    @media only screen and (max-width: 1650px) {
        .productIcon{
            font-size: 1.1vw;
        }
        .productButton{
            font-size: 0.9vw;
        }
    }
    @media only screen and (max-width: 1220px) {
        .productIcon{
            font-size: 1.4vw;
        }
        .productButton{
            font-size: 1.2vw;
        }
        .extraPadding{
            margin: 0.5rem
        }
    }
    @media only screen and (max-width: 900px) {
        .productIcon{
            font-size: 2vw;
        }
        .productButton{
            font-size: 1.6vw;
            width: 170px;
        }
        .extraPadding{
            margin: 0.5rem
        }
    }
    @media only screen and (max-width: 764px) {
        .productIcon{
            font-size: 2vw;
        }
        .productButton{
            font-size: 1.6vw;
            width: 170px;
        }
        .extraPadding{
            margin: 0.5rem
        }
    }
    @media only screen and (max-width: 600px) {
        .productIcon{
            font-size: 2.8vw;
        }
        .productButton{
            font-size: 2.4vw;
          
        }
        .extraPadding{
            margin: 0.5rem;
            width: 40%;
            max-width: 180px;
        }
        .rounded-pil{
            max-width: 185px;
        }
    }
    @media only screen and (max-width: 500px) {
        .productIcon{
            font-size: 3.5vw;
        }
        .productButton{
            font-size: 3vw;
          
        }
        .extraPadding{
            margin: 0.5rem;
            width: 40%;
           
        }
        .rounded-pil{
            max-width: 185px;
        }
    }

</style>

<!-- ========== MAIN CONTENT ========== -->
<main id="content" role="main">
    <!-- breadcrumb -->
    <div class="bg-gray-13 bg-md-transparent">
        <div class="container">
            <!-- breadcrumb -->
            <div class="my-md-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="/">Home</a></li>
                        @php($cat1=DB::table('cats')->where('cat_id',$product_details->cat_id)->count()) @php($sub_cats1=DB::table('sub_cats')->where('sub_cat_id',$product_details->sub_cat_id)->count())
                        @php($sub_sub_cats1=DB::table('sub_sub_cats')->where('sub_sub_cat_id',$product_details->sub_sub_cat_id)->count()) @if($cat1!=0) @php($cat=DB::table('cats')->where('cat_id',$product_details->cat_id)->first())
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="/shop?cat_id={{$product_details->cat_id}}">{{$cat->cat_name}}</a></li>

                        @if($sub_cats1!=0) @php($sub_cats=DB::table('sub_cats')->where('sub_cat_id',$product_details->sub_cat_id)->first())
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="/shop?cat_id={{$cat->cat_id}}&sub_cat_id={{$sub_cats->sub_cat_id}}">{{$sub_cats->sub_cat_name}}</a></li>

                        @if($sub_sub_cats1!=0) @php($sub_sub_cats=DB::table('sub_sub_cats')->where('sub_sub_cat_id',$product_details->sub_sub_cat_id)->first())
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1">
                            <a href="/shop?cat_id={{$cat->cat_id}}&sub_cat_id={{$sub_cats->sub_cat_id}}&sub_sub_cat_id={{$sub_sub_cats->sub_sub_cat_id}}">{{$sub_sub_cats->sub_sub_cat_name}}</a>
                        </li>

                        @endif @endif @endif

                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">{{$product_details->product_name}}</li>
                    </ol>
                </nav>
            </div>
            <!-- End breadcrumb -->
        </div>
    </div>
    <!-- End breadcrumb -->

    <input type="hidden" id="_token" value="<?php echo csrf_token();?>" />
    @auth
    <input type="hidden" id="user_id" value="{{ Auth::user()->id }}" />
    @else
    <input type="hidden" id="user_id" value="0" />
    @endauth
    <input type="hidden" id="product_id" value="{{$product_details->product_id}}" />
    <input type="hidden" id="avaStock" value="{{$product_details->available_stock}}" />
    <!-- Single Product Body -->

    <div class="container">
        <!-- Single Product Body -->
        <div class="mb-xl-14 mb-6">
            <div class="row">
                <div class="col-md-5 mb-4 mb-md-0">
                    <div
                        id="sliderSyncingNav"
                        class="js-slick-carousel u-slick mb-2"
                        data-infinite="true"
                        data-arrows-classes="d-none d-lg-inline-block u-slick__arrow-classic u-slick__arrow-centered--y rounded-circle"
                        data-arrow-left-classes="fas fa-arrow-left u-slick__arrow-classic-inner u-slick__arrow-classic-inner--left ml-lg-2 ml-xl-4"
                        data-arrow-right-classes="fas fa-arrow-right u-slick__arrow-classic-inner u-slick__arrow-classic-inner--right mr-lg-2 mr-xl-4"
                        data-nav-for="#sliderSyncingThumb"
                    >
                   
                        @foreach($product_image as $key=>$product_image1)
                        <div class="js-slide">
                            <a href="/product_image/{{$product_image1->image}}" target="_black"><img class="img-fluid" src="/product_image/{{$product_image1->image}}" alt="{{$product_details->product_name}}" /></a>
                        </div>
                        @endforeach
                    </div>

                    <div
                        id="sliderSyncingThumb"
                        class="js-slick-carousel u-slick u-slick--slider-syncing u-slick--slider-syncing-size u-slick--gutters-1 u-slick--transform-off"
                        data-infinite="true"
                        data-slides-show="5"
                        data-is-thumbs="true"
                        data-nav-for="#sliderSyncingNav"
                    >
                        @foreach($product_image as $key=>$product_image1)
                            <div class="js-slide" style="cursor: pointer;">
                                <img class="img-fluid" src="/product_image/{{$product_image1->image}}" alt="{{$product_details->product_name}} " />
                            </div>
                        @endforeach
                      
                    </div>
                    <input type="hidden" id="currentImage" value=''>
                </div>
                <div class="col-md-7 mb-md-6 mb-lg-0">
                    <div class="mb-2">
                        <div class="border-bottom mb-3 pb-md-1 pb-3">
                            <a href="/shop?brand={{$product_details->brand_id}}&name={{$product_details->brand_name}}" class="font-size-12 text-gray-5 mb-2 d-inline-block">{{$product_details->brand_name}}</a>
                            <h2 class="font-size-25 text-lh-1dot2">{{$product_details->product_name}}</h2>
                            <div class="mb-2">
                                <a class="d-inline-flex align-items-center small font-size-15 text-lh-1" href="#">
                                    <div class="text-warning mr-2">
                                      @php($a=(int)$product_details->review) 
                                      @for($i=0;$i<$a;$i++)
                                      <small class="fas fa-star"></small>
                                      @endfor
                                      @for($i=0;$i<5-$a;$i++)
                                      <small class="far fa-star text-muted"></small>
                                      @endfor
                                    </div>
                                    <span class="text-secondary font-size-13">({{$product_details->total_review}} customer reviews)</span>
                                  </a>
                            </div>
               
                        </div>
                        <div >
                      
                          <div class="mb-4">    
                            @if($product_details->available_stock<15 && $product_details->available_stock!=0)
                                <span style="font-size: 16px; margin-left: 0px; vertical-align: middle; color: rgb(56, 142, 60); padding-top: 5px; font-weight: 700;">{{$product_details->available_stock}} items left</span><br/>
                            @elseif($product_details->available_stock<1)
                                <span style="font-size: 16px; margin-left: 0px; vertical-align: middle; color: rgb(245, 0, 0); padding-top: 5px; font-weight: 700;">Out of Stock</span><br/>
                            @endif                  
                            <div class="d-flex align-items-baseline" style="margin: 0px 0px 7px;">
                              @php($progst=$product_details->selling_price*$sub_cat->gst/100)
                              <h4 class="font-size-36 text-decoration-none" style="margin-bottom: 0px;line-height: 1;">₹{{round($product_details->selling_price+$progst)}}</h4>  
                              @if($product_details->mrp>round($product_details->selling_price+$progst))
                                <span class="font-size-20 ml-2 text-gray-6" style="font-size: 16px; margin-left: 12px; vertical-align: middle; color: rgb(135, 135, 135); text-decoration: line-through; padding-top: 5px;">₹{{$product_details->mrp}}</span>
                              @endif
                              @php($dis=(($product_details->mrp-$product_details->selling_price+$progst)/$product_details->mrp)*100)
                              @if($dis>0)
                                <span style="font-size: 16px; margin-left: 7px; vertical-align: middle; color: rgb(56, 142, 60); padding-top: 5px; font-weight: 600;">{{round($dis)}}% off</span>
                              @endif
                            </div>
                            <div class="text-gray-9 font-size-14">Sold By: <span class="text-blue font-weight-bold" >{{$product_details->companey_name}}</span></div>
                          </div>


                          
                        </div>





                     
                        <div class="mb-2 border-bottom">
                            <ul class="font-size-14 text-gray-110" style="padding: 0;">
                              <li style="line-height: 1;list-style-type:none"><label>Product Code:</label><span style="color: #565656;font-weight: 600;"> {{$product_details->product_code}}</span></li>
                              <li style="line-height: 1;list-style-type:none"><label>HSN Code:</label><span style="color: #565656;font-weight: 600;"> {{$product_details->hsn_code}}</span></li>
                              <li style="line-height: 1;list-style-type:none"><label>Tags:</label><span> <a href="/shop?cat_id={{$cat->cat_id}}" style="color: #565656;font-weight: 600;">{{$cat->cat_name}}</a> <i style="font-weight:600"> > </i> <a href="/shop?cat_id={{$cat->cat_id}}&sub_cat_id={{$sub_cat->sub_cat_id}}" style="color: #565656;font-weight: 600;">{{$sub_cat->sub_cat_name}}</a> @if($product_details->sub_sub_cat_id!=0) <i style="font-weight:600"> > </i> <a href="/shop?cat_id={{$cat->cat_id}}&sub_cat_id={{$sub_cat->sub_cat_id}}&sub_sub_cat_id={{$sub_sub_cat->sub_sub_cat_id}}" style="color: #565656;font-weight: 600;">{{$sub_sub_cat->sub_sub_cat_name}}</a> @endif</span></li>
                              <li style="line-height: 1;list-style-type:none"><label>Contry of Origin:</label><span style="color: #565656;font-weight: 600;"> {{$product_details->origin}}</span></li>
                            </ul>
                        </div>
                       
                      <div class="row d-md-flex align-items-end mb-3">
                        <div class="max-width-150 mb-md-0 extraPadding">
                            <h6 class="font-size-14">Quantity</h6>
                            <!-- Quantity -->
                            <div class="border rounded-pill py-2 px-3 border-color-1" style="height: auto;">
                                <div class="js-quantity row align-items-center" style="font-size: 1vw">
                                    <div class="col" style="width: 5vh;">
                                        <input class="js-result  border-0 rounded p-0 shadow-none" type="text" value="1" id="quantity" readonly style="background-color: transparent;border: none;"/>
                                    </div>
                                    <div class="col-auto pr-1">
                                        <a class="js-minus btn btn-icon btn-xs btn-outline-secondary rounded-circle border-0" href="javascript:;" onClick="down();">
                                            <small class="fas fa-minus btn-icon__inner"></small>
                                        </a>
                                        <a class="js-plus btn btn-icon btn-xs btn-outline-secondary rounded-circle border-0" href="javascript:;"  onClick="up();">
                                            <small class="fas fa-plus btn-icon__inner"></small>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- End Quantity -->
                        </div>
                        <div class="ml-md-3 extraPadding">
                            <a href="javascript:void(0)"  id="addto_CartButton" class="btn px-5 btn-primary-dark transition-3d-hover productButton"  onclick="add_to_cart();"><i class="ec ec-add-to-cart mr-2 productIcon" ></i> Add to Cart</a>
                            <a style="display:none;" href="/cart" id="goto_CartButton" class="btn px-5 btn-primary-dark transition-3d-hover productButton"><i class="ec ec-add-to-cart mr-2 productIcon" ></i> Go to Cart</a>
                       
                       
                          </div>
                        <div class="ml-md-3 extraPadding">
                          <a href="javascript:void(0)"   class="btn px-5 btn btn-dark transition-3d-hover productButton" onClick="booking();"> Buy Now</a>
                      </div>
                      <div class="ml-md-3 extraPadding" >
                                                           
                        <a href="javascript:void(0)" id="addto_WishButton" onclick="add_to_wishlist();" class="btn transition-3d-hover productButton" ><i class="ec ec-favorites mr-1 productIcon" ></i>Add To Wishlist</a>
                        <a style="display:none;" href="javascript:void(0)"  id="removeto_WishButton" onclick="remove_to_wishlist();" class="btn transition-3d-hover productButton"><i class="ec ec-favorites mr-1 productIcon" style="color:red;font-size: 1.1vw;"></i>Remove To Wishlist</a>
                    
                    </div>

                  
                
                  
                  {{-- <div id="addto_WishButton" class="wishlist" data-toggle="tooltip" title="" data-original-title="Add to Wishlist">
                        <a  href="javascript:void(0)" onclick="add_to_wishlist();"><span><i class="fa fa-heart"></i></span></a>
                  </div>
                  <div id="removeto_WishButton" class="wishlist" data-toggle="tooltip" title="" data-original-title="Remove to Wishlist" style="display:none;">
               
                          <a  href="javascript:void(0)" onclick="remove_to_wishlist();" ><span ><i class="fa fa-heart" style="color:red;"></i></span></a>
                  </div>
                         --}}
                    </div>
                    @if($product_details->group_id!=0)
                        <div class="border-top border-bottom py-3 mb-4 row" style="margin:0">

                          @if(count($color)!=0)
                            <div class="mb-4 mb-md-0" style="margin-right: 2rem; padding: 0px 0px 7px 0px">
                              <h6 class="font-size-14">Color</h6>
                              <!-- Quantity -->
            
                                    <div class="row">
                                      @foreach($color as $color1)
                                      @php($product_image2=DB::table('product_images')->where('product_id',$color1->product_id)->first())
                                        @if($color1->color==$product_details->color)
                                          <div class="border ml-md-3" style="border-color: #333e48 !important;padding: 5px">
                                            <div class="js-quantity row align-items-center">
                                              <a href="javascript:void(0)" style="color: #333e48; font-weight: 700;" data-toggle="tooltip" data-placement="top" title="" data-original-title="{{$color1->color}}">
                                                  <div class="col">
                                                    <img src="/product_image/{{$product_image2->image}}" style="height: 50px">
                                                  </div>
                                              </a>
                                            </div>
                                          </div>
                                        @else
                                          <div class="border border-color-1 ml-md-3" style="padding: 5px" onmouseover="colorImage('/product_image/{{$product_image2->image}}')" onmouseout="colorImageOut()">
                                              <div class="js-quantity row align-items-center">
                                                <a href="/productDetails/{{$product_details->group_id}}/{{$product_details->size}}/{{$color1->color}}" style="color: #333e48; font-weight: 700;" data-toggle="tooltip" data-placement="top" title="" data-original-title="{{$color1->color}}">
                                                    <div class="col">
                                                      <img src="/product_image/{{$product_image2->image}}" style="height: 50px">
                                                    </div>
                                                </a>
                                              </div>
                                          </div>
                                        @endif
                                      @endforeach
                                      </div>
                            
                              <!-- End Quantity -->
                            </div>
                          @endif
                          @if(count($size)!=0)
                            <div class="mb-4 mb-md-0">
                              <h6 class="font-size-14">Size</h6>
                              <!-- Quantity -->
                              <div class="row">
                              @foreach($size as $size1)
                                @if($size1->size==$product_details->size)
                                  <div class="border rounded-pill py-2 px-3 ml-md-3" style="border-color: #333e48 !important">
                                    <div class="js-quantity row align-items-center">
                                      <a href="javascript:void(0)" style="color: #333e48; font-weight: 700;" data-toggle="tooltip" data-placement="top" title="" data-original-title="{{$size1->size}}">
                                          <div class="col">
                                            {{$size1->size}}
                                          </div>
                                      </a>
                                    </div>
                                  </div>
                                @else
                                  <div class="border rounded-pill py-2 px-3 border-color-1 ml-md-3">
                                      <div class="js-quantity row align-items-center">
                                        <a href="/productDetails/{{$product_details->group_id}}/{{$size1->size}}/{{$product_details->color}}" style="color: #333e48; font-weight: 700;" data-toggle="tooltip" data-placement="top" title="" data-original-title="{{$size1->size}}">
                                            <div class="col">
                                              {{$size1->size}}
                                            </div>
                                        </a>
                                      </div>
                                  </div>
                                @endif
                              @endforeach
                              </div>
                              <!-- End Quantity -->
                            </div>
                          @endif



                        </div>
                      @endif
                    
                    </div>
                </div>
            </div>
        </div>
        <!-- End Single Product Body -->
        <!-- Single Product Tab -->
        <div class="mb-8">
            <div class="position-relative position-md-static px-md-6">
                <ul class="nav nav-classic nav-tab nav-tab-lg justify-content-xl-center flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble border-0 pb-1 pb-xl-0 mb-n1 mb-xl-0" id="pills-tab-8" role="tablist">
                    <li class="nav-item flex-shrink-0 flex-xl-shrink-1 z-index-2">
                        <a class="nav-link active" id="Jpills-one-example1-tab" data-toggle="pill" href="#Jpills-one-example1" role="tab" aria-controls="Jpills-one-example1" aria-selected="true">Overview</a>
                    </li>
                    @php($feature=count($product_feature))
                    @if($feature!=0)
                    <li class="nav-item flex-shrink-0 flex-xl-shrink-1 z-index-2">
                        <a class="nav-link" id="Jpills-two-example1-tab" data-toggle="pill" href="#Jpills-two-example1" role="tab" aria-controls="Jpills-two-example1" aria-selected="false">Features</a>
                    </li>
                    @endif
                    @php($specification=count($product_specification))
                    @if($specification!=0)
                    <li class="nav-item flex-shrink-0 flex-xl-shrink-1 z-index-2">
                        <a class="nav-link" id="Jpills-three-example1-tab" data-toggle="pill" href="#Jpills-three-example1" role="tab" aria-controls="Jpills-three-example1" aria-selected="false">Specification</a>
                    </li>
                    @endif
                    <li class="nav-item flex-shrink-0 flex-xl-shrink-1 z-index-2">
                        <a class="nav-link" id="Jpills-five-example1-tab" data-toggle="pill" href="#Jpills-five-example1" role="tab" aria-controls="Jpills-three-example1" aria-selected="false">Seller Details</a>
                    </li>
                    <li class="nav-item flex-shrink-0 flex-xl-shrink-1 z-index-2">
                        <a class="nav-link" id="Jpills-four-example1-tab" data-toggle="pill" href="#Jpills-four-example1" role="tab" aria-controls="Jpills-four-example1" aria-selected="false">Reviews</a>
                    </li>
                </ul>
            </div>
            <!-- Tab Content -->
            <div class="borders-radius-17 border p-4 mt-4 mt-md-0 px-lg-10 py-lg-9">
                <div class="tab-content" id="Jpills-tabContent">
                    <div class="tab-pane fade active show" id="Jpills-one-example1" role="tabpanel" aria-labelledby="Jpills-one-example1-tab">
                        <h3 class="font-size-24 mb-3">Description</h3>
                       <?php echo $product_details->description ?>
                    </div>
                    <div class="tab-pane fade" id="Jpills-two-example1" role="tabpanel" aria-labelledby="Jpills-two-example1-tab">
                        <h3 class="font-size-24 mb-3">Features</h3>
                        <ul>
                            @foreach($product_feature as $product_feature)
                            <li>{{$product_feature->features}}</li>
                          
                            @endforeach
                        </ul>

                      
                    </div>
                    <div class="tab-pane fade" id="Jpills-three-example1" role="tabpanel" aria-labelledby="Jpills-three-example1-tab">
                        <div class="mx-md-5 pt-1">
                            <h3 class="font-size-18 mb-4">Specifications</h3>
                            <div class="table-responsive mb-4">
                                <table class="table table-hover">
                                    <tbody>
                                        @foreach($product_specification as $product_specification1)
                                        <tr>
                                            <th class="px-4 px-xl-5 border-top-0" style="width:30%">{{$product_specification1->description}}</th>
                                            <td class="border-top-0">{{$product_specification1->title}}</td>
                                        </tr>
                                     @endforeach
                                        
                                       
                                    </tbody>
                                </table>
                            </div>
                           
                        </div>
                    </div>
                    <div class="tab-pane fade" id="Jpills-four-example1" role="tabpanel" aria-labelledby="Jpills-four-example1-tab">
                        <div class="row mb-8">
                            <div class="col-md-5">
                                <div style="    position: sticky;top: 100px;">
                                <div class="mb-3">
                                    <h3 class="font-size-18 mb-6">Based on {{$product_details->total_review}} reviews</h3>
                                    <h2 class="font-size-30 font-weight-bold text-lh-1 mb-0">{{$product_details->review}}</h2>   
                                    <div class="text-lh-1">overall</div>
                                </div>

                                <!-- Ratings -->
                                <ul class="list-unstyled">
                                    <li class="py-1">
                                        <a class="row align-items-center mx-gutters-2 font-size-1" href="javascript:;">
                                            <div class="col-auto mb-2 mb-md-0">
                                                <div class="text-warning text-ls-n2 font-size-16" style="width: 80px;">
                                                    <small class="fas fa-star"></small>
                                                    <small class="fas fa-star"></small>
                                                    <small class="fas fa-star"></small>
                                                    <small class="fas fa-star"></small>
                                                    <small class="fas fa-star"></small>
                                                </div>
                                            </div>
                                            <div class="col-auto mb-2 mb-md-0">
                                                <div class="progress ml-xl-5" style="height: 10px; width: 200px;">
                                                    <div class="progress-bar" role="progressbar" style="width: {{$reviewPercentage5}}%;" aria-valuenow="{{$reviewPercentage5}}" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                            <div class="col-auto text-right">
                                                <span class="text-gray-90">{{$review_star5}}</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="py-1">
                                        <a class="row align-items-center mx-gutters-2 font-size-1" href="javascript:;">
                                            <div class="col-auto mb-2 mb-md-0">
                                                <div class="text-warning text-ls-n2 font-size-16" style="width: 80px;">
                                                    <small class="fas fa-star"></small>
                                                    <small class="fas fa-star"></small>
                                                    <small class="fas fa-star"></small>
                                                    <small class="fas fa-star"></small>
                                                    <small class="far fa-star text-muted"></small>
                                                </div>
                                            </div>
                                            <div class="col-auto mb-2 mb-md-0">
                                                <div class="progress ml-xl-5" style="height: 10px; width: 200px;">
                                        
                                                    <div class="progress-bar" role="progressbar" style="width: {{$reviewPercentage4}}%;" aria-valuenow="{{$reviewPercentage4}}" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                            <div class="col-auto text-right">
                                                <span class="text-gray-90">{{$review_star4}}</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="py-1">
                                        <a class="row align-items-center mx-gutters-2 font-size-1" href="javascript:;">
                                            <div class="col-auto mb-2 mb-md-0">
                                                <div class="text-warning text-ls-n2 font-size-16" style="width: 80px;">
                                                    <small class="fas fa-star"></small>
                                                    <small class="fas fa-star"></small>
                                                    <small class="fas fa-star"></small>
                                                    <small class="far fa-star text-muted"></small>
                                                    <small class="far fa-star text-muted"></small>
                                                </div>
                                            </div>
                                            <div class="col-auto mb-2 mb-md-0">
                                                <div class="progress ml-xl-5" style="height: 10px; width: 200px;">
                                                    <div class="progress-bar" role="progressbar" style="width: {{$reviewPercentage3}}%;" aria-valuenow="{{$reviewPercentage3}}" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                            <div class="col-auto text-right">
                                                <span class="text-gray-90">{{$review_star3}}</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="py-1">
                                        <a class="row align-items-center mx-gutters-2 font-size-1" href="javascript:;">
                                            <div class="col-auto mb-2 mb-md-0">
                                                <div class="text-warning text-ls-n2 font-size-16" style="width: 80px;">
                                                    <small class="fas fa-star"></small>
                                                    <small class="fas fa-star"></small>
                                                    <small class="far fa-star text-muted"></small>
                                                    <small class="far fa-star text-muted"></small>
                                                    <small class="far fa-star text-muted"></small>
                                                </div>
                                            </div>
                                            <div class="col-auto mb-2 mb-md-0">
                                                <div class="progress ml-xl-5" style="height: 10px; width: 200px;">
                                                    <div class="progress-bar" role="progressbar" style="width: {{$reviewPercentage2}}%;" aria-valuenow="{{$reviewPercentage2}}" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                            <div class="col-auto text-right">
                                                <span class="text-gray-90">{{$review_star2}}</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="py-1">
                                        <a class="row align-items-center mx-gutters-2 font-size-1" href="javascript:;">
                                            <div class="col-auto mb-2 mb-md-0">
                                                <div class="text-warning text-ls-n2 font-size-16" style="width: 80px;">
                                                    <small class="fas fa-star"></small>
                                                    <small class="far fa-star text-muted"></small>
                                                    <small class="far fa-star text-muted"></small>
                                                    <small class="far fa-star text-muted"></small>
                                                    <small class="far fa-star text-muted"></small>
                                                </div>
                                            </div>
                                            <div class="col-auto mb-2 mb-md-0">
                                                <div class="progress ml-xl-5" style="height: 10px; width: 200px;">
                                                    <div class="progress-bar" role="progressbar" style="width: {{$reviewPercentage1}}%;" aria-valuenow="{{$reviewPercentage1}}" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                            <div class="col-auto text-right">
                                                <span class="text-gray-90">{{$review_star1}}</span>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                                <!-- End Ratings -->
                            </div>
                            </div>
                            <div class="col-md-7">
                                <h3 class="font-size-18 mb-5">Customer review</h3>
                                <div>
                                @if($product_details->total_review>0)
                                @foreach($revie as $revie1)
                                  <div class="border-bottom border-color-1 pb-4 mb-4">
                                      <!-- Review Rating -->
                                      <div class="d-flex justify-content-between align-items-center text-secondary font-size-1 mb-2">
                                          <div class="text-warning text-ls-n2 font-size-16" style="width: 80px;">
                                              @php($a=(int)$revie1->review) 
                                              @for($i=0;$i<$a;$i++)
                                              <small class="fas fa-star"></small>
                                              @endfor
                                              @for($i=0;$i<5-$a;$i++)
                                              <small class="far fa-star text-muted"></small>
                                              @endfor
                                              
                                          </div>
                                          
                                      </div>
                                      <!-- End Review Rating -->
                                      @if($revie1->image!=null)
                                      <img src="/review_image/{{$revie1->image}}" style="height:80px;cursor: pointer;" data-toggle="modal" data-target="#exampleModalCenterwww{{$revie1->review_id}}">
                                         <div class="modal fade" id="exampleModalCenterwww{{$revie1->review_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                           <div class="modal-dialog modal-dialog-centered" role="document">
                                             <div class="modal-content">
                                               
                                               <div class="modal-body row" style="">
                                                 <div class="">
           
                                                 <img src="/review_image/{{$revie1->image}}" style="max-height: 600px;">
                                                 </div>
                                                 <div class="" style="    max-width: 320px;padding: 30px 25px 0 10px;">
                                                   <h4 class="mt-5 mb-10"><span class="review_span">{{$revie1->review}} <i class="fa fa-star" style="font-size:12px "></i></span> {{$revie1->review_title}}</h4>
                                                   <div class="form-group required">
                                                     <p>{{$revie1->review_details}}</p>
                                                   </div>
                                                 </div>
                                               </div>
                                              
                                             </div>
                                           </div>
                                         </div>
                                     @endif
                                      <p class="text-gray-90">
                                          {{$revie1->review_details}}
                                      </p>
          
                                      <!-- Reviewer -->
                                      <div class="mb-2">
                                          @php($dde=DB::table('users')->where('id',$revie1->user_id)->first())
                                          <strong>{{$dde->name}}</strong>
                                          <span class="font-size-13 text-gray-23">- {{date('M d, Y', strtotime($revie1->updated_at))}}</span>
                                      </div>
                                      <!-- End Reviewer -->
                                  </div>
                                  @endforeach
                                  @else
          
                                  <center>No review Found</center>
                                  @endif
                                </div>
                                  
                            </div>
                        </div>
                        <!-- Review -->

           
                        <!-- End Review -->
                   
                    </div>
                    <div class="tab-pane fade" id="Jpills-five-example1" role="tabpanel" aria-labelledby="Jpills-two-example1-tab">
                        <h3 class="font-size-24 mb-3">Seller Details</h3>
                        Seller Name : {{$product_details->name}}<br/>
                        Companey Name : {{$product_details->companey_name}}


                      
                    </div>
                </div>
            </div>
            <!-- End Tab Content -->
        </div>
        <!-- End Single Product Tab -->
        <!-- Related products -->
        <div class="mb-6">
            <div class="d-flex justify-content-between align-items-center border-bottom border-color-1 flex-lg-nowrap flex-wrap mb-4">
                <h3 class="section-title mb-0 pb-2 font-size-22">Related products</h3>
            </div>
            <ul class="row list-unstyled products-group no-gutters">


                @foreach($related_product_details as $related_product_details1) 
                @php($product_id=$related_product_details1->product_id)
                @php($image1=DB::table('product_images')->where('product_id',$product_id)->limit(1)->first())
                @php($brand=DB::table('brands')->where('brand_id',$related_product_details1->brand_id)->first())
                @php($sub_cat=DB::table('sub_cats')->where('sub_cat_id',$related_product_details1->sub_cat_id)->first())
                <li class="col-6 col-md-3 col-xl-2gdot4-only col-wd-2 product-item">
                    <div class="product-item__outer h-100">
                        <div class="product-item__inner px-xl-4 p-3">
                            <div class="product-item__body pb-xl-2">
                                <div class="mb-2"><a href="/shop?brand={{$brand->brand_id}}&name={{$brand->brand_name}}" class="font-size-12 text-gray-5">{{$brand->brand_name}}</a></div>
                                <h5 class="mb-1 product-item__title"><a href="/product/{{$product_id}}" class="text-blue font-weight-bold"><?php echo substr($related_product_details1->product_name,0,20); ?>@if(strlen($related_product_details1->product_name)>20){{'...'}} @endif</a></h5>
                                <div class="mb-2">
                                    <a href="/product/{{$product_id}}" class="d-block text-center"><img class="img-fluid" src="/product_image/{{$image1->image}}" alt="Image Description" /></a>
                                </div>
                                <div class="flex-center-between mb-1">
                                    <div class="prodcut-price">
                                        @php($progst=$related_product_details1->selling_price*$sub_cat->gst/100)
                                        <div class="text-gray-100">₹{{round($related_product_details1->selling_price+$progst)}}</div>
                                    </div>
                                    <div class="d-none d-xl-block prodcut-add-cart" >
                                        @php($a=(int)$related_product_details1->review)
                                        @for($i=0;$i<$a;$i++)
                                      
                                        <small class="fas fa-star" style="color: #ffc107"></small>
                                        @endfor
                                        @for($i=0;$i<5-$a;$i++)
                                        <small class="far fa-star text-muted"></small>
                                        @endfor
                                        <span style="font-size: 10px;">({{$related_product_details1->total_review}})</span>
                                    </div>
                                </div>
                            </div>
                            <div class="product-item__footer">
                                <div class="border-top pt-2 flex-center-between flex-wrap">
                                    <a href="javascript:void(0)" onclick="cart({{$product_id}})" class="text-gray-6 font-size-13"><i class="ec ec-add-to-cart"></i> Cart</a>
                                    <a href="javascript:void(0)" onclick="wishlist({{$product_id}})" class="text-gray-6 font-size-13"><i class="ec ec-favorites mr-1 font-size-15"></i> Wishlist</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                @endforeach



              
            </ul>
        </div>
        <!-- End Related products -->
        <!-- Brand Carousel -->
        <div class="mb-8">
            <div class="py-2 border-top border-bottom">
                <div
                    class="js-slick-carousel u-slick my-1"
                    data-slides-show="5"
                    data-slides-scroll="1"
                    data-arrows-classes="d-none d-lg-inline-block u-slick__arrow-normal u-slick__arrow-centered--y"
                    data-arrow-left-classes="fa fa-angle-left u-slick__arrow-classic-inner--left z-index-9"
                    data-arrow-right-classes="fa fa-angle-right u-slick__arrow-classic-inner--right"
                    data-responsive='[{
                                  "breakpoint": 992,
                                  "settings": {
                                      "slidesToShow": 2
                                  }
                              }, {
                                  "breakpoint": 768,
                                  "settings": {
                                      "slidesToShow": 1
                                  }
                              }, {
                                  "breakpoint": 554,
                                  "settings": {
                                      "slidesToShow": 1
                                  }
                              }]'
                >



                    @php($brands=DB::table('brands')
                ->get())
                 @foreach($brands as $brands)
                 <div class="js-slide">
                    <a href="/shop?brand={{$brands->brand_id}}&name={{$brands->brand_name}}" class="link-hover__brand">
                        <img class="img-fluid m-auto max-height-50" src="/brand_logo/{{$brands->brand_image}}" title="{{$brands->brand_name}}" alt="{{$brands->brand_name}}">
                    </a>
                </div>
                @endforeach       



                </div>
            </div>
        </div>
        <!-- End Brand Carousel -->
    </div>

</main>
<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>

<script>
$(document).ready(function(){
var product_id=$("#product_id").val();
carttoggle(product_id);
wishtoggle(product_id);
$("#pincheck").click(function(){
var pin=$("#pin").val();
var token=$("#_token").val();
var product_id=$("#product_id").val();
if(pin.length==6){
$.ajax({
    url:'/pinckeck',
    type:'POST',
    data:{pin:pin,_token:token,product_id:product_id},
    success:function(response)
    {
        if(response.success==false){
            $("#delivery_msg").html('Delivery Unavailable').css('color','red');
            $("#delivery_msg1").html('Change the Delivery Pncode').css('color','red');
        }else{
            $("#pincheck").html('Change');
            $("#delivery_msg").html('Delivery Available').css('color','green');
            $("#delivery_msg1").html(response.msg).css('color','green');
        }
    }
});
}else if(pin.length==0){
            $("#delivery_msg").html('Invalid Pincode').css('color','red');
            $("#delivery_msg1").html('Please Enter Delivery Pincode').css('color','red');
}else{
                $("#delivery_msg").html('Invalid Pincode').css('color','red');
            $("#delivery_msg1").html('Please Check Delivery Pincode').css('color','red');
}
$("[data-toggle='tooltip']").tooltip('hide');
});
});


function  up()
    {
        var ava=$("#avaStock").val();
        var value1=$("#quantity").val();
        var value2=Number(value1)+1;
        if(value2<=ava){
            $('#quantity').val(value2);   
        }else{
            mdtoast('Only '+ava+' items available in our store.', { 
                type: 'warning',
                duration: 3000
                });
        }     
    }
    function down()
    {
        var value1=$("#quantity").val();
        var value2=Number(value1)-1;
        if(value2<=1)
        {
            $('#quantity').val(1);
        }
        else{
            $('#quantity').val(value2);
        }
    }

function  add_to_cart()
{
    var quantity=$("#quantity").val();
    var user_id=$("#user_id").val();
    var product_id=$("#product_id").val();
    var token=$("#_token").val();
    if(user_id!=0){
        $.ajax({
            url:'/add_to_cart_ajax1',
            type:'POST',
            data:{_token:$("#_token").val(),product_id:product_id,quantity:quantity},
            success:function(response)
            {
                if(response==2)
                {
                    swal("Item Already Present In The Basket!");
                }
                else 
                {
                    carttoggle(product_id);
                    cart_ajax();
                    mdtoast('Add to Busket Sucessfully.', { 
                    interaction: true, 
                    duration: 2000,
                    actionText: 'View Cart',
                    action: function(){
                    this.hide(); 
                    window.location.href="{{route('cart')}}"
                        },
                        });
                }
            }
        });
    }else{
        mdtoast('Please login first then item added to Cart.', { 
        type: 'warning',
        duration: 3000
        });
    }
    $("[data-toggle='tooltip']").tooltip('hide');
}

function add_to_wishlist()
{
        var qty=$("#quantity").val();
        var product_id=$("#product_id").val();
        var token=$("#_token").val();
        if(user_id!=0){
        $.ajax({
        url:'/addtowishlist_ajax1',
        type:'POST',
        data:{_token:$("#_token").val(),product_id:product_id,qty:qty},
        success:function(response)
        {
            if(response==2)
            {
                swal("Item Already Present In The Wishlist!");
            }
            else
            {
                wishtoggle(product_id); 
                
                mdtoast('Add to Wishlist sucessfully.', { 
                interaction: true, 
                duration: 2000,
                actionText: 'View Wishlist',
                action: function(){
                this.hide(); 
                 window.location.href="wishlist"
                 },
                });
                // wishlist_ajax();
            }
            }
        });
    }else{
            mdtoast('Please login first then item added to Wishlist.', { 
        type: 'warning',
        duration: 3000
        });
    }
    $("[data-toggle='tooltip']").tooltip('hide');
}

function carttoggle(id){
    var token=$("#_token").val();
    $.ajax({
        url:'/toggle',
        type:'POST',
        data:{_token:token,product_id:id},
        success:function(response)
        {
            if(response==2){
                $('#addto_CartButton').css('display','inline-block');
                $('#goto_CartButton').css('display','none');
            }else{
                $('#addto_CartButton').css('display','none');
                $('#goto_CartButton').css('display','inline-block');
            }
        }
    });
}

function wishtoggle(id){
    var token=$("#_token").val();
    $.ajax({
        url:'/toggle_wish',
        type:'POST',
        data:{_token:token,product_id:id},
        success:function(response)
        {
            if(response==2){
                $('#addto_WishButton').css('display','inline-block');
                $('#removeto_WishButton').css('display','none');
            }else{
                $('#addto_WishButton').css('display','none');
                $('#removeto_WishButton').css('display','inline-block');
            }
        }
    });
}

function remove_to_wishlist(){
        var product_id=$("#product_id").val();
        var token = $("#_token").val();
        var user_id=$("#user_id").val();
        $.ajax({
            url:'/removetowishlist_ajax',
            type:'POST',
            data:{_token:token,product_id:product_id},
            success:function(response)
            {
                mdtoast('Remove  to Wishlist sucessfully.', { 
                type: 'info',
                duration: 3000
                });
                wishtoggle(product_id);
                cart_ajax();  
            }   
        });
}
function colorImage(url){
    document.getElementById('currentImage').value=document.querySelector('.slick-active > a >img').src;
    document.querySelector('.slick-active > a >img').src=url;
  }
  function colorImageOut(){
    document.getElementById('currentImage').value=document.getElementById('currentImage').value;
    document.querySelector('.slick-active > a >img').src=document.getElementById('currentImage').value;
   
  }
</script>
<script>
    function booking(){
        var quantity=$("#quantity").val();
        var user_id=$("#user_id").val();
        var product_id=$("#product_id").val();
        var token=$("#_token").val();
        if(user_id!=0){
            $.ajax({
                url:'/add_to_noe_cart',
                type:'POST',
                data:{_token:token,product_id:product_id,quantity:quantity,user_id:user_id},
                    success:function(response)
                    {
                        // window.location.href= `/checkout?id=${product_id}`;
                        if(window.location.href.split('/').length==7){
                                window.location.href= `../../../../checkout?id=${product_id}`;
                        }else if(window.location.href.split('/').length==6){
                            window.location.href= `../../../checkout?id=${product_id}`;
                        }else if(window.location.href.split('/').length==8){
                            window.location.href= `../../../../../checkout?id=${product_id}`;
                        }else if(window.location.href.split('/').length==5){
                            window.location.href= `../../checkout?id=${product_id}`;
                        }else if(window.location.href.split('/').length==4){
                            window.location.href= `../checkout?id=${product_id}`;
                        }else if(window.location.href.split('/').length==3){
                            window.location.href= `checkout?id=${product_id}`;
                        }
                    }
                });
        }else{
                mdtoast('Please login first then item Buy.', { 
            type: 'warning',
            duration: 3000
            });
        }
    }
    </script>
    
    
    

@endsection
