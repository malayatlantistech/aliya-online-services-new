@extends('vendor.layouts.menu')
@section('body')

<div class="container-fluid pt-8">
							<div class="page-header mt-0  p-3">
								<h3 class="mb-sm-0">All Category Details</h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">Vendor Dashboard</li>
								</ol>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="card shadow">
										<div class="card-header">
											<h2 class="mb-0">Category</h2>
										</div>
										<div class="card-body">
											<div class="table-responsive">
												<table id="example" class="table table-striped table-bordered w-100 text-nowrap">
													<thead>
														<tr>
															<th>Category Name &nbsp;&nbsp;
												
											</th>
															<th >Sub Category Name  
											<span style="padding-left:35%">Sub SubCategory Name  
                                            </th>
                                               
															
															
														</tr>
													</thead>
													<tbody>
													@foreach($cat as $cat)
														<tr>
	                                                   <td>{{$cat->cat_name}}<br> <h2 style="float:right">
													   
                                                       &nbsp;&nbsp;
													   @php($count=  $cat_name=DB::table('cats')->join('sub_cats','sub_cats.cat_id','=','cats.cat_id')->where('sub_cats.cat_id',$cat->cat_id)->count())
														

													  </h2><br><br><br>
												
													   </td>
                                                       @php($cat_id=$cat->cat_id)
                                                       @php($sub_cat=DB::table('sub_cats')->where('cat_id',$cat_id)->get())
                                                       <td colspan="2">
                                                      
                                                       @foreach($sub_cat as $sub_cat)
                                                       
                                                       <table style=" " class="table table-striped table-bordered w-100 text-nowrap">
                                                      
                                                       <tr><td style="width:460px " >
													   <table style="border: none;">
															<tr>
															<td style="width:460px;border: none; "> {{$sub_cat->sub_cat_name}}</td><td style="width:150px;border: none; "></td>
															</tr>
															<tr>
															<td  style="border: none;"> </td><td  style="border: none;">
															@php($sub=DB::table('sub_sub_cats')->where('sub_cat_id',$sub_cat->sub_cat_id)->count())
														</tr>
															<tr><td colspan="2"  style="border: none;"> <span style="float:left">
											<center>	

													  
												</span></center>


									
												

												</td></tr>
													   </table>
													 
													  </td>
                                                       @php($sub_cat_id=$sub_cat->sub_cat_id)
                                                       @php($sub_sub_cat=DB::table('sub_sub_cats')->where('sub_cat_id',$sub_cat_id)->get())
                                                       <td style=" ">
                                                       <table style="border:0; " class="table table-striped table-bordered w-100 text-nowrap">
                                                     
                                                     @foreach($sub_sub_cat as $sub_sub_cat)
                                                       <tr>
														<td style="border:0;width:250px " >{{$sub_sub_cat->sub_sub_cat_name}}
														
														<br><br>
													
											
													   </td></tr>
                                                       @endforeach
                                                       
                                                       
                                                       
                                                       </table>



                                                        </td>
                                                       
                                                       
                                                       </tr>
                                                     </table>
                                                     @endforeach
                                                            </td>
                                                            
															
														</tr>
														@endforeach

													</tbody>
												</table>
											</div>
											<input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>"/>	
											
										</div>
									</div>
								</div>
							</div>

							<script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous">
</script>
	<script>
	function front_view(abc){

var token=$("#_token").val();
$.ajax({
  
	url:'front_view1',
	
	type:'POST',
	
	data:{_token:token,sub_cat_id:abc},
	
	
	success:function(response)
	{
		
	
	}
 
});

	}
</script>
@endsection
	