@extends('vendor.layouts.menu')
@section('body')

<div class="container-fluid pt-8">
							<div class="page-header mt-0  p-3">
								<h3 class="mb-sm-0">Delivery Boy Withdral Request</h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">Admin Dashboard</li>
								</ol>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="card shadow">
									<div class="card-header">
										@if ($message = Session::get('success'))
										<h2 class="mb-0" style="color:red"><b>{{ $message }}</b></h2>	
															@else
															<h2 class="mb-0">Delivery Boy Withdral Request</h2>
													 @endif
										
										</div>
										<div class="card-body">
											<div class="table-responsive">
												<table id="example" class="table table-striped table-bordered w-100 text-nowrap">
													<thead>
														<tr>
                                                            <th>Withdrawl <br>Request Date</th>
															<th>Delivery <br>Boy Name</th>
															<th>Delivery Boy <br>Phone Number</th>
                                                            <th>Withdrawl <br>Amount</th>
                                                            <th>Current <br>Wallet Amount</th>
															<th >Action</th>
															
														</tr>
													</thead>
													<tbody>
                                                        @foreach($withdrawal as $withdrawal)
										
														<tr>

															<td>{{$withdrawal->created_at}}</td>
															<td>{{$withdrawal->name}}</td>
                                                            <td>{{$withdrawal->mobile}}</td>
                                                            <td>&#8377; {{$withdrawal->withdrawal_amount}}</td>
                                                            <td>&#8377; {{$withdrawal->wallet_ammount}}</td>
															<td>

                                                                <a type="button" class="btn btn-info mt-1 mb-1 btn-sm" data-toggle="modal" data-target="#modal-default{{$withdrawal->withdrawal_request_id}}" style="color:white">Bank Details</a>
                                                                <div class="modal fade" id="modal-default{{$withdrawal->withdrawal_request_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
                                                                    <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h2 class="modal-title" id="modal-title-default">{{$withdrawal->name}} Bank Details</h2>
                                                                                <a type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">×</span>
                                                                                </a>
                                                                            </div>
                                                                        
                                                                            <div class="modal-body">
                                                                
                                                                                Account Holder Name : {{$withdrawal->a_h_name}}<br>
                                                                                Account Number : {{$withdrawal->a_no}}<br>
                                                                                Bank Name : {{$withdrawal->qr_code}}<br>
                                                                                Branch Name : {{$withdrawal->b_name}}<br>
                                                                                IFSC Code : {{$withdrawal->ifcs_code}}<br>
                                                                                UPI ID : {{$withdrawal->upi_id}}
        
                                                                            </div>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                </div>
        
        
                                                                @if($withdrawal->withdrawal_status=='NO')
                                                                     <a href="/withdrawl_pay?id={{$withdrawal->withdrawal_request_id}}" class="btn btn-secondary mt-1 mb-1 btn-sm">Payment</a>

                                                                @else
                                                                <span class="badge badge-primary">Payment Successfull</span>
                                                              
                                                                @endif
                                                               
                                                            </td>
														
                                                        </tr>
                                                    @endforeach
											
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
							<script
							src="https://code.jquery.com/jquery-3.4.1.js"
							integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
							crossorigin="anonymous">
					</script>
							<script>
								$(document).ready(function() {
									  
	
									  $('#example').DataTable( {
										   "order": [[ 0, "desc" ]]
									   } );
									   
								   });
															   </script>
@endsection