@extends('vendor.layouts.menu')
@section('body')

<div class="container-fluid pt-8">
							<div class="page-header mt-0  p-3">
								<h3 class="mb-sm-0"><a href="add_group" type="button" class="btn btn-primary mt-1 mb-1">Add More Group</a></h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">Product group</li>
								</ol>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="card shadow">
									<div class="card-header">
										@if ($message = Session::get('success'))
										<h2 class="mb-0" style="color:red"><b>{{ $message }}</b></h2>	
															@else
															<h2 class="mb-0">Product Group</h2>
													 @endif
										
										</div>
										<div class="card-body">
											<div class="table-responsive">
												<table id="example" class="table table-striped table-bordered w-100 text-nowrap">
													<thead>
														<tr>
															<th>Group Name</th>
															
                                                            <th>Total Product</th>
															
															<th >Action</th>
															
														</tr>
													</thead>
													<tbody>
													@foreach($group as $group)
														<tr>

															<td>{{$group->group_name}}</td>
															
                                                            <td>
                                                            
                                                            @php($pcount=0)
                                                           
                                                            {{$pcount}}
                                                            </td>
															
															<td>
                                                            <a href="update_group?id={{$group->group_id}}" type="button" class="btn btn-info mt-1 mb-1">Update</a>
															@php($pcount=DB::table('products')->where('group_id',$group->group_id)->count())
                                                            @if($pcount==0)
                                                            <a onclick="return confirm('Are you sure you want to delete this group?');" href="{{ url('/delete_group?id='.$group->group_id)}}" type="button" class="btn btn-warning mt-1 mb-1">Delete</a>

                                                            @endif
															</td>
														
														</tr>
														@endforeach
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
@endsection