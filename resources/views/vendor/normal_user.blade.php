@extends('vendor.layouts.menu')
@section('body')

<div class="container-fluid pt-8">
							<div class="page-header mt-0  p-3">
								<h3 class="mb-sm-0">Normal User</h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">Admin Dashboard</li>
								</ol>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="card shadow">
										<div class="card-header">
											<h2 class="mb-0">Normal User</h2>
										</div>
										<div class="card-body">
											<div class="table-responsive">
												<table id="example" class="table table-striped table-bordered w-100 text-nowrap">
													<thead>
														<tr>
															<th class="wd-15p">Customer Name</th>
															<th class="wd-15p">Customer Email</th>
															<th class="wd-15p">Contact No</th>
															<th class="wd-15p">Block</th>
															
															
														</tr>
													</thead>
													<tbody>
													@foreach($user as $user)
													<tr>
                                                    <td>
													{{$user->name}}<span style="float:right">
												
													
													@if($user->block=='YES')


														<button class="btn btn-icon btn-sm btn-danger mt-1 mb-1" type="button" data-original-title="Block" data-placement="top"  data-toggle="tooltip">
															<span class="btn-inner--icon"><i class="fe fe-info"></i></span>
														</button>
													@endif
														
													</span>
                                                    </td>
                                                    <td>
													{{$user->email}}
                                                    </td>
                                                    <td>
													{{$user->mobile}}
                                                    </td>
													<td>
													<label class="custom-switch">
															<input onchange="user_block({{$user->id}},this.checked);" type="checkbox" name="" value="1" class="custom-switch-input" @if($user->block=='YES') {{'checked=""'}} @endif>
															<span class="custom-switch-indicator custom-switch-indicator-square custom-switch-indicator-lg"></span>
														</label>
													</td>
													@endforeach
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
							<input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>"/>	
<script>
function user_block(id,value){
	var token = $("#_token").val();
$.ajax({

url:'user_block',


type:'POST',

data:{_token:token,id:id,value:value},

success:function(response)
{

  }   
});


}

</script>
@endsection