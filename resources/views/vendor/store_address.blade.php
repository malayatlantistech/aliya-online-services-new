@extends('vendor.layouts.menu')


@section('body')

<div class="container-fluid pt-8">
							<div class="page-header mt-0  p-3">
						
							@if(Session::has('message'))
                             
                               
							 <strong style="color:red">{{ Session::get('message') }}</strong>
								@else
								Store Address
								@endif</h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">Vendor Dashboard</li>
									
								</ol>
							</div>
							<div class="row">
								<div class="col-md-12">
									<form method="post" action="store_address" enctype="multipart/form-data">
									<div class="card shadow">
										<div class="card-header">
										@csrf
										<div class="card-body">

										<div class="row">
											
												<div class="col-md-6">
													<div class="form-group">
														<label class="form-label">Company Name</label>
														<input type="text" name="companey_name" value="{{Auth::user()->companey_name}}" readonly class="form-control"  placeholder="Name">
													</div>
													<div class="form-group">
														<label class="form-label">Pin/ZIP Code <font style="font-size:13px">(Example : 721657)</font>* </label>
														<input type="text" pattern="\d*" maxlength="6" class="form-control"   name="pin_code" placeholder="Enter Pin/ZIP Code"  @if($address_count!=0) value="{{ $address->pincode }}"  @endif >
														<span style="font-size:12px" id="pin_error"></span>
													</div>
													<div class="form-group">
														<label class="form-label">Location <font style="font-size:13px">(Example : Barghasipur)</font>*</label>
														<input type="text" class="form-control" id="location" required name="area" placeholder="Enter Store Location"  @if($address_count!=0) value="{{$address->location}}"  @endif >
														
													</div>
													<div class="form-group">
														<label class="form-label">City/Taluk <font style="font-size:13px">(Example : Haldia)</font>*</label>
														<input type="text" required class="form-control" id="city" name="city"  placeholder="Enter Store City" @if($address_count!=0) value="{{$address->city}}"  @endif >
													</div>
                                                    <div class="form-group">
														<label class="form-label">District <font style="font-size:13px">(Example : East Midnapore)</font>*</label>
														<input type="text" class="form-control" id="dist" required name="district" placeholder="Enter Store District"  @if($address_count!=0) value="{{$address->district}}"  @endif >
													</div>
													
													<div class="form-group">
														<label class="form-label">State <font style="font-size:13px">(Example : WEST BENGAL)</font>*</label>
														<input type="text" required class="form-control" id="state"  name="state"  placeholder="Enter Store State" @if($address_count!=0) value="{{$address->state}}" @endif >
													</div>
												</div>
												<div class="col-md-6">
												<div class="form-group">
														<label class="form-label">Vendor Name</label>
														<input type="text" name="name" value="{{Auth::user()->name}}" readonly class="form-control"  placeholder="Name">
													</div>
                                                <div class="form-group">
														<label class="form-label">Store Name/No <font style="font-size:13px">(Example : Modual-104)</font>*</label>
														<input type="text" required class="form-control"  name="house_no" placeholder="Enter Store Name" @if($address_count!=0) value="{{$address->flat}}"  @endif >
													</div>
													<div class="form-group">
														<label class="form-label">Landmark <font style="font-size:13px">(Example : Beside Mosjit)</font>*</label>
														<input type="text" required class="form-control" name="landmark"  placeholder="Enter Store Landmark" @if($address_count!=0) value="{{$address->landmark}}" @endif >
													</div>
													<div class="form-group">
														<label class="form-label">Address <font style="font-size:13px">(Example : Mutant Facility, Sector 3)</font>*</label>
														<input type="text" required class="form-control" name="address"  placeholder="Enter Store Address" @if($address_count!=0) value="{{$address->address}}" @endif >
													</div>
													<div class="form-group">
														<label class="form-label">Store Phone No <font style="font-size:13px">(Example : 9635341856)</font>*</label>
														<input type="number" required class="form-control" name="phone"  placeholder="Enter Store Phone Number" @if($address_count!=0) value="{{$address->phone_no}}" @endif >
													</div>
													
                                                   
													
                                                    <div class="form-group">
                                                            <center><input type="submit" id="but" value="Save Changes" class="btn btn-icon  btn-primary mt-1 mb-1"></center>
                                                    </div>
												</div>
                                           
											</div>
										</div>
									</div>
                                    </form>
								</div>
<input type="hidden" id="_token" name="_token" value="{{csrf_token()}}">
						

@endsection