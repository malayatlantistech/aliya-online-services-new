@extends('vendor.layouts.menu')
@section('body')

<div class="container-fluid pt-8">
							<div class="page-header mt-0  p-3">
								<h3 class="mb-sm-0">All Category</h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">Vendor Dashboard</li>
								</ol>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="card shadow">
										<div class="card-header">
										@if ($message = Session::get('success'))
										<h2 class="mb-0" style="color:red"><b>{{ $message }}</b></h2>	
															@else
															<h2 class="mb-0">Category</h2>
													 @endif
											
										</div>
										<div class="card-body">
											<div class="table-responsive">
												<table id="example" class="table table-striped table-bordered w-100 text-nowrap">
													<thead>
														<tr>
															<th>Category Name</th>
														
															<th >View Sub Category</th>
															
															
														</tr>
													</thead>
													<tbody>
													@foreach($cat as $cat)
														<tr>

															<td>{{$cat->cat_name}}</td>
							
															<td><a href="{{ url('/view_sub_category?cat_id='.$cat->cat_id)}}" type="button" class="btn btn-icon btn-pill btn-primary mt-1 mb-1 btn-sm">View Sub Category</a></td>

												
														</tr>
														@endforeach
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
@endsection