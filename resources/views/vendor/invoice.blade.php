<!DOCTYPE html>
<html>
<head>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="assets/img/brand/favicon.png" rel="icon" type="image/png">

	<!-- Fonts -->
    <title>Invoice : E-Kirana</title>



</head>
<body class="app sidebar-mini rtl" >
	<div id="global-loader" ></div>

						<!-- Top navbar-->

						<!-- Page content -->
						<div class="container-fluid pt-8">
                        <BR>
                            
                            <table class="table"><tr>
                                <td style="width:25%"><b style="font-size:25px">Tax Invoice</b></td>
                                <td style="width:25%">Order id: <B>{{$book->order_id}}</B><br>
                                Order Date:@php($view=$book->created_at)
								@php($vieww=explode(" ",$view))
								{{$vieww[0]}}
								
								</td>
                                
                                <td style="width:15%">GSTIN: 05CEUPB7367D2ZB<br>PAN: </td>
                            </tr></table>
							<div class="row invoice">
								<div class="col-md-12">
									<div class="card-box card shadow">
										<table class="table"><tr>
										@php($product_id=$book->product_id)
										@php($vendor=DB::table('products')->where('product_id',$product_id)->first())
						
                                           
										    <td style="width:40%"><B>Shipping Address</B><br>
                                          
										  @php($customer_id=$book->customer_id)
										  
										@php($shipping=DB::table('addres')->where('id',$book->address_id)->first())
										{{$book->name}}, Ph no.-{{$shipping->phone_no}}/{{$shipping->optional_phone}}
										   <br>{{$shipping->flat}},{{$shipping->address}},<br>{{$shipping->landmark}}
                                       {{$shipping->location}},{{$shipping->city}},{{$shipping->district}}
									   <br>{{$shipping->state}},Pin Code - {{$shipping->pincode}}<br>GSTIN - {{$shipping->gst_no}}</td>
                                          
                                           <td style="width:50%"><B>Billing Address</B><br>
										   @php($billing=DB::table('billing_adds')->where('user_id',$customer_id)->first())
										   {{$book->name}},
										   <br>{{$billing->address}},{{$billing->landmark}}<br>
                                       {{$billing->location}},{{$billing->city}},{{$billing->district}}
									   <br>{{$billing->state}},Pin Code - {{$billing->pincode}}</td>
                                        </tr></table>
										<div class="card-body">
											<div class="mb-0">
										
												<div class="row mt-4">
													<div class="col-md-12">
														<div class="table-responsive">
															<table class="table table-bordered m-t-30 text-nowrap">
																<thead >
																	<tr>
																		<th>Product</th>
																		<th>Description</th>
																		<th>Qty</th>
                                                                        <th>Gross<br>Amount</th>
                                                                        
                                                                        <th>Taxable <br>Value</th>
                                                                        <th>IGST</th>
																		<th class="text-right">Total</th>
																		
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td>{{$vendor->product_name}}<br>Product Order ID : {{$book->order_id}}-{{$book->multi_id}} <br>Product Code : SSJ{{$vendor->product_code}} </td>
                                                                        <td>HSN:{{$vendor->hsn_code}}<br>
																		@php($sub_cat=$vendor->sub_cat_id)
																		@php($sub_cat=DB::table('sub_cats')->where('sub_cat_id',$sub_cat)->first())
																		GST:{{$gst=$sub_cat->gst}}%
																		@php($quan=$book->quantity1)
																		@php($weight=(1000)*($quan))
																		@php($res=($weight)/500)
																		@if($weight<=500)
																		 @php($dc=45)
																		@else
																			 @for($k=1;$k<=$res;$k++)
																					 @php($valuee=45+(20*$k))
																					 @php($dc=$valuee)
																			 @endfor
																		@endif
                                                                      
                                                                        </td>
																		<td>{{$quan}}</td>

													
																		<td>{{$price1=($book->product_price+$book->gst)*$quan}}<br><br></td>

																		@php($igst=$book->gst*$quan)
                                                                        <td>{{$Price33=$price1-$igst}}<br></td>
                                                                        <td>{{$igst}}<br></td>
																		<td class="text-right">{{$price1}}<br></td>
																	</tr>
																
																
																
																</tbody>
															</table>
														</div>
													</div>
												</div>
												<div class="row" >
                                                    <div class="col-xl-4 col-12 offset-xl-8" style="padding-right: 20px;">
                                                        <table class="table m-t-30 text-nowrap">
                                                            <tr>
                                                                <td width=""><p class=" mt-3 font-weight-600"><b style="font-size:large">Total Qty :{{$quan}} </b></p></td>
                                                                <td></td>
                                                                <td><p class="text-right mt-3 font-weight-600">Sub-total: &#8377;{{($price1)}}</p>
                                                                    
                                                                 
                                                                    <h4 class="text-right text-xl">Grand Total : &#8377;{{($price1)}}</h4></td>
                                                            
                                                            </tr>
                                                       
<tr><td style="font-size:12px">
                                                       <b style="color:black"> Seller Registered Address </b>: 
													   
													   Siddharth Shah Jewelers,<br>
														1453, Jain Bhavan, Pagadbandh Ln, <br>
														Saraf Bazar, Nashik, Maharashtra,India-422001<br>
														Phone No- 0253 2599299, +91 8177916999
														<br>
																											
													   
													   
													   
													 
                                            <B>Declaration : </b>
                                                
                                                The goods sold are intended for end user consumption and not for resale.<br>
                                                **Conditions Apply. Please refer to the product page for more details<br>E.& O.E
                                            </td>
                                            
                                            <td><br><br><br>
                                                <center>
                                                Ordered Through<br>
                                        <img src="/logo/SSJ%20Jewellery%20Logo%20PNG.png" height="30px">
									         </center>
                                            </td>
                                            
                                            
                                            <td class="text-right">
                                           
										   
<br>
<br>
<br>
<br>

    <font size="2px" style="margin-top:-5px"> Authorized Signature</font><br>
	<font size="4px">{{"E-Kirana"}}</font>
									   
                                            </td>
                                        </tr> 
                                            
                                            </table>
                                            </div>
												</div>
												<hr>
												<div class="d-print-none">
													<div class="float-right">
														<a href="generatePDF58?id={{$book->multi_id}}" class="btn btn-primary"><i class="fa fa-print"></i> Download</a>
														
													</div>
													<div class="clearfix"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- end row -->

							<!-- Footer -->
							<footer class="footer">
								<div class="row align-items-center justify-content-xl-between">
									<div class="col-xl-6">
										
									</div>
									
								</div>
							</footer>
							<!-- Footer -->
						</div>
					</div>
				</div>
			</div>
			<!-- app-content -->
		</div>
	</div>
<!--
	<script src="../admin_assets/plugins/jquery/dist/jquery.min.js"></script>
	<script src="../admin_assets/js/popper.js"></script>
	<script src="../admin_assets/plugins/bootstrap/js/bootstrap.min.js"></script>


	<script src="../admin_assets/plugins/chart.js/dist/Chart.min.js"></script>
	<script src="../admin_assets/plugins/chart.js/dist/Chart.extension.js"></script>

	


    <script src="../admin_assets//plugins/datatable/jquery.dataTables.min.js"></script>
	<script src="../admin_assets//plugins/datatable/dataTables.bootstrap4.min.js"></script>
	<script src="../admin_assets//plugins/datatable/dataTables.responsive.min.js"></script>
	<script src="../admin_assets//plugins/datatable/responsive.bootstrap4.min.js"></script>


	<script src="../admin_assets/plugins/toggle-sidebar/js/sidemenu.js"></script>

	
    <script src="../admin_assets/plugins/fileuploads/js/dropify.min.js"></script>
	

	<script src="../admin_assets/plugins/jquery-ui/jquery-ui.min.js"></script>

	<script src="../admin_assets/plugins/customscroll/jquery.mCustomScrollbar.concat.min.js"></script>


	<script src="../admin_assets/js/custom.js"></script>
	<script src="../admin_assets/js/othercharts.js"></script>
	<script src="../admin_assets/js/datatable.js"></script>
	<script src="../admin_assets/js/dashboard-hr.js"></script> -->
</body>
</html>