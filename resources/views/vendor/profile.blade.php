@extends('vendor.layouts.menu')


@section('body')

<div class="container-fluid pt-8">
							<div class="page-header mt-0 shadow p-3">
								<h3 class="mb-sm-0">User profile</h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">User profile</li>
								</ol>
							</div>
                          
							<div class="row">
								<div class="col-md-12">
									<div class="card card-profile  overflow-hidden">
										<div class="card-body text-center cover-image" data-image-src="../admin_assets/img/profile-bg.jpg">
											<div class=" card-profile">
												<div class="row justify-content-center">
													<div class="">
														<div class="">
															<a href="#">
																<img src="../user_details/{{Auth::user()->profile_image}}" class="rounded-circle" alt="profile" style="width:200px;height:200px">
															</a>
														</div>
													</div>
												</div>
											</div>
											<h3 class="mt-3 text-white">{{Auth::user()->name}}</h3>
											<p class="mb-4 text-white">Vendor</p>
                                            
										
											<a href="edit_profile" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt" aria-hidden="true"></i> Edit profile</a>
											<a href="change_password" class="btn btn-primary btn-sm"><i class="fas fa-lock" aria-hidden="true"></i> Change Password</a>
											</div>
										
									</div>
									<div class="card shadow ">
										<div class="card-body pb-0">
											<div class="tab-content" id="myTabContent">
												<div class="tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
													<h2>About Me</h2>
													<div class="table-responsive border ">
														<table class="table row table-borderless w-100 m-0 ">
															<tbody class="col-lg-6 p-0">
																<tr>
																	<td><strong>Full Name : </strong> {{Auth::user()->name}}</td>
																</tr>
																<tr>
																	<td><strong>Email ID : </strong>{{Auth::user()->email}}</td>
																</tr>
																<tr>
																	<td><strong>Contact No : </strong> {{Auth::user()->mobile}}</td>
																</tr>
																
															</tbody>
															<tbody class="col-lg-6 p-0">
															<tr>
																	<td><strong>Address : </strong> {{Auth::user()->address}}</td>
																</tr>
																<tr>
																	<td><strong>Pin Code : </strong> {{Auth::user()->pin_no}}</td>
																</tr>
																<tr>
																	<td><strong>Approve Status : </strong> 
																	@if(Auth::user()->status=='YES')
																		<b style="color:green;font-size:15px">{{' YES'}}</b>
																	@else
																		<b style="color:red;font-size:15px">{{' NO'}}</b>
																	@endif	
																	</td>
																</tr>
																
															</tbody>
														</table>
													</div>
													<div class="media-heading mt-3">
														<h3><strong>Documents</strong></h3>
													</div>
													<br>
														<strong>GSTIN NO :</strong> {{Auth::user()->gst_no}}<br>
														<strong>Companey Name :</strong> {{Auth::user()->companey_name}}
													<table style="width:100%">
													<tr><td style="width:50%">
															<div class="col-md-6 p-0">
																
																	<strong>Aadhar No :</strong> {{Auth::user()->aadhar_no}}<br>
																
																	<strong>Aaadhar Image:</strong><br><br>
																	<a href="../user_details/{{Auth::user()->aadhar_image}}" target="_blank"><img src="../user_details/{{Auth::user()->aadhar_image}}" width="400px"></a>
																<br><br>
																
															</div>
															</td><td style="width:50%;padding-top:0px;margin-top:-5px">
															<div class="col-md-6 p-0">
															<strong>Pan Card No :</strong> {{Auth::user()->pan_no}}
																
																<strong>Pan Image:</strong><br><br>
																<a href="../user_details/{{Auth::user()->pan_image}}" target="_blank"><img src="../user_details/{{Auth::user()->pan_image}}" width="400px"></a>
																
																
																	<br>
																
																
															</div>
															<td>
															</tr>
															<tr><td style="width:50%">
															<div class="col-md-6 p-0">
																
																	
																
																	<strong>Your Sign:</strong><br><br>
																	<a href="../user_details/{{Auth::user()->vendor_sign}}" target="_blank"><img src="../user_details/{{Auth::user()->vendor_sign}}" width="400px"></a>
																<br><br>
																
															</div>
															</td><td style="width:50%">
															<div class="col-md-6 p-0">
															
																
															<strong>Companey Logo:</strong><br><br>
																	<a href="../user_details/{{Auth::user()->companey_logo}}" target="_blank"><img src="../user_details/{{Auth::user()->companey_logo}}" width="400px"></a>
																	<br>
															</div>
															<td>
														</tr>
														</table>
													</div>
											
											
													@php($idd=Auth::user()->id)
													@php($bank_details=DB::table('bank_details')->where('delivery_id',$idd)->first())
													@php($bank_count=DB::table('bank_details')->where('delivery_id',$idd)->count())
													@if($bank_count !='0')
													<h2>Bank Details</h2>
													
													<div class="table-responsive border ">
														<table class="table row table-borderless w-100 m-0 ">
															<tbody class="col-lg-6 p-0">
																<tr>
																	<td><strong>Account Holder Name :</strong> {{$bank_details->a_h_name}}</td>
																</tr>
																<tr>
																	<td><strong>Bank Name:</strong>{{$bank_details->qr_code}}</td>
																</tr>
																<tr>
																	<td><strong>Branch Name:</strong>{{$bank_details->b_name}}</td>
																</tr>
																
																
															</tbody>
															<tbody class="col-lg-6 p-0">
															<tr>
																	<td><strong>Account No :</strong> {{$bank_details->a_no}}</td>
																</tr>
																<tr>
																	<td><strong>IFCS Code:</strong> {{$bank_details->ifcs_code}}</td>
																</tr>
																<tr>
																	<td><strong>UPI ID:</strong> {{$bank_details->upi_id}}</td>
																</tr>
																
																
															</tbody>
														</table>
													</div>

													@endif
												
												
											</div>
										</div>
									</div>
								</div>
							
                            
                            </div>
                            @endsection
		

