@extends('vendor.layouts.menu')


@section('body')

<div class="container-fluid pt-8">
							<div class="page-header mt-0  p-3">
								<h3 class="mb-sm-0">Bank Details</h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">Vendor Dashboard</li>
								</ol>
							</div>
							<div class="row">
								<div class="col-md-12">
									<form method="post" action="add_bank" enctype="multipart/form-data">
									<div class="card shadow">
										<div class="card-header">
										@csrf
										<div class="card-body">
                                     
											<div class="row">
						
													<div class="card-body row">
														<div class="col-md-6">
															<div class="form-group">
																<label for="exampleInputEmail1">Account Holder Name</label>
															<input type="text" class="form-control" name="a_h_name" id="a_h_name" value="@if(isset($details->a_h_name)) {{$details->a_h_name}}@endif" placeholder="Account Holder Name">
															</div>
															<span class="text-danger">{{ $errors->first('a_h_name') }}</span>
														</div>	
														<div class="col-md-6">

													  <div class="form-group">
														<label for="exampleInputPassword1">Bank Name</label>
														<input type="text" class="form-control" id="bank_name" name="bank_name" value="@if(isset($details->qr_code)) {{$details->qr_code}} @endif" placeholder="Bank Name">
													  </div>
													  <span class="text-danger">{{ $errors->first('bank_name') }}</span>
													</div>	
													<div class="col-md-6">
													  <div class="form-group">
														<label for="exampleInputPassword1">Branch Name</label>
														<input type="text" class="form-control" id="b_name" name="b_name" value="@if(isset($details->b_name)) {{$details->b_name}} @endif" placeholder="Branch Name">
													  </div>
													  <span class="text-danger">{{ $errors->first('b_name') }}</span>
													</div>	
													<div class="col-md-6">
													  <div class="form-group">
														  <label for="exampleInputPassword1">Account No</label>
														  <input type="text" class="form-control" name="a_no" id="a_no" value="@if(isset($details->a_no)) {{$details->a_no}} @endif" placeholder="Account No">
													  </div>
													  <span class="text-danger">{{ $errors->first('a_no') }}</span>
													</div>	
													<div class="col-md-6">
													  <div class="form-group">
														  <label for="exampleInputPassword1">IFSC code</label>
														  <input type="text" class="form-control" id="ifcs_code" name="ifcs_code" value="@if(isset($details->ifcs_code)) {{$details->ifcs_code}} @endif" placeholder="IFCS code">
													  </div>
													  <span class="text-danger">{{ $errors->first('ifcs_code') }}</span>
													</div>	
													<div class="col-md-6">
													  <div class="form-group">
														  <label for="exampleInputPassword1">UPI Id</label>
														  <input type="text" class="form-control" id="upi_id" name="upi_id" value="@if(isset($details->upi_id)) {{$details->upi_id}} @endif" placeholder="UPI Id">
													  </div>
													  <span class="text-danger">{{ $errors->first('upi_id') }}</span>
													</div>	

												 
													  {{-- <div class="form-group">
														  <label for="exampleInputFile">Choose QR code</label>
														  <div class="input-group">
															<div class="custom-file">
															  <input type="file" class="custom-file-input" id="qr_code" name="qr_code">
															  <label class="custom-file-label" for="exampleInputFile">Choose file</label>
															</div>
														  
														  </div>
														</div> --}}
														<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
													 
													<!-- /.card-body -->
													<div class="text-center">
													
													</div>
											
												
                                           	<center style="margin-left: 44%"><input type="submit" class="btn btn-primary form-control"  value="Save Bank Details"></center>
											</div>
										</div>
									</div>
                                    </form>
								</div>

								
							</div>

@endsection