@extends('vendor.layouts.menu')


@section('body')

<div class="container-fluid pt-8">
							<div class="page-header mt-0  p-3">
								<h3 class="mb-sm-0">Edit Profile</h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">Vendor Dashboard</li>
								</ol>
							</div>
							<div class="row">
								<div class="col-md-12">
									<form method="post" action="/edit_profile_code" enctype="multipart/form-data">
									<div class="card shadow">
										<div class="card-header">
										@csrf
										<div class="card-body">
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<label class="form-label">Enter Name</label>
														<input type="text" name="name" value="{{Auth::user()->name}}" required class="form-control"  placeholder="Name">
														<span class="text-danger">{{ $errors->first('name') }}</span>
													</div>
													<div class="form-group">
														<label class="form-label">Enter Email</label>
														<input type="email" class="form-control" value="{{Auth::user()->email}}" readonly name="email" placeholder="Email">
														<span class="text-danger">{{ $errors->first('email') }}</span>
													</div>
													<div class="form-group">
														<label class="form-label">Enter Phone No</label>
														<input type="number" class="form-control" value="{{Auth::user()->mobile}}" required name="mobile" placeholder="Phone No" readonly>
														<span class="text-danger">{{ $errors->first('mobile') }}</span>
													</div>
                                                    <div class="form-group">
														<label class="form-label">Enter Address</label>
														<input type="text" class="form-control" required name="address" value="{{Auth::user()->address}}" placeholder="Address">
														<span class="text-danger">{{ $errors->first('address') }}</span>
													</div>
                                                    <div class="form-group">
														<label class="form-label">Enter Pin Code</label>
														<input type="number" class="form-control" required name="pin_no" value="{{Auth::user()->pin_no}}" placeholder="Pin/ZIP Code">
														<span class="text-danger">{{ $errors->first('pin_no') }}</span>
													</div>
													<div class="form-group">
														<label class="form-label">Enter Companey Name</label>
														<input type="text" class="form-control" required name="companey_name" value="{{Auth::user()->companey_name}}" placeholder="Companey Name">
														<span class="text-danger">{{ $errors->first('companey_name') }}</span>
													</div>
                                                    <div class="form-group">
														<label class="form-label">Select Profile Image</label>
														<input type="file" id="profile" onchange="validateImage(this.id);" class="form-control" name="profile_image" placeholder="">
														<span class="text-danger">{{ $errors->first('profile_image') }}</span>
                                                        <input type="hidden" name="org_profile_image" value="{{Auth::user()->profile_image}}">
													</div>
												</div>
												<div class="col-md-6">
                                                <div class="form-group">
														<label class="form-label">Enter Aadhar No</label>
														<input type="number" required class="form-control" value="{{Auth::user()->aadhar_no}}" name="aadhar_no" placeholder="Aadhar No" @if(Auth::user()->change_parmission=='NO'){{'readonly'}} @endif >
														<span class="text-danger">{{ $errors->first('aadhar_no') }}</span>
													</div>
													<div class="form-group">
														<label class="form-label">Enter Pan No</label>
														<input type="text" required class="form-control" name="pan_no" value="{{Auth::user()->pan_no}}" placeholder="Pan No" @if(Auth::user()->change_parmission=='NO'){{'readonly'}} @endif >
														<span class="text-danger">{{ $errors->first('pan_no') }}</span>
													</div>
													<div class="form-group">
														<label class="form-label">Enter GST IN No</label>
														<input type="text" required class="form-control" name="gst_no" value="{{Auth::user()->gst_no}}" placeholder="GSTIN No" @if(Auth::user()->change_parmission=='NO'){{'readonly'}} @endif >
														<span class="text-danger">{{ $errors->first('gst_no') }}</span>
													</div>
                                                    <div class="form-group">
														<label class="form-label">Select Aadhar Image</label>
														<input type="file" class="form-control" id="adhar" onchange="validateImage(this.id);" name="aadhar_image" placeholder="" @if(Auth::user()->change_parmission=='NO'){{'readonly'}} @endif >
														<span class="text-danger">{{ $errors->first('adhar') }}</span>
                                                        <input type="hidden" name="org_aadhar_image" value="{{Auth::user()->aadhar_image}}">
													</div>
                                                    <div class="form-group">
														<label class="form-label">Select Pan Image</label>
														<input type="file" class="form-control" id="pan_image" onchange="validateImage(this.id);" name="pan_image" placeholder="" @if(Auth::user()->change_parmission=='NO'){{'readonly'}} @endif >
														<span class="text-danger">{{ $errors->first('pan_image') }}</span>
                                                        <input type="hidden" name="org_pan_image" value="{{Auth::user()->pan_image}}">
													</div>
													<div class="form-group">
														<label class="form-label">Select Companey Logo</label>
														<input type="file" class="form-control" name="companey_logo" placeholder="" id="companeyLogi" onchange="validateImage('companeyLogi');">
														<span class="text-danger">{{ $errors->first('companey_logo') }}</span>
                                                        <input type="hidden" name="org_companey_logo" value="{{Auth::user()->companey_logo}}">
													</div>
													<div class="form-group">
														<label class="form-label">Select Your Sign</label>
														<input type="file" class="form-control" name="vendor_sign" placeholder="" id="vendor_sign" onchange="validateImage('vendor_sign');" >
														<span class="text-danger">{{ $errors->first('vendor_sign') }}</span>
                                                        <input type="hidden" name="org_vendor_sign" value="{{Auth::user()->vendor_sign}}">
													</div>
                                                    <div class="form-group">
                                                            <center><input type="submit" value="Save Changes" class="btn btn-icon  btn-primary mt-1 mb-1"></center>
                                                    </div>
												</div>
                                           
											</div>
										</div>
									</div>
                                    </form>
								

								
							</div>
							<script type="text/javascript">
function validateImage(id) {
    var formData = new FormData();
    var file = document.getElementById(id).files[0];
    formData.append("Filedata", file);
    var t = file.type.split('/').pop().toLowerCase();
    if (t != "jpeg" && t != "jpg" && t != "png" && t != "bmp" && t != "gif") {
        alert('Please select a valid image file');
        document.getElementById(id).value = '';
        return false;
    }
    if (file.size > 1024000) {
        alert('Max Upload size is 1MB only');
        document.getElementById(id).value = '';
        return false;
    }
    return true;
}
</script>
@endsection