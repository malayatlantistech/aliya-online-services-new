@extends('vendor.layouts.menu')


@section('body')

<div class="container-fluid pt-8">
							<div class="page-header mt-0  p-3">
								<h3 class="mb-sm-0">Change Password</h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">Vendor Dashboard</li>
								</ol>
							</div>
							<div class="row">
								<!-- BASIC WIZARD -->
								<div class="col-xl-12">
									<div class="card m-b-20">
										
										<div class="card-body">
											<form method="post" action="change_password1">
												<div id="basicwizard" class="border pt-0">
													<ul class="nav nav-tabs nav-justified">
                                                    @foreach ($errors->all() as $error)
                            <p class="text-danger">{{ $error }}</p>
                         @endforeach 
													</ul>
													<div class="tab-content card-body  b-0 mb-0">
														<div class="" id="tab1">
															<div class="row">
																<div class="col-12">
																	<div class="form-group clearfix">
																		<div class="row ">
																			<div class="col-lg-3">
																				<label class="control-label form-label" for="userName">Current Password</label>
																			</div>
																			<div class="col-lg-9">
																				<input class="form-control required" required id="current_password" name="current_password" type="password" class="form-control{{ $errors->has('current_password') ? ' is-invalid' : '' }}">
                                                                               
                                                                                @if (@isset($_GET['pas']))
                               
                                                                                 <strong style="color:red">{{ 'Current Password not match' }}</strong>
                       
                                                                              @endif
                                                                                </div>
																		</div>
																	</div>
                                                                    @csrf
																	<div class="form-group  clearfix">
																		<div class="row ">
																			<div class="col-lg-3">
																				<label class="control-label form-label " for="password">New Password</label>
																			</div>
																			<div class="col-lg-9">
																				<input id="password" name="password" required type="password" value="{{ old('passwprd') }}"  class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}">
                                                                                @if ($errors->has('password'))
                               
                                                                                        <strong style="color:red">{{ $errors->first('password') }}</strong>
                                                                                
                                                                                @endif
                                                                            </div>
																		</div>
																	</div>
																	<div class="form-group clearfix">
																		<div class="row ">
																			<div class="col-lg-3">
																				<label class="control-label form-label" for="confirm">Confirm Password</label>
																			</div>
																			<div class="col-lg-9">
																				<input id="confirm" name="confirm" required type="password" class="required form-control" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}">
                                                                                @if (@isset($_GET['con']))
                               
                                                                                        <strong style="color:red">{{ 'Confirm Password not match' }}</strong>
                                                                                
                                                                                @endif
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<center><input type="submit" value="Change Password" class="btn btn-primary mb-0 waves-effect waves-light" ></center>
														
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>

@endsection