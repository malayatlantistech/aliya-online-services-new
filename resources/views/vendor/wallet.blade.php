@extends('vendor.layouts.menu')
@section('body')
<div class="container-fluid pt-8">
<div class="page-header mt-0 p-3">
    <h3 class="mb-sm-0">wallet</h3>
    <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item">
            <a href="/"><i class="fe fe-home"></i></a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">wallet</li>
    </ol>
</div>

<div class="row">
    <div class="col-xl-6 col-lg-6">
        <div class="card bg-gradient-danger border-0 overflow-hidden">
            <div class="">
                <div class="card-body">
                    {{-- <img src="assets/img/circle.svg" class="card-img-absolute" alt="circle-image" /> --}}
                    <div class="media d-flex">
                        <div class="media-body text-left">
                            <h4 class="text-white">Wallet Balance</h4>
                            <h2 class="text-white mb-0">₹ {{round($wallet_details->wallet_amount)}}</h2>
                        </div>
                        <div class="align-self-center">
                            <i class="fe fe-credit-card success font-large-2 text-white float-right"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-6 col-lg-6">
        <div class="card bg-gradient-primary border-0 overflow-hidden">
            <div class="">
                <div class="card-body">
                    {{-- <img src="assets/img/circle.svg" class="card-img-absolute" alt="circle-image" /> --}}
                    <div class="media d-flex">
                        <div class="media-body text-left">
                            <h4 class="text-white">Cash Withdrawal </h4>
                            <h2 class="text-white mb-0"><a class="btn btn-info btn-sm" data-toggle="modal" data-target="#exampleModalCenter"> Withdrawal <i class="fas fa-arrow-right"></i></a></h2>
                            <!-- Modal -->
                            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Cash Withdrawal</h5>

                                    <a type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </a>

                                    </div>
                                    <div class="modal-body">

                                    <form name="doctor_withdrawal" id="doctor_withdrawal" method="post" action="/vendor_withdrawal" enctype="multipart/form-data">
                                        <div class="row">
                                              <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="form-label">Enter Amount</label>
                                                    <input type="number" class="form-control is-valid state-valid" name="withdrawal_amount" id="withdrawal_amount" placeholder="Enter Amount" value=""/>
                                                    <div class="text-danger error1"><strong id="withdrawal_amount_error"></strong></div>
                                                </div>
                                                
                                                <input type="hidden" value="{{$bank_count}}" id="bank_count" name="bank_count">
                                                <input type="hidden" name="request_count" id="request_count" value="{{$request_count}}">
                                                <input type="hidden" name="wallet_amount" id="wallet_amount" value="{{$wallet_details->wallet_amount}}">
                                                <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
                                    
                                            </div>
                                            <div class="col-md-4"></div>

                                            <div class="col-md-4">
                                                <center><input type="submit" value="Submit" id="catagory_bttn" class="btn btn-primary" /></center>
                                            </div>
                                            <div class="col-md-4"></div>

                                        </div>


                                        </form>

                                  
                                    </div>
                                    <div class="modal-footer">
                                    <a type="button" class="btn btn-secondary btn-sm text-white" data-dismiss="modal">Close</a>
                               
                                    </div>
                                </div>
                                </div>
                            </div>
                                                
                       
                       
                       
                       
                       
                        </div>
                        <div class="align-self-center">
                           
                            <i class="fas fa-money-bill-alt text-white font-large-2 float-right"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
  
</div>

<div class="row">
    <div class="col">
        <div class="card shadow">
            <div class="card-header bg-transparent border-0">
                <h2 class="mb-0">Wallet Transaction</h2>
            </div>
            <div class="">
                <div class="grid-margin">
                    <div class="">
                        <div class="table-responsive">
                            <table class="table card-table table-vcenter text-nowrap align-items-center">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>Date </th>
                                        <th>Time</th>
                                        <th>Transaction Id</th>
                                        <th>Remarks</th>
                                        <th>Transaction <br>Type</th>
                                        <th>Transaction <br>Amount</th>
                                        <th>Balance</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($wallet_transaction))
                                    @foreach($wallet_transaction as $key=> $wallet_transaction1)
                                    <tr>
                                    <td>{{date('d-m-Y', strtotime($wallet_transaction1->created_at))}}</td>
                                    <td>{{date('h:i a', strtotime($wallet_transaction1->created_at))}}</td>
                                    <td>{{$wallet_transaction1->transaction_id}}</td>
                                    <td>{{$wallet_transaction1->remarks}}</td>
                                    <td>{{$wallet_transaction1->tranasaction_type}}</td>
                                    @if($wallet_transaction1->tranasaction_type=='Credit')
                                    <td class="text-green"> + {{round($wallet_transaction1->transaction_amount)}}</td>
                                    @elseif($wallet_transaction1->tranasaction_type=='Debit')
                                    <td class="text-red"> - {{round($wallet_transaction1->transaction_amount)}}</td>
                                    @endif
                                    <td>₹ {{round($wallet_transaction1->after_transaction_amount)}}</td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                       
                                        <td >3,542</td>
                                        
                                    </tr>
                                    @endif
                                
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
    $('#doctor_withdrawal').submit(function(event){
        event.preventDefault();
        document.getElementById("catagory_bttn").disabled = true;
        var bank_count=Number($('#bank_count').val());
        var wallet_amount=Number($('#wallet_amount').val());
        var withdrawal_amount=Number($('#withdrawal_amount').val());
        var request_count=Number($('#request_count').val());
        var _token=$('#_token').val();
    //    alert(request_count);
    
        if(bank_count>0){
        if(wallet_amount>=withdrawal_amount){  
        if(request_count==0) 
        {
        var formdata = new FormData($(this)[0]);
        $.ajax({
          url: $(this).attr('action'),
          type: 'POST',
          dataType: 'json',
          processData: false,
          contentType: false,
          cache:false,
          data:formdata,
          success: function (response) {
            //   alert();
            if(response.success == true){
                $('.error1').html('');
                mdtoast(response.msg, {
                        type: "success",
                    });
                    document.getElementById("catagory_bttn").disabled = true;
                    setTimeout(function(){location.reload(), 5000} );  
            }
          },error: function (jqXHR) {
            $('#myModal').modal('show');
              var errormsg = jQuery.parseJSON(jqXHR.responseText);
              $.each(errormsg.errors,function(key,value) {
                  $('#'+key+'_error').html(value);
                  document.getElementById("catagory_bttn").disabled = false;
              });
          }
        });
        }else{
            mdtoast('Your Have Already Send a Request', {
                        type: "warning",
                    });
                    document.getElementById("catagory_bttn").disabled = false;
        }
        }else{
            mdtoast('Your Wallet Amount is Too Low', {
                        type: "warning",
                    });
                    document.getElementById("catagory_bttn").disabled = false;
        }
    }else{
        mdtoast('At first Add your Bank Account', {
                        type: "warning",
                    });
                    document.getElementById("catagory_bttn").disabled = false;

    }

    });
});

</script>

