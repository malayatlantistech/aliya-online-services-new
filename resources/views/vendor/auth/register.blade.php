@extends('vendor.layouts.loginmenu')

@section('body')
<body class="bg-gradient-primary">
	<div class="page">
		<div class="page-main">
			<div class="limiter">
				<div class="container-login100">
						
					<div class="wrap-login100 p-5">
			
						<form class=" validate-form" method="POST" action="{{ route('vendor_register_action') }}" enctype="multipart/form-data">
						@csrf
						<input type="hidden" name="role" value="VENDOR">
							<div class="logo-img text-center pb-3">
								<img src="/img/fevicon/favicon.png" alt="logo-img" height="50px">
								
							</div>
							<span class="login100-form-title">
							
								Vendor Register | ALIYA Online Shopping
							</span>


							<div class="row">
								<div class="col-md-6">
										<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
										<input class="input100" id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}"  placeholder="Name" required>
										
										
										@if ($errors->has('name'))
											
											<center>  <strong style="color:red">{{ $errors->first('name') }}</strong></center>
									
										@endif
										<span class="symbol-input100">
											<i class="fa fa-user " aria-hidden="true"></i>
										</span>

								
									</div>
									
							<div class="wrap-input100 validate-input" data-validate = "Valid email is required: 9765436768">
								<input class="input100" id="phone_no" type="number" class="form-control{{ $errors->has('mobile') ? ' is-invalid' : '' }}" name="mobile" value="{{ old('mobile') }}"   placeholder="Phone no" required>
								
								
                                @if ($errors->has('mobile'))
                                    
                                      <center>  <strong style="color:red">{{ $errors->first('mobile') }}</strong></center>
                               
                                @endif
								<span class="symbol-input100">
									<i class="fa fa-user " aria-hidden="true"></i>
								</span>

								
							</div>
									<div class="wrap-input100 validate-input" data-validate = "Valid email is required: 123456">
										<input class="input100" id="pin_no" type="pin" class="form-control{{ $errors->has('pin_no') ? ' is-invalid' : '' }}" name="pin_no" value="{{ old('pin_no') }}"   placeholder="Pin/ZIP Code" required>
										
										
										@if ($errors->has('pin_no'))
											
											<center>  <strong style="color:red">{{ $errors->first('pin_no') }}</strong></center>
									
										@endif
										<span class="symbol-input100">
											<i class="fa fa-user " aria-hidden="true"></i>
										</span>

										
									</div>

							
							<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
								<input class="input100" id="aadhar_no" type="number" class="form-control{{ $errors->has('aadhar_no') ? ' is-invalid' : '' }}" name="aadhar_no" value="{{ old('aadhar_no') }}"   placeholder="Aadhar No" required>
								
								
                                @if ($errors->has('aadhar_no'))
                                    
                                      <center>  <strong style="color:red">{{ $errors->first('aadhar_no') }}</strong></center>
                               
                                @endif
								<span class="symbol-input100">
									<i class="fa fa-user " aria-hidden="true"></i>
								</span>

								
							</div>
							Enter GSTIN Number
							<div class="wrap-input100 validate-input" data-validate = "Valid email is required: dfgd">
								<input class="input100" id="gst_no" type="text" class="form-control{{ $errors->has('gst_no') ? ' is-invalid' : '' }}" name="gst_no" value="{{ old('gst_no') }}"   placeholder="GSTIN NO" required>
								
								
                                @if ($errors->has('gst_no'))
                                    
                                      <center>  <strong style="color:red">{{ $errors->first('gst_no') }}</strong></center>
                               
                                @endif
								<span class="symbol-input100">
									<i class="fa fa-user " aria-hidden="true"></i>
								</span>

								
							</div>

							Profile Picture
							<div class="wrap-input100 validate-input" data-validate = "Valid email is required: 123456">
								<input class="input100" id="profile_image" onchange="validateImage(this.id);" type="file" class="form-control{{ $errors->has('profile_image') ? ' is-invalid' : '' }}" name="profile_image" value="{{ old('profile_image') }}"  placeholder="Profile Picture" required>
								
								
                                @if ($errors->has('profile_image'))
                                    
                                      <center>  <strong style="color:red">{{ $errors->first('profile_image') }}</strong></center>
                               
                                @endif
								<span class="symbol-input100">
									<i class="fa fa-user " aria-hidden="true"></i>
								</span>

								
							</div>


							Vendor Sign
							<div class="wrap-input100 validate-input" data-validate = "Valid email is required: 123456">
								<input class="input100" id="vendor_sign" onchange="validateImage(this.id);" type="file" class="form-control{{ $errors->has('pan_image') ? ' is-invalid' : '' }}" name="vendor_sign" value="{{ old('pan_image') }}" placeholder="PAN Image" required>
								
								
                                @if ($errors->has('vendor_sign'))
                                    
                                      <center>  <strong style="color:red">{{ $errors->first('vendor_sign') }}</strong></center>
                               
                                @endif
								<span class="symbol-input100">
									<i class="fa fa-user " aria-hidden="true"></i>
								</span>

								
							</div>







									<div class="wrap-input100 validate-input" data-validate = "Password is required">
										<input class="input100" id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>
									
										@if ($errors->has('password'))
									
												<strong style="color:red">{{ $errors->first('password') }}</strong>
									
										@endif
										<span class="symbol-input100">
											<i class="fa fa-lock" aria-hidden="true"></i>
										</span>

										
									</div>







						</div>
						<div class="col-md-6">
								<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
										<input class="input100" placeholder="Email" id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}"  required >
										
										
										@if ($errors->has('email'))
											
											<center>  <strong style="color:red">{{ $errors->first('email') }}</strong></center>
									
										@endif
										<span class="symbol-input100">
											<i class="fa fa-envelope" aria-hidden="true"></i>
										</span>

										
									</div>
									
									<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
										<input class="input100" id="address" type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="{{ old('address') }}" placeholder="Address" required> 
										
										
										@if ($errors->has('address'))
											
											<center>  <strong style="color:red">{{ $errors->first('address') }}</strong></center>
									
										@endif
										<span class="symbol-input100">
											<i class="fa fa-user " aria-hidden="true"></i>
										</span>

										
									</div>


									<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
								<input class="input100" id="companey_name" type="text" class="form-control{{ $errors->has('aadhar_no') ? ' is-invalid' : '' }}" name="companey_name" value="{{ old('companey_name') }}"  placeholder="Companey Name" required>
								
								
                                @if ($errors->has('aadhar_no'))
                                    
                                      <center>  <strong style="color:red">{{ $errors->first('aadhar_no') }}</strong></center>
                               
                                @endif
								<span class="symbol-input100">
									<i class="fa fa-user " aria-hidden="true"></i>
								</span>

								</div>

								<div class="wrap-input100 validate-input" data-validate = "Valid email is required: 3453">
								<input class="input100" placeholder="Pan No" id="pan_no" type="text" class="form-control{{ $errors->has('pan_no') ? ' is-invalid' : '' }}" name="pan_no" value="{{ old('pan_no') }}" required>
								
								
                                @if ($errors->has('pan_no'))
                                    
                                      <center>  <strong style="color:red">{{ $errors->first('pan_no') }}</strong></center>
                               
                                @endif
								<span class="symbol-input100">
									<i class="fa fa-envelope" aria-hidden="true"></i>
								</span>

								
							</div>

							Aadhar Card Image
							<div class="wrap-input100 validate-input" data-validate = "Valid email is required: 123456">
								<input class="input100" id="aadhar_image"  onchange="validateImage(this.id);" type="file" class="form-control{{ $errors->has('aadhar_image') ? ' is-invalid' : '' }}" name="aadhar_image" value="{{ old('aadhar_image') }}"  placeholder="Aadhar Image" required>
								
                                @if ($errors->has('aadhar_image'))
                                    
                                      <center>  <strong style="color:red">{{ $errors->first('aadhar_image') }}</strong></center>
                               
                                @endif
								<span class="symbol-input100">
									<i class="fa fa-user " aria-hidden="true"></i>
								</span>

							</div>


							PAN Card Image
							<div class="wrap-input100 validate-input" data-validate = "Valid email is required: 123456">
								<input class="input100" id="pan_image" onchange="validateImage(this.id);" type="file" class="form-control{{ $errors->has('pan_image') ? ' is-invalid' : '' }}" name="pan_image" value="{{ old('pan_image') }}"  placeholder="PAN Image" required>
								
								
                                @if ($errors->has('pan_image'))
                                    
                                      <center>  <strong style="color:red">{{ $errors->first('pan_image') }}</strong></center>
                               
                                @endif
								<span class="symbol-input100">
									<i class="fa fa-user " aria-hidden="true"></i>
								</span>
							</div>
							Companey LOGO
							<div class="wrap-input100 validate-input" data-validate = "Valid email is required: 123456">
								<input class="input100" id="companey_logo" onchange="validateImage(this.id);" type="file" class="form-control{{ $errors->has('pan_image') ? ' is-invalid' : '' }}" name="companey_logo" value="{{ old('pan_image') }}"   placeholder="PAN Image" required>
								
								
                                @if ($errors->has('companey_logo'))
                                    
                                      <center>  <strong style="color:red">{{ $errors->first('companey_logo') }}</strong></center>
                               
                                @endif
								<span class="symbol-input100">
									<i class="fa fa-user " aria-hidden="true"></i>
								</span>

								
							</div>
									
									<div class="wrap-input100 validate-input" data-validate = "Password is required">
										<input class="input100" id="password-confirm" type="password" class="form-control" name="password_confirmation" value="{{ old('password_confirmation') }}" placeholder="Comfirm Password" required>
									
										@if ($errors->has('password_confirmation'))
									
												<strong style="color:red">{{ $errors->first('password_confirmation') }}</strong>
									
										@endif
										<span class="symbol-input100">
											<i class="fa fa-lock" aria-hidden="true"></i>
										</span>
									</div>
						</div>
						</div>

							<div class="container-login100-form-btn">
							<button type="submit" class="login100-form-btn btn-primary">
                                    {{ __('Register') }}
                                </button>
								
							</div>
						
							<div class="text-center pt-2">			
							</div>

							<div class="text-center pt-1">
								<a class="txt2" href="{{ route('vendor_login') }}">
									Alredy Have an account? Please Login
									<i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
								</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
function validateImage(id) {
    var formData = new FormData();
 
    var file = document.getElementById(id).files[0];
 
    formData.append("Filedata", file);
    var t = file.type.split('/').pop().toLowerCase();
    if (t != "jpeg" && t != "jpg" && t != "png" && t != "bmp" && t != "gif") {
        alert('Please select a valid image file');
        document.getElementById(id).value = '';
        return false;
    }
    if (file.size > 1024000) {
        alert('Max Upload size is 1MB only');
        document.getElementById(id).value = '';
        return false;
    }
    return true;
}
</script>
@endsection
	