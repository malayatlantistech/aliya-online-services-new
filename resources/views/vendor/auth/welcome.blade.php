<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
	<meta content="Fully Responsive Bootstrap 4 Admin Dashboard Template" name="description">
	<meta content="Spruko" name="author">

	<!-- Title -->
	<title>Sell on ALIYA Online Shopping | Grow your business today with best online selling platform for e-commerce solutions.</title>

	<!-- Favicon -->
	<link href="../vendor_assets/img/brand/bansal_logo.png" rel="icon" type="image/png">

	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:300,400,600,700,800" rel="stylesheet">

	<!-- Icons -->
	<link href="../vendor_assets/css/icons.css" rel="stylesheet">

	<!--Bootstrap.min css-->
	<link rel="stylesheet" href="../vendor_assets/plugins/bootstrap/css/bootstrap.min.css">

	<!-- Adon CSS -->
	<link href="../vendor_assets/css/dashboard.css" rel="stylesheet" type="text/css">

	<!-- Custom scroll bar css-->
	<link href="../vendor_assets/plugins/customscroll/jquery.mCustomScrollbar.css" rel="stylesheet" />

	<!-- Sidemenu Css -->
	<link href="../vendor_assets/plugins/toggle-sidebar/css/sidemenu.css" rel="stylesheet">

</head>
<body>
	<div class="page bg-gradient-primary">
		<div class="page-main">
			<div id="notfound">
				<div class="notfound">
					@isset($name)
					<h1>Hi {{$name}},</h1>
					@endisset
                
					<h2>Your are sucessfully register on ALIYA Online Services. Please wait for admin approval.
					<br>
					<font style="font-size:15px">Or contact to support@aliya.com || 0987653345 </font>
					</h2>
					
					<a href="/" class="btn btn-primary btn-pill mt-4">Back to Home</a>

					
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
								
				</div>
				
			</div>
		
		</div>
	</div>
	<!-- Adon Scripts -->

	<!-- Core -->
	<script src="../vendor_assets/plugins/jquery/dist/jquery.min.js"></script>
	<script src="../vendor_assets/js/popper.js"></script>
	<script src="../vendor_assets/plugins/bootstrap/js/bootstrap.min.js"></script>

	<!-- Optional JS -->
	<script src="../vendor_assets/plugins/chart.js/dist/Chart.min.js"></script>
	<script src="../vendor_assets/plugins/chart.js/dist/Chart.extension.js"></script>

	<!-- Data tables -->
	<script src="../vendor_assets/plugins/datatable/jquery.dataTables.min.js"></script>
	<script src="../vendor_assets/plugins/datatable/dataTables.bootstrap4.min.js"></script>

	<!-- Fullside-menu Js-->
	<script src="../vendor_assets/plugins/toggle-sidebar/js/sidemenu.js"></script>

	<!-- Custom scroll bar Js-->
	<script src="../vendor_assets/plugins/customscroll/jquery.mCustomScrollbar.concat.min.js"></script>

	<!-- Adon JS -->
	<script src="../vendor_assets/js/custom.js"></script>
</body>
</html>