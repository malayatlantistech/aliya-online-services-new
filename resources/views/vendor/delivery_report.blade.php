@extends('vendor.layouts.menu')
@section('body')

<div class="container-fluid pt-8">
							<div class="page-header mt-0  p-3">
								<h3 class="mb-sm-0">Delivery Report</h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">Admin Dashboard</li>
								</ol>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="card shadow">
										<div class="card-header">
											<h2 class="mb-0">Delivery Report</h2>
										</div>
										<div class="card-body">
											<div class="table-responsive">
											
												<table id="example" class="table table-striped table-bordered w-100 text-nowrap">
													<thead>
														<tr>

															<th class="wd-15p">Order <br>Date</th>
															<th class="wd-15p">Order<br>ID</th>
															<th class="wd-15p">Order <br>Value</th>
															<th class="wd-15p">Current <br>Order <br>Status<br></th>
															<th class="wd-15p">Custromar <br>Pincode</th>
															<th class="wd-15p">Delivery <br>Boy Name</th>
															<th class="wd-15p">Delivery <br>Date</th>
															<th class="wd-15p">Return <br>Boy Name</th>
															<th class="wd-15p">Return <br>Date<br></th>
														
                                                  			
														
															
														</tr>
													</thead>
													<tbody>
													
													@foreach($booking as $booking)
													<tr>
                                                            <td>{{$booking->order_date}}</td>
															<td>{{$booking->order_id}}-{{$booking->multi_id}}</td>
															<td>&#8377; {{($booking->product_price+$booking->gst) * ($booking->quantity1)}}/-</td>
															<td>

																@if($booking->order_status==1)
																	<span class="badge badge-primary">Order Under Process</span><br>
																@elseif($booking->order_status==2)
																	<span class="badge badge-primary">Order In Transit</span><br>
																@elseif($booking->order_status==3)
																	<span class="badge badge-primary">Out for Delivery</span><br>
																@elseif($booking->order_status==4)
																	<span class="badge badge-info">Delivered</span>
																@elseif($booking->order_status==5)
																	<span class="badge badge-primary">Return Under Process</span><br>
																@elseif($booking->order_status==6)
																	<span class="badge badge-primary">Out for Pickup</span><br>
																@elseif($booking->order_status==7)
																	<span class="badge badge-warning">Returned</span>
																@endif
																
																
																
															
															<td>
																@php($address=DB::table('addres')->where('id',$booking->address_id)->first())
																{{$address->pincode }}({{$address->city}})


															</td>

															<td>
																@if($booking->delivery_boy_id!=0)
																@php($seli=DB::table('users')->where('id',$booking->delivery_boy_id)->first())
																{{$seli->name}}<br>
															{{$seli->email}}
															@else
															--
																@endif

															</td>
															<td>
																@if($booking->delivery_boy_id!=0)
																{{$booking->delivery_date}}
																@else
																--
																	@endif
															</td>

															<td>	@if($booking->return_delivery_boy_id!=0)
																@php($seli=DB::table('users')->where('id',$booking->return_delivery_boy_id)->first())
																{{$seli->name}}<br>
															{{$seli->email}}
															@else
															--
																@endif</td>
															<td>
																@if($booking->return_delivery_boy_id!=0)
																{{$booking->return_date}}
																@else
															--
																@endif
															
															</td>
												
															
														
                                       
                                                            
														
													</tr>													@endforeach
                                                   
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
	<script
			src="https://code.jquery.com/jquery-3.4.1.js"
			integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
			crossorigin="anonymous">
	</script>
	<script>
		$(document).ready(function() {
			

			$('#example').DataTable( {
				"order": [[ 0, "desc" ]]
			} );
			
		});
									   
	</script>
@endsection