@extends('vendor.layouts.menu')
@section('body')

<div class="container-fluid pt-8">
							<div class="page-header mt-0  p-3">
								<h3 class="mb-sm-0">Inventory Report</h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">Vendor Dashboard</li>
								</ol>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="card shadow">
										<div class="card-header">
											<h2 class="mb-0">Inventory Report</h2>
										</div>
										<div class="card-body">
											<div class="table-responsive">
												<table id="example" class="table table-striped table-bordered w-100 text-nowrap">
													<thead>
														<tr>
														
															<th class="wd-15p">Product<br>Code</th>
															<th class="wd-15p">Product<br>Name</th>
															<th class="wd-15p">Product<br>Weight</th>
															<th class="wd-15p">Product<br>Size</th>
															<th class="wd-15p">Product<br>Color</th>
															<th class="wd-15p">Total-Stock</th>
                                                            <th class="wd-15p">Out-Stock</th>
                                                            <th class="wd-15p">Available Stock</th>
															<th class="wd-15p">View<br>More Details</th>
															
														</tr>
													</thead>
													<tbody>
													
													@foreach($product as $product)
													<tr>
                                                    <td>
													{{$product->product_code}}
													
                                                    </td>
                                                    <td>
													{{$product->product_name}}
                                                    </td>
													<td>
													{{$product->weight}}gm ({{round($product->weight/1000,2)}} Kg)
													</td>
													<td>
														{{$product->size}}
														</td>
														<td>
															{{$product->color}}
															</td>
                                                    <td>
													{{$product->total_stock}}
                                                    </td>
                                                    <td>
													{{$product->total_stock-$product->available_stock}}
                                                    </td>
                                                    <td>
												<b style="font-size: 20px;">	{{$product->available_stock}}</b> @if($product->available_stock<6) &nbsp;<span class="badge badge-pill badge-danger"><b>Low Stock</b></span>@endif
                                                    </td>
													<td>
													<a href="inventory_details?product_id={{$product->product_id}}" class="btn btn-icon btn-sm  btn-primary mt-1 mb-1" style="color:white">
												<span class="btn-inner--icon"><i class="fe fe-eye"></i></span>
												<span class="btn-inner--text">More Details</span>
											</a>
											<a href="add_stock?product_id={{$product->product_id}}" class="btn btn-icon btn-sm btn-info mt-1 mb-1" style="color:white">
												<span class="btn-inner--icon"><i class="fe fe-plus "></i></span>
												<span class="btn-inner--text">Add Stoke</span>
											</a>
													</td>
											</tr>
@endforeach
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
	
@endsection