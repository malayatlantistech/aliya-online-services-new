<!DOCTYPE html>
<html>
<head>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="assets/img/brand/favicon.png" rel="icon" type="image/png">

	<!-- Fonts -->
    <title>Delivery Slip : E-Kirana</title>

<style>
.clearfix {
  overflow: auto;
}
body{
    font-size:10px
}
</style>

<style>

  @media print {
    #printPageButton {
      display: none;
    }
  }
    </style>
</head>
<body class="" style="padding:5px;margin:5px">

 <div class="row" style="border-bottom: solid 1px;">
 {{--   <div class="col-xs-12">
<table  border="1" style="width:100%;border-top: none; ">
    <tr >

        <td rowspan="2" style="border-left: none">a</td>
        <td colspan="2">b</td>
        
        <td rowspan="2">d</td>

    </tr>
    <tr style="border-left: none;border-right: none;">

        
        <td>f</td>
        <td>g</td>
      

    </tr>
</table>
</div> --}}


    <div class="col-xs-2"></div>
    <div class="col-xs-6" style="border-left: solid 1px;border-right: solid 1px;">
        <div>E-Kirana Logistics</div>
        <div class="row" style="border-top: solid 1px;">
            <div class="col-xs-7" style="border-right: solid 1px;    font-size: 11px;font-weight: 800;">
                @php( $debit_amount=$book->wallet_amount+$book->coupon_discount-$book->delivery_charge)
                @php($per=(($book->product_price+$book->gst)/($book->price))*100)
             @if($book->payment_type=='cash')
                COD : Rs. {{round(($book->product_price+$book->gst)-(($debit_amount*$per)/100))}}/-
                @else
                COD : Rs. 0/-
               @endif

            </div>
            <div class="col-xs-5">
                Order Date : {{date('d-m-Y h:i:s',strtotime($book->created_at))}}
            </div>
            
        </div>

    </div>
    <div class="col-xs-4" style="font-weight: 800;font-size: 16px;">
        {{date('d-m-Y')}}
    </div>
 

</div>
<div class="row" style="border-bottom: solid 1px;">
    <div class="col-xs-6" style="border-right: solid 1px; padding:14px">
        <center>
            {{$book->order_id}}-{{$book->multi_id}}<br>
            <img src="data:image/png;base64,{{DNS1D::getBarcodePNG($book->booking_id.'-'.$book->multi_id, 'C39+')}}" alt="barcode" /> <br/><br/>
            {{-- <img style="width:70%" src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSgLmqxvAfvb76EK2splIIZoiZUlaZYyGFdIQ&usqp=CAU">
            --}}
        </center>
    </div>
    @php($shipping=DB::table('addres')->where('id',$book->address_id)->first())
    <div class="col-xs-6" style="padding:10px ">
        <span><b>Delivery Address : </b></span><br>
        {{$book->name}}, Ph no.-{{$shipping->phone_no}}/ {{$shipping->optional_phone}}<br>
        {{$shipping->flat}}, {{$shipping->address}}, {{$shipping->location}},<br>
        {{$shipping->landmark}}, {{$shipping->city}}, {{$shipping->district}},<br>
        {{$shipping->state}}, Pin Code - {{$shipping->pincode}}<br>
    </div>

</div>
<div class="row" style="">
    <div class="col-xs-6" style=" padding:14px 14px 14px 20px">
        <span><b>Return Address : </b></span><br>
        MalayApi, Ph no.-9733118409/6294008510<br>
        Modual-104,Webel IT Park,<br>
        Beside Mosjit Devog,Haldia,Purba Medinipur<br>
        West Bengal,India-721657<br>
       
    </div>
    <div class="col-xs-6" style="padding:15px;border-left: solid 1px; ">
        <center>
            <img src="data:image/png;base64,{{DNS2D::getBarcodePNG($book->booking_id.'-'.$book->multi_id, 'QRCODE')}}" alt="barcode" /><br/><br/>
            {{-- <img style="width:40%" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAY1BMVEX///8AAABBQUHo6OhnZ2d9fX2VlZV4eHj39/ecnJyZmZmvr6/Hx8cnJyfi4uKHh4fS0tJwcHBVVVXBwcGpqamOjo5fX1+7u7vc3NxMTEw1NTWioqK1tbUsLCxaWloaGhoODg7AOte/AAANK0lEQVR4nO2daWOqPBCFW60bbrgirvf//8rXzOGtB4cJiW1vbW/OpwhhyIOaZTIJLy9JSUlJSUlJSUlJT6ZtJ1D7nmVCTp+Gkl5IeqfyFHuvcWTakyGo57+MtDUJO6+herNM4DQKtpR0rvKM/ba1IegtuHSdRJgIE+H/BTtLWtc0EYR82ecRnkeZrVGpCIfl9qolPuycZgt35G0qWeUs63yY7eqaTWpPT8xJnhnSXSIsvaU7BxGOzPNOPU2oH/fJ+4j1DbrWN4mHxIRmOyUaBRFmHyfcRxLOPoswS4SJsJGwodqbRhKa/8M/X0i4XgzqWhSKcCh5evnEiQ3JkRzm1u5DvlCEOd1gLnlWOL2Ti3HPiVgaKsJClW4dTchFgvqKEI/+YplDi4/2cK4IV3QETU3t5460bvFB2FelW0QTDoIJp37C/O8QDhJhInyEEB08/A83XsLSImRz30d46W+umrOhzB3ZHAunkUv2QdtbuSNrOXsobkJFPMQFTDgX07ou/buEr/pxc0PNlfJRjmBsUXskWmxat4ffTog+zUwRjhNhInwaQut/+EMJL+O108Gp6rXTkYMkx2dFuKzON2uNe8rVh++uS6f80Nkct4dzRRgkNveEhLpPE034VH2aRPh0hHp8uIklZH8pCNGnOX6ccKNyxo8Pi/6dNhM/4UZyHW8qYE4OFVsUr3BpmCiz+ztUQonl7MYinGzuryqiCS21jC3Cvx6zPcRpffw7PFGJMBF+F2H4vMXzEYbNW5Q9nzoW4Uq8ppJnfpETC0ljnLGduzT6caVL90aSH23JcEUuVyaUwyvul3a8pSuDCEPU0h6CiscWKP1B0gc8ETI0swxpn3eI/hohjw9XksaPiD1RCyLca0N63iIR/uuE/kmxCMLT9xLuTcLeW6j6TIhp+a07vsXE/MClz1zTaMJcLsiI8ARDFmE/uHT+FiVGDX4apLnDbBGygmZIv0OJUJQInRLh55VYhPSw6xMQuvvSiYskBzo7yolKfiwfRh13dkQmcJu8U95UPSTJw4QhJWoTmjF0hrWfhnXmy5gQsmIx2BOFKj2XdKnNsUB49pZoEUTYIULta2M9GE+jvfogXIYQ+lv8QSJMhL+LcHjVS+8yvQqDdUlW6T+SXrwMb6oIJc2E03tdxi7Ty7oidPknFSFdLNfV4jc1IZfof0IqUQshBGd8nx59QY9+Zz1KJtTNWOb9GmrBVX5C+H+t6YFTECHcDvCeI2YMzvheOOFM3WDkJawNevyEmB5YG4bCRsCJMBH+RELubLUQTh8iNF3LINwGE9pejA7Ze1GE0ETZq9VczMmCq3ysDNWaMf2oLKGOZ8crZK4DqRHyEpAQwoZHrxskngN+oYJxh3n4GGFBV9mr1hJhIvxZhFzT9I2C1RROyI8KBWtY1hBOuFYlKv2XtXj1+5RzR/ZaHj37abRy6244zc4C/m1Z0SZtSoSJMBH+PMKqh+snRId5ok+EEPJQnwl1BzfGm5iLIwCPfizpjAgn0/1+fxrI8Z1L7zvkPhi6A/sLtxY4vjldj0+ruDYx0SfCJZtAicVQzd9TLdxwhvb83c5bPBeakD1R8BYcmFDSLdEm/hY/2hPFhD11fBPElggT4e8gzIjwTIRB/VJ04TmCFgXbkblvJ9yfl1dturOrqt65S852pTteasLe8l3n083EGTXkSczhlhdJv5HR6uHtZu/qirmy6M7q6h7L5Z3slsNPCOk4B3PZ4NL6Ngw1uEN0jLBuWAtlqM0T9W2EDb62RJgImwk5WAuVH/cCIwj909LRhCiR3u5GE7bNzGwXTkXuVEUk5DcdFzfNmXDgjlSzjnJ2YM3m93b5nQomlPu/IQ+OzMUcquOlnMU3uXpzH0Yu6w7e4VIu01vi1AlfjWcFsSeqw4ThsnaNaIjc0+WC2OeN9hBdhzBPFGQNd5iwYXz4GKEZmwiVAYSTRPiPEPKAM/x/GLECAeoro2Yku0WILvyBCBu68E2lX101gb3F/F09/j67xepdBc7jppPb8Qlq0bWkuVWcSyZd1Q0LZW4jdx4qwr6YQL94Jndby1WZpK3vpS6ep4PWRs6WtdzWjgOWGn7umtBq8cM8UZ9MaK1WT4SJ8MOEujv5af/DLyQM85eus9FV6/FVR64CmXDWv2bJUGUMJStU5RELB9Slc0mjkZ0fXabD6E5ZwYQwZxH2x3cmMtScM7ms2gBGSmd9I/Uf18AgbGlZ9dfAj167qhtCCyxC7afRXQe4fdpiMXRsoiZs2ftSi+O8WQ0/rnDCg0EYMwJOhD+N0Pof6kCf6P/hpxHypmGacNZCaNWlgyPVmaP7urTWFcvua8uqLmXCrdSHeISlmF6RuTGqY03Yp3ItxMRhfK+2lc6Q373Cfhq0h3/85vSKEjx6cxiGyzQhC41MfOQepPs0fkJzvzaLMHpHOv/4MBEmwnvxRgGfRtjyP9S+Nv//0O7ThBBuJ7exdLUTFkb3bEKOFCjeWIbgYxnvD4hw17t5ETBwr8b4MNGjMf7IpTfsY4GzAKN7jh3ei2vAv+9FG2HFSYSV2ISOEdZ7X7IifgwsHUHb5okKJ4zYkS6EsOZrs6TXzMTHJibC30IYMmUURMiBql9IyP/Ds99EpT7NLul6ei/HUVsN3245q9GGzBjtME1U3CaXdhmZswhPPCdF5ioTGzFRq1Fl1gl90b3MfWlncyutIozY0JGFxuTgJaxJm9NjC9aj+yjofdFaLPkJH1zLzYQ62gSK8bUlwkToIcyeg7DLcUjK0kLFRM0o3UUEFLZ3olilEt7EsTtUHsQEByF0OxTRxIRbMXchwqy8hVVBElxVxtSiIUEiDXFtnLZWyUK8CK5BbAhiPw3Ey23jW4jYPk3LLkoWYcR+bZqQW7GYyL1EmAifnxBB7wuJhufV/NP9nU6om7sXitVHVoTNa0IOxofzocch9py+INyftJVA/5zyHKa3iP0aYUvcPm9Cxu2Ef9rPbAOZ0GrGeFdBc8cBlBi/LfaWjBUhvPptcW3+eQstM57GGh+yGvZr04S8lptnSDmehgk/NjOTCBNhIvx7hBCvt4XYK9JQJKT9nqhXVbCa2JBfOnKvLa4tdi13wxZdSIcQRu8jrPVo9GUiTIQvP5CQIwLM9f2xhBEjYEsg5MWoYTsO8K6ChZGT96f5o4v3ahDq+FJzHTDLchbw/OGj+7WFEJob52hC3YVPhIkwEXoI/TVNzYX75YTWmJ0J4/cvhTdg4zYo/FOLa8PGiZKzRC7aILHaA6GrfAlMeHQmLvVlEs3bMFY+b7UBY7UN44Euq8olsnunj7X4ZvCyRRi0lhtp/4uFuZ3mH0PM2OLnEDas5UmEv5KQ94Lmgln9UnNlVwgh7wVtbi392f9D3s8bGqq9snlX7V3nfgPurjYxlz28C2M/bxTsIna2uhQgzNXm3fzkJ1KKThBhrMxoE5Z/T3Z+9A2OXGtPdmu/ticnbIinSYROifAjiiDkVUE8Lc2EF33x6SFCe2Ym9j0zQ3kvTM6ElKf2nplS3kKzlgu4wDBRCYfONxMLfqNVHy+y8RKWMzLUoNh3BVWFZELOFLv+8EWbeCVC9nl/bO/LRJgIfx9hrSOpCfU23A8SbhWh3qE1jDD8/YfDgRyCcXmp4WByU/Xawp18AOfSZRqgbdzJxShkd0B3sAhhaEiEYm7OsSNhhLHvsKzJuox93ggw1u9dM2W1h1phhLHvIY0mtN4slwgT4bMRToiwwRBLuwysl52EzXLHvpf7Usibty3Co7s4gyNA3st9XMsRtJMdef/2iF7nXb2XHEYReza/L0vGMbBvxe1aa9+gJsJPeLf6izKE9lC/HsaciPT7abhcYWJC/Tak/scJ2RPFetDXlggT4RcQWv/DJye8jNdXVcV2yfUB1Xshx3mZ8mCM807c/6xWOh9uZzNNOD+8a13ri8Ioye7gP0bYIGsJNsTtYY0QH7hE1p7s/pd/te3e8mmE1sKNoNXqfkKrTwO1zcwkwkQYSNiy8wfEKwbCCb/of9jfXFXdeVxctdJOka3LsznIWUxNduSyzB0pjnJ2sypu2pDGdPxAx9/IEGSP4T9ECBPeZ9vQHvLYYqpNcOn0W8kg/NzDVsl+OaHu05hvj4f0TsmaMGZHukT4uwj1+PBZCVcPEq4Xg7oWhSIcIg8TDu7Vw3TPWT6s88lkkq/p9IEIT3K2FgdWTfY7R2mOxmcrl43zd7ds3n+Q0BITVmJCLTwAnj8MWunMhNY7bllfSdjQULMefHs8E+q9LxPhv04YPm/xUwnPo8zWqGRCTL/jFl01IS9z6rP5ebvdLnMinM9us/mYx58stzdxueXsbHA7uSzcxbPReVvXeYO7BRGGKGhsoQMB9WskGpZJWOYg/IasZQ1hPu9PI0QDxpuIM2H02+P9hPH76ifCRNhIyC6yLyFs2wlr2wnUHtVE99R8+gTChaS5plnvbybQ7O7ERG0pwVSZq71H25nY6xdG5s7Q/tF9eJKSkpKSkpKSkpK+WP8BNfZx4gqtLCYAAAAASUVORK5CYII="> --}}
            {{$book->order_id}}-{{$book->multi_id}}
        </center>
    </div>

</div>
<div class="">
    <table class="table"  style="border-bottom:solid 1px;margin-bottom: 0px;">
        <tr style="border-bottom:solid 2px;border-top:solid 2px;">
            <th ></th>
            <th style="text-align: center; border:solid 1px; border-top:none">Seller Name</th>
            <th style="text-align: center;border:solid 1px;border-top:none">GSTIN</th>
            <th style="text-align: center;border:solid 1px;border-top:none">Invoice Number</th>
            <th style="text-align: center;border-top:none">Date</th>
        </tr>
        <tbody >
        <tr style="height: 250px;">
            <td align="center">1</td>
            <td align="center" style="border:solid 1px; border-top:none">E-Kirana</td>
            <td align="center" style="border:solid 1px; border-top:none">DFEFSEFSF55</td>
            <td align="center" style=" border:solid 1px; border-top:none">{{$book->order_id}}-{{$book->multi_id}}</td>
            <td align="center">{{date('d-m-Y',strtotime($book->created_at))}}</td>

        </tr>
    </tbody>

    </table>
</div>
<div class="row">

    <div class="col-xs-5" style=" border:solid 1px; border-top:none;border-left:none">
        <center>Quality Products.<br>
        Delivered Fast
        </center>

    </div>
    <div class="col-xs-7" style=" border:solid 1px; border-top:none;border-left:none;border-right:none">
      
            The goods slod are<br>
            <b>Not For Resale</b>
        
    </div>


</div>
<div class="row">
    <div class="col-xs-12" style=" border-bottom:solid 1px; border-top:none;border-left:none">
        <div style="margin:10px">Order Through :   <img style="height:20px" src="http://admin.localhost:8000/logo/OHMAGO%20Delivery%20Services%20Logo%20PNG.png"> <b style="font-size:12px">E-Kirana</b>
        </div>

    </div>
   


</div>
<div class="row">
  
    <div class="col-xs-3" style=" border-right:solid 1px; border-top:none">
        <center></center>

    </div>
    <div class="col-xs-2" style=" border-right:solid 1px; border-top:none">
        <center>OTY : {{$book->quantity1}}</center>

    </div>
    <div class="col-xs-3" style=" border-right:solid 1px; border-top:none;border-left:none">
      Pack : 1

    </div>
    <div class="col-xs-3" style=" ">
      Invoice : 1
    </div>


</div>
<a onclick="window.print()" class="btn btn-primary" id="printPageButton"><i class="fa fa-print"></i> Print</a>
{{-- <img src="data:image/png;base64,{{DNS1D::getBarcodePNG('11', 'C39')}}" alt="barcode" /> <br/><br/>
	<img src="data:image/png;base64,{{DNS1D::getBarcodePNG('DEWRFEWFEGFER', 'C39+')}}" alt="barcode" /> <br/><br/>
	<img src="data:image/png;base64,{{DNS1D::getBarcodePNG('13', 'C39E')}}" alt="barcode" /><br/><br/>
	<img src="data:image/png;base64,{{DNS1D::getBarcodePNG('14', 'C39E+')}}" alt="barcode" /><br/><br/>
	<img src="data:image/png;base64,{{DNS1D::getBarcodePNG('15', 'C93')}}" alt="barcode" /><br/>
	<br/>
	<br/>
	<img src="data:image/png;base64,{{DNS1D::getBarcodePNG('19', 'S25')}}" alt="barcode" /><br/><br/>
	<img src="data:image/png;base64,{{DNS1D::getBarcodePNG('20', 'S25+')}}" alt="barcode" /><br/><br/>
	<img src="data:image/png;base64,{{DNS1D::getBarcodePNG('21', 'I25')}}" alt="barcode" /><br/><br/>
	<img src="data:image/png;base64,{{DNS1D::getBarcodePNG('22', 'MSI+')}}" alt="barcode" /><br/><br/>
	<img src="data:image/png;base64,{{DNS1D::getBarcodePNG('23', 'POSTNET')}}" alt="barcode" /><br/>
	<br/>
	<br/>
	<img src="data:image/png;base64,{{DNS2D::getBarcodePNG('EFRETGRTGTRFGDF', 'QRCODE')}}" alt="barcode" /><br/><br/>
	<img src="data:image/png;base64,{{DNS2D::getBarcodePNG('17', 'PDF417')}}" alt="barcode" /><br/><br/>
	<img src="data:image/png;base64,{{DNS2D::getBarcodePNG('18', 'DATAMATRIX')}}" alt="barcode" /> --}}


</body>
</html>