@extends('layouts.menu')

@section('title')
Privacy Policy : E-Kirana
@endsection

@section('content')

<div class="breadcrumb section pt-60 pb-60">
    <div class="container">
      <h1 class="uppercase">Privacy Policy</h1>
      <ul>
        <li><a href="/"><i class="fa fa-home"></i></a></li>
        <li class="active">Privacy Policy</li>
      </ul>
    </div>
  </div>
  <!-- =====  BREADCRUMB END===== -->
  <div class="page-about section">
    <!-- =====  CONTAINER START  ===== -->
    <div class="container">
      <div class="row ">        
        
        <div class="col-lg-12 col-xl-12 mb-20">
          <!-- about  -->
          <div class="row">
      
            <div class="col-md-12">
              <div class="about-text">
                <div class="about-heading-wrap">
                  <h2 class="about-heading mb-20 mt-20 py-2">Personal <span>Information </span></h2>
                </div>
                <p>E-KIRANA is the licensed owner of the brand E-KIRANA.CO.IN and the website <a href="/">ekirana.co.in</a> (”The Site”) from Supermarket Grocery Supplies. E-KIRANA respects your privacy. This Privacy Policy provides succinctly the manner your data is collected and used by E-KIRANA on the Site. As a visitor to the Site/ Customer you are advised to please read the Privacy Policy carefully. By accessing the services provided by the Site you agree to the collection and use of your data by E-KIRANA in the manner provided in this Privacy Policy.</p>
              </div>
            </div>
          </div>
          <!-- =====  What We Do? ===== -->
          <div class="row">
            <div class="col-md-12">
              <div class="heading-part mb-20 mt-40">
                <h3 class="section_title" style=" text-align: left">What information is, or may be, collected form you</h3>
              </div>
                <div id="">
                  <p>As part of the registration process on the Site, E-KIRANA may collect the following personally identifiable information about you: Name including first and last name, alternate email address, mobile phone number and contact details, Postal code, Demographic profile (like your age, gender, occupation, education, address etc.) and information about the pages on the site you visit/access, the links you click on the site, the number of times you access the page and any such browsing information.</p>
             
                </div>
              
              </div>
  
            </div>
            <div class="col-md-12">
              <div class="heading-part mb-20 mt-40 " >
                <h3 class="section_title" style=" text-align: left">How do we Collect the Information</h3>
              </div>
              <div id="">
                  <p>E-KIRANA will collect personally identifiable information about you only as part of a voluntary registration process, on-line survey or any combination thereof. The Site may contain links to other Web sites. E-KIRANA is not responsible for the privacy practices of such Web sites which it does not own, manage or control. The Site and third-party vendors, including Google, use first-party cookies (such as the Google Analytics cookie) and third-party cookies (such as the Double-click cookie) together to inform, optimize, and serve ads based on someone's past visits to the Site.</p>
              </div>
            </div>
            <div class="col-md-12">
              <div class="heading-part mb-20 mt-40 ">
                <h3 class="section_title" style=" text-align: left">How is information used</h3>
              </div>
              <div id="">
                  <p>E-KIRANA will use your personal information to provide personalized features to you on the Site and to provide for promotional offers to you through the Site and other channels.E-KIRANA will also provide this information to its business associates and partners to get in touch with you when necessary to provide the services requested by you.E-KIRANA  will use this information to preserve transaction history as governed by existing law or policy. E-KIRANA may also use contact information internally to direct its efforts for product improvement, to contact you as a survey respondent, to notify you if you win any contest; and to send you promotional materials from its contest sponsors or advertisers. E-KIRANA will also use this information to serve various promotional and advertising materials to you via display advertisements through the Google Ad network on third party websites. You can opt out of Google Analytics for Display Advertising and customize Google Display network ads using the Ads Preferences Manager. Information about Customers on an aggregate (excluding any information that may identify you specifically) covering Customer transaction data and Customer demographic and location data may be provided to partners of E-KIRANA for the purpose of creating additional features on the website, creating appropriate merchandising or creating new products and services and conducting marketing research and statistical analysis of customer behavior and transactions.</p>
              </div>
            </div>
            <div class="col-md-12">
              <div class="heading-part mb-20 mt-40 ">
                <h3 class="section_title" style=" text-align: left">With whom your information will be shared</h3>
              </div>
              <div id="">
                  <p>E-KIRANA will not use your financial information for any purpose other than to complete a transaction with you. E-KIRANA does not rent, sell or share your personal information and will not disclose any of your personally identifiable information to third parties. In cases where it has your permission to provide products or services you've requested and such information is necessary to provide these products or services the information may be shared with E-KIRANA’s business associates and partners. E-KIRANA may, however, share consumer information on an aggregate with its partners or thrird parties where it deems necessary. In addition E-KIRANA may use this information for promotional offers, to help investigate, prevent or take action regarding unlawful and illegal activities, suspected fraud, potential threat to the safety or security of any person, violations of the Site’s terms of use or to defend against legal claims; special circumstances such as compliance with subpoenas, court orders, requests/order from legal authorities or law enforcement agencies requiring such disclosure.</p>
              </div>
            </div>
            <div class="col-md-12">
              <div class="heading-part mb-20 mt-40 ">
                <h3 class="section_title" style=" text-align: left">What Choice are available to you regarding collection, use and distribution of your information</h3>
              </div>
              <div id="">
                  <p>To protect against the loss, misuse and alteration of the information under its control, E-KIRANA has in place appropriate physical, electronic and managerial procedures. For example, E-KIRANA servers are accessible only to authorized personnel and your information is shared with employees and authorized personnel on a need to know basis to complete the transaction and to provide the services requested by you. Although E-KIRANA will Endeavour to safeguard the confidentiality of your personally identifiable information, transmissions made by means of the Internet cannot be made absolutely secure. By using this site, you agree that E-KIRANA will have no liability for disclosure of your information due to errors in transmission or unauthorized acts of third parties.</p>
              </div>
            </div>
            <div class="col-md-12">
              <div class="heading-part mb-20 mt-40 ">
                <h3 class="section_title" style=" text-align: left">How can you correct inaccuracies in the information</h3>
              </div>
              <div id="">
                  <p>To correct or update any information you have provided, the Site allows you to do it online. In the event of loss of access details you can send an e-mail to: <a href="mailto:support@ekirana.com">support@ekirana.com</a></p>
              </div>
            </div>
            <div class="col-md-12">
              <div class="heading-part mb-20 mt-40 ">
                <h3 class="section_title" style=" text-align: left">Policy updates</h3>
              </div>
              <div id="">
                  <p>E-KIRANA reserves the right to change or update this policy at any time. Such changes shall be effective immediately upon posting to the Site.</p>
              </div>
            </div>
          </div>
          <!-- =====  end  ===== -->
          <div class="team-section section mt-40">
          <!--Team Carousel -->
          <div class="heading-part mb-10">
           
          </div>
       
        </div>
        </div>
      </div>
    </div>
    <hr>
  </div>
@endsection