<div class="table-responsive">
  <table class="table" cellspacing="0">
      <thead>
          <tr>
              <th class="product-remove">&nbsp;</th>
              <th class="product-thumbnail">&nbsp;</th>
              <th class="product-name">Product</th>
              <th class="product-price">Unit Price</th>
              <th class="product-Stock w-lg-15">Stock Status</th>
              <th class="product-subtotal min-width-200-md-lg">&nbsp;</th>
          </tr>
      </thead>
      <tbody>
 
        @php($user_id=Auth::user()->id)
        @php($for_cart_view=DB::table('wishlists')->where('user_id',$user_id)->count())
    
    @if($for_cart_view!=0)
    
    @foreach($item as $item)  
    @php($view_product_id=$item->product_id)
    @php($view_product_id=$item->product_id)
    @php($product=DB::table('products')->where('products.product_id',$view_product_id)->first())
    @php($product_image=DB::table('product_images')->where('product_id',$view_product_id)->limit(1)->get())
    @php($sub_cat=DB::table('sub_cats')->where('sub_cat_id',$product->sub_cat_id)->first())
        

                      

          <tr>
              <td class="text-center">
                  <a href="javascript:void(0)" onclick="remove_wishlist({{$view_product_id}},{{$item->product_price_id}});" class="text-gray-32 font-size-26">×</a>
              </td>
              <td class="d-none d-md-table-cell">
              @foreach($product_image as $product_image)
                  <a href="/product/{{$view_product_id}}"><img class="img-fluid max-width-100 p-1 border border-color-1" src="product_image/{{$product_image->image}}" alt="{{$product->product_name}}"></a>
             @endforeach
              </td>

              <td data-title="Product">
                  <a href="/product/{{$view_product_id}}" class="text-gray-90"><?php echo wordwrap($product->product_name,30,"<br>\n"); ?> @if($product->size!='NO')({{$product->size}}) @endif  @if($product->color!='NO')  ({{$product->color}})  @endif</a>
              </td>

              <td data-title="Unit Price">
                @php($progst=$product->selling_price*$sub_cat->gst/100)
                <span>₹{{$product->selling_price+$progst}}</span>
              </td>

              <td data-title="Stock Status">
              @if($product->available_stock>0)
                      <span>In-stock </span>
                      @else
                      <span>Out-stock </span>
                      @endif
                  <!-- Stock Status -->
                  <span>In stock</span>
                  <!-- End Stock Status -->
              </td>

              <td>
                  <button type="button" onclick="wishlisttoaddtocat({{$view_product_id}},{{$item->product_price_id}});" class="btn btn-soft-secondary mb-3 mb-md-0 font-weight-normal px-5 px-md-4 px-lg-5 w-100 w-md-auto">Add to Cart</button>
              </td>
          </tr>
        



    
    
                 @endforeach
                 </tbody>
  </table>
</div>


  @else
<tr><td colspan="6">
 <br>
       <center> <h4> No Item In Your Wishlist </h4> </center>
      <center> <a  href="/shop" class="btn btn-success" type="button" data-original-title="Add" style="border: none;background: #3f8dda;color:white"> Continue Shopping</a></center>
       </td>
       
       </tr>
  @endif


<script type="text/javascript">

$(document).ready(function() {

//  wishlist_table();
});

function wishlisttoaddtocat(id,price_id){
//   alert();
var token = $("#_token").val();


remove_wishlist(id,price_id);
cart(id,price_id);
wishlist_table();
wishlist_ajax();
}


</script>
