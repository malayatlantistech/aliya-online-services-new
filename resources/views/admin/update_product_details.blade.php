
@extends('admin.layouts.menu')
@section('body')

<style>
label
{
    color:black;
}
</style>
<link href="../assets/plugins/fileuploads/css/dropify.css" rel="stylesheet" type="text/css" />
<div class="container-fluid pt-8">
							<div class="page-header mt-0  p-3">
								<h3 class="mb-sm-0">Update Details</h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">Admin Dashboard</li>
								</ol>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="card shadow">
										<div class="card-header">
											<h2 class="mb-0">Update Product </h2>
										</div>
										<form method="POST" class="appointment-form" id="" action="update_product_details_code" role="form" name="frm" enctype="multipart/form-data">
										<div class="card-body">
                                        <div class="row">
                                        
                                        <div class="col-md-4">
                                            <h3>Product Code</h3>
													<div class="form-group">
														<input type="text" class="form-control" name="product_code" readonly required placeholder="Product Code " value="{{$pro->product_code}}">
													</div>
													
											</div>
                                           
											<div class="col-md-4">
                                            <h3>Product Name</h3>
													<div class="form-group">
														<input type="text" class="form-control" name="product_name" placeholder="Product name " value="{{$pro->product_name}}">
                                                        @if ($errors->has('product_name'))
                                                                    <strong style="color:red">{{ $errors->first('product_name') }}</strong>
                                                        @endif
                                                	</div>
													
											</div>
                                  
                                          <div class="col-md-4">
                                                <div class="form-group">
                                                <h3>HSN Code</h3>
                                                    <div class="form-group">
                                                    <input type="text" id="hsn" name="hsn_code" class="form-control" value="{{$pro->hsn_code}}" placeholder="HSN Code"/>
                                                    @if ($errors->has('hsn_code'))
                                                                    <strong style="color:red">{{ $errors->first('hsn_code') }}</strong>
                                                           @endif
                                                    </div>
                                                </div>


                                        </div>
                                       
                                        <div class="col-md-3">
                                           <h3> Product Brand</h3>
                                             <div class="form-group">
                                             <select class="form-control"  name="brand_id" required>

                                                <option value="0" disabled selected>--Select Product Brand Name-- </option>

                                                @foreach($brand as $brand)
                                                <option value="{{$brand->brand_id}}" @if($brand->brand_id == $pro->brand_id) selected @endif >{{$brand->brand_name}}</option>
                                                @endforeach

                                                </select>
                                                @if ($errors->has('brand_id'))
                                                                    <strong style="color:red">{{ $errors->first('brand_id') }}</strong>
                                                           @endif
                                        </div>
                                      </div>
                                        <div class="col-md-3">
                                           <h3> Product Group</h3>
                                             <div class="form-group">
                                             <select class="form-control"  name="group_id" required>

<option value="0" >--Select Product Group Name-- </option>

@foreach($group as $group)
<option value="{{$group->group_id}}" @if($group->group_id == $pro->group_id) selected @endif >{{$group->group_name}}</option>
@endforeach

</select>
@if ($errors->has('group_id'))
                                                                    <strong style="color:red">{{ $errors->first('group_id') }}</strong>
                                                           @endif
                                        </div></div>
                                        <div class="col-md-3">
                                          <h3>Product Size(Default : NO)</h3>
                        <div class="form-group">
                          <input type="text" class="form-control" name="size"  required placeholder="Product Size " value="{{$pro->size}}">
                        </div>
                        
                    </div>
                    <div class="col-md-3">
                      <h3>Product Color(Default : NO)</h3>
    <div class="form-group">
      <input type="text" class="form-control" name="color"  required placeholder="Product Color " value="{{$pro->color}}">
    </div>
    
</div>

                                    
                                        <div class="col-md-4">
                                        <h3>Category Name</h3>
													<div class="form-group">
														<select class="form-control" id="cat" name="cat_id" required>

                                                        <option disabled selected>--Select Category-- </option>
                                                        @foreach($cat1 as $cat)
                                                        <option value="{{$cat->cat_id}}" @if($cat->cat_id == $pro->cat_id) selected @endif >{{$cat->cat_name}}</option>
                                                        @endforeach
                                                        </select>
                                                        @if ($errors->has('cat_id'))
                                                                    <strong style="color:red">{{ $errors->first('cat_id') }}</strong>
                                                           @endif
													</div>
													
											</div>
											<div class="col-md-4">
                                            <h3>Sub-category Name</h3>
													<div class="form-group">

                                                    @php($sub_cat=DB::table('sub_cats')->get())
                                                    <select class="form-control" id="sub_cat" name="sub_cat_id" required>
                                                    @foreach($sub_cat as $sub_cat1)
                                                        <option value="{{$sub_cat1->sub_cat_id}}" @if($sub_cat1->sub_cat_id == $pro->sub_cat_id) selected @endif >{{$sub_cat1->sub_cat_name}} </option>
                                                    @endforeach    
                                                        </select>
                                                        @if ($errors->has('sub_cat_id'))
                                                                    <strong style="color:red">{{ $errors->first('sub_cat_id') }}</strong>
                                                           @endif
													</div>
													
											</div>
                                            <div class="col-md-4">
                                            <h3>Sub-sub-category Name</h3>
													<div class="form-group">

                                                    @php($sub_cat_a=DB::table('sub_sub_cats')->get())
                                                    <select class="form-control" id="sub_sub_cat" name="sub_sub_cat_id" required>

                                                    @foreach($sub_cat_a as $sub_cat_a1)
                                                    <option value="{{$sub_cat_a1->sub_sub_cat_id}}" @if($sub_cat_a1->sub_sub_cat_id == $pro->sub_sub_cat_id) selected @endif>{{$sub_cat_a1->sub_sub_cat_name}} </option>
                                                    @endforeach   

                                                        </select>
                                                        @if ($errors->has('sub_sub_cat_id'))
                                                                    <strong style="color:red">{{ $errors->first('sub_sub_cat_id') }}</strong>
                                                           @endif
													</div>
													
											</div>
                                        </div>
                             
                                       
                                        <div class="container">  
                
           </div>  
                             

                              
                                 




                                         <input type="hidden" name="product_id" id="product_id" value="{{$pro->product_id}}">
											<input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>"/>	
											
										
									
									</div>
									<center><input type="submit" class="btn btn-success mt-1 mb-1" value="Submit"></center>
								<br>
                                </div>

								</form>
											
								</div>
							</div>
                           
                            <script src="../assets/plugins/fileuploads/js/dropify.min.js"></script>

							<script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous">
</script>
	

	<script type="text/javascript">


   //sub category view
$('#cat').change(function(){
  
    var cat = $("#cat").val();
    var token = $("#_token").val();
   
  $.ajax({

    url:'sub_cat_ajax',

    type:'POST',

    data:{cat:cat,_token:token},
    dataType:"text",
  success:function(response)
    {
  
        $("#sub_cat").html(response);

    }

    });
    
 

});












</script>




<script type="text/javascript">


   //sub category view
$('#sub_cat').change(function(){

  
    var sub_cat = $("#sub_cat").val();
    var token = $("#_token").val();
  
 
  $.ajax({

    url:'sub_sub_cat_ajax',

    type:'POST',

    data:{sub_cat:sub_cat,_token:token},
    dataType:"text",
  success:function(response)
    {
    
   
        $("#sub_sub_cat").html(response);


  
    }

    });
    
 

});












</script>










<script type="text/javascript">

$(document).ready(function() {

$('#click').click(function(){

$.ajax({
   url:'add_product',



    type:"GET",



    data:{},



    dataType:"text",



    success:function(data)



    {

       

    $('#view').append(' <input type="text" value="" name="fetuars[]" class="form-control"  placeholder="Product Fetuars "> <br>'
										
    );


	
    }
});

a++;

});
});

</script>	





<script type="text/javascript">

$(document).ready(function() {

$('#click1').click(function(){

$.ajax({
   url:'add_product',



    type:"GET",



    data:{},



    dataType:"text",



    success:function(data)



    {

       

    $('#view1').append(' <input type="text" value="" class="form-control" name="specification1[]"  placeholder="Product specification Type"><br> <input type="text" value="" name="specification[]" class="form-control"  placeholder="Product Specification "> <br>'
										
    );


	
    }
});

a++;

});


$('#sub_cat').change(function(){

   
  
  var scat = $("#sub_cat").val();
 
  var token = $("#_token").val();

$.ajax({

  url:'findgst',

  type:'POST',

  data:{scat:scat,_token:token},
  dataType:"text",
success:function(response)
  {
    
 
 $("#gst").val(response+'%');
  }

  });
});


});




</script>	


<script>  
 $(document).ready(function(){  
      var i=1;  
      $('#add').click(function(){  
           i++;  
           $('#dynamic_field').append('<tr id="row'+i+'"><td><input type="text" name="color_code[]" placeholder="Enter Color Code" class="form-control name_list" /></td> <td><input type="text" name="color_name[]" placeholder="Enter Color Name" class="form-control name_list" /></td><td><input type="text" name="qty[]" placeholder="Enter Available Qty" class="form-control name_list" /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');  
      });  
      $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });  
      $('#submit').click(function(){            
           $.ajax({  
                url:"name.php",  
                method:"POST",  
                data:$('#add_name').serialize(),  
                success:function(data)  
                {  
                     alert(data);  
                     $('#add_name')[0].reset();  
                }  
           });  
      });  
 });  
 </script>





							@endsection