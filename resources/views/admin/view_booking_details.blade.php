@extends('admin.layouts.menu')
@section('body')

						<!-- Page content -->
                        @foreach($booking_details as $booking_details)
						@php($customer_id=$booking_details->customer_id)
						@php($customer=DB::table('users')->where('id',$customer_id)->get())
						<input type="hidden" id="aa" name="aa" value="{{$customer_id}}">

							<div class="container-fluid pt-8">
									<div class="page-header mt-0  p-3">
										<h3 class="mb-sm-0">Invoice</h3>
										<ol class="breadcrumb mb-0">
											<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
											<li class="breadcrumb-item active" aria-current="page">Invoice</li>
										</ol>
									</div>
									
									<div class="row invoice">
										<div class="col-md-12">
											<div class="card-box card shadow">
												<div class="card-body border-bottom">
													<div class="clearfix">
														<div class="float-left">
															<h3 class="logo mb-0">#Order ID-{{$booking_details->order_id}}</h3>
														</div>
														<div class="float-right">
															<h3 class="mb-0">Date: {{$booking_details->created_at}}</h3>
														</div>
													</div>
												</div>
												<div class="card-body">
													<div class="mb-0">
														<div class="row">
															<div class="col-md-12">
																<div class="float-left">
																@foreach($customer as $customer)
																@php($address=DB::table('addres')->where('id',$booking_details->address_id)->get())
																@foreach($address as $address)
																	<h4><strong>Customer Details: </strong></h4>
																	<address>
																		<strong>{{$customer->name}}</strong><br>
																		
																		{{$customer->email}}<br>
																		Phone: {{$address->phone_no}}
																	</address>
																</div>
																<div class="float-right text-right">
																	<h4><strong>Customer Address: </strong></h4>
																	<address>
																		<strong>{{$address->landmark}} </strong><br>
																		{{$address->location}}<br>
																		{{$address->city}},{{$address->district}},{{$address->state}}<br>
																		{{$address->flat}}
																		{{$address->pincode}}
																		{{$address->optional_phone}}
																		@php($pincode=$address->pincode)
																	</address>
																</div>
																@endforeach
																@endforeach
															</div><!-- end col -->
		
													
														<!-- end row -->
														</div>
														<div class="row mt-4">
															<div class="col-md-12">
																<div class="table-responsive">
																	<table class="table table-bordered m-t-30 text-nowrap">
																		<thead >
																			<tr>
																			<th>Product<br>Order ID</th>
																				<th>Product <br>Code</th>
																				<th>Product <br>Name</th>
																				<th>Product <br>Size</th>
																				<th>Price</th>
																				<th>Quantity</th>
																				
																				<th class="text-right">Amount(RS)</th>
																			
																				<th>Action</th>
																				@if(isset($_GET['cancel']) || isset($_GET['return']) || isset($_GET['complete_return']) || isset($_GET['complete_return_order']) )
																				<th>Refandable Amount</th>
																				@endif
																				<th>Status</th>
																			</tr>
																		</thead>
																		<tbody>
																		@php($sum=0)
																		@foreach($multi_details as $multi_details)
																	   
																		@php($product_id=$multi_details->product_id)
																		@php($for_product=DB::table('products')->where('product_id',$product_id)->get())
																		@foreach($for_product as $for_product)
																			<tr>
																			<td>{{$multi_details->order_id}}-{{$multi_details->multi_id}}</td>
																				<td>SSJ{{$for_product->product_code}}</td>
																				<td><a href="/view_details?product_id={{$for_product->product_id}}">{{$for_product->product_name}}</a></td>
																			   
																				<td>@if($for_product->weight>0) @if($for_product->weight>999){{$for_product->weight/1000}}kg @else {{$for_product->weight}}gm @endif @endif</td>
																			  <!--price-->
																			  @php($progst=$multi_details->gst)
																			  @php($price1=$multi_details->product_price+$progst)
																				<td>{{$price1}}</td>
																				
																				<td>{{$qty=$multi_details->quantity1}}</td>
																				
																				
																			   
																 
		
																				<td class="text-right">{{$new_sum=$price1*$qty}}</td>
																			
																				<td class="text-center">
																				@if($multi_details->order_status==8 || $multi_details->order_status==5 || $multi_details->order_status==6 || $multi_details->order_status==7)
																				<a href="invoice?id={{$multi_details->multi_id}}" data-toggle="modal" data-target="#modal-default-{{$multi_details->multi_id}}" class="btn btn-danger btn-sm" >View Reason</a>
																				<div class="modal fade" id="modal-default-{{$multi_details->multi_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true" style="display: none;">
																				<div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
																					<div class="modal-content">
																						<div class="modal-header">
																							<h2 class="modal-title" id="modal-title-default">View Reason & Refund Details</h2>
																							<a type="button" class="close" data-dismiss="modal" aria-label="Close">
																								<span aria-hidden="true">×</span>
																							</a>
																						</div>
																						<div class="modal-body">
																						@php($cancel_reason=DB::table('cancel_reasons')->where('book_muti_item_id',$multi_details->multi_id)->first())
																							<center><h3>Selected Option</h3>
																							{{$cancel_reason->cancel_option}}<br><br>
																							<h3>Optional Message</h3>
																							{{$cancel_reason->message}}
																							@if($cancel_reason->refund_type!=null)
																						<br><br>
																								<h3>Refund Method</h3>
																								{{$cancel_reason->refund_type}}
																							@endif
		
																							@if($cancel_reason->acc_no!='NO')
																						<br><br>
																								<h3>Refund Bank Details</h3>
																								Account No : {{$cancel_reason->acc_no}}<br>
																								Account Holder Name : {{$cancel_reason->acc_name}}<br>
																								Account IFSC No : {{$cancel_reason->acc_ifsc}}<br>
																							@endif
																							</center>
		
																						</div>
																						<div class="modal-footer">
																						
																							<a type="button" class="btn btn-link  ml-auto" data-dismiss="modal">Close</a>
																						</div>
																					</div>
																				</div>
																			</div>
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
																			
																				@else
																				<a target="_blank" href="invoice?id={{$multi_details->multi_id}}" class="btn btn-primary btn-sm" >Invoice</a>
																				<a target="_blank" href="delivery_slip?id={{$multi_details->multi_id}}" class="btn btn-info btn-sm" >Delivery Slip</a>
																				
																				@endif
																				
																				</td>
																			@if(isset($_GET['cancel']) || isset($_GET['return']) || isset($_GET['complete_return']) || isset($_GET['complete_return_order']))
																			  @php($book_multi_id=$multi_details->multi_id)
																			  @php($product_price=($multi_details->product_price+$multi_details->gst)*$multi_details->quantity1)
																			  @php($booking_price=$multi_details->price)
																			  @php($wallet_amount=$multi_details->wallet_amount)
																			  @php($coupon_amount=$multi_details->coupon_discount)
																			  @php($delivery_charge=$multi_details->delivery_charge)
																			  @php($persentage=($product_price/$booking_price)*100)
																				
																				@if($wallet_amount!=0)
																				@php($wallet_refund_amount=$wallet_amount*($persentage/100))
																				@else
																				@php($wallet_refund_amount=0)
																				@endif
		
																			  @if($coupon_amount!=0)
																				@php($coupon_refund_amount=$coupon_amount*($persentage/100))
																				@else
																				@php($coupon_refund_amount=0)
		
																			  @endif
		
																			  @if($delivery_charge!=0)
																			  
																				@php($delivery_charge_amount=$delivery_charge*($persentage/100))
																				@else
																				@php($delivery_charge_amount=0)
		
																			   @endif
		
		
																			   @php($refund_amount=round(($product_price+$delivery_charge_amount)-($coupon_refund_amount+$wallet_refund_amount)))
		
		
		
																				<td class="text-center" ><b>₹{{$refund_amount}}</b><br> 
																				@if($multi_details->order_status==7)
																				@php($aa=DB::table('cancel_reasons')->where('book_muti_item_id',$multi_details->multi_id)->first())
																				&nbsp; <span class="badge badge-primary">@if($aa->refund_type=='Wallet') Wallet @else  Original Payment Method @endif</span><br>
																				
																				@if($multi_details->refund_status=='NO')
																				@if($aa->refund_type=='Wallet')
																				<a href="javascript:void(0);" type="button" class="btn btn-primary btn-sm" onClick="refund({{$refund_amount}},{{$multi_details->multi_id}},{{$multi_details->customer_id}},'{{$multi_details->order_id}}');" >Refund To the wallet</a>
																				@else
																				<a href="javascript:void(0);" type="button" class="btn btn-primary btn-sm" onClick="refund1({{$multi_details->multi_id}});" style="margin-left:5px;">Refund to the bank</a>
																				@endif
																				@else
																				<span class="badge badge-success" style="margin:3px">Refund Completed </span><br>
		
																				@endif
																			
																			
																			
																				@endif
																				
																				</td>
		
		
																			   @endif
		
																			  
																			  
																				<td class="text-center">
																					<input type="hidden" id="return_id{{$multi_details->multi_id}}" value="{{$multi_details->return_delivery_boy_id}}">
																				@if($multi_details->order_status==1)
																				<span class="badge badge-primary">Order Under Process</span><br>
																				{{-- <a onclick="tracking('{{$multi_details->multi_id}}',{{$multi_details->order_status}});" href="javascript:void(0)" class="btn btn-primary btn-sm" >Order In Transit</a> --}}
																		   
																				
																				@elseif($multi_details->order_status==2)
																				<span class="badge badge-primary">Order In Transit</span><br>
																				{{-- <a onclick="tracking('{{$multi_details->multi_id}}',{{$multi_details->order_status}});" href="javascript:void(0)" class="btn btn-primary btn-sm" >Out for Delivery</a> --}}
																			
																				@elseif($multi_details->order_status==3)
																				<span class="badge badge-primary">Out for Delivery</span><br>
																				{{-- <a onclick="tracking('{{$multi_details->multi_id}}',{{$multi_details->order_status}});" href="javascript:void(0)" class="btn btn-primary btn-sm" >Delivered</a> --}}
																				
																				@elseif($multi_details->order_status==4)
																				<span class="badge badge-info">Delivered</span>
																				
																				@elseif($multi_details->order_status==5)
																				<span class="badge badge-primary">Return Under Process</span><br>
																				<a onclick="tracking('{{$multi_details->multi_id}}',{{$multi_details->order_status}});" href="javascript:void(0)" class="btn btn-primary btn-sm" >Out for Pickup</a>
																			
																			
																				
																				@elseif($multi_details->order_status==6)
																				<span class="badge badge-primary">Out for Pickup</span><br>
																				{{-- <a onclick="tracking('{{$multi_details->multi_id}}',{{$multi_details->order_status}});" href="javascript:void(0)" class="btn btn-primary btn-sm" >Returned</a> --}}
																					
																				@elseif($multi_details->order_status==7)
																				<span class="badge badge-warning">Returned</span>
																				
																				@elseif($multi_details->order_status==8)
																				<span class="badge badge-danger">Canceled</span>
																				@endif
																				<br>
																				
																				
																				</td>
		
																			</tr>
																			@php($sum=$sum+$new_sum)
		
		
																			@endforeach
																			
																			@endforeach
																		</tbody>
																	</table>
																</div>
															</div>
														
														</div>



														<div class="row">
															<div class="col-xl-4 col-12 offset-xl-8">
																<p class="text-right mt-3 font-weight-600">Sub-total:  ₹{{$sum}}</p>
																<p class="text-right mt-3 font-weight-600">Coupon Discount:  ₹{{$booking_details->coupon_discount}}</p>
																<p class="text-right mt-3 font-weight-600">Wallet Amount:  ₹{{$booking_details->wallet_amount}}</p>
																<p class="text-right mt-3 font-weight-600">Delivery charge:  ₹{{$booking_details->delivery_charge}}</p>
																<hr>
																<h4 class="text-right text-xl">Grand Total :  ₹{{($sum-($booking_details->coupon_discount+$booking_details->wallet_amount)-$booking_details->delivery_charge)}}</h4>
																<h5 class="text-right text-xl" style="margin-bottom: 0;">@if($booking_details->payment_type=='online')<span class="badge badge-default">Online Payment</span> @elseif($booking_details->payment_type=='cash') <span class="badge badge-info">Cash</span> @else <span class="badge badge-info">Wallet Payment</span> @endif</h5>
																<h5 class="text-right">  @if($booking_details->payment_type=='online')<span class="badge badge-danger" style="font-size: 12px;">Razorpay Pement ID - {{$booking_details->razorpay_payment_id}}</span>@endif</h5>
															</div>
														</div>

														<hr>
														<div class="d-print-none">
															<div class="float-right">
																<a href="javascript:window.print()" class="btn btn-primary"><i class="fa fa-print"></i> Print</a>&nbsp;&nbsp;
																<input type="hidden" id="booking_id" value="{{$booking_details->booking_id}}">
																<input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>"/>	
															</div>
															<div class="clearfix"></div>
														</div>
													</div>
													</div>
												</div>
											</div>
										</div>
									
									</div>
							</div>
							<!-- end row -->
							


                            @endforeach
							<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
                            <script
      
							src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous">
</script>
<script>
		function refund1(aa)
		{
			var token = $("#_token").val();
			$("#overlay").fadeIn(300);
			$.ajax({
               type:'post',
			   url:'refund1',
			   data:{_token:token,booking_multi_id:aa},
			   success:function()
			   {

			             	$("#overlay").fadeOut(300);
							swal("Amount Refund successfully!", "You clicked the button!", "success").then(function(){
							location.reload();
							});
						

			   }
			});

			
		}
		function refund(aa,bb,cc,dd)
		{
		    	var token = $("#_token").val();
				var booking_id= $("#booking_id").val();
				$("#overlay").fadeIn(300);

				$.ajax({
				url:'refund',
				type:'post',
				data:{_token:token,booking_id,booking_id,refound_amount:aa,multi_id:bb,user_id:cc,order_id:dd},
						success:function(data)
						{
							$("#overlay").fadeOut(300);
							swal("Amount Refund successfully!", "You clicked the button!", "success").then(function(){
							location.reload();
							});
						

						}
				});
		}



			
		function tracking(book_multi_id,status){
		var token = $("#_token").val();
		var idd= $("#aa").val();
		var booking_id= $("#booking_id").val();


		
	//	alert(status);
	
		if(status==2 || status==5){
	 
	 // alert(name);
	 // alert(phon222);

	
	 var name = window.prompt("Enter Courier boy name");
	 if(name.length<1){
		alert('Please Enter correct phone number');
		 phone = window.prompt("Enter Courier boy phone number");
	}


	  
if(name.length>1){

	var phone = window.prompt("Enter Courier boy phone number");
	if(phone.length<10){
		alert('Please Enter correct phone number');
		 phone = window.prompt("Enter Courier boy phone number");
	}


		if(phone.length>9){
			$("#overlay").fadeIn(300);　
	$.ajax({
				
				url:'order_status',
			
				type:'POST',
			
				data:{_token:token,book_multi_id:book_multi_id,idd:idd,booking_id:booking_id,name:name,phone:phone},
				
				success:function(response)
				{
					// alert(response);
					$("#overlay").fadeOut(300);　
					location.reload(true); 
				}
			
				});
		}else{
			alert('Please Enter correct phone number');
		}
	}else{
		alert('Please Enter Courier boy name');
	}

  }else{
	$("#overlay").fadeIn(300);　
		$.ajax({
		
			url:'order_status',
		
			type:'POST',
		
			data:{_token:token,book_multi_id:book_multi_id,idd:idd,booking_id:booking_id},
			
			success:function(response)
			{
				// alert(response);
				$("#overlay").fadeOut(300);
				if(status==3){
					window.location.href="view_booking_details?booking_id="+{{$_GET['booking_id']}}+"&complete_order=wckL9yINe1W33ptSc7V1blZVxEWPKKyw07nJ0IBQ"
				}else if(status==6){
					window.location.href="view_booking_details?booking_id="+{{$_GET['booking_id']}}+"&complete_return_order=wckL9yINe1W33ptSc7V1blZVxEWPKKyw07nJ0IBQ"
				}
		
				else{
					　
				location.reload(true); 
				}
			}
		
			});
   }
 
}	

</script>


@endsection