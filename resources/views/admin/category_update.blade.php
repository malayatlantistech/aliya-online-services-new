@extends('admin.layouts.menu')
@section('body')
<div class="container-fluid pt-8">
	<div class="page-header mt-0  p-3">
		<h3 class="mb-sm-0">Update Category</h3>
		<ol class="breadcrumb mb-0">
			<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
			<li class="breadcrumb-item active" aria-current="page">Admin Dashboard</li>
		</ol>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="card shadow">
				<div class="card-header">
					<h2 class="mb-0">Update category </h2>
				</div>
				<form method="POST" class="appointment-form" id="" action="cat_update_action" role="form" name="frm" enctype="multipart/form-data">
				<div class="card-body"> 
					<div class="row" >
					@foreach($cat as $cat)
					<div class="col-md-6"><label>Category Name</label>
							<div class="form-group">
						
						   <input type="hidden" class="form-control" name="cat_id"  value="{{$cat->cat_id}}">
								<input type="text" class="form-control" name="cat_name" placeholder="Add Category" value="{{$cat->cat_name}}">
						
							</div>
							
					</div>

					<div class="col-md-6"><label>Category Icon</label>
						<div class="form-group">
							<input type="file" class="form-control" name="cat_icon" placeholder="Add Category icon" value="">
						</div>
					 </div>
					
				</div>
			
			
		</div>
					@endforeach
					<div class="col-md-12"><label></label>
							<div class="form-group">
						   
						<center>	<input style="margin-top:7px" type="submit" value="Update" class="btn btn-primary" ></center>
					
							</div>
							
					</div>
					<input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>"/>	
					
				</div>
				
			</div>
			<center></center><br><br>
		</div>

		</form>
					
		</div>
	</div>
							@endsection