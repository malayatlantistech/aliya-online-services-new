@extends('admin.layouts.menu')
@section('body')

<div class="container-fluid pt-8">
							<div class="page-header mt-0  p-3">
								<h3 class="mb-sm-0">Delivered Pincodes List</h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">Admin Dashboard</li>
								</ol>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="card shadow">
										<div class="card-header">
                                        @if ($message = Session::get('success'))
										<h2 class="mb-0" style="color:red"><b>{{ $message }}</b></h2>	
															@else
															<h2 class="mb-0">Delivery Boy Name : {{$delivery_boy->name}}</h2>
													 @endif
											
										</div>
										<div class="card-body">
											<div class="table-responsive">
												<table id="example" class="table table-striped table-bordered w-100 text-nowrap">
													<thead>
														<tr>
															<th class="wd-15p">Pincode </th>
                                                            <th class="wd-15p">Delivered <br>From</th>
															<th class="wd-15p">Active/<br>Deactivate</th>
                                                          
														</tr>
													</thead>
													<tbody>
													@foreach($pin as $pin)
													<tr>
													<td>
													{{$pin->pincode}}
													<span style="float:right">
												
														
													</span>
                                                    </td>
                                                    <td>{{$pin->created_at}}</td>
                                                    
                                                
												<td>
												<span>	@if($pin->active_status=='NO')
												<p style="    margin: 0px 0px -5px 0px;">	<a href="pin_approve?value=YES&id={{$pin->id}}" ><input type="submit" name="submit" value="Active" class="btn btn-primary mt-1 mb-1 btn-sm"></a></p>
													<span class="badge badge-danger"><b>Pincode is Deactivate</b></span>
													@else
													<p style="    margin: 0px 0px -5px 0px;">	<a href="pin_approve?value=NO&id={{$pin->id}}"><input type="submit" name="submit" value="Deactivate" class="btn btn-danger mt-1 mb-1 btn-sm"></a></p>
													<span class="badge badge-primary"><b>Pincode is Active</b></span>
												@endif
												</span>
													</td>
										
													@endforeach
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
                            <input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>"/>	
                            <script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous">
</script>
<script>
function user_block(id,value){
	var token = $("#_token").val();
$.ajax({

url:'user_block',


type:'POST',

data:{_token:token,id:id,value:value},

success:function(response)
{

  }   
});


}

</script>
<script>								    
$(document).ready(function() {
   $('#example').DataTable( {
        "order": [[ 0, "desc" ]]
    } );
    
});
</script> 
@endsection