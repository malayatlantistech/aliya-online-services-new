@extends('admin.layouts.menu')
@section('body')
<div class="container-fluid pt-8">
	<div class="page-header mt-0  p-3">
		<h3 class="mb-sm-0">Basic Forms</h3>
		<ol class="breadcrumb mb-0">
			<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
			<li class="breadcrumb-item active" aria-current="page">Basic Forms</li>
		</ol>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="card shadow">
				<div class="card-header">
					<h2 class="mb-0">Update Sub Subcategory </h2>
				</div>
				<form method="POST" class="appointment-form" id="" action="sub_sub_cat_update_action" role="form" name="frm" enctype="multipart/form-data">
				<div class="card-body">
					@foreach($sub_sub_cat as $sub_sub_cat)

					<div class="row" >
					<div class="col-md-6"><label>Sub Subcategory Name</label>
							<div class="form-group">
								<input type="text" class="form-control" name="sub_sub_cat_name" placeholder="Update Sub sub Category" value="{{$sub_sub_cat->sub_sub_cat_name}}">
							</div>
							
					</div>

					<div class="col-md-6"><label>Sub Subcategory Icon</label>
						<div class="form-group">
							<input type="file" class="form-control" name="sub_sub_cat_icon" placeholder="Update Sub sub Category icon" value="">
							<input type="hidden" class="form-control" name="sub_sub_cat_id"  value="{{$sub_sub_cat->sub_sub_cat_id}}">
						</div>
						
				</div>
				
					<input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>"/>	
					@endforeach
					
				</div>
				
			</div>
			<center><input type="submit" value="Update" class="btn btn-primary"></center>
		</div>

		</form>
					
		</div>
	</div>


							@endsection