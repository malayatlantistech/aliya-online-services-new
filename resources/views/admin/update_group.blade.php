@extends('admin.layouts.menu')
@section('body')
<div class="container-fluid pt-8">
							<div class="page-header mt-0  p-3">
								<h3 class="mb-sm-0">Update Group</h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">Admin Dashboard</li>
								</ol>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="card shadow">
										<div class="card-header">
                                        <h2 class="mb-0">Update Group</h2>
										</div>
										<form method="POST" class="appointment-form" id="" action="update_group_action" enctype="multipart/form-data" role="form" name="frm">
										<div class="card-body">
                                            <div class="row">


											<div class="col-md-6"><label>Group Name</label>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="group_name"  placeholder="Enter Product Group Name" value="{{$group->group_name}}">
                                                </div>
                                                
                                        </div>
									
                                        <input type="hidden" name="id" id="id" value="{{$group->group_id}}"/>
                                           
                                            <input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>"/>
                                            <input type="submit" name="submit" class="btn btn-primary" id="submit" value="Save Changes" style="    height: 51px;margin-top: 31px;"/>
                                    </div>
										
									</div>
									<center><span id="submit"></span></center>
								</div>

								</form>
											
								</div>
							</div>

							
</div>
							@endsection
