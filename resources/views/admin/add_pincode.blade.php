@extends('admin.layouts.menu')
@section('body')
<div class="container-fluid pt-8">
							<div class="page-header mt-0  p-3">
								<h3 class="mb-sm-0">Add Pincode</h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">Admin Dashboard</li>
								</ol>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="card shadow">
										<div class="card-header">
											<h2 class="mb-0">Add Pincode </h2>
										</div>
										<form method="POST" class="appointment-form" id="" action="add_pincode_code" role="form" name="frm">
										<div class="card-body">
											<div class="row" id="view">
											<div class="col-md-3"><label>Pin Code</label>
													<div class="form-group {{ $errors->has('pincode') ? 'has-error' : '' }}" >
														<input type="text" class="form-control" name="pincode" placeholder="Add Pin Code" value="">
														<span class="text-danger">{{ $errors->first('pincode') }}</span>
													</div>
										
													</div><div class="col-md-3"><label>Minimum Order</label>
													<div class="form-group {{ $errors->has('min_price') ? 'has-error' : '' }}" >
														<input type="text" class="form-control" name="min_price" placeholder="Add Minimum Order" value="">
														<span class="text-danger">{{ $errors->first('min_price') }}</span>
													</div>

													</div><div class="col-md-3"><label>Delivery Charge</label>
													<div class="form-group {{ $errors->has('delivery_charge') ? 'has-error' : '' }}" >
														<input type="text" class="form-control" name="delivery_charge" placeholder="Add Delivary Charge" value="">
														<span class="text-danger">{{ $errors->first('delivery_charge') }}</span>
													</div>
													</div>
													<div class="col-md-3"><label>COD Available</label>
													<div class="form-group {{ $errors->has('cod') ? 'has-error' : '' }}" >
														<select name="cod" id="cod" class="form-control">
														    <option value="" disable selected >-- Select Option --</option>
															<option value="YES">YES</option>
															<option value="NO">NO</option>
														</select>
														<span class="text-danger">{{ $errors->first('cod') }}</span>
													</div>	
													
											</div>
										
											</div>
											<input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>"/>
                                            
									
									</div>
									<center><input type='submit' name='submit' value='Final Submission' class='btn btn-primary mt-1 mb-1'></center>
								</div>

								</form>
											
								</div>
							</div>

						
</div>
							@endsection
