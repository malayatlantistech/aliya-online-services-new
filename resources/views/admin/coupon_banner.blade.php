<!-- Page content -->
@extends('admin.layouts.menu')
@section('body')
<div class="container-fluid pt-8">
							<div class="page-header mt-0 shadow p-3">
								<h3 class="mb-sm-0">Update Banner</h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">Update Banner</li>
								</ol>
							</div>
							 <form role="form" name="frm" method="post" enctype="multipart/form-data" action="add_banner">
							<div class="row">
													
								<div class="col-md-12">
									<div class="card card-profile  overflow-hidden">
										<div class="card-body text-center cover-image" data-image-src="assets/img/profile-bg.jpg">
											
												
																
												
													
											<input type="file" class="dropify" data-height="" name="m_pic" required />
											<div class="col-md-12">
											<label>Banner Title</label>
													<div class="form-group">
														<input type="text" class="form-control" name="title" placeholder="Sale Name" value="">
													</div>
													
											</div> 
											<div class="col-md-12">
											<label>Description</label>
													<div class="form-group">
														<input type="text" class="form-control" name="description" placeholder="Description" value="">
													</div>
													
											</div> 
											<div class="col-md-12">
											<label>Discount</label>
													<div class="form-group">
														<input type="text" class="form-control" name="discount" placeholder="Discount" value="">
													</div>
													
											</div>    

											<div class="col-md-12">
											<label>Coupon validity(In Day)</label>
													<div class="form-group">
														<input type="text" class="form-control" name="validity" placeholder="Coupon validity" value="">
													</div>
													
											</div>    
											
											<input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>"/>
											
											
											<p class="mb-4 text-white">Add Banner</p>
											
											<input type="submit" class="btn btn-success btn-sm" value="submit" name="submit">
										</div>
										
										<div class="card-body">
											<div class="nav-wrapper p-0">
												<ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
													
													
													<li class="nav-item">
														<a class="nav-link mb-sm-3 mb-md-0 active show mt-md-2 mt-0 mt-lg-0" id="tabs-icons-text-3-tab" data-toggle="tab" href="#tabs-icons-text-3" role="tab" aria-controls="tabs-icons-text-3" aria-selected="true"><i class="far fa-images mr-2"></i>Banner</a>
													</li>
													
												</ul>
											</div>
										</div>
									</div>
									</form>
									<div class="card shadow ">
										<div class="card-body pb-0">
											<div class="tab-content" id="myTabContent">
												
													
																				
																				
																				
												<div class="tab-pane fade show active" id="tabs-icons-text-3" role="tabpanel" aria-labelledby="tabs-icons-text-3-tab">
													<div class="row">
													@foreach($banner as $banner)
														<div class="col-lg-4 profile-image">
															<div class="card shadow"><a href="delete_banner?id={{$banner->banner_id}}"><i class="side-menu__icon ion-close-round"></i></a>
                                                          
																<img src="../banner/{{$banner->banner_image}}" alt="gallery">
															</div>
														</div>
														@endforeach
														
													</div>
												</div>
												
												
											</div>
										</div>
									</div>
								</div>
							</div>
                            @endsection