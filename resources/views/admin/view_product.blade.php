@extends('admin.layouts.menu')
@section('body')

<div class="container-fluid pt-8">
							<div class="page-header mt-0  p-3">
								<h3 class="mb-sm-0">Product List</h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">Product List</li>
								</ol>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="card shadow">
										
										<div class="card-body">
											<div class="table-responsive">
												<table id="example" class="table table-striped table-bordered w-100 text-nowrap">
													<thead>
														<tr>
															<th class="wd-15p">Product <br>Code</th>
                                                            <th class="wd-15p">Product <br>Details</th>
															
															<th class="wd-15p">Brand<br>name</th>
															<th class="wd-15p">category<br>name</th>
															<th class="wd-15p">sub <br>category<br>name</th>
															<th class="wd-15p">sub sub <br>category<br>name</th>
															<th class="wd-15p">Vendor <br>name</th>
														
														
															<th class="wd-20p">Action</th>
															
														</tr>
													</thead>
													<tbody>
													@foreach($product as $product1)
													
														<tr>
                                                        <td>{{$product1->product_code}}</td>
                                                        <td><font size="3px"><b><?php echo wordwrap($product1->product_name,23,"<br>\n"); ?></b></font>
														({{$product1->hsn_code}})<br>
														Color : {{$product1->color}},&nbsp; Size : {{$product1->size}}<br>
												
													
														

														@if($product1->active_status=='YES')

Admin : <span class="badge badge-primary" ><b>Active </b></span>

@else 
Admin : <span class="badge badge-danger"><b> Inactive </b></span>

@endif

@if($product1->vendor_id!==Auth::user()->id)
@if($product1->vendor_activate_status=='YES')

Vendor : <span class="badge badge-primary" ><b>Active </b></span>

@else 
Vendor : <span class="badge badge-danger"><b> Inactive </b></span>

@endif
@endif									
														</td>
														@if($product1->brand_id!=0)
														@php($brand=DB::table('brands')->where('brand_id',$product1->brand_id)->first())
														
															<td><?php echo wordwrap($brand->brand_name,15,"<br>\n"); ?></td>
														@else
															<td>----</td>
														@endif


														@if($product1->cat_id!=0)
														@php($cat=DB::table('cats')->where('cat_id',$product1->cat_id)->first())
														
															<td><?php echo wordwrap($cat->cat_name,15,"<br>\n"); ?></td>
														@else
															<td>----</td>
														@endif

														@if($product1->sub_cat_id!=0)
														@php($subcat=DB::table('sub_cats')->where('sub_cat_id',$product1->sub_cat_id)->first())
														
															<td><?php echo wordwrap($subcat->sub_cat_name,15,"<br>\n"); ?></td>
														@else
															<td>----</td>
														@endif
														@if($product1->sub_sub_cat_id!=0)
														@php($subsubcat=DB::table('sub_sub_cats')->where('sub_sub_cat_id',$product1->sub_sub_cat_id)->first())
													
															<td><?php echo wordwrap($subsubcat->sub_sub_cat_name,15,"<br>\n"); ?></td>
														@else
															<td>----</td>
														@endif
														
												
                                                       <td>
														@if($product1->vendor_id==Auth::user()->id)
														Your Product

														@else
															<a href="viewDetails?id={{$product1->id}}"><font size="3px"><b>{{$product1->name}}</b></font></a><br>
															Email : &nbsp;{{$product1->email}}<br>
															Contact No : &nbsp;{{$product1->mobile}}<br>
															Company Name :&nbsp;{{$product1->companey_name}}<br>

														@endif

													   </td>

															<td>
															
												
															
															<a href="view_details?product_id={{$product1->product_id}}"  class="btn btn-primary mt-1 mb-1 btn-sm">View & Update</a></h3>
														<br>
														
@if($product1->active_status!='YES')
<a href="approve1?product_id={{$product1->product_id}}&y=yes" class="btn btn-icon btn-pill btn-info mt-1 mb-1 btn-sm">Active</a>
@else
<a href="approve1?product_id={{$product1->product_id}}&y=no" class="btn btn-icon btn-pill btn-danger mt-1 mb-1 btn-sm">Inactive</a>
@endif
												
															</td>
															
														</tr>
														@endforeach
														
													</tbody>
												</table>
											
											</div>
												
										</div>
									
									</div>
								</div>
							</div>
							<input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>"/>
         
							<script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous">
</script>
							<script type="text/javascript">
function cod(status,id){
	var token = $("#_token").val();
	$.ajax({

url:'cod',

type:'POST',

data:{product_id:id,_token:token,status:status},
dataType:"text",
success:function(response)
{

window.reload(true);
//	alert('success');



}

});

}
$(document).ready(function() {
//sub category view
$('#today').click(function(){
    var product_id = $("#product_id").val();
    var token = $("#_token").val();
 alert(product_id);
$.ajax({

    url:'add_deals',

    type:'POST',

    data:{product_id:product_id,_token:token},
    dataType:"text",
  success:function(response)
    {
    
   
        alert('success');


  
    }

    });
});


});

</script>
<script>								    
$(document).ready(function() {
   $('#example').DataTable( {
        "order": [[ 0, "desc" ]]
    } );
    
});
</script> 
@endsection