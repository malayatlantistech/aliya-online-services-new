@extends('admin.layouts.menu')
@section('body')
<div class="container-fluid pt-8">
	<div class="page-header mt-0  p-3">
		<h3 class="mb-sm-0" style="color:red"> <a href="/add_sub_category?cat_id={{$_GET['cat_id']}}"><input type="button" class="btn btn-success" value="Add Sub Category"></a>

</h3>
		<ol class="breadcrumb mb-0">
		
			<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
			<li class="breadcrumb-item active" aria-current="page">Admin Dashboard </li>
		</ol>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="card shadow">
				<div class="card-header">
				@if ($message = Session::get('success'))
				<h2 class="mb-0" style="color:red"><b>{{ $message }}</b></h2>	
									@else
									<h2 class="mb-0">Category : <b>{{$cat_name->cat_name}}</b></h2>
							 @endif
				
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table id="example" class="table table-striped table-bordered w-100 text-nowrap">
							<thead>
								<tr>
								<th class="wd-15p">Sl No.</th>
									<th class="wd-15p">Sub Category Name </th>
									<th class="wd-15p">GST(%)</th>
									<th class="wd-15p">Commission(%)</th>
									<td>Sub Category<br> Icon</td>
									<td>View Sub Sub Category</td>

								
									<th class="wd-20p">Action</th>
									
								</tr>
							</thead>
							<tbody>
							
							@foreach($cats as $key=>$cats)
								<tr>
								
							  
						
								<td>{{$key+1}}</td>
								<td>{{$cats->sub_cat_name}}</td>
							
								<td>{{$cats->gst}}%</td>
								<td>{{$cats->commission}}%</td>
								<td><img src="/sub_cat_icon/{{$cats->sub_cat_icon}}" width="50px"/></td>
							
						
									 
								<td><a href="/view_sub_sub_category?sub_cat_id={{$cats->sub_cat_id}}" class="btn btn-icon btn-pill btn-primary mt-1 mb-1 btn-sm">View Sub Sub Category</a></td>
									
									  <td><a href="/update_sub_category?sub_cat_id={{$cats->sub_cat_id}}" class="btn btn-icon btn-pill btn-info mt-1 mb-1 btn-sm">update</a>
										
									@php($sub=DB::table('sub_sub_cats')->where('sub_cat_id',$cats->sub_cat_id)->count())
									@if($sub==0)
									<a href="/delete_sub_category?sub_cat_id={{$cats->sub_cat_id}}" class="btn btn-warning btn-pill btn-info mt-1 mb-1 btn-sm">Delete</a>
									@endif
										 
										  </td>
									  

									
								</tr>

								@endforeach
							
								
							</tbody>
						</table>
							</div>

			
				</div>
					
			</div>
		</div>
	</div>

@endsection