@extends('admin.layouts.menu')


@section('body')

<div class="container-fluid pt-8">
							<div class="page-header mt-0 shadow p-3">
								<h3 class="mb-sm-0">User profile</h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">User profile</li>
								</ol>
							</div>
                            @foreach($vendor as $vendor)
							<div class="row">
								<div class="col-md-12">
									<div class="card card-profile  overflow-hidden">
										<div class="card-body text-center cover-image" data-image-src="../admin_assets/img/profile-bg.jpg">
											<div class=" card-profile">
												<div class="row justify-content-center">
													<div class="">
														<div class="">
															<a href="#">
																<img src="../user_details/{{$vendor->profile_image}}" class="rounded-circle" alt="profile" style="width:200px;height:200px">
															</a>
														</div>
													</div>
												</div>
											</div>
											<h3 class="mt-3 text-white">{{$vendor->name}}</h3>
											<p class="mb-4 text-white">Vendor</p>
											</div>
									<!--	<div class="card-body">
											<div class="nav-wrapper p-0">
												<ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
													<li class="nav-item">
														<a class="nav-link mb-sm-3 mb-md-0 mt-md-2 mt-0 mt-lg-0 active show" id="tabs-icons-text-1-tab" data-toggle="tab" href="#tabs-icons-text-1" role="tab" aria-controls="tabs-icons-text-1" aria-selected="false"><i class="fas fa-home mr-2"></i>About</a>
													</li>
													<li class="nav-item">
														<a class="nav-link mb-sm-3 mb-md-0 mt-md-2 mt-0 mt-lg-0" id="tabs-icons-text-2-tab" data-toggle="tab" href="#tabs-icons-text-2" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="fas fa-user mr-2"></i>Product</a>
													</li>
													<li class="nav-item">
														<a class="nav-link mb-sm-3 mb-md-0 mt-md-2 mt-0 mt-lg-0" id="tabs-icons-text-3-tab" data-toggle="tab" href="#tabs-icons-text-3" role="tab" aria-controls="tabs-icons-text-3" aria-selected="true"><i class="far fa-images mr-2"></i>Total Delivery</a>
													</li>
													<li class="nav-item">
														<a class="nav-link mb-sm-3 mb-md-0 mt-md-2 mt-0 mt-lg-0" id="tabs-icons-text-4-tab" data-toggle="tab" href="#tabs-icons-text-4" role="tab" aria-controls="tabs-icons-text-4" aria-selected="false"><i class="fas fa-newspaper mr-2"></i>Send Message</a>
													</li>
													
												</ul>
											</div>
										</div>
									</div>-->
									<div class="card shadow ">
										<div class="card-body pb-0">
											<div class="tab-content" id="myTabContent">
												<div class="tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
													<h2>About Me</h2>
													<div class="table-responsive border ">
														<table class="table row table-borderless w-100 m-0 ">
															<tbody class="col-lg-6 p-0">
																<tr>
																	<td><strong>Full Name :</strong> {{$vendor->name}}</td>
																</tr>
																<tr>
																	<td><strong>Email ID :</strong>{{$vendor->email}}</td>
																</tr>
																<tr>
																	<td><strong>Contact No :</strong> {{$vendor->mobile}}</td>
																</tr>
																
															</tbody>
															<tbody class="col-lg-6 p-0">
															<tr>
																	<td><strong>Address :</strong> {{$vendor->address}}</td>
																</tr>
																<tr>
																	<td><strong>Pin Code:</strong> {{$vendor->pin_no}}</td>
																</tr>
																<tr>
																	<td><strong>Approve Status :</strong> 
																	@if($vendor->status=='YES')
																		<b style="color:green;font-size:15px">{{' YES'}}</b>
																	@else
																		<b style="color:red;font-size:15px">{{' NO'}}</b>
																	@endif	
																	</td>
																</tr>
																
															</tbody>
														</table>
													</div>
													<div class="media-heading mt-3">
														<h3><strong>Store Address</strong></h3>
													</div>
													<br>
													@if($address_count==0)

													<div class="alert alert-warning" role="alert">
												<span class="alert-inner--icon"><i class="fe fe-info"></i></span>
												<span class="alert-inner--text"><strong>Warning!</strong> This vendor not added his/her store address. Contact to vendor and tell him/her add bank details.</span>
											</div>



													@else
													<table style="width:100%">
													<tr>
													<td style="width:50%">
															<div class="col-md-6 p-0">
																
																	<strong>Pin/ZIP Code :</strong> {{ $address->pincode }}<br>
															</div>
														</td>
														<td style="width:50%">
															<div class="col-md-6 p-0">
																
																	<strong>Store Name/No :</strong> {{$address->flat}}<br>
															</div>
														</td>


													</tr>
													<tr>
													<td style="width:50%">
															<div class="col-md-6 p-0">
																
																	<strong>Location :</strong> {{ $address->location }}<br>
															</div>
														</td>
														<td style="width:50%">
															<div class="col-md-6 p-0">
																
																	<strong>Landmark :</strong> {{$address->landmark}}<br>
															</div>
														</td>


													</tr>
													<tr>
													<td style="width:50%">
															<div class="col-md-6 p-0">
																
																	<strong>City/Taluk :</strong> {{ $address->city }}<br>
															</div>
														</td>
														<td style="width:50%">
															<div class="col-md-6 p-0">
																
																	<strong>Address :</strong> {{$address->address}}<br>
															</div>
														</td>


													</tr>
													<tr>
													<td style="width:50%">
															<div class="col-md-6 p-0">
																
																	<strong>District :</strong> {{ $address->district }}<br>
															</div>
														</td>
														<td style="width:50%">
															<div class="col-md-6 p-0">
																
																	<strong>Store Phone No :</strong> {{$address->phone_no}}<br>
															</div>
														</td>


													</tr>
													<tr>
													<td style="width:50%">
															<div class="col-md-6 p-0">
																
																	<strong>State :</strong> {{ $address->state }}<br>
															</div>
														</td>
														<td style="width:50%">
															<div class="col-md-6 p-0">
																
																	<strong>Optional Phone :</strong> {{$address->optional_phone}}<br>
															</div>
														</td>


													</tr>
													</table>
													@endif
													<br>
													<div class="media-heading mt-3">
														<h3><strong>Documents</strong></h3>
													</div>
													<br>
														<strong>GSTIN NO :</strong> {{$vendor->gst_no}}<br>
														<strong>Companey Name :</strong> {{$vendor->companey_name}}
													<table style="width:100%">
													<tr><td style="width:50%">
															<div class="col-md-6 p-0">
																
																	<strong>Aadhar No :</strong> {{$vendor->aadhar_no}}<br>
																
																	<strong>Aaadhar Image:</strong><br><br>
																	<a href="../user_details/{{$vendor->aadhar_image}}" target="_blank"><img src="../user_details/{{$vendor->aadhar_image}}" width="400px"></a>
																<br><br>
																
															</div>
															</td><td style="width:50%">
															<div class="col-md-6 p-0">
															<strong>Pan Card No :</strong> {{$vendor->pan_no}}<br>
																
																	<strong>Pan Image:</strong><br><br>
																	<a href="../user_details/{{$vendor->pan_image}}" target="_blank"><img src="../user_details/{{$vendor->pan_image}}" width="400px"></a>
																	<br><br>
																
																
															</div>
															<td>
														</tr>
														<tr><td style="width:50%">
															<div class="col-md-6 p-0">
																
																	
																
																	<strong>Aaadhar Image:</strong><br><br>
																	<a href="../user_details/{{$vendor->vendor_sign}}" target="_blank"><img src="../user_details/{{$vendor->vendor_sign}}" width="400px"></a>
																<br><br>
																
															</div>
															</td><td style="width:50%">
															<div class="col-md-6 p-0">
														
																
																	<strong>Pan Image:</strong><br><br>
																	<a href="../user_details/{{$vendor->companey_logo}}" target="_blank"><img src="../user_details/{{$vendor->companey_logo}}" width="400px"></a>
																	<br><br>
																
																
															</div>
															<td>
														</tr>
														</table>
													</div>

													@php($idd=$vendor->id)
													@php($bank_details=DB::table('bank_details')->where('delivery_id',$idd)->first())
													@php($bank_count=DB::table('bank_details')->where('delivery_id',$idd)->count())
													
													<h2>Bank Details</h2>
													@if($bank_count !='0')
													<div class="table-responsive border ">
														<table class="table row table-borderless w-100 m-0 ">
															<tbody class="col-lg-6 p-0">
																<tr>
																	<td><strong>Account Holder Name :</strong> {{$bank_details->a_h_name}}</td>
																</tr>
																<tr>
																	<td><strong>Bank Name:</strong>{{$bank_details->qr_code}}</td>
																</tr>
																<tr>
																	<td><strong>Branch Name:</strong>{{$bank_details->b_name}}</td>
																</tr>
																
																
															</tbody>
															<tbody class="col-lg-6 p-0">
															<tr>
																	<td><strong>Account No :</strong> {{$bank_details->a_no}}</td>
																</tr>
																<tr>
																	<td><strong>IFCS Code:</strong> {{$bank_details->ifcs_code}}</td>
																</tr>
																<tr>
																	<td><strong>UPI ID:</strong> {{$bank_details->upi_id}}</td>
																</tr>
																
																
															</tbody>
															.
															
														</table>
                                                    </div>
                                                    @else
                                                    
													<div class="alert alert-warning" role="alert">
                                                        <span class="alert-inner--icon"><i class="fe fe-info"></i></span>
                                                        <span class="alert-inner--text"><strong>Warning!</strong> Vendor not added his/her Bank Details. Contact to vendor and tell him/her add bank details.</span>
                                                    </div>
        
        
                                                  

													@endif
												<div aria-labelledby="tabs-icons-text-2-tab" class="tab-pane fade" id="tabs-icons-text-2" role="tabpanel">
													<div class="row">
														<div class="col-md-12">
															<div class="content content-full-width" id="content">
																<!-- begin profile-content -->
																<div class="profile-content">
																	<!-- begin tab-content -->
																	<div class="tab-content p-0">
																		<!-- begin #profile-friends tab -->
																		<div class="tab-pane fade in active show" id="profile-friends">
																			<h4 class="mt-0 mb-4">Total Category (14)</h4><!-- begin row -->
																			<div class="row">
																				<!-- end col-6 -->
                                                                                @foreach($forcat as $forcat)
																				<div class=" col-lg-6 col-md-12">
																					<div class="card border shadow over-flow-hidden">
																						<div class="media card-body media-xs overflow-visible ">
																							<a class="media-left" href="javascript:;"><img alt="" class="avatar avatar-md img-circle" src="assets/img/faces/female/2.jpg"></a>
																							<div class="media-body valign-middle">
																								<b class="text-inverse">{{$forcat->product_code}}</b>
																							</div>
																							<div class="media-body valign-middle text-right overflow-visible">
																								<div class="btn-group">
																									<button class="btn btn-sm btn-primary" type="button">View</button> <button class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown" type="button"><span class="caret"></span> <span class="sr-only">View profile</span></button>
																									<div class="dropdown-menu">
																										<a class="dropdown-item" href="#">Follow</a>
																										<a class="dropdown-item" href="#">Message</a>
																										<a class="dropdown-item" href="#">Suggestion</a>
																										<div class="dropdown-divider"></div><a class="dropdown-item" href="#">Delete</a>
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				
																						<!-- end col-6 -->
																				@endforeach
																			</div><!-- end row -->
																		</div><!-- end #profile-friends tab -->
																	</div><!-- end tab-content -->
																</div><!-- end profile-content -->
															</div>
														</div>
													</div>
												</div>
												<div class="tab-pane fade " id="tabs-icons-text-3" role="tabpanel" aria-labelledby="tabs-icons-text-3-tab">
                                                <div class="row">
								<div class="col-md-12">
									<div class="card shadow">
										<div class="card-header">
											<h2 class="mb-0">Data Table</h2>
										</div>
										<div class="card-body">
											<div class="table-responsive">
												<table id="example" class="table table-striped table-bordered w-100 text-nowrap">
													<thead>
														<tr>
															<th class="wd-15p">Customer Name</th>
															<th class="wd-15p">Email Address</th>
															<th class="wd-20p">Contact No</th>
															<th class="wd-20p">Action</th>
														</tr>
													</thead>
													<tbody>
                                                    <tr>
													<td>Sayantani</td>
                                                    <td>Sayantani@gmail.com</td>
                                                    <td>123456789</td>
                                                    <td><a href="#" type="button" class="btn btn-primary mt-1 mb-1">View Booking Details</a></td>
                                                    </tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
												</div></div>
												<div class="tab-pane fade" id="tabs-icons-text-4" role="tabpanel" aria-labelledby="tabs-icons-text-4-tab">
													<div class="row profile ng-scope">
														<div class="col-md-12">
															<div class="card">
																<div class="mt ng-pristine ng-valid" >
																	<div class="form-group mb-0">
																		<label class="sr-only" for="new-event">New event</label>
																		<textarea class="form-control border-top-0 border-left-0 border-right-0 border-radius-0" id="new-event" placeholder="Post something..." rows="3" name="message"></textarea>
																	</div>
																	
																	<input type="hidden" name="vendor" value="{{$vendor->id}}" id="vendor_id">
																	<input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>"/>
																	<div class="btn-toolbar">
																		
																		<input type="button" class="btn btn-primary mt-1 mb-1" value="POST" id="message_click">
																	</div>
																</div>
															</div>
															<div class="activities">
																<section class="event card shadow">
																	<div class="d-flex">
																		<span class="thumb-sm avatar pull-left mr-sm"><img class="img-circle" src="../admin_assets/img/faces/female/32.jpg" alt="..."></span>
																		<div >
																			<h4 class="event-heading"><a href="#">John doe</a> <small><a href="#">@nils</a></small></h4>
																			<p class="text-xs text-muted">February 22, 2014 at 01:59 PM</p>
																		</div>
																	</div>
																	<p class="text-sm mb-0">There is no such thing as maturity. There is instead an ever-evolving process of maturing. Because when there is a maturity, there is ...</p>
																	<footer>
																		<ul class="post-links">
																			<li><a href="#">1 hour</a>
																			</li>
																			<li><a href="#"><span class="text-danger"><i class="fa fa-heart"></i> Like</span></a>
																			</li>
																			<li><a href="#">Comment</a>
																			</li>
																		</ul>
																	</footer>
																</section>
																

															</div>
														</div>
													</div>
												</div>
												
											</div>
										</div>
									</div>
								</div>
							@endforeach
                            
                       
							<script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous"></script>
							<script type="text/javascript">








$(document).ready(function() {

$('#message_click').click(function(){
	
    var new_event = $("#new-event").val();
	var vendor_id= $("#vendor_id").val();
    var token = $("#_token").val();
	
$.ajax({

    url:'admin_message_add',

    type:'POST',

    data:{new_event:new_event,
	      vendor_id:vendor_id,
	_token:token},
    dataType:"text",
  success:function(response)
    {
    alert('1');
    $("#sub_cat").html(response);
  
    }

    });
});
});
</script>
                            @endsection