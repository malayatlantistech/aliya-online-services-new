
@extends('admin.layouts.menu')
@section('body')
<div class="container-fluid pt-8">
    <div class="page-header mt-0  p-3">
        <h3 class="mb-sm-0">Update Coupon Code</h3>
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
            <li class="breadcrumb-item active" aria-current="page">Admin Dashboard</li>
        </ol>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card shadow">
                <form action="update_coupon_action" method="post" enctype="multipart/form-data">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Coupon Title</label>
                            <input type="text" class="form-control " name="coupon_title" value="{{$coupon->title}}" placeholder="Enter Color Name">
                           
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Coupon Image(1110px X 345px)</label>
                                <input type="file" class="form-control " name="coupon_image"  onchange="validateImage('coupon_image')" id="coupon_image" placeholder="Enter Color Code">
                                <input type="hidden" value="{{$coupon->coupon_image}}" name="coupon_image2">
                            </div>
                           
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Coupon Value</label>
                            <input type="number" class="form-control" name="coupon_value" value="{{$coupon->coupon_value}}" placeholder="Enter Color Name">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Coupon Validity Date</label>
                                <input type="date" class="form-control " name="coupon_validity" value="{{$coupon->coupon_validity}}" placeholder="Enter Color Code">
                            </div>
                           
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Coupon Code</label>
                                <input type="text" class="form-control " name="coupon_code" value="{{$coupon->coupon_code}}" placeholder="Enter Coupon Code">
                            </div>
                           
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Minimum Value</label>
                                <input type="text" class="form-control " name="min_price" value="{{$coupon->min_price}}" placeholder="Enter Minimum Value">
                            </div>
                           
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Category Name</label>
                                <select class="form-control " name="cat_id">
                                    <option value="0">NO Category</option>
                                    @php($cat=DB::table('cats')->get())
                                    @foreach($cat as $cat)
                                                        <option value="{{$cat->cat_id}}" @if($cat->cat_id==$coupon->cat_id) {{'Selected'}}@endif>{{$cat->cat_name}}</option>
                                                        @endforeach
                                    
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Coupon Type</label>
                                <select class="form-control" name="coupon_type">
                                    <option value="FLAT" @if($cat->cat_id=='FLAT') {{'Selected'}}@endif>Flat Discount</option>
                                    <option value="PERCENTAGE" @if($cat->cat_id=='PERCENTAGE') {{'Selected'}}@endif>Percentage(%) Discount</option>
                                </select>
                           
                            </div>
                        </div>
                        <input type="hidden" name="coupon_id" id="coupon_id" value="{{$coupon->id}}">
                        <input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>"/>
                    </div>
<center><input type="submit" class="btn btn-primary mt-1 mb-1"></center>

                </div>
            </form>
            </div>
           
        </div>

    </div>
    </div>

    <script
src="https://code.jquery.com/jquery-3.4.1.js"
integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
crossorigin="anonymous">
</script>
    
    <script type="text/javascript">
function validateImage(id) {

   
    var formData = new FormData();
 
    var file = document.getElementById(id).files[0];
 
    formData.append("Filedata", file);
    var t = file.type.split('/').pop().toLowerCase();
    if (t != "jpeg" && t != "jpg" && t != "png" && t != "bmp" && t != "gif") {
        alert('Please select a valid image file');
        document.getElementById(id).value = '';
        return false;
    }
    if (file.size > 1024000) {
        alert('Max Upload size is 1MB only');
        document.getElementById(id).value = '';
        return false;
    }
    return true;
}
</script>

    


@endsection
