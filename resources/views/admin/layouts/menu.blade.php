<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
	<meta content="SSJ Jewellers Admin Dashboard" name="description">
	<meta content="Quantex Consulting Services Pvt. Ltd." name="author">

	<!-- Title -->
	<title>Admin Dashboard : ALIYA Online Shopping</title>

	<link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
<link rel="manifest" href="favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:300,400,600,700,800" rel="stylesheet">

	<!-- Icons -->
	<link href="admin_assets/css/icons.css" rel="stylesheet">

	<!--Bootstrap.min css-->
	<link rel="stylesheet" href="admin_assets/plugins/bootstrap/css/bootstrap.min.css">

	<!-- Dashboard CSS -->
	<link href="admin_assets/css/dashboard.css" rel="stylesheet" type="text/css">

	<!-- Tabs CSS -->
	<link href="admin_assets/plugins/tabs/style.css" rel="stylesheet" type="text/css">

	<!-- Custom scroll bar css-->
	<link href="admin_assets/plugins/customscroll/jquery.mCustomScrollbar.css" rel="stylesheet" />

	<!-- Sidemenu Css -->
	<link href="admin_assets/plugins/toggle-sidebar/css/sidemenu.css" rel="stylesheet">
	
	<!-- file uploads Css -->
	<link href="admin_assets/plugins/fileuploads/css/dropify.css" rel="stylesheet" type="text/css" />

		<!-- Data table css -->
	<link href="admin_assets/plugins/datatable/dataTables.bootstrap4.min.css" rel="stylesheet" />
	<link href="admin_assets/plugins/datatable/responsivebootstrap4.min.css" rel="stylesheet" />

		<!--Select2 css-->
		<link rel="stylesheet" href="admin_assets/plugins/select2/select2.css">
	<style>
#overlay{	
	position: fixed;
	top: 0;
	z-index: 100;
	width: 100%;
	height:100%;
	display: none;
	background: rgba(0,0,0,0.6);
}
.cv-spinner {
	height: 100%;
	display: flex;
	justify-content: center;
	align-items: center;  
}
.spinner {
	width: 40px;
	height: 40px;
	border: 4px #ddd solid;
	border-top: 4px #2e93e6 solid;
	border-radius: 50%;
	animation: sp-anime 0.8s infinite linear;
}
@keyframes sp-anime {
	100% { 
		transform: rotate(360deg); 
	}
}
.is-hide{
	display:none;
}
</style>

</head>
<body class="app sidebar-mini rtl" >
<div id="overlay">
	<div class="cv-spinner">
		<span class="spinner"></span>
	</div>
</div>
	<div id="global-loader" ></div>
	<div class="page">
		<div class="page-main">
			<!-- Sidebar menu-->
			<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
			<aside class="app-sidebar ">
				<div class="sidebar-img">
					<a class="navbar-brand" href="/"><img alt="..." class="navbar-brand-img main-logo" src="../logo/E-Kirana Logo PNG.png1"> <img alt="Logo" class="navbar-brand-img logo" src="../logo/E-Kirana Logo PNG.png"></a>
					<ul class="side-menu">
						<li class="slide">
							<a class="side-menu__item"  href="/"><i class="side-menu__icon fe fe-home"></i><span class="side-menu__label">Dashboard</span></a>
							
						</li>
						<li class="slide">
							<a class="side-menu__item"  href="/all_cats_details"><i class="side-menu__icon fe fe-underline"></i><span class="side-menu__label">Category at a Glance</span></a>
						
						</li>
						<li class="slide">
							<a class="side-menu__item"  href="/view_category"><i class="side-menu__icon fe fe-underline"></i><span class="side-menu__label">View Category</span></a>
						
						</li>
						<li class="slide">
							<a class="side-menu__item"  href="/view_vendor"><i class="side-menu__icon fe fe-bar-chart-2"></i><span class="side-menu__label">View Vendor</span></a>
						
						</li>
						<li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fe fe-folder"></i><span class="side-menu__label">Product Group</span><i class="angle fa fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="/add_group" class="slide-item">Add Product Group</a>
								</li>
								<li>
									<a href="group" class="slide-item">View Product Group</a>
								</li>
								
							</ul>
						</li>
						<li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fe fe-folder"></i><span class="side-menu__label">Product Brand</span><i class="angle fa fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="/add_brand" class="slide-item">Add Brand</a>
								</li>
								<li>
									<a href="brand" class="slide-item">View Brand</a>
								</li>
								
							</ul>
						</li>
						<li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fe fe-edit"></i><span class="side-menu__label">Product Details</span><i class="angle fa fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="/add_product" class="slide-item">Add Product</a>
								</li>
								<li>
									<a href="/view_product" class="slide-item">View Product</a>
								</li>
							
							</ul>
						</li>
						<li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fe fe-bar-chart-2"></i><span class="side-menu__label">Banner Details</span><i class="angle fa fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="/display_banner" class="slide-item">Display Banner</a>
								</li>
								<li>
									<a href="/promotion_banner" class="slide-item">Promotion Banner</a>
								</li>
								<li>
									<a href="/view_coupon" class="slide-item">Coupon Banner</a>
								</li>
								
							</ul>
						</li>
						<li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fe fe-folder"></i><span class="side-menu__label">Delivery Pincode</span><i class="angle fa fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="/add_pincode" class="slide-item">Add Pincode</a>
								</li>
								<li>
									<a href="pincode" class="slide-item">View Pincode</a>
								</li>
								
							</ul>
						</li>
						<li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fe fe-italic"></i><span class="side-menu__label">Admin Order Details</span><i class="angle fa fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="/complete_order_admin" class="slide-item">Completed Order</a>
								</li>
								<li>
									<a href="/pending_order_admin" class="slide-item">Pending Order</a>
								</li>
								<li>
									<a href="/cancel_order_admin" class="slide-item">Canceled Order</a>
								</li>
								<li>
									<a href="/return_order_admin" class="slide-item"> Pending Return Order</a>
								</li>
								<li>
									<a href="/complete_return_order_admin" class="slide-item">Completed Return Order</a>
								</li>
								
							</ul>
						</li>


						<li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fe fe-italic"></i><span class="side-menu__label">Order Details</span><i class="angle fa fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="/complete_order" class="slide-item">Completed Order</a>
								</li>
								<li>
									<a href="/pending_order" class="slide-item">Pending Order</a>
								</li>
								<li>
									<a href="/cancel_order" class="slide-item">Canceled Order</a>
								</li>
								<li>
									<a href="/return_order" class="slide-item"> Pending Return Order</a>
								</li>
								<li>
									<a href="/complete_return_order" class="slide-item">Completed Return Order</a>
								</li>
								
							</ul>
						</li>

						<li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fe fe-map"></i><span class="side-menu__label">Wallet Report</span><i class="angle fa fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="/withdrawal_request" class="slide-item">Wallet Report</a>
								</li>
							</ul>
						</li>
						<li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fe fe-map"></i><span class="side-menu__label">User Details</span><i class="angle fa fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="/normal_user" class="slide-item">All Users</a>
								</li>
								<!-- <li>
									<a href="/mediator_user" class="slide-item">Mediator Users</a>
								</li> -->
								{{-- <li>
									<a href="/subscribe_user" class="slide-item">Subscribed Users</a>
								</li> --}}
							</ul>
						</li>
						<li class="slide">
							<a class="side-menu__item" href="/inventory_report"><i class="side-menu__icon fe fe-file-text"></i><span class="side-menu__label">Inventory Report</span></a>
							
						</li>

						{{-- <li class="slide">
							<a class="side-menu__item" href="/delivery_report"><i class="side-menu__icon fe fe-shopping-cart"></i><span class="side-menu__label">Delivery Report</span></a>
							
						</li> --}}
					
						<li class="slide">
							<a class="side-menu__item" href="/sales_report"><i class="side-menu__icon fe fe-user"></i><span class="side-menu__label">Sales Report</span></a>
							
						</li>
						<li class="slide">
							<a class="side-menu__item" href="/tax_report"><i class="side-menu__icon fe fe-shopping-cart"></i><span class="side-menu__label">Tex Report</span></a>
							
						</li>
						{{-- <li class="slide">
							<a class="side-menu__item" href="/withdrawal"><i class="side-menu__icon fe fe fe-folder"></i><span class="side-menu__label">Withdrawl Request</span></a>
							
						</li>
						 --}}

						
					</ul>
				</div>
			</aside>
			<!-- Sidebar menu-->

			<!-- app-content-->
			<div class="app-content ">
				<div class="side-app">
					<div class="main-content">
						
						<!-- Top navbar -->
						<nav class="navbar navbar-top  navbar-expand-md navbar-dark" id="navbar-main">
							<div class="container-fluid">
								<a aria-label="Hide Sidebar" class="app-sidebar__toggle" data-toggle="sidebar" href="#"></a>
							
								<!-- User -->
								<ul class="navbar-nav align-items-center ">
									<li class="nav-item dropdown">
										<a aria-expanded="false" aria-haspopup="true" class="nav-link pr-md-0 mr-md-2 pl-1" data-toggle="dropdown" href="#" role="button">
											<div class="media align-items-center">
												<span class="avatar avatar-sm rounded-circle"><img alt="Image placeholder" src="image/user_icon.png"></span>

											</div>
										</a>
										<div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
											<div class=" dropdown-header noti-title text-center border-bottom pb-3">
													<h3 class="text-capitalize text-dark mb-1">{{Auth::user()->name}}</h3>
												<h6 class="text-overflow m-0">Administrator</h6>
											</div>	
											<a class="dropdown-item" href="/admin_logout"><i class="ni ni-user-run"></i> <span>Logout</span></a>
										</div>
									</li>
									
									<li class="nav-item d-none d-md-flex">
										<div class="dropdown d-none d-md-flex mt-2 ">
											<a class="nav-link full-screen-link  pr-0"><i class="fe fe-maximize-2 floating " id="fullscreen-button"></i></a>
										</div>
									</li>
								</ul>
							</div>
						</nav>
					

						@yield('body')
							<!-- Footer -->
							<footer class="footer">
								<div class="row align-items-center justify-content-xl-between">
									<div class="col-xl-6">
										<div class="copyright text-center text-xl-left text-muted">
											<p class="text-sm font-weight-500">Copyright 2020 © All Rights Reserved. <b>E-Kirana</b></p>
										</div>
									</div>
									<div class="col-xl-6" style="padding-right:30px">
									<p class="float-right text-sm font-weight-500" align="right">Design and developed by <a data-toggle="tooltip" title="" href="https://quantex.co.in" target="_blank" data-original-title="Quantex Consulting Services Pvt. Ltd."><img src="/qcs_logo/qcs_logo.png" style="height:20px"></a></p>	</div>
							</footer>
							<!-- Footer -->
						</div>
					</div>
				</div>
			</div>
			<!-- app-content-->
		</div>
	</div>

	<!-- Back to top -->
	<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

	<!-- Adon Scripts -->

	<!-- Core -->
	<script src="admin_assets/plugins/jquery/dist/jquery.min.js"></script>
	<script src="admin_assets/js/popper.js"></script>
	<script src="admin_assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<script src="admin_assets/plugins/chart-circle/circle-progress.min.js"></script>

	<!-- Optional JS -->
	<script src="admin_assets/plugins/chart.js/dist/Chart.min.js"></script>
	<script src="admin_assets/plugins/chart.js/dist/Chart.extension.js"></script>

	<!-- Fullside-menu Js-->
	<script src="admin_assets/plugins/toggle-sidebar/js/sidemenu.js"></script>
	
	<!-- file uploads js -->
    <script src="admin_assets/plugins/fileuploads/js/dropify.min.js"></script>
	<!-- Custom scroll bar Js-->
	<script src="admin_assets/plugins/customscroll/jquery.mCustomScrollbar.concat.min.js"></script>

	<!-- peitychart -->
	<script src="admin_assets/plugins/peitychart/jquery.peity.min.js"></script>
	<script src="admin_assets/plugins/peitychart/peitychart.init.js"></script>

	<!-- Adon JS -->
	<script src="admin_assets/js/custom.js"></script>
	<script src="admin_assets/js/datatable.js"></script>
	<script src="admin_assets/js/othercharts.js"></script>
	<script src="admin_assets/js/dashboard-finance.js"></script>
	<script src="admin_assets/js/select2.js"></script>

	
	<!-- Data tables -->
	<script src="admin_assets/plugins/datatable/jquery.dataTables.min.js"></script>
	<script src="admin_assets/plugins/datatable/dataTables.bootstrap4.min.js"></script>
	<script src="admin_assets/plugins/datatable/dataTables.responsive.min.js"></script>
	<script src="admin_assets/plugins/datatable/responsive.bootstrap4.min.js"></script>

	<!-- jquery-ui min js -->
	<script src="admin_assets/plugins/jquery-ui/jquery-ui.min.js"></script>
		
	<!-- Date Picker-->
	<script src="admin_assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>

	<!--Select2 js-->
	<script src="admin_assets/plugins/select2/select2.full.js"></script>
  <!-- Toast css-->
  <link rel="stylesheet" href="Material-Toast-master\mdtoast.min.css" />
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- Toast JS-->
	<script src="toast\toast.js"></script>
    <script src="Material-Toast-master\mdtoast.min.js"></script>

  
</body>
</html>
