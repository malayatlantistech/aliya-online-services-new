@extends('admin.layouts.menu')
@section('body')

<div class="container-fluid pt-8">
							<div class="page-header mt-0  p-3">
								<h3 class="mb-sm-0">Withdrawal Request</h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">Withdrawal Request</li>
								</ol>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="card shadow">
										<div class="card-header">
                                        @if ($message = Session::get('success'))
										<h2 class="mb-0" style="color:red"><b>{{ $message }}</b></h2>	
															@else
															<h2 class="mb-0">Withdrawal Request</h2>
													 @endif
											
										</div>
										<div class="card-body">
											<div class="table-responsive">
												<table id="example" class="table table-striped table-bordered w-100 text-nowrap">
													<thead>
														<tr>
                                                            <th class="wd-15p">Vendor Name</th>
															<th class="wd-15p">Email </th>
															<th class="wd-15p">Contact No</th>
															<th class="wd-15p">Amount</th>
                                                            <th class="wd-15p">Bank details</th>
															<th class="wd-15p">Action</th>
														</tr>
													</thead>
													<tbody>
                                                        @foreach($withdrawal_request as $withdrawal_request1)
                                                        <tr>
                                                            <td> {{$withdrawal_request1->name}}</td>
                                                            <td> {{$withdrawal_request1->email}}</td>
                                                            <td> {{$withdrawal_request1->mobile}}</td>
                                                            <td> {{$withdrawal_request1->withdrawal_amount}}</td>
                                                            <td>
                                                                <!-- Button trigger modal -->
                                                                <a type="button" class="btn btn-primary text-white btn-sm" data-toggle="modal" data-target="#exampleModalCenter{{$withdrawal_request1->withdrawal_request_id}}">
                                                                    View
                                                                </a>
                                                                
                                                                <!-- Modal -->
                                                                <div class="modal fade" id="exampleModalCenter{{$withdrawal_request1->withdrawal_request_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLongTitle">Bank Details</h5>
                                                                        <a type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </a>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                          @foreach($bankDetails as $bankDetails1)  
                                                                             @if($withdrawal_request1->id==$bankDetails1->delivery_id)
                                                                             Account Holder Name : {{$bankDetails1->a_h_name}}<br>
                                                                             Bank Name : {{$bankDetails1->b_name}}<br>
                                                                             Account Number : {{$bankDetails1->a_no}}<br>
                                                                             IFCS Code : {{$bankDetails1->ifcs_code}}<br>
                                                                             UPI ID : {{$bankDetails1->upi_id}}<br>
                                                                             @endif
                                                                          @endforeach
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                        <a type="button" class="btn btn-secondary text-white btn-sm" data-dismiss="modal">Close</a>
                                                                        </div>
                                                                    </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td>
                                                               
                                                                @if($withdrawal_request1->withdrawal_status=='YES')
                                                                <a href="javascript:void(0)" class="btn btn-success btn-sm"> Payment Complete</a>
                                                                @else
                                                                <a href="updatepaymentStatus?payment_id={{$withdrawal_request1->withdrawal_request_id}}" class="btn btn-success btn-sm"> Payment</a>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
                            <input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>"/>	
                            <script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous">
</script>
<script>
function user_block(id,value){
	var token = $("#_token").val();
$.ajax({

url:'user_block',


type:'POST',

data:{_token:token,id:id,value:value},

success:function(response)
{

  }   
});


}

</script>
<script>								    
$(document).ready(function() {
   $('#example').DataTable( {
        "order": [[ 0, "desc" ]]
    } );
    
});
</script> 
@endsection