
@extends('admin.layouts.menu')
@section('body')
<div class="container-fluid pt-8">
							<div class="page-header mt-0  p-3">
								<h3 class="mb-sm-0">{{$product2->product_name}}</h3>
								<ol class="breadcrumb mb-0">
									<li class="breadcrumb-item"><a href="#"><i class="fe fe-home"></i></a></li>
									<li class="breadcrumb-item active" aria-current="page">{{$product2->product_name}}</li>
								</ol>
							</div>

							
							<div class="row">
							<?php $product_id=$_GET['product_id']  ?>


							<div class="col-md-6 col-xl-6">
								<div class="card shadow">
									<div class="card-header">
										<h2 class="mb-0">Product Details</h2>
									</div>
									<div class="card-body text-left">
										
											Name : <b style="font-size:18px">{{$product2->product_name}}</b><br>
											Product Code : <b>{{$product2->product_code}}</b><br>
											HSN Code : <b>{{$product2->hsn_code}}</b><br>
											Category Name :
											@if($product2->cat_id!=0)
												<b>{{$cat->cat_name }}</b>
											@else
												<b>----</b>
											@endif
											<br>
											Sub-category Name :
											@if($product2->sub_cat_id!=0)
												<b>{{$sub_cat->sub_cat_name}}</b>
											@else
												<b>----</b>
											@endif
											<br>
											Sub-sub-category Name :
											@if($product2->sub_sub_cat_id!=0)
												<b>{{$sub_sub_cat->sub_sub_cat_name}}</b>
											@else
												<b>----</b>
											@endif
											<br>
										
											Brand Name : @if($product2->brand_id!=0) <b>{{$brand->brand_name}}</b> @else ------ @endif<br>
											Group Name : @if($product2->group_id!=0) <b>{{$group->group_name}}</b> @else ------ @endif<br>
											Size : <b>{{$product2->size}}</b><br>
											Color : <b>{{$product2->color}}</b><br>

										
											Active Status : <b>{{$product2->active_status}}</b><br>
											Vendor Active Status : <b>{{$product2->vendor_activate_status}}</b><br>
											Review : <b>{{$product2->review}}</b><br>
											Total Review : <b>{{$product2->total_review}}</b><br>
											

											<a href="update_product_details?product_id={{$product2->product_id}}" class="btn btn-icon btn-default mt-1 mb-1">
																											<span class="btn-inner--icon"><i class="fe fe-edit"></i></span>
																											<span class="btn-inner--text">Update</span>
																								</a>

									</div>
								</div>
							

							</div>
							<div class="col-md-6 col-xl-6">
								<div class="card shadow">
									<div class="card-header">
										<h2 class="mb-0">Product Description</h2>
									</div>
									<div class="card-body text-center">
										

										<?php echo $product2->description; ?>


										<br>

<a href="update_product_description?product_id={{$product2->product_id}}" class="btn btn-icon btn-default mt-1 mb-1">
																<span class="btn-inner--icon"><i class="fe fe-edit"></i></span>
																<span class="btn-inner--text">Update</span>
															</a>


									</div>
								</div>
							</div>
						</div>

						<div class="row">
							
							
							<div class="col-md-6 col-xl-6">
								<div class="card shadow">
									<div class="card-header">
										<h2 class="mb-0">Features</h2>
									</div>
									<div class="card-body text-center" >
										<table width="100%">
										 
									@foreach($feature as $key => $feature1)
									<tr>
									 <td width="45%">	
									  <p style="text-align: left">{{$key+1}}.{{$feature1->features}}  </p> 
									  </td>
                                          <td width="15%">
											<a href="delete_feature?id={{$feature1->product_feature_id }}&product_id={{$feature1->product_id}}"><i class="fa fa-trash"></i></a></td>
									  </tr>
									 @endforeach

									 </table>

	                               <a href="add_features?id={{$_GET['product_id']}}" class="btn btn-primary btn-pill mt-1 mb-1">Add Features</a>

									</div>
								</div>
							</div>
							<div class="col-md-6 col-xl-6">
								<div class="card shadow">
									<div class="card-header">
										<h2 class="mb-0">Product Specification</h2>
									</div>
									<div class="card-body text-left">
										
								<table width="100%">
									
									 @foreach($specification as $specification1)
									 <tr>
									 <td width="30%%" class="text-left"><b>{{$specification1->description}}</b></td>
										 <td width="60%" class="text-center">{{$specification1->title}}</td>	
										 <td width="10%">
										
											<a href="delete_specification?id={{$specification1->product_spacifications_id}}&product_id={{$specification1->product_id}}"><i class="fa fa-trash"></i></a>
										
										</td>
									 </tr>
									 @endforeach
									
								</table>
								<br>
								<center>	<a href="add_specification?id={{$product2->product_id}}" class="btn btn-icon  btn-primary mt-1 mb-1" >
									<span class="btn-inner--icon"><i class="fe fe-check-square"></i></span>
									<span class="btn-inner--text">Add Specification</span>
								</a></center>



									</div>
								</div>
							</div>
						</div>
							<div class="row">
							<div class="col-md-12 col-xl-12">
								<div class="card shadow">
									<div class="card-header">
										<h2 class="mb-0">Product Price & Stock Details</h2>
									</div>
									<div class="card-body">
										<div class="row">

										<table class="table table-bordered">
											<thead>
												<th>Product <br>Weight</th>
												<th>Product <br>MRP(RS)</th>
												<th>Selling <br>Price(RS)</th>
												<th>Total <br>Stock(Qty)</th>
												<th>Out <br>Stock(Qty)</th>
												<th>Available <br>Stock(Qty)</th>
											
											</thead>
											<tbody>
											<tr>
												<td>{{$product2->weight}}gm ({{round($product2->weight/1000,2)}} Kg)</td>
												<td>&#8377; {{$product2->mrp}}</td>
												@php($progst=$product2->selling_price*$sub_cat->gst/100)
												<td>&#8377; {{$product2->selling_price+$progst}}({{$progst}} GST)</td>
												<td>{{$product2->total_stock}}</td>

												<td>{{$product2->total_stock-$product2->available_stock}}</td>
												<td>{{$product2->available_stock}}</td>
											</tr>
											<tr>
											<td colspan="6">
											<center>
										
															<a href="update_price?product_id={{$product2->product_id }}" class="btn btn-icon btn-default mt-1 mb-1" >
																<span class="btn-inner--icon"><i class="fe fe-edit"></i></span>
																<span class="btn-inner--text">Update</span>
															</a>
															@if($product2->vendor_id==Auth::user()->id)
														<a href="inventory_details?product_id={{$product2->product_id }}" class="btn btn-icon btn-danger mt-1 mb-1">
																<span class="btn-inner--icon"><i class="fe fe-eye"></i></span>
																<span class="btn-inner--text">Inventory</span>
															</a>
															@endif
														
											</center>
											</td>
											</tr>
											</tbody>

										</table>

							
									</div>
									<!--	<center>
										<a href="add_color?id={{$product2->product_id}}"><button class="btn btn-icon btn-primary mt-1 mb-1" type="button">
												<span class="btn-inner--icon"><i class="fe fe-check-square"></i></span>
												<span class="btn-inner--text">Add Size</span>
											</button></a>
										</center>-->

									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12 col-xl-12">
								<div class="card shadow">
									<div class="card-header">
										<h2 class="mb-0">Product image</h2>
									</div>
									<div class="card-body">
										<div class="row">
										@php($s=sizeof($image))
											@foreach($image as $image)
										
										
									<div class="" style="padding:5px">
										<img src="/product_image/{{$image->image}}" height="160px" style="padding:10px">	
										<br><center>
											@if($s>1)
											<a href="delete_product_image?id={{$image->product_image_id }}" class="btn btn-icon btn-pill btn-danger mt-1 mb-1 btn-sm" >
											<span class="btn-inner--icon">Delete</span>
										</a>
										@endif
										<br><br>
									
									</div>
								
											@endforeach
										</div>	
											<form action="add_product_image" method="post" enctype="multipart/form-data">
												<input type="hidden" value="{{$product2->product_id}}" name="product_id">
												<input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>"/>	
												<div class="row" style="padding-left:70px;padding-top: 27px;border: solid 1px;">


											<div class="col-md-8">
													<input type="file" required onchange="validateImage(this.id)" id="img" name="image[]" multiple>
											</div>

											<div class="col-md-4">


												<input type="submit" class="btn btn-primary">
												
											
											</div>
												
												</div>
											</form>
										
									</div>
								</div>
							</div>
						</div>


						@if($product2->group_id!=0)

						<div class="row">
							<div class="col-md-12 col-xl-12">
								<div class="card shadow">
									<div class="card-header">
										<h2 class="mb-0">Same Group Product</h2>
									</div>
									<div class="card-body">
										<div class="row">
											@foreach($related_product as $related_products)
										    @php($image11=DB::table('product_images')->where('product_id',$related_products->product_id)->count())
											@if($image11>0)
										
											@php($image111=DB::table('product_images')->where('product_id',$related_products->product_id)->first())
										
									<div class="" style="padding:5px;margin: 5px 5px;border: solid 1px;">
									<a href="view_details?product_id={{$related_products->product_id}}">	<img src="/product_image/{{$image111->image}}" height="160px" style="padding:10px">	
										<br><center>
											<span style="color:#4e4c8a">Size : <b>{{$related_products->size}}</b>, &nbsp; Color: <b>{{$related_products->color}}</b><br/>
										{{$related_products->weight}}gm / ({{$related_products->weight/1000}}kg)</span>
											</center>
										</a>
										
									
									</div>
								@endif
											@endforeach
											<br><br>
										</div>	
											
										
									</div>
								</div>
							</div>
						</div>
						@endif


						<script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous">
</script>	
						
						
<script type="text/javascript">
function validateImage(id) {
    var formData = new FormData();
 
    var file = document.getElementById(id).files[0];
 
    formData.append("Filedata", file);
    var t = file.type.split('/').pop().toLowerCase();
    if (t != "jpeg" && t != "jpg" && t != "png" && t != "bmp" && t != "gif") {
        alert('Please select a valid image file');
        document.getElementById(id).value = '';
        return false;
    }
    if (file.size > 1024000) {
        alert('Max Upload size is 1MB only');
        document.getElementById(id).value = '';
        return false;
    }
    return true;
}
</script>
					
                        @endsection							