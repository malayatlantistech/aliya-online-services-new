<style>
  @media only screen and (max-width: 600px) {
      #gotobutton{display:none}
  }
  </style>
  
  <div class="mb-10 cart-table">
                     
                          <table class="table" cellspacing="0">
                              <thead>
                                  <tr>
                                      <th class="product-remove" style="    text-align: -webkit-center;">Remove</th>
                                      <th class="product-thumbnail" style="    text-align: -webkit-center;">Image</th>
                                      <th class="product-name" style="    text-align: -webkit-center;">Product</th>
                                      <th class="product-price" style="    text-align: -webkit-center;">Price</th>
                                      <th class="product-quantity w-lg-15" style="    text-align: -webkit-center;">Quantity</th>
                                      <th class="product-subtotal" style=" text-align: -webkit-center;">Total</th>
                                  </tr>
                              </thead>
                              <tbody>
                                @php($user_id=Auth::user()->id)
                                @php($for_cart_view2=DB::table('carts')->where('user_id',$user_id)->count())
                                
      @if($for_cart_view2==0)
      <tr>
                                      <td colspan="6" class="border-top justify-content-center" style="padding-bottom: 2.188rem;border-bottom: solid 1px #eae7e7;">
  <center><h4>No Item In Your Cart</h4>
  <a href="/shop"> <button type="button" class="btn btn-block btn-dark px-4 mb-3 mb-md-0 font-weight-normal px-5 px-md-4 px-lg-5 w-100 w-md-auto" >Continue Shopping</button></a>
                                 
  </center>
  </td>
  </tr>
      @else
      @php($stock=1)
            @php($grand_total=0)
            @foreach($for_cart_view as $key=>$for_cart_view)
            @php($view_product_id2=$for_cart_view->product_id)
            @php($product_image2=DB::table('product_images')->where('product_id',$view_product_id2)->limit(1)->get())
                                 
            
  
                                  <tr class="">
                                      <td class="text-center" style=" text-align: -webkit-center;">
                                   
                                          <a href="javascript:void(0)"  onclick="remove_item({{$for_cart_view->product_id}},{{$for_cart_view->product_price_id}});" class="text-gray-32 font-size-26">×</a>
                                     
                                      </td>
                                      <td class="d-none d-md-table-cell" style=" text-align: -webkit-center;">
                                      @foreach($product_image2 as $product_image2)
                                          <a href="/product/{{$view_product_id2}}"><img class="img-fluid max-width-100 p-1 border border-color-1" src="../product_image/{{$product_image2->image}}" alt="{{$for_cart_view->product_name}}"></a>
                                          @endforeach
                                      </td>
  
                                      <td data-title="Product" style=" text-align: -webkit-center;">
                                          <a href="/product/{{$view_product_id2}}" class="text-gray-90">{{$for_cart_view->product_name}}({{$for_cart_view->size}})</a>
                                        
                                          @if($for_cart_view->available_stock<1)
                                          @php($stock=0)
                                          <span style="background-color: #e40b0b61;padding: 5px;border-radius: 20px;font-size: 12px;color: red;font-weight: bolder;">Out of Stock</span>
                                    @endif
                                      </td>
  
                                      <td data-title="Price" style=" text-align: -webkit-center;">
                                     
                                        @php($progst=$for_cart_view->selling_price*$for_cart_view->gst/100)
                           
                                        ₹{{$price=$for_cart_view->selling_price+$progst}}
                                        <input type="hidden" id="database_price-{{$for_cart_view->id}}" value="{{$price}}">
                                      </td>
  
                                      <td data-title="Quantity" style=" text-align: -webkit-center;">
                                          <span class="sr-only">Quantity</span>
                                          <!-- Quantity -->
                                          <div class="border rounded-pill py-1 width-122 w-xl-80 px-3 border-color-1">
                                              <div class="js-quantity row align-items-center">
                                                  <div class="col">
                                                      <input class="js-result form-control h-auto border-0 rounded p-0 shadow-none gst1" type="text" value="{{$for_cart_view->quantity}}" id="val-{{$for_cart_view->id}}">
                                                  </div>
                                                  <div class="col-auto pr-1">
                                                      <a class="js-minus btn btn-icon btn-xs btn-outline-secondary rounded-circle border-0 qtybutton down" href="javascript:void(0);" id="down-{{$for_cart_view->id}}" onclick="down({{$for_cart_view->id}})">
                                                          <small class="fas fa-minus btn-icon__inner"></small>
                                                      </a>
                                                      <a class="js-plus btn btn-icon btn-xs btn-outline-secondary rounded-circle border-0 qtybutton up" href="javascript:void(0);" id="up-{{$for_cart_view->id}}" onclick="up({{$for_cart_view->id}})">
                                                          <small class="fas fa-plus btn-icon__inner"></small>
                                                      </a>
                                                  </div>
                                              </div>
                                          </div>
             

                                          <!-- End Quantity -->
                                      </td>
  
                                      <td data-title="Total" style=" text-align: -webkit-center;">
                                          <span class="">&#8377;
                                          <input type="text" class="tp-{{$key}}" readonly id="total_product_price-{{$for_cart_view->id}}" value="{{$price*$for_cart_view->quantity}}" style="width:54px;border:0">
  </span>
                                      </td>
                                  </tr>
      @endforeach
      <input type="hidden" id="total_product" value="{{$key}}">
                                
                                  <tr>
                                      <td colspan="6" class="border-top justify-content-center">
                                      <div class=" cart-total">
                      <div class="row">
                          <div class="col-xl-5 col-lg-6 offset-lg-6 offset-xl-7 col-md-8 offset-md-4">
                              <div class="border-bottom border-color-1 mb-3">
                                  <h3 class="d-inline-block section-title mb-0 pb-2 font-size-26" style="    padding-right: 20px;">Cart totals</h3><span class="amount" style="float:right;font-size: 20px;">&#8377;<input readonly type="text"  style="width:120px;border:0" id="sub_total" value="0"></span>
                              </div>
                          </div>
                      </div>
                  </div>
                                          <div class="pt-md-3">
                                              <div class="d-block d-md-flex flex-center-between">
                                                  <div class="mb-3 mb-md-0 w-xl-40" id="gotobutton">
                                                      <!-- Apply coupon Form -->
                                                     <a href="/shop"> <button type="button" class="btn btn-block btn-dark px-4 mb-3 mb-md-0 font-weight-normal px-5 px-md-4 px-lg-5 w-100 w-md-auto">Goto Shopping</button></a>
                                           <!-- End Apply coupon Form -->
                                                  </div>
                                                  <div class="d-md-flex">
                                                 
                                                     <a href="/checkout"> <button  class="btn btn-primary-dark-w ml-md-2 px-5 px-md-4 px-lg-5 w-100 w-md-auto d-none d-md-inline-block" @if($stock==0) disabled @endif style="cursor: pointer">Proceed to checkout</button></a>
                                                  </div>
                                                  
                                              </div><br><center style="color:red"><b>@if($stock==0) *Please remove out of stock item from cart for checkout @endif</b></center>
                                          </div>
                                      </td>
                                  </tr>
                              </tbody>
                          </table>
                     
                  </div>
                  @endif
                
      <script>
  /*
  alert();
     // sub_total();
  
  
  function remove_item(id){
  
  
      remove_cart(id);
      
  show_cart() ;
   
  
  
  }
  
  function up(product_id){
    // alert(product_id);
    alert(0);
      //alert(value);
      var value=Number($('#val-'+product_id).val())+1;
      alert(value);
     
      $('#val-'+product_id).val(value);
   
          var price= Number($('#database_price-'+product_id).val());
          var total_price=price*value;
      
    // alert(price);
     // alert(total_price);
      $('#total_product_price-'+product_id).val(total_price);
      $('#product_price-'+product_id).val(price);
      sub_total();
      update(product_id);
  }
  function down(product_id){
      alert(0);
      //alert(value);
      var value=Number($('#val-'+product_id).val());
      alert(value);
      var price= Number($('#database_price-'+product_id).val());
          var total_price=price*value;
      
    // alert(price);
     // alert(total_price);
      $('#total_product_price-'+product_id).val(total_price);
      $('#product_price-'+product_id).val(price);
      sub_total();
      update(product_id);
  
      }
  }
  function sub_total(){
      
      var total_product=$('#total_product').val();
      //alert(total_product);
  var sub_total=0;
  var i;
  //alert($('.tp-0').val());
  //var p=$('.tp-0').val();
      //alert(p);
  for(i=0;i<=total_product;i++){
      var p=$('.tp-'+i).val();
     // alert(p);
      sub_total=sub_total+Number(p);
     // alert(sub_total);
  }
  $('#sub_total').val(sub_total);
  }
  
  
  
  function update(id){
   
      var value=$('#val-'+id).val();
  
    
      var token=$("#_token").val();
  
      
  
  $.ajax({
  
   url:'/update_qty',
  
   type:'POST',
  
   data:{_token:token,id:id,value:value},
  
  success:function(response)
   {
  
  
   }
  
   });
  }
  */
  
  </script>
  
  
  