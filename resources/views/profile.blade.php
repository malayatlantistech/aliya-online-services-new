



@extends('layouts.menu')

@section('title')
Profile - {{Auth::user()->name}} : ALIYA Online Services
@endsection

@section('content')


<main id="content" role="main">
            <!-- breadcrumb -->
            <div class="bg-gray-13 bg-md-transparent">
                <div class="container">
                    <!-- breadcrumb -->
                    <div class="my-md-3">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                                <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="/">Home</a></li>
                                <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">My Account</li>
                            </ol>
                        </nav>
                    </div>
                    <!-- End breadcrumb -->
                </div>
            </div>
            <!-- End breadcrumb -->

            <div class="container">
                @if(Auth::user()->name==null && Auth::user()->email==null)
                <div class="alert alert-danger" role="alert">
                    Please add your <strong>Name</strong> and <strong>Email ID</strong> in your profile. 
                </div>
                @elseif(Auth::user()->name==null)
                <div class="alert alert-danger" role="alert">
                    Please add your <strong>Name</strong> in your profile. 
                </div>
                @elseif(Auth::user()->email==null)
                <div class="alert alert-danger" role="alert">
                    Please add your <strong>Email ID</strong> in your profile. 
                </div>
                @endif
                <div class="mb-4">
                    <h1 class="text-center">My Account</h1>
                </div>
            
                @if ($message = Session::get('success'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif


@if ($message = Session::get('error'))
<div class="alert alert-danger alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif


@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
	<strong>{{ $message }}</strong>
</div>
@endif


@if ($message = Session::get('info'))
<div class="alert alert-info alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
	<strong>{{ $message }}</strong>
</div>
@endif


@if ($errors->any())
<div class="alert alert-danger">
	<button type="button" class="close" data-dismiss="alert">×</button>	
	Please check the form below for errors
</div>
@endif
                <div class="my-4 my-xl-8">
                    <div class="row">
                        <div class="col-md-5 ml-xl-auto mr-md-auto mr-xl-0 mb-8 mb-md-0">
                            <!-- Title -->
                            <div class="border-bottom border-color-1 mb-6">
                                <h3 class="d-inline-block section-title mb-0 pb-2 font-size-26">Update Profile</h3>
                            </div>
                            <p class="text-gray-90 mb-4">Welcome back! Update your profile.</p>
                            <!-- End Title -->
                            <form class="js-validate" novalidate="novalidate" action="update_profile" method="post" enctype="multipart/form-data">
                            @csrf
                                <!-- Form Group -->
                                <div class="js-form-message form-group">
                                    <label class="form-label" for="signinSrEmailExample3">Name
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" value="{{Auth::user()->name}}" class="form-control @error('name') is-invalid @enderror" name="name" id="signinSrEmailExample3" placeholder="Enter your name" aria-label="Username or Email address" required="" data-msg="Please enter a valid email address." data-error-class="u-has-error" data-success-class="u-has-success" required>
                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                </div>
                                <!-- End Form Group -->

                                <!-- Form Group -->
                                <div class="js-form-message form-group">
                                    <label class="form-label" for="signinSrPasswordExample2">Email ID <span class="text-danger">*</span> @if(Auth::user()->email=='demo@gmail.com')  @else {{Auth::user()->email}} @endif</label>
                                    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value=""  id="" placeholder="Enter Your Email ID" aria-label="Password" >
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                </div>
                                <!-- End Form Group -->

                                <!-- Form Group -->
                                <div class="js-form-message form-group">
                                    <label class="form-label" for="signinSrPasswordExample2">Phone No <span class="text-danger">*</span> ({{Auth::user()->mobile}})</label>
                                    <input type="number" class="form-control @error('mobile') is-invalid @enderror" name="mobile" id="signinSrPasswordExample2" value="" placeholder="Enter Your Phone Number" aria-label="Password" data-msg="Your password is invalid. Please try again." data-error-class="u-has-error" data-success-class="u-has-success">
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                </div>
                                <!-- End Form Group -->

                               

                         

                                <!-- Button -->
                                <div class="mb-1">
                                    <div class="mb-3">
                                        <button type="submit" class="btn btn-primary-dark-w px-5">Update Profile</button>
                                    </div>
                                   
                                </div>
                                <!-- End Button -->
                            </form>
                        </div>
                        <div class="col-md-1 d-none d-md-block">
                            <div class="flex-content-center h-100">
                                <div class="width-1 bg-1 h-100"></div>
                                <div class="width-50 height-50 border border-color-1 rounded-circle flex-content-center font-italic bg-white position-absolute">or</div>
                            </div>
                        </div>
                        <div class="col-md-5 ml-md-auto ml-xl-0 mr-xl-auto">
                            <!-- Title -->
                            <div class="border-bottom border-color-1 mb-6">
                                <h3 class="d-inline-block section-title mb-0 pb-2 font-size-26">Change Password</h3>
                            </div>
                            <p class="text-gray-90 mb-4">Change your account password and secure your account.</p>
                            <!-- End Title -->
                            <!-- Form Group -->
                            <form class="e" novalidate="novalidate"  method="post" action="change_password">
                                        <!-- Form Group -->
                                        @csrf
                                        <div class="js-form-message form-group">
                                    <label class="form-label" for="signinSrPasswordExample2">Current Password <span class="text-danger">*</span></label>
                                    <input type="password" class="form-control" name="current_password"  placeholder="Password" aria-label="Password" required=""  >
                             
                                @if ($errors->has('current_password'))
                               
                                                                                        <strong style="color:red;font-size: 11px;">{{ $errors->first('current_password') }}</strong>
                                                                                
                                                                                @endif    </div>
                                      <!-- Form Group -->
                                      <div class="js-form-message form-group">
                                    <label class="form-label" for="signinSrPasswordExample2">New Password <span class="text-danger">*</span></label>
                                    <input type="password" class="form-control" name="password"  placeholder="Password" aria-label="Password" required="">
                               
                                @if ($errors->has('password'))
                               
                                                                                        <strong style="color:red;font-size: 11px;">{{ $errors->first('password') }}</strong>
                                                                                
                                                                                @endif  </div>
                                <!-- End Form Group -->
                                        <!-- Form Group -->
                                        <div class="js-form-message form-group">
                                    <label class="form-label" for="signinSrPasswordExample2">Confirm Password <span class="text-danger">*</span></label>
                                    <input type="password" class="form-control" name="confirm"  placeholder="Password" aria-label="Password" required="" >
                                    @if ($errors->has('confirm'))
                               
                                                                                        <strong style="color:red;font-size: 11px;">{{ $errors->first('confirm') }}</strong>
                                                                                
                                                                                @endif 
                                </div>
                                <!-- End Form Group -->
                                <div class="mb-1">
                                    <div class="mb-3">
                                        <button type="submit" class="btn btn-primary-dark-w px-5">Change Password</button>
                                    </div>
                                   
                                </div>
                            </form>
                            
                        </div>
                    </div>
                </div>
            </div>
        </main>


        
<main id="content" role="main">
           
            <!-- End breadcrumb -->

            <div class="container">
                <div class="mb-4">
                    <h1 class="text-center">Billing Address</h1>
                </div>
                @if ($message = Session::get('success1'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif


                <div class="my-4 my-xl-8">
                <form  method="post" action="billing_address">
                    <div class="row">
                   
                    <div class="col-xl-1">
                        </div>
                        <div class="col-xl-5 col-lg-12 col-md-12 col-sm-12  ">
                                <div class="ps-form__billing-info">
                                   
                                    <div class="form-group">
                                        <label>Pincode<sup>*</sup>
                                        </label>
                                        <div class="form-group__content">

                                   <input type="number" class="form-control" id="pin" name="pin" onchange="fetch_address(this.value,'{{csrf_token()}}')" placeholder="Pincode" required="" value="@if(isset($address->pincode)){{(int)$address->pincode}}@endif">
                                   @if ($errors->has('pin'))
                               
                               <strong style="color:red;font-size: 11px;">{{ $errors->first('pin') }}</strong>
                       
                       @endif 
                        
                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Address<sup>*</sup>
                                        </label>
                                        <div class="form-group__content">
                                        <input type="text" class="form-control" id="address" name="address" placeholder="Address" required="" value="@if(isset($address->address)) {{$address->address}}@endif">
                                        @if ($errors->has('address'))
                               
                               <strong style="color:red;font-size: 11px;">{{ $errors->first('address') }}</strong>
                       
                       @endif 
                           </div>
                                    </div>
                                    
                                    
                                </div>
                        </div>
                        <div class="col-xl-5 col-lg-12 col-md-12 col-sm-12  ">
                                <div class="ps-form__billing-info">
                                   
                                   
                                    <div class="form-group">
                                        <label>Location<sup>*</sup>
                                        </label>
                                        <div class="form-group__content">
                                        <input type="text" class="form-control" id="location" name="location" placeholder="location" required="" value="@if(isset($address->location)) {{$address->location}}@endif">
                                        @if ($errors->has('location'))
                               
                               <strong style="color:red;font-size: 11px;">{{ $errors->first('location') }}</strong>
                       
                       @endif 
                               </div>
                               
                                    </div>
                                </div>
                                <div class="form-group">
                                        <label>Landmark<sup>*</sup>
                                        </label>
                                        <div class="form-group__content">
                                        <input type="text" class="form-control" id="landmark" name="landmark" placeholder="Landmark" required="" value="@if(isset($address->landmark)) {{$address->landmark}}@endif">
                                        @if ($errors->has('landmark'))
                               
                               <strong style="color:red;font-size: 11px;">{{ $errors->first('landmark') }}</strong>
                       
                       @endif 
                     </div>
                                    </div>
                        </div>

                        </div>
                        <div class="row">
                        <div class="col-xl-1">
                        </div>
                        <div class="col-xl-3 col-lg-12 col-md-12 col-sm-12  ">
                                <div class="ps-form__billing-info">
                                   
                                    <div class="form-group">
                                        <label>City<sup>*</sup>
                                        </label>
                                        <div class="form-group__content">
                                        <input type="text" class="form-control" id="city" name="city"  placeholder="City" required="" value="@if(isset($address->city)) {{$address->city}}@endif">
                                        @if ($errors->has('city'))
                               
                               <strong style="color:red;font-size: 11px;">{{ $errors->first('city') }}</strong>
                       
                       @endif 
                          </div>
                                    </div>
                                   
                                    
                                    
                                </div>
                        </div>
                        <div class="col-xl-4 col-lg-12 col-md-12 col-sm-12  ">
                                <div class="ps-form__billing-info">
                                   
                                    <div class="form-group">
                                        <label>District<sup>*</sup>
                                        </label>
                                        <div class="form-group__content">
                                        <input type="text" class="form-control" id="district" name="district" placeholder="Disrict" required="" value="@if(isset($address->district)) {{$address->district}}@endif">
                                        @if ($errors->has('district'))
                               
                               <strong style="color:red;font-size: 11px;">{{ $errors->first('district') }}</strong>
                       
                       @endif 
                         </div>
                                    </div>
                                   
                                </div>
                        </div>
                        @csrf
                        <div class="col-xl-3 col-lg-12 col-md-12 col-sm-12  ">
                                <div class="ps-form__billing-info">
                                   
                                    <div class="form-group">
                                        <label>Region/State<sup>*</sup>
                                        </label>
                                        <div class="form-group__content">
                                        <input type="text" class="form-control" id="state" name="state" placeholder="Region/state" required="" value="@if(isset($address->state)) {{$address->state}}@endif">
                                        @if ($errors->has('state'))
                               
                               <strong style="color:red;font-size: 11px;">{{ $errors->first('state') }}</strong>
                       
                       @endif 
                           </div>
                                    </div>
                                   
                                </div>
                        </div>

                        </div>
                        <div class="row">
                        <div class="col-xl-4">
                        </div>
                        <div class="col-xl-4">
                       <center> <input type="submit" name="submit" id="saveaddress" class="btn btn-primary-dark-w px-5" style="height:50px" value="Save address"></center>
                        </div>
                        <div class="col-xl-3">
                        </div>

                        
                    </div></form>
                </div>
            </div>
        </main>
        <script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous">
</script>
<script>
function fetch_address(pin,token){


    if(pin.length==6){
        //alert(pin);
   $("#overlay").fadeIn(300);
    //var token = $("#malay").val();
   // alert(token);
    $.ajax({
    
    url:'fetch_address',
    type:'POST',
    data:{_token:token,pin:pin},
   
  success:function(response)
    {
      //  alert(response);
        var data=response.split("|");
        if(data[4]==1){
       
        $('#location').val(data[0]);
        $('#city').val(data[1]);
        $('#district').val(data[2]);
        $('#state').val(data[3]);
        $('#saveaddress').prop('disabled', false);
   }else{
    mdtoast('Please enter correct pin code.', { 
  interaction: true, 
  actionText: 'Close',
  
  action: function(){
    this.hide(); 
   
  },
 

});
$('#saveaddress').prop('disabled', true);
   }
  
    //alert(response[0]['location']);
    $("#overlay").fadeOut(300);
    }   
 });
    }
}
</script>
@endsection

