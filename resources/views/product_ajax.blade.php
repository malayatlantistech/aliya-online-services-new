{{$pro_count}}malayssj
@if($pro_count==0)
   <span style="font-weight: 900;color: black; padding-left: 34%;font-size: 30px;padding-top:50px">No Product Found</span>
@else



<div class="tab-content" id="pills-tabContent">
    <div class="tab-pane fade pt-2 show active" id="pills-one-example1" role="tabpanel" aria-labelledby="pills-one-example1-tab" data-target-group="groups">
        <ul class="row list-unstyled products-group no-gutters">
            @foreach($pro as $related_product_details1)
                            @php($product_id=$related_product_details1->product_id)
                            @php($image1=DB::table('product_images')->where('product_id',$product_id)->limit(1)->first())
                            @php($image2=DB::table('product_images')->where('product_id',$product_id)->limit(1)->orderby('product_image_id','desc')->first())
                            @php($brand=DB::table('brands')->where('brand_id',$related_product_details1->brand_id)->first())
                            @php($sub_cat=DB::table('sub_cats')->where('sub_cat_id',$related_product_details1->sub_cat_id)->first())

            <li class="col-6 col-md-3 col-wd-2gdot4 product-item">
                <div class="product-item__outer h-100">
                    <div class="product-item__inner px-xl-4 p-3">
                        <div class="product-item__body pb-xl-2">
                            
                            <div class="mb-2"><a href="/shop?brand={{$brand->brand_id}}&name={{$brand->brand_name}}" class="font-size-12 text-gray-5">{{$brand->brand_name}}</a></div>
                           
                            <h5 class="mb-1 product-item__title"><a href="/product/{{$product_id}}" class="text-blue font-weight-bold"><?php echo substr($related_product_details1->product_name,0,18); ?>@if(strlen($related_product_details1->product_name)>18){{'...'}} @endif</a></a></h5>
                            <div class="mb-2">
                                <a href="/product/{{$product_id}}" class="d-block text-center"><img class="img-fluid" src="/product_image/{{$image1->image}}" title="{{$related_product_details1->product_name}}" alt="Image Description"></a>
                            </div>
                            <div class="flex-center-between mb-1">
                                <div class="prodcut-price">
                                    @php($progst=$related_product_details1->selling_price*$sub_cat->gst/100)
                                    <div class="text-gray-100">₹{{round($related_product_details1->selling_price+$progst)}}</div>
                                </div>
                                <div class="d-none d-xl-block prodcut-add-cart">
                                    <span style="font-size: 10px;">
                                        @php($a=(int)$related_product_details1->review)
                                        @for($i=0;$i<$a;$i++)
                                        <span class="" style="    width:  max-contant;"><i class="fa fa-star" style="color:#fa4251"></i></span>
                                        @endfor
                                        @for($i=0;$i<5-$a;$i++)
                                        <span class="" style="    width: max-contant;"><i class="fa fa-star"     style="color: #79797975;"></i></span> 
                                        @endfor
                                        ({{$related_product_details1->review}})
                                        </span>
                                </div>
                            </div>
                        </div>
                        <div class="product-item__footer">
                            <div class="border-top pt-2 flex-center-between flex-wrap">
                                <a href="javascript:void(0);" class="text-gray-6 font-size-13" onclick="cart({{$product_id}});"><i class="ec ec-add-to-cart mr-1 font-size-15"></i> Cart</a>
                                <a href="javascript:void(0);" class="text-gray-6 font-size-13" onclick="wishlist({{$product_id}});"><i class="ec ec-favorites mr-1 font-size-15"></i> Wishlist</a>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            @endforeach

           
        </ul>
    </div>

    <div class="tab-pane fade pt-2" id="pills-two-example1" role="tabpanel" aria-labelledby="pills-two-example1-tab" data-target-group="groups">
        <ul class="row list-unstyled products-group no-gutters">
            @foreach($pro as $related_product_details1)
                            @php($product_id=$related_product_details1->product_id)
                            @php($image1=DB::table('product_images')->where('product_id',$product_id)->limit(1)->first())
                            @php($image2=DB::table('product_images')->where('product_id',$product_id)->limit(1)->orderby('product_image_id','desc')->first())
                            @php($brand=DB::table('brands')->where('brand_id',$related_product_details1->brand_id)->first())
                            @php($sub_cat=DB::table('sub_cats')->where('sub_cat_id',$related_product_details1->sub_cat_id)->first())

            <li class="col-6 col-md-3 col-wd-2gdot4 product-item">
                <div class="product-item__outer h-100">
                    <div class="product-item__inner px-xl-4 p-3">
                        <div class="product-item__body pb-xl-2">
                            <div class="mb-2"><a href="/shop?brand={{$brand->brand_id}}&name={{$brand->brand_name}}" class="font-size-12 text-gray-5">{{$brand->brand_name}}</a></div>

                            <h5 class="mb-1 product-item__title"><a href="/product/{{$product_id}}" class="text-blue font-weight-bold"><?php echo substr($related_product_details1->product_name,0,18); ?>@if(strlen($related_product_details1->product_name)>18){{'...'}} @endif</a></a></h5>

                            <div class="mb-2">

                                <a href="/product/{{$product_id}}" class="d-block text-center"><img class="img-fluid" src="/product_image/{{$image1->image}}" title="{{$related_product_details1->product_name}}" alt="Image Description"></a>
                            </div>
                            <div class="mb-3">
                                <a class="d-inline-flex align-items-center small font-size-14" href="#">
                                    <span style="font-size: 10px;">
                                        @php($a=(int)$related_product_details1->review)
                                        @for($i=0;$i<$a;$i++)
                                        <span class="" style="    width:  max-contant;"><i class="fa fa-star" style="color:#fa4251"></i></span>
                                        @endfor
                                        @for($i=0;$i<5-$a;$i++)
                                        <span class="" style="    width: max-contant;"><i class="fa fa-star"     style="color: #79797975;"></i></span> 
                                        @endfor
                                        ({{$related_product_details1->review}})
                                        </span>
                                </a>
                            </div>
                            
                            <div class="flex-center-between mb-1">
                                <div class="prodcut-price">
                                    @php($progst=$related_product_details1->selling_price*$sub_cat->gst/100)
                                    <div class="text-gray-100">₹{{round($related_product_details1->selling_price+$progst)}}
                                    <span style="font-size: 16px;"><s><span class="currencySymbol">₹</span>{{$related_product_details1->mrp}}</span></s>
                                    @php($dis=(($related_product_details1->mrp-$related_product_details1->selling_price+$progst)/$related_product_details1->mrp)*100)
                                    <span style="font-size: 16px;color: #388e3c;    font-weight: 700;"><span class="currencySymbol">{{round($dis)}}</span>% off</span>
                                </div>
                                </div>
                              
                            </div>
                        </div>
                        <div class="product-item__footer">
                            <div class="border-top pt-2 flex-center-between flex-wrap">
                                <a href="javascript:void(0);" class="text-gray-6 font-size-13" onclick="cart({{$product_id}});"><i class="ec ec-add-to-cart mr-1 font-size-15"></i> Cart</a>
                                <a href="javascript:void(0);" class="text-gray-6 font-size-13" onclick="wishlist({{$product_id}});"><i class="ec ec-favorites mr-1 font-size-15"></i> Wishlist</a>
                           </div>
                        </div>
                    </div>
                </div>
            </li>
            @endforeach

           
        </ul>
    </div>

    <div class="tab-pane fade pt-2" id="pills-three-example1" role="tabpanel" aria-labelledby="pills-three-example1-tab" data-target-group="groups">
        <ul class="d-block list-unstyled products-group prodcut-list-view">
            @foreach($pro as $related_product_details1)
            @php($product_id=$related_product_details1->product_id)
            @php($image1=DB::table('product_images')->where('product_id',$product_id)->limit(1)->first())
            @php($image2=DB::table('product_images')->where('product_id',$product_id)->limit(1)->orderby('product_image_id','desc')->first())
            @php($brand=DB::table('brands')->where('brand_id',$related_product_details1->brand_id)->first())
            @php($sub_cat=DB::table('sub_cats')->where('sub_cat_id',$related_product_details1->sub_cat_id)->first())

            <li class="product-item remove-divider">
                <div class="product-item__outer w-100">
                    <div class="product-item__inner remove-prodcut-hover py-4 row">
                        <div class="product-item__header col-6 col-md-4">
                           
                            <div class="mb-2">

                                <a href="/product/{{$product_id}}" class="d-block text-center"><img class="img-fluid" src="/product_image/{{$image1->image}}" alt="Image Description" title="{{$related_product_details1->product_name}}"></a>
                            </div>
                        </div>
                        <div class="product-item__body col-6 col-md-5">
                            <div class="pr-lg-10">
                                <div class="mb-2"><a href="/shop?brand={{$brand->brand_id}}&name={{$brand->brand_name}}" class="font-size-12 text-gray-5">{{$brand->brand_name}}</a></div>
                                <h5 class="mb-1 product-item__title"><a href="/product/{{$product_id}}" class="text-blue font-weight-bold"><?php echo substr($related_product_details1->product_name,0,25); ?>@if(strlen($related_product_details1->product_name)>18){{'...'}} @endif</a></a></h5>
                                <div class="prodcut-price mb-2 d-md-none">
                                    <div class="text-gray-100">$685,00</div>
                                </div>
                                <div class="mb-3 d-none d-md-block">
                                    <a class="d-inline-flex align-items-center small font-size-14" href="#">
                                        <div class="text-warning mr-2">
                                            <span style="font-size: 10px;">
                                                @php($a=(int)$related_product_details1->review)
                                                @for($i=0;$i<$a;$i++)
                                                <span class="" style="    width:  max-contant;"><i class="fa fa-star" style="color:#fa4251"></i></span>
                                                @endfor
                                                @for($i=0;$i<5-$a;$i++)
                                                <span class="" style="    width: max-contant;"><i class="fa fa-star"     style="color: #79797975;"></i></span> 
                                                @endfor
                                                ({{$related_product_details1->review}})
                                            </span>
                                        </div>
                                       
                                    </a>
                                </div>
                                <ul class="font-size-12 p-0 text-gray-110 mb-4 d-none d-md-block">
                                        @php($progst=$related_product_details1->selling_price*$sub_cat->gst/100)
                                        <h4> ₹{{round($related_product_details1->selling_price+$progst)}}</h4>
                                        <span style="font-size: 16px;"><s><span class="currencySymbol">₹</span>{{$related_product_details1->mrp}}</span></s>
                                        @php($dis=(($related_product_details1->mrp-$related_product_details1->selling_price+$progst)/$related_product_details1->mrp)*100)
                                        <span style="font-size: 16px;color: #388e3c;    font-weight: 700;"><span class="currencySymbol">{{round($dis)}}</span>% off</span>
                                  
                                </ul>
                            </div>
                        </div>
                        <div class="product-item__footer col-md-3 d-md-block">
                           
                            <div class="flex-horizontal-center justify-content-between justify-content-wd-center flex-wrap">
                                <div class="prodcut-add-cart">
                                    <a href="javascript:void(0);" onclick="cart({{$product_id}});" class="btn btn-sm btn-block btn-primary-dark btn-wide transition-3d-hover">Add to cart</a>
                                </div>
                            </div>
                            <br>

                            <div class="flex-horizontal-center justify-content-between justify-content-wd-center flex-wrap">
                                <div class="prodcut-add-cart">
                                    <a href="javascript:void(0);" onclick="wishlist({{$product_id}});" class="btn btn-sm btn-block btn-primary-dark btn-wide transition-3d-hover">Add to wishlist</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            @endforeach

           
        </ul>
    </div>

    <div class="tab-pane fade pt-2" id="pills-four-example1" role="tabpanel" aria-labelledby="pills-four-example1-tab" data-target-group="groups">
        <ul class="d-block list-unstyled products-group prodcut-list-view-small">
            @foreach($pro as $related_product_details1)
            @php($product_id=$related_product_details1->product_id)
            @php($image1=DB::table('product_images')->where('product_id',$product_id)->limit(1)->first())
            @php($image2=DB::table('product_images')->where('product_id',$product_id)->limit(1)->orderby('product_image_id','desc')->first())
            @php($brand=DB::table('brands')->where('brand_id',$related_product_details1->brand_id)->first())
            @php($sub_cat=DB::table('sub_cats')->where('sub_cat_id',$related_product_details1->sub_cat_id)->first())

            <li class="product-item remove-divider">
                <div class="product-item__outer w-100">
                    <div class="product-item__inner remove-prodcut-hover py-4 row">
                        <div class="product-item__header col-6 col-md-2">
                            <div class="mb-2">
                                <a href="/product/{{$product_id}}" class="d-block text-center"><img class="img-fluid" src="/product_image/{{$image1->image}}" alt="Image Description"  title="{{$related_product_details1->product_name}}"></a>
                            </div>
                        </div>
                        <div class="product-item__body col-6 col-md-7">
                            <div class="pr-lg-10">
                                <div class="mb-2"><a href="../shop/product-categories-7-column-full-width.html" class="font-size-12 text-gray-5">Speakers</a></div>
                                <h5 class="mb-2 product-item__title"><a href="../shop/single-product-fullwidth.html" class="text-blue font-weight-bold">Wireless Audio System Multiroom 360 degree Full base audio</a></h5>
                                <div class="prodcut-price d-md-none">
                                    <div class="text-gray-100">$685,00</div>
                                </div>
                                <ul class="font-size-12 p-0 text-gray-110 mb-4 d-none d-md-block">
                                    <li class="line-clamp-1 mb-1 list-bullet">Brand new and high quality</li>
                                    <li class="line-clamp-1 mb-1 list-bullet">Made of supreme quality, durable EVA crush resistant, anti-shock material.</li>
                                    <li class="line-clamp-1 mb-1 list-bullet">20 MP Electro and 28 megapixel CMOS rear camera</li>
                                </ul>
                                <div class="mb-3 d-none d-md-block">
                                    <a class="d-inline-flex align-items-center small font-size-14" href="#">
                                        <div class="text-warning mr-2">
                                            <span style="font-size: 10px;">
                                                @php($a=(int)$related_product_details1->review)
                                                @for($i=0;$i<$a;$i++)
                                                <span class="" style="    width:  max-contant;"><i class="fa fa-star" style="color:#fa4251"></i></span>
                                                @endfor
                                                @for($i=0;$i<5-$a;$i++)
                                                <span class="" style="    width: max-contant;"><i class="fa fa-star"     style="color: #79797975;"></i></span> 
                                                @endfor
                                                ({{$related_product_details1->review}})
                                            </span>
                                        </div>
                                      
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="product-item__footer col-md-3 d-md-block">
                            <div class="mb-2 flex-center-between">
                                <div class="prodcut-price">
                                    <div class="text-gray-100">  @php($progst=$related_product_details1->selling_price*$sub_cat->gst/100)
                                        <h4> ₹{{round($related_product_details1->selling_price+$progst)}}</h4></div>
                                </div>
                                <div class="prodcut-add-cart">
                                    <span style="font-size: 16px;"><s><span class="currencySymbol">₹</span>{{$related_product_details1->mrp}}</span></s>
                                    @php($dis=(($related_product_details1->mrp-$related_product_details1->selling_price+$progst)/$related_product_details1->mrp)*100)
                                    <span style="font-size: 16px;color: #388e3c;    font-weight: 700;"><span class="currencySymbol">{{round($dis)}}</span>% off</span>
                              
                                </div>
                            </div>
                            <div class="flex-horizontal-center justify-content-between justify-content-wd-center flex-wrap border-top pt-3">
                                <a href="javascript:void(0);" class="text-gray-6 font-size-13" onclick="cart({{$product_id}});"><i class="ec ec-add-to-cart mr-1 font-size-15"></i> Cart</a>
                                <a href="javascript:void(0);" class="text-gray-6 font-size-13" onclick="wishlist({{$product_id}});"><i class="ec ec-favorites mr-1 font-size-15"></i> Wishlist</a>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            @endforeach
          
        </ul>
    </div>
</div>



<center>   {!! $pro->links() !!} </center>




{{-- <div class="row">
                            @foreach($pro as $related_product_details1)
                            @php($product_id=$related_product_details1->product_id)
                            @php($image1=DB::table('product_images')->where('product_id',$product_id)->limit(1)->first())
                            @php($image2=DB::table('product_images')->where('product_id',$product_id)->limit(1)->orderby('product_image_id','desc')->first())
                            @php($brand=DB::table('brands')->where('brand_id',$related_product_details1->brand_id)->first())
                            @php($sub_cat=DB::table('sub_cats')->where('sub_cat_id',$related_product_details1->sub_cat_id)->first())

                                <div class="product-layout  product-grid col-lg-3 col-6 ">
                                    <div class="item">
                                    <div class="product-thumb transition">
                                        <div class="image">
                                            <div class="first_image"> <a href="/product/{{$product_id}}"> <img src="/product_image/{{$image1->image}}" alt="pure-spice-3" title="{{$related_product_details1->product_name}}" class="img-responsive"> </a> </div>
                                            <div class="swap_image"> <a href="/product/{{$product_id}}"> <img src="/product_image/{{$image2->image}}" alt="pure-spice-3" title="{{$related_product_details1->product_name}}" class="img-responsive"> </a></div>
                                        </div>
                                        <div class="product-details">
                                        <div class="caption">
                                            <h4><a href="/shop?brand={{$brand->brand_id}}">{{$brand->brand_name}}</a></h4>
                                            @php($progst=$related_product_details1->selling_price*$sub_cat->gst/100)
                       
                                           <p class="price">₹{{round($related_product_details1->selling_price+$progst)}}</p>
                                            <div class="product_option">
                                            <div class="form-group required ">
                                                <a href="/product/{{$product_id}}"><?php echo substr($related_product_details1->product_name,0,18); ?>@if(strlen($related_product_details1->product_name)>18){{'...'}} @endif</a>
                                  
                                            </div>
                                            <div class="input-group button-group">
                                                <span style="font-size: 10px;">
                                                @php($a=(int)$related_product_details1->review)
                                                @for($i=0;$i<$a;$i++)
                                                <span class="" style="    width:  max-contant;"><i class="fa fa-star" style="color:#fa4251"></i></span>
                                                @endfor
                                                @for($i=0;$i<5-$a;$i++)
                                                <span class="" style="    width: max-contant;"><i class="fa fa-star"     style="color: #79797975;"></i></span> 
                                                @endfor
                                                
                        
                                                ({{$related_product_details1->review}})
                                                </span>
                                                
                                                <button type="button" class="addtocart pull-right addtocartbtn" onclick="cart({{$product_id}});">Add</button>
                                              </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>

                               
                              @endforeach

                            </div>
                          
                                <center>   {!! $pro->links() !!} </center>
                              


 --}}

                     

                            @endif