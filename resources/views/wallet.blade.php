@extends('layouts.menu')

@section('title')
My Wallet : E-Kirana
@endsection

@section('content')

<style>
@media only screen and (max-width: 600px) {
  .mobile_hide {
    display:none;
  }
}
@media only screen and (min-width: 600px) {
  .desktop_hide {
    display:none;
  }
}
</style>


<div class="bg-gray-13 bg-md-transparent">
  <div class="container">
      <!-- breadcrumb -->
      <div class="my-md-3">
          <nav aria-label="breadcrumb">
              <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                  <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="/">Home</a></li>
                  <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">My wallet</li>
              </ol>
          </nav>
      </div>
      <!-- End breadcrumb -->
  </div>
</div>

  <!-- =====  BREADCRUMB END===== -->
  <!-- =====  HEADER END  ===== -->
  <div class="page-cart section">
  <!-- =====  CONTAINER START  ===== -->
    <div class="container">
      <div class="row ">
        <div class="col-12 mt-20 mb-20" style="min-height: 300px;">
            <div class="row" style="padding: 20px">
                <div>
                    <img src="https://d1nhio0ox7pgb.cloudfront.net/_img/g_collection_png/standard/512x512/wallet.png" style="height:100px">
                </div>
                <div style="padding-left:10px">
                    <br>
                   
                    <h1><b>&#8377;{{$wallet_amount->wallet_ammount}}<b></h1>
                    <h3>Wallet Blance</h3>

                </div>
<br>
            </div>
            <table class="table mobile_hide" cellspacing="0" >
                <thead>
                    <th >Date & Time</th>
                    <th  style="text-align: center;">Order ID</th>
                    <th  style="text-align: center;">Remarks</th>
                    <th style="text-align: center;">Transaction Amount</th>
                   
                    <th  style="text-align: center;">After Transaction Amount</th>
                </thead>
                <tbody>
                    @foreach($user_wallet_transactions as $user_wallet_transactions)
                        <tr>
                            <td>{{date('d M, Y h:i A',strtotime($user_wallet_transactions->created_at))}}</td>
                            <td  align="center">{{$user_wallet_transactions->order_id}}</td>
                            <td  align="center">{{$user_wallet_transactions->remarks}}</td>

                            <td align="center">
                                @if($user_wallet_transactions->tranasaction_type=='Debit')
                                    <b style="color:red;font-size:20px">- &#8377;{{$user_wallet_transactions->transaction_amount}}</b>
                                @else
                                    <b style="color:green;font-size:20px">+ &#8377;{{$user_wallet_transactions->transaction_amount}}</b>
                                @endif
                        </td>
                          
                            <td  align="center" style="color:black"><b>&#8377;{{$user_wallet_transactions->after_transaction_amount}}.00<b></td>
                        </tr>
                    @endforeach
                </tbody>

            </table>


            <table class="table desktop_hide" cellspacing="0">
               
                <tbody>
                    @foreach($user_wallet_transactions1 as $user_wallet_transactions1)
                        <tr>
                            <td>
                                @if($user_wallet_transactions1->tranasaction_type=='Debit')
                               Paid for {{$user_wallet_transactions1->remarks}}<br>
                                @else
                              Received for  {{$user_wallet_transactions1->remarks}}<br>
                                @endif
                                
                               
                               <span style="    font-size: 12px;"> {{date('d M, Y h:i A',strtotime($user_wallet_transactions1->created_at))}}</span></td>
                        
                            <td align="right">
                                @if($user_wallet_transactions1->tranasaction_type=='Debit')
                                    <b style="color:red;font-size:20px">- &#8377;{{$user_wallet_transactions1->transaction_amount}}</b>
                                @else
                                    <b style="color:green;font-size:20px">+ &#8377;{{$user_wallet_transactions1->transaction_amount}}</b>
                                @endif
                                <br>
                                <span style="    font-size: 12px;">Closing Blance : &#8377;{{$user_wallet_transactions1->after_transaction_amount}}</span>
                        </td>
                          
                            
                        </tr>
                    @endforeach
                </tbody>

            </table>

        </div>
      </div>
    <!-- =====  Brand start ===== -->
  <hr>
    <!-- =====  Brand end ===== -->
    </div>
  <!-- =====  CONTAINER END  ===== -->
  </div>

@endsection