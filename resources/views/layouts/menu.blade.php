<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Title -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title')</title>
      

        <!-- Required Meta Tags Always Come First -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Favicon -->
        <link rel="shortcut icon" href={{ asset('favicon.png')}}>

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&display=swap" rel="stylesheet">

        <!-- CSS Implementing Plugins -->
        <link rel="stylesheet" href={{ asset('assets/vendor/font-awesome/css/fontawesome-all.min.css')}}>
        <link rel="stylesheet" href={{ asset('assets/css/font-electro.css')}}>
        
        <link rel="stylesheet" href={{ asset('assets/vendor/animate.css/animate.min.css')}}>
        <link rel="stylesheet" href={{ asset('assets/vendor/hs-megamenu/src/hs.megamenu.css')}}>
        <link rel="stylesheet" href={{ asset('assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css')}}>
        <link rel="stylesheet" href={{ asset('assets/vendor/fancybox/jquery.fancybox.css')}}>
        <link rel="stylesheet" href={{ asset('assets/vendor/slick-carousel/slick/slick.css')}}>
        <link rel="stylesheet" href={{ asset('assets/vendor/bootstrap-select/dist/css/bootstrap-select.min.css')}}>


        {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
       
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
            <!-- CSS Electro Template --> --}}
        <link rel="stylesheet" href={{ asset('assets/css/theme.css')}}>
        <link rel="stylesheet" href={{ asset('Material-Toast-master\mdtoast.min.css')}} />
        <script async src={{ asset('assets/price_slider/price.js')}}></script>
       <link rel="stylesheet" href={{ asset('assets/price_slider/price.css')}}>
        <style>
            @media only screen and (max-width: 600px) {
                #wis{display:none !important}
            }

            .main_cat:hover{
              height: auto !important;
              visibility: visible !important;
              opacity: 1 !important;
              overflow: visible !important;
            }
            
            #overlay{	
                position: fixed;
                top: 0;
                width: 100%;
                z-index: 100001;
                background: white;
                border-bottom: solid 2px #fed700;
            }
           
               
            .u-scrolled > .sticky{
                position: fixed;
                top: 0;
                width: 100%;
                z-index: 100001;
                background: white;
                border-bottom: solid 2px #fed700;
            }
           
        </style>
        <script>
            $(window).scroll(function(){
                // alert();
                $('#headerMenu').addClass("sticky");
            })

        </script>
    <style>
        .main_cat:hover{
            height: auto !important;
            visibility: visible !important;
            opacity: 1 !important;
            overflow: visible !important;
        }
        
        #overlay{	
            position: fixed;
            top: 0;
            z-index: 100;
            width: 100%;
            height:100%;
            display: none;
            background: rgba(0,0,0,0.6);
        }
        .cv-spinner {
            height: 100%;
            display: flex;
            justify-content: center;
            align-items: center;  
        }
        .spinner {
            width: 80px;
            height: 80px;
            border: 4px #ddd solid;
            border-top: 4px #2e93e6 solid;
            border-radius: 50%;
            animation: sp-anime 0.8s infinite linear;
        
        }
        @keyframes sp-anime {
            100% { 
                transform: rotate(360deg); 
            }
        }
        #text{
            position: absolute;
            top: 35%;
            left: 50%;
            font-size: 30px;
            color: white;
            transform: translate(-50%,-50%);
            -ms-transform: translate(-50%,-50%);	
            
        }
        #text1{
            position: absolute;
            top: 96%;
            left: 50%;
            font-size: 17px;
            color: #ffffffb8;
            
            transform: translate(-50%,-50%);
            -ms-transform: translate(-50%,-50%);
        }
        .is-hide{
            display:none;
        }
        .dropdown-item {
            
            padding: 0.25rem 0;
            
        }
        
            @media only screen and (max-width: 600px) {
            .aa
            {
            display:none;
            }
            .bb
            {
            display:block;
            }
            .pb-15 {
            padding: 11px 0px 10px 0px;
            
            margin-top: 10px;
        
        }
        #top .responsive-bar .fa-bars::before {
            content: "";
            background: url(/use_assets/images/sprite.png) no-repeat -1px -65px #333 !important;
            top: 25px;
            left: 0px;
            width: 43px;
            height: 41px !important;
            border-radius: 5px;
            position: absolute;
            z-index: 9;
        }
        #responsive-menu{
            padding-top: 30px !important;
        }
        .apps-hide{
            display:none;
        }
        #brand_carouse{
                padding-bottom: 80px;
        }
        .header-static-block{
                border-top: solid 1px #bebebe;
        }
        .header{
            border-bottom: solid 1px #bebebe;
        }
        .breadcrumb {
            display:none;
        }
        
            }
            @media only screen and (min-width: 600px) {
            .aa
            {
            display:block;
            }
            .bb
            {
            display:none;
            }
        
        }
        
        @media (max-width: 767px){
            .icon-block .search_icon .fa:before {
            background: none !important;
            content: "\f004";
            font-size: x-large;
        }
        .icon-block .login_icon .fa:before {
            background: url('https://static.thenounproject.com/png/52864-200.png') !important;
            content: "";
            font-size: x-large;
        }
        .icon-block .telephone_icon .fa:before {
            background: none !important;
            content: "\f022";
            font-size: x-large;
        }
        
        }
        
    </style>
    </head>

    <body>
        @guest
        <input type="hidden" id="login_data" value="0">
        
        @else
        
        <input type="hidden" id="login_data" value="{{Auth::user()->id}}">
        @endguest
        <div id="overlay" style="    z-index: 1002;">
            <div id="text" style="display:none">Do not press back button or refresh the page.</div>
              <div class="cv-spinner">
                <span class="spinner"></span>
              </div>
                <div id="text1" style=""><center><font style="font-size:14px">Design and develop by</font><br><span style="line-height: 0.8;">Quantex Consulting Services Pvt. Ltd.</span><center></div>
            </div>
        <!-- ========== HEADER ========== -->
        <header id="header" class="u-header u-header-left-aligned-nav">
            <div class="u-header__section">
                <!-- Topbar -->
                <div class="u-header-topbar py-2 d-none d-xl-block">
                    <div class="container">
                        <div class="d-flex align-items-center">
                            <div class="topbar-left">
                                <a href="#" class="text-gray-110 font-size-13 u-header-topbar__nav-link">Welcome to Aliya Online Services</a>
                            </div>
                            <div class="topbar-right ml-auto">
                                <ul class="list-inline mb-0">
                                    <li class="list-inline-item mr-0 u-header-topbar__nav-item u-header-topbar__nav-item-border ">
                                    </li>
                                   @guest
                                    <li class="list-inline-item mr-0 u-header-topbar__nav-item u-header-topbar__nav-item-border ">
                                        <!-- Account Sidebar Toggle Button -->
                                        <a  href="/login" style="color:black">
                                            <i class="ec ec-user mr-1"></i> <b>Login </b><span class="text-gray-50 ">                       </a>
                                        <!-- End Account Sidebar Toggle Button -->
                                    </li>

                                    @else
                                    <li class="list-inline-item mr-0 u-header-topbar__nav-item u-header-topbar__nav-item-border u-header-topbar__nav-item-no-border u-header-topbar__nav-item-border-single">
                                        <div class="d-flex align-items-center">
                                            <!-- Language -->
                                            <div class="position-relative">
                                                <a id="languageDropdownInvoker" class="dropdown-nav-link dropdown-toggle d-flex align-items-center u-header-topbar__nav-link font-weight-normal" href="javascript:;" role="button"
                                                    aria-controls="languageDropdown"
                                                    aria-haspopup="true"
                                                    aria-expanded="false"
                                                    data-unfold-event="hover"
                                                    data-unfold-target="#languageDropdown"
                                                    data-unfold-type="css-animation"
                                                    data-unfold-duration="300"
                                                    data-unfold-delay="300"
                                                    data-unfold-hide-on-scroll="true"
                                                    data-unfold-animation-in="slideInUp"
                                                    data-unfold-animation-out="fadeOut">
                                                    <span class="d-inline-block d-sm-none">US</span>
                                                    <span class="d-none d-sm-inline-flex align-items-center"><i class="fas fa-user"></i>&nbsp; @if(Auth::user()->name=='')    {{Auth::user()->mobile}}  @else   {{Auth::user()->name}}   @endif</span>
                                                </a>

                                                <div id="languageDropdown" class="dropdown-menu dropdown-unfold" aria-labelledby="languageDropdownInvoker">
                                                    <a class="dropdown-item active" href="/complete_orderprofile" style="padding-left: 6px;"> <i class="fas fa-user"></i> My Profile</a>
                                                    <a class="dropdown-item" href="/order" style="padding-left: 6px;"> <i class="fab fa-first-order"></i> Orders</a>
                                                    <a class="dropdown-item" href="/cart" style="padding-left: 5px;"> <i class="fas fa-shopping-cart"></i> Cart</a>
                                                    <a class="dropdown-item" href="/wishlist" style="padding-left: 6px;"> <i class="fas fa-heart"></i> Wishlist</a>
                                                    <a class="dropdown-item" href="/wallet" style="padding-left: 6px;"> <i class="fas fa-wallet"></i> Wallet</a>
                                                    <a class="dropdown-item" href="/offer" style="padding-left: 6px;"> <i class="fas fa-tags"></i> Coupon</a>
                                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                                        onclick="event.preventDefault();
                                                                        document.getElementById('logout-form').submit();" style="padding-left: 6px;">
                                                        <i class="fas fa-sign-out-alt"></i> {{ __('Logout') }}
                                                          </a>
                                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                          @csrf
                                                       </form>
                                                </div>
                                            </div>
                                            <!-- End Language -->
                                        </div>
                                    </li>
                                    @endguest
                                    <li class="list-inline-item mr-0 u-header-topbar__nav-item u-header-topbar__nav-item-border u-header-topbar__nav-item-no-border u-header-topbar__nav-item-border-single">
                                        <div class="d-flex align-items-center">
                                            <!-- Language -->
                                            <div class="position-relative">
                                                <a id="languageDropdownInvoker1" class="dropdown-nav-link dropdown-toggle d-flex align-items-center u-header-topbar__nav-link font-weight-normal" href="javascript:;" role="button"
                                                    aria-controls="languageDropdown1"
                                                    aria-haspopup="true"
                                                    aria-expanded="false"
                                                    data-unfold-event="hover"
                                                    data-unfold-target="#languageDropdown1"
                                                    data-unfold-type="css-animation"
                                                    data-unfold-duration="300"
                                                    data-unfold-delay="300"
                                                    data-unfold-hide-on-scroll="true"
                                                    data-unfold-animation-in="slideInUp"
                                                    data-unfold-animation-out="fadeOut">
                                                    <span class="d-inline-block d-sm-none">US</span>
                                                    <span class="d-none d-sm-inline-flex align-items-center">More</span>
                                                </a>

                                                <div id="languageDropdown1" class="dropdown-menu dropdown-unfold" aria-labelledby="languageDropdownInvoker1" >
                                                    <a class="dropdown-item active" href="#" style="margin-left: 6px;">About Us</a>
                                                    <a class="dropdown-item active" href="#" style="margin-left: 6px;">Contact Us  </a>
                                                    <a class="dropdown-item active" href="#" style="margin-left: 6px;">Sell on ALIYA</a>
                                                    <a class="dropdown-item" href="#" style="margin-left: 6px;">Download App</a>
                                                    
                                                </div>
                                            </div>
                                            <!-- End Language -->
                                        </div>
                                    </li>

                                    <li class="list-inline-item mr-0 u-header-topbar__nav-item u-header-topbar__nav-item-border ">
                                     
                                     </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Topbar -->

                <!-- Logo-Search-header-icons -->
                <div class="py-2 py-xl-5 bg-primary-down-lg" id="headerMenu">
                    <div class="container my-0dot5 my-xl-0">
                        <div class="row align-items-center">
                            <!-- Logo-offcanvas-menu -->
                            <div class="col-auto">
                                <!-- Nav -->
                                <nav class="navbar navbar-expand u-header__navbar py-0 justify-content-xl-between max-width-270 min-width-270">
                                    <!-- Logo -->
                                    <a class="order-1 order-xl-0 navbar-brand u-header__navbar-brand u-header__navbar-brand-center" href="/" aria-label="Electro">
                                        <svg version="1.1" x="0px" y="0px" width="175.748px" height="42.52px" viewBox="0 0 175.748 42.52" enable-background="new 0 0 175.748 42.52" style="margin-bottom: 0;">
                                            <ellipse class="ellipse-bg" fill-rule="evenodd" clip-rule="evenodd" fill="#FDD700" cx="170.05" cy="36.341" rx="5.32" ry="5.367"></ellipse>
                                            <path fill-rule="evenodd" clip-rule="evenodd" fill="#333E48" d="M30.514,0.71c-0.034,0.003-0.066,0.008-0.056,0.056
                                                C30.263,0.995,29.876,1.181,29.79,1.5c-0.148,0.548,0,1.568,0,2.427v36.459c0.265,0.221,0.506,0.465,0.725,0.734h6.187
                                                c0.2-0.25,0.423-0.477,0.669-0.678V1.387C37.124,1.185,36.9,0.959,36.701,0.71H30.514z M117.517,12.731
                                                c-0.232-0.189-0.439-0.64-0.781-0.734c-0.754-0.209-2.039,0-3.121,0h-3.176V4.435c-0.232-0.189-0.439-0.639-0.781-0.733
                                                c-0.719-0.2-1.969,0-3.01,0h-3.01c-0.238,0.273-0.625,0.431-0.725,0.847c-0.203,0.852,0,2.399,0,3.725
                                                c0,1.393,0.045,2.748-0.055,3.725h-6.41c-0.184,0.237-0.629,0.434-0.725,0.791c-0.178,0.654,0,1.813,0,2.765v2.766
                                                c0.232,0.188,0.439,0.64,0.779,0.733c0.777,0.216,2.109,0,3.234,0c1.154,0,2.291-0.045,3.176,0.057v21.277
                                                c0.232,0.189,0.439,0.639,0.781,0.734c0.719,0.199,1.969,0,3.01,0h3.01c1.008-0.451,0.725-1.889,0.725-3.443
                                                c-0.002-6.164-0.047-12.867,0.055-18.625h6.299c0.182-0.236,0.627-0.434,0.725-0.79c0.176-0.653,0-1.813,0-2.765V12.731z
                                                M135.851,18.262c0.201-0.746,0-2.029,0-3.104v-3.104c-0.287-0.245-0.434-0.637-0.781-0.733c-0.824-0.229-1.992-0.044-2.898,0
                                                c-2.158,0.104-4.506,0.675-5.74,1.411c-0.146-0.362-0.451-0.853-0.893-0.96c-0.693-0.169-1.859,0-2.842,0h-2.842
                                                c-0.258,0.319-0.625,0.42-0.725,0.79c-0.223,0.82,0,2.338,0,3.443c0,8.109-0.002,16.635,0,24.381
                                                c0.232,0.189,0.439,0.639,0.779,0.734c0.707,0.195,1.93,0,2.955,0h3.01c0.918-0.463,0.725-1.352,0.725-2.822V36.21
                                                c-0.002-3.902-0.242-9.117,0-12.473c0.297-4.142,3.836-4.877,8.527-4.686C135.312,18.816,135.757,18.606,135.851,18.262z
                                                M14.796,11.376c-5.472,0.262-9.443,3.178-11.76,7.056c-2.435,4.075-2.789,10.62-0.501,15.126c2.043,4.023,5.91,7.115,10.701,7.9
                                                c6.051,0.992,10.992-1.219,14.324-3.838c-0.687-1.1-1.419-2.664-2.118-3.951c-0.398-0.734-0.652-1.486-1.616-1.467
                                                c-1.942,0.787-4.272,2.262-7.134,2.145c-3.791-0.154-6.659-1.842-7.524-4.91h19.452c0.146-2.793,0.22-5.338-0.279-7.563
                                                C26.961,15.728,22.503,11.008,14.796,11.376z M9,23.284c0.921-2.508,3.033-4.514,6.298-4.627c3.083-0.107,4.994,1.976,5.685,4.627
                                                C17.119,23.38,12.865,23.38,9,23.284z M52.418,11.376c-5.551,0.266-9.395,3.142-11.76,7.056
                                                c-2.476,4.097-2.829,10.493-0.557,15.069c1.997,4.021,5.895,7.156,10.646,7.957c6.068,1.023,11-1.227,14.379-3.781
                                                c-0.479-0.896-0.875-1.742-1.393-2.709c-0.312-0.582-1.024-2.234-1.561-2.539c-0.912-0.52-1.428,0.135-2.23,0.508
                                                c-0.564,0.262-1.223,0.523-1.672,0.676c-4.768,1.621-10.372,0.268-11.537-4.176h19.451c0.668-5.443-0.419-9.953-2.73-13.037
                                                C61.197,13.388,57.774,11.12,52.418,11.376z M46.622,23.343c0.708-2.553,3.161-4.578,6.242-4.686
                                                c3.08-0.107,5.08,1.953,5.686,4.686H46.622z M160.371,15.497c-2.455-2.453-6.143-4.291-10.869-4.064
                                                c-2.268,0.109-4.297,0.65-6.02,1.524c-1.719,0.873-3.092,1.957-4.234,3.217c-2.287,2.519-4.164,6.004-3.902,11.007
                                                c0.248,4.736,1.979,7.813,4.627,10.326c2.568,2.439,6.148,4.254,10.867,4.064c4.457-0.18,7.889-2.115,10.199-4.684
                                                c2.469-2.746,4.012-5.971,3.959-11.063C164.949,21.134,162.732,17.854,160.371,15.497z M149.558,33.952
                                                c-3.246-0.221-5.701-2.615-6.41-5.418c-0.174-0.689-0.26-1.25-0.4-2.166c-0.035-0.234,0.072-0.523-0.045-0.77
                                                c0.682-3.698,2.912-6.257,6.799-6.547c2.543-0.189,4.258,0.735,5.52,1.863c1.322,1.182,2.303,2.715,2.451,4.967
                                                C157.789,30.669,154.185,34.267,149.558,33.952z M88.812,29.55c-1.232,2.363-2.9,4.307-6.13,4.402
                                                c-4.729,0.141-8.038-3.16-8.025-7.563c0.004-1.412,0.324-2.65,0.947-3.726c1.197-2.061,3.507-3.688,6.633-3.612
                                                c3.222,0.079,4.966,1.708,6.632,3.668c1.328-1.059,2.529-1.948,3.9-2.99c0.416-0.315,1.076-0.688,1.227-1.072
                                                c0.404-1.031-0.365-1.502-0.891-2.088c-2.543-2.835-6.66-5.377-11.704-5.137c-6.02,0.288-10.218,3.697-12.484,7.846
                                                c-1.293,2.365-1.951,5.158-1.729,8.408c0.209,3.053,1.191,5.496,2.619,7.508c2.842,4.004,7.385,6.973,13.656,6.377
                                                c5.976-0.568,9.574-3.936,11.816-8.354c-0.141-0.271-0.221-0.604-0.336-0.902C92.929,31.364,90.843,30.485,88.812,29.55z">
                                            </path>
                                        </svg>
                                    </a>
                                    <!-- End Logo -->

                                    <!-- Fullscreen Toggle Button -->
                                    <button id="sidebarHeaderInvokerMenu" type="button" class="navbar-toggler d-block btn u-hamburger mr-3 mr-xl-0"
                                        aria-controls="sidebarHeader"
                                        aria-haspopup="true"
                                        aria-expanded="false"
                                        data-unfold-event="click"
                                        data-unfold-hide-on-scroll="false"
                                        data-unfold-target="#sidebarHeader1"
                                        data-unfold-type="css-animation"
                                        data-unfold-animation-in="fadeInLeft"
                                        data-unfold-animation-out="fadeOutLeft"
                                        data-unfold-duration="500">
                                        <span id="hamburgerTriggerMenu" class="u-hamburger__box">
                                            <span class="u-hamburger__inner"></span>
                                        </span>
                                    </button>
                                    <!-- End Fullscreen Toggle Button -->
                                </nav>
                                <!-- End Nav -->

                                <!-- ========== HEADER SIDEBAR ========== -->
                                <aside id="sidebarHeader1" class="u-sidebar u-sidebar--left" aria-labelledby="sidebarHeaderInvokerMenu">
                                    <div class="u-sidebar__scroller">
                                        <div class="u-sidebar__container">
                                            <div class="u-header-sidebar__footer-offset pb-0">
                                                <!-- Toggle Button -->
                                                <div class="position-absolute top-0 right-0 z-index-2 pt-4 pr-7">
                                                    <button type="button" class="close ml-auto"
                                                        aria-controls="sidebarHeader"
                                                        aria-haspopup="true"
                                                        aria-expanded="false"
                                                        data-unfold-event="click"
                                                        data-unfold-hide-on-scroll="false"
                                                        data-unfold-target="#sidebarHeader1"
                                                        data-unfold-type="css-animation"
                                                        data-unfold-animation-in="fadeInLeft"
                                                        data-unfold-animation-out="fadeOutLeft"
                                                        data-unfold-duration="500">
                                                        <span aria-hidden="true"><i class="ec ec-close-remove text-gray-90 font-size-20"></i></span>
                                                    </button>
                                                </div>
                                                <!-- End Toggle Button -->

                                                <!-- Content -->
                                                <div class="js-scrollbar u-sidebar__body">
                                                    <div id="headerSidebarContent" class="u-sidebar__content u-header-sidebar__content">
                                                        <!-- Logo -->
                                                        <a class="d-flex ml-0 navbar-brand u-header__navbar-brand u-header__navbar-brand-vertical" href="/" aria-label="Electro">
                                                            <svg version="1.1" x="0px" y="0px" width="175.748px" height="42.52px" viewBox="0 0 175.748 42.52" enable-background="new 0 0 175.748 42.52" style="margin-bottom: 0;">
                                                                <ellipse class="ellipse-bg" fill-rule="evenodd" clip-rule="evenodd" fill="#FDD700" cx="170.05" cy="36.341" rx="5.32" ry="5.367"></ellipse>
                                                                <path fill-rule="evenodd" clip-rule="evenodd" fill="#333E48" d="M30.514,0.71c-0.034,0.003-0.066,0.008-0.056,0.056
                                                                    C30.263,0.995,29.876,1.181,29.79,1.5c-0.148,0.548,0,1.568,0,2.427v36.459c0.265,0.221,0.506,0.465,0.725,0.734h6.187
                                                                    c0.2-0.25,0.423-0.477,0.669-0.678V1.387C37.124,1.185,36.9,0.959,36.701,0.71H30.514z M117.517,12.731
                                                                    c-0.232-0.189-0.439-0.64-0.781-0.734c-0.754-0.209-2.039,0-3.121,0h-3.176V4.435c-0.232-0.189-0.439-0.639-0.781-0.733
                                                                    c-0.719-0.2-1.969,0-3.01,0h-3.01c-0.238,0.273-0.625,0.431-0.725,0.847c-0.203,0.852,0,2.399,0,3.725
                                                                    c0,1.393,0.045,2.748-0.055,3.725h-6.41c-0.184,0.237-0.629,0.434-0.725,0.791c-0.178,0.654,0,1.813,0,2.765v2.766
                                                                    c0.232,0.188,0.439,0.64,0.779,0.733c0.777,0.216,2.109,0,3.234,0c1.154,0,2.291-0.045,3.176,0.057v21.277
                                                                    c0.232,0.189,0.439,0.639,0.781,0.734c0.719,0.199,1.969,0,3.01,0h3.01c1.008-0.451,0.725-1.889,0.725-3.443
                                                                    c-0.002-6.164-0.047-12.867,0.055-18.625h6.299c0.182-0.236,0.627-0.434,0.725-0.79c0.176-0.653,0-1.813,0-2.765V12.731z
                                                                    M135.851,18.262c0.201-0.746,0-2.029,0-3.104v-3.104c-0.287-0.245-0.434-0.637-0.781-0.733c-0.824-0.229-1.992-0.044-2.898,0
                                                                    c-2.158,0.104-4.506,0.675-5.74,1.411c-0.146-0.362-0.451-0.853-0.893-0.96c-0.693-0.169-1.859,0-2.842,0h-2.842
                                                                    c-0.258,0.319-0.625,0.42-0.725,0.79c-0.223,0.82,0,2.338,0,3.443c0,8.109-0.002,16.635,0,24.381
                                                                    c0.232,0.189,0.439,0.639,0.779,0.734c0.707,0.195,1.93,0,2.955,0h3.01c0.918-0.463,0.725-1.352,0.725-2.822V36.21
                                                                    c-0.002-3.902-0.242-9.117,0-12.473c0.297-4.142,3.836-4.877,8.527-4.686C135.312,18.816,135.757,18.606,135.851,18.262z
                                                                    M14.796,11.376c-5.472,0.262-9.443,3.178-11.76,7.056c-2.435,4.075-2.789,10.62-0.501,15.126c2.043,4.023,5.91,7.115,10.701,7.9
                                                                    c6.051,0.992,10.992-1.219,14.324-3.838c-0.687-1.1-1.419-2.664-2.118-3.951c-0.398-0.734-0.652-1.486-1.616-1.467
                                                                    c-1.942,0.787-4.272,2.262-7.134,2.145c-3.791-0.154-6.659-1.842-7.524-4.91h19.452c0.146-2.793,0.22-5.338-0.279-7.563
                                                                    C26.961,15.728,22.503,11.008,14.796,11.376z M9,23.284c0.921-2.508,3.033-4.514,6.298-4.627c3.083-0.107,4.994,1.976,5.685,4.627
                                                                    C17.119,23.38,12.865,23.38,9,23.284z M52.418,11.376c-5.551,0.266-9.395,3.142-11.76,7.056
                                                                    c-2.476,4.097-2.829,10.493-0.557,15.069c1.997,4.021,5.895,7.156,10.646,7.957c6.068,1.023,11-1.227,14.379-3.781
                                                                    c-0.479-0.896-0.875-1.742-1.393-2.709c-0.312-0.582-1.024-2.234-1.561-2.539c-0.912-0.52-1.428,0.135-2.23,0.508
                                                                    c-0.564,0.262-1.223,0.523-1.672,0.676c-4.768,1.621-10.372,0.268-11.537-4.176h19.451c0.668-5.443-0.419-9.953-2.73-13.037
                                                                    C61.197,13.388,57.774,11.12,52.418,11.376z M46.622,23.343c0.708-2.553,3.161-4.578,6.242-4.686
                                                                    c3.08-0.107,5.08,1.953,5.686,4.686H46.622z M160.371,15.497c-2.455-2.453-6.143-4.291-10.869-4.064
                                                                    c-2.268,0.109-4.297,0.65-6.02,1.524c-1.719,0.873-3.092,1.957-4.234,3.217c-2.287,2.519-4.164,6.004-3.902,11.007
                                                                    c0.248,4.736,1.979,7.813,4.627,10.326c2.568,2.439,6.148,4.254,10.867,4.064c4.457-0.18,7.889-2.115,10.199-4.684
                                                                    c2.469-2.746,4.012-5.971,3.959-11.063C164.949,21.134,162.732,17.854,160.371,15.497z M149.558,33.952
                                                                    c-3.246-0.221-5.701-2.615-6.41-5.418c-0.174-0.689-0.26-1.25-0.4-2.166c-0.035-0.234,0.072-0.523-0.045-0.77
                                                                    c0.682-3.698,2.912-6.257,6.799-6.547c2.543-0.189,4.258,0.735,5.52,1.863c1.322,1.182,2.303,2.715,2.451,4.967
                                                                    C157.789,30.669,154.185,34.267,149.558,33.952z M88.812,29.55c-1.232,2.363-2.9,4.307-6.13,4.402
                                                                    c-4.729,0.141-8.038-3.16-8.025-7.563c0.004-1.412,0.324-2.65,0.947-3.726c1.197-2.061,3.507-3.688,6.633-3.612
                                                                    c3.222,0.079,4.966,1.708,6.632,3.668c1.328-1.059,2.529-1.948,3.9-2.99c0.416-0.315,1.076-0.688,1.227-1.072
                                                                    c0.404-1.031-0.365-1.502-0.891-2.088c-2.543-2.835-6.66-5.377-11.704-5.137c-6.02,0.288-10.218,3.697-12.484,7.846
                                                                    c-1.293,2.365-1.951,5.158-1.729,8.408c0.209,3.053,1.191,5.496,2.619,7.508c2.842,4.004,7.385,6.973,13.656,6.377
                                                                    c5.976-0.568,9.574-3.936,11.816-8.354c-0.141-0.271-0.221-0.604-0.336-0.902C92.929,31.364,90.843,30.485,88.812,29.55z">
                                                                </path>
                                                            </svg>
                                                        </a>
                                                        <!-- End Logo -->

                                                        <!-- List -->
                                                        <ul id="headerSidebarList" class="u-header-collapse__nav">
                                                            <!-- Home Section -->
                                                            @php($category1=DB::table('cats')->get())
                                                            @php($sub_category1=DB::table('sub_cats')->get())
                                                            @php($sub_sub_category1=DB::table('sub_sub_cats')->get())

                                                            @foreach($category1 as $key => $category)
                                                            <li class="u-has-submenu u-header-collapse__submenu">
                                                               

                                                            <a class="u-header-collapse__nav-link u-header-collapse__nav-pointer" href="/shop?cat_id={{$category->cat_id}}&name={{$category->cat_name}}" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="headerSidebarHomeCollapse-{{$key}}" data-target="#headerSidebarHomeCollapse-{{$key}}">
                                                                    {{$category->cat_name}}
                                                                </a>
                                                                @foreach($sub_category1 as $sub_category)
                                                                    @if($sub_category->cat_id==$category->cat_id)
                                                                        <div id="headerSidebarHomeCollapse-{{$key}}" class="collapse" data-parent="#headerSidebarContent">
                                                                            <ul id="headerSidebarHomeMenu" class="u-header-collapse__nav-list">
                                                                                <li><a class="u-header-collapse__submenu-nav-link" href="/shop?cat_id={{$category->cat_id}}&sub_cat_id={{$sub_category->sub_cat_id}}&name={{$sub_category->sub_cat_name}}"><b>{{$sub_category->sub_cat_name}}</b></a></li>
                                                                                    
                                                                                @foreach($sub_sub_category1 as $sub_sub_category)
                                                                                        @if($sub_sub_category->sub_cat_id==$sub_category->sub_cat_id)
                                                                                            <li><a class="u-header-collapse__submenu-nav-link" href="/shop?cat_id={{$category->cat_id}}&sub_cat_id={{$sub_sub_category->sub_cat_id}}&sub_sub_cat_id={{$sub_sub_category->sub_sub_cat_id}}&name={{$sub_sub_category->sub_sub_cat_name}}">{{$sub_sub_category->sub_sub_cat_name}}</a></li>
                                                                                        @endif
                                                                                    @endforeach
                                                                            </ul>
                                                                        </div>
                                                                    @endif
                                                                @endforeach


                                                            </li>
                                                            @endforeach
                                                            <!-- End Home Section -->

                                                        </ul>
                                                        <!-- End List -->
                                                    </div>
                                                </div>
                                                <!-- End Content -->
                                            </div>
                                        </div>
                                    </div>
                                </aside>
                                <!-- ========== END HEADER SIDEBAR ========== -->
                            </div>
                            <!-- End Logo-offcanvas-menu -->
                            <!-- Search Bar -->
                            <div class="col d-none d-xl-block">
                                <form class="js-focus-state">
                                    <label class="sr-only" for="searchproduct">Search</label>
                                    <div class="input-group">
                                        <input type="email" class="form-control py-2 pl-5 font-size-15 border-right-0 height-40 border-width-2 rounded-left-pill border-primary" name="email" id="searchproduct-item" placeholder="Search for Products" aria-label="Search for Products" aria-describedby="searchProduct1" required>
                                        <div class="input-group-append">
                                            <!-- Select -->
                                            <select class="js-select selectpicker dropdown-select custom-search-categories-select"
                                                data-style="btn height-40 text-gray-60 font-weight-normal border-top border-bottom border-left-0 rounded-0 border-primary border-width-2 pl-0 pr-5 py-2">
                                                <option value="one" selected>All Categories</option>
                                                <option value="two">Two</option>
                                                <option value="three">Three</option>
                                                <option value="four">Four</option>
                                            </select>
                                            <!-- End Select -->
                                            <button class="btn btn-primary height-40 py-2 px-3 rounded-right-pill" type="button" id="searchProduct1">
                                                <span class="ec ec-search font-size-24"></span>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- End Search Bar -->
                            <!-- Header Icons -->
                            <div class="col col-xl-auto text-right text-xl-left pl-0 pl-xl-3 position-static">
                                <div class="d-inline-flex">
                                    <ul class="d-flex list-unstyled mb-0 align-items-center">
                                        <!-- Search -->
                                        <li class="col d-xl-none px-2 px-sm-3 position-static">
                                            <a id="searchClassicInvoker" class="font-size-22 text-gray-90 text-lh-1 btn-text-secondary" href="javascript:;" role="button"
                                                data-toggle="tooltip"
                                                data-placement="top"
                                                title="Search"
                                                aria-controls="searchClassic"
                                                aria-haspopup="true"
                                                aria-expanded="false"
                                                data-unfold-target="#searchClassic"
                                                data-unfold-type="css-animation"
                                                data-unfold-duration="300"
                                                data-unfold-delay="300"
                                                data-unfold-hide-on-scroll="true"
                                                data-unfold-animation-in="slideInUp"
                                                data-unfold-animation-out="fadeOut">
                                                <span class="ec ec-search"></span>
                                            </a>

                                            <!-- Input -->
                                            <div id="searchClassic" class="dropdown-menu dropdown-unfold dropdown-menu-right left-0 mx-2" aria-labelledby="searchClassicInvoker">
                                                <form class="js-focus-state input-group px-3">
                                                    <input class="form-control" type="search" placeholder="Search Product">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-primary px-3" type="button"><i class="font-size-18 ec ec-search"></i></button>
                                                    </div>
                                                </form>
                                            </div>
                                            <!-- End Input -->
                                        </li>
                                        <!-- End Search -->
                                        {{-- <li class="col d-none d-xl-block"><a href="../shop/compare.html" class="text-gray-90" data-toggle="tooltip" data-placement="top" title="Compare"><i class="font-size-22 ec ec-compare"></i></a></li> --}}
                                    
                                        <li class="col pr-xl-0 px-2 px-sm-3">
                                            <a href="/wishlist" class="text-gray-90 position-relative d-flex " data-toggle="tooltip" data-placement="top" title="Wishlist" id="wis">
                                                <i class="font-size-22 ec ec-favorites"></i>
                                                <span class="bg-lg-down-black width-22 height-22 bg-primary position-absolute d-flex align-items-center justify-content-center rounded-circle left-12 top-8 font-weight-bold font-size-12"><div id="wish_value">0</div></span>
                                            </a>
                                        </li>
                                        <li class="col d-none d-xl-block"></li>

                                        <li class="col pr-xl-0 px-2 px-sm-3">
                                            <a href="/cart" class="text-gray-90 position-relative d-flex " data-toggle="tooltip" data-placement="top" title="Cart">
                                                <i class="font-size-22 ec ec-shopping-bag"></i>
                                                <span class="bg-lg-down-black width-22 height-22 bg-primary position-absolute d-flex align-items-center justify-content-center rounded-circle left-12 top-8 font-weight-bold font-size-12"><div id="cart_qty">0</div></span>
                                                
                                                {{-- <span class="d-none d-xl-block font-weight-bold font-size-16 text-gray-90 ml-3">$1785.00</span> --}}
                                            </a>
                                        </li>
                                        <li class="col d-none d-xl-block"></li>
                                     
                                        <li>
                                            <button id="sidebarHeaderInvokerMenu" type="button" class=" col d-xl-none px-2 px-sm-3 navbar-toggler d-block btn u-hamburger mr-3 mr-xl-0"
                                            aria-controls="sidebarHeader"
                                            aria-haspopup="true"
                                            aria-expanded="false"
                                            data-unfold-event="click"
                                            data-unfold-hide-on-scroll="false"
                                            data-unfold-target="#sidebarHeader2"
                                            data-unfold-type="css-animation"
                                            data-unfold-animation-in="fadeInRight"
                                            data-unfold-animation-out="fadeOutRight"
                                            data-unfold-duration="500">
                                            {{-- <span id="hamburgerTriggerMenu" class="u-hamburger__box">
                                                <span class="u-hamburger__inner"></span>
                                            </span> --}}
                                            <i class="font-size-22 ec ec-user"></i>
                                        </button>

                                        <aside id="sidebarHeader2" class="u-sidebar u-sidebar--right" aria-labelledby="sidebarHeaderInvokerMenu">
                                            <div class="u-sidebar__scroller">
                                                <div class="u-sidebar__container">
                                                    <div class="u-header-sidebar__footer-offset pb-0">
                                                        <!-- Toggle Button -->
                                                        <div class="position-absolute top-0 right-0 z-index-2 pt-4 pr-7">
                                                            <button type="button" class="close ml-auto"
                                                                aria-controls="sidebarHeader"
                                                                aria-haspopup="true"
                                                                aria-expanded="false"
                                                                data-unfold-event="click"
                                                                data-unfold-hide-on-scroll="false"
                                                                data-unfold-target="#sidebarHeader2"
                                                                data-unfold-type="css-animation"
                                                                data-unfold-animation-in="fadeInRight"
                                                                data-unfold-animation-out="fadeOutRight"
                                                                data-unfold-duration="500">
                                                                <span aria-hidden="true"><i class="ec ec-close-remove text-gray-90 font-size-20"></i></span>
                                                            </button>
                                                        </div>
                                                        <!-- End Toggle Button -->
        
                                                        <!-- Content -->
                                                        <div class="js-scrollbar u-sidebar__body">
                                                            <div id="headerSidebarContent" class="u-sidebar__content u-header-sidebar__content">
                                                                <!-- Logo -->
                                                                <a class="d-flex ml-0 navbar-brand u-header__navbar-brand u-header__navbar-brand-vertical" href="/" aria-label="Electro">
                                                                    <svg version="1.1" x="0px" y="0px" width="175.748px" height="42.52px" viewBox="0 0 175.748 42.52" enable-background="new 0 0 175.748 42.52" style="margin-bottom: 0;">
                                                                        <ellipse class="ellipse-bg" fill-rule="evenodd" clip-rule="evenodd" fill="#FDD700" cx="170.05" cy="36.341" rx="5.32" ry="5.367"></ellipse>
                                                                        <path fill-rule="evenodd" clip-rule="evenodd" fill="#333E48" d="M30.514,0.71c-0.034,0.003-0.066,0.008-0.056,0.056
                                                                            C30.263,0.995,29.876,1.181,29.79,1.5c-0.148,0.548,0,1.568,0,2.427v36.459c0.265,0.221,0.506,0.465,0.725,0.734h6.187
                                                                            c0.2-0.25,0.423-0.477,0.669-0.678V1.387C37.124,1.185,36.9,0.959,36.701,0.71H30.514z M117.517,12.731
                                                                            c-0.232-0.189-0.439-0.64-0.781-0.734c-0.754-0.209-2.039,0-3.121,0h-3.176V4.435c-0.232-0.189-0.439-0.639-0.781-0.733
                                                                            c-0.719-0.2-1.969,0-3.01,0h-3.01c-0.238,0.273-0.625,0.431-0.725,0.847c-0.203,0.852,0,2.399,0,3.725
                                                                            c0,1.393,0.045,2.748-0.055,3.725h-6.41c-0.184,0.237-0.629,0.434-0.725,0.791c-0.178,0.654,0,1.813,0,2.765v2.766
                                                                            c0.232,0.188,0.439,0.64,0.779,0.733c0.777,0.216,2.109,0,3.234,0c1.154,0,2.291-0.045,3.176,0.057v21.277
                                                                            c0.232,0.189,0.439,0.639,0.781,0.734c0.719,0.199,1.969,0,3.01,0h3.01c1.008-0.451,0.725-1.889,0.725-3.443
                                                                            c-0.002-6.164-0.047-12.867,0.055-18.625h6.299c0.182-0.236,0.627-0.434,0.725-0.79c0.176-0.653,0-1.813,0-2.765V12.731z
                                                                            M135.851,18.262c0.201-0.746,0-2.029,0-3.104v-3.104c-0.287-0.245-0.434-0.637-0.781-0.733c-0.824-0.229-1.992-0.044-2.898,0
                                                                            c-2.158,0.104-4.506,0.675-5.74,1.411c-0.146-0.362-0.451-0.853-0.893-0.96c-0.693-0.169-1.859,0-2.842,0h-2.842
                                                                            c-0.258,0.319-0.625,0.42-0.725,0.79c-0.223,0.82,0,2.338,0,3.443c0,8.109-0.002,16.635,0,24.381
                                                                            c0.232,0.189,0.439,0.639,0.779,0.734c0.707,0.195,1.93,0,2.955,0h3.01c0.918-0.463,0.725-1.352,0.725-2.822V36.21
                                                                            c-0.002-3.902-0.242-9.117,0-12.473c0.297-4.142,3.836-4.877,8.527-4.686C135.312,18.816,135.757,18.606,135.851,18.262z
                                                                            M14.796,11.376c-5.472,0.262-9.443,3.178-11.76,7.056c-2.435,4.075-2.789,10.62-0.501,15.126c2.043,4.023,5.91,7.115,10.701,7.9
                                                                            c6.051,0.992,10.992-1.219,14.324-3.838c-0.687-1.1-1.419-2.664-2.118-3.951c-0.398-0.734-0.652-1.486-1.616-1.467
                                                                            c-1.942,0.787-4.272,2.262-7.134,2.145c-3.791-0.154-6.659-1.842-7.524-4.91h19.452c0.146-2.793,0.22-5.338-0.279-7.563
                                                                            C26.961,15.728,22.503,11.008,14.796,11.376z M9,23.284c0.921-2.508,3.033-4.514,6.298-4.627c3.083-0.107,4.994,1.976,5.685,4.627
                                                                            C17.119,23.38,12.865,23.38,9,23.284z M52.418,11.376c-5.551,0.266-9.395,3.142-11.76,7.056
                                                                            c-2.476,4.097-2.829,10.493-0.557,15.069c1.997,4.021,5.895,7.156,10.646,7.957c6.068,1.023,11-1.227,14.379-3.781
                                                                            c-0.479-0.896-0.875-1.742-1.393-2.709c-0.312-0.582-1.024-2.234-1.561-2.539c-0.912-0.52-1.428,0.135-2.23,0.508
                                                                            c-0.564,0.262-1.223,0.523-1.672,0.676c-4.768,1.621-10.372,0.268-11.537-4.176h19.451c0.668-5.443-0.419-9.953-2.73-13.037
                                                                            C61.197,13.388,57.774,11.12,52.418,11.376z M46.622,23.343c0.708-2.553,3.161-4.578,6.242-4.686
                                                                            c3.08-0.107,5.08,1.953,5.686,4.686H46.622z M160.371,15.497c-2.455-2.453-6.143-4.291-10.869-4.064
                                                                            c-2.268,0.109-4.297,0.65-6.02,1.524c-1.719,0.873-3.092,1.957-4.234,3.217c-2.287,2.519-4.164,6.004-3.902,11.007
                                                                            c0.248,4.736,1.979,7.813,4.627,10.326c2.568,2.439,6.148,4.254,10.867,4.064c4.457-0.18,7.889-2.115,10.199-4.684
                                                                            c2.469-2.746,4.012-5.971,3.959-11.063C164.949,21.134,162.732,17.854,160.371,15.497z M149.558,33.952
                                                                            c-3.246-0.221-5.701-2.615-6.41-5.418c-0.174-0.689-0.26-1.25-0.4-2.166c-0.035-0.234,0.072-0.523-0.045-0.77
                                                                            c0.682-3.698,2.912-6.257,6.799-6.547c2.543-0.189,4.258,0.735,5.52,1.863c1.322,1.182,2.303,2.715,2.451,4.967
                                                                            C157.789,30.669,154.185,34.267,149.558,33.952z M88.812,29.55c-1.232,2.363-2.9,4.307-6.13,4.402
                                                                            c-4.729,0.141-8.038-3.16-8.025-7.563c0.004-1.412,0.324-2.65,0.947-3.726c1.197-2.061,3.507-3.688,6.633-3.612
                                                                            c3.222,0.079,4.966,1.708,6.632,3.668c1.328-1.059,2.529-1.948,3.9-2.99c0.416-0.315,1.076-0.688,1.227-1.072
                                                                            c0.404-1.031-0.365-1.502-0.891-2.088c-2.543-2.835-6.66-5.377-11.704-5.137c-6.02,0.288-10.218,3.697-12.484,7.846
                                                                            c-1.293,2.365-1.951,5.158-1.729,8.408c0.209,3.053,1.191,5.496,2.619,7.508c2.842,4.004,7.385,6.973,13.656,6.377
                                                                            c5.976-0.568,9.574-3.936,11.816-8.354c-0.141-0.271-0.221-0.604-0.336-0.902C92.929,31.364,90.843,30.485,88.812,29.55z">
                                                                        </path>
                                                                    </svg>
                                                                </a>
                                                                <!-- End Logo -->
        
                                                               @auth

                                                                   <li class="u-has-submenu u-header-collapse__submenu">
                                                                        <a class="u-header-collapse__nav-link " href="/profile" >
                                                                            My Profile
                                                                        </a>
                                                                    </li>
                                                                    <li class="u-has-submenu u-header-collapse__submenu">
                                                                        <a class="u-header-collapse__nav-link " href="/order" >
                                                                            Order
                                                                        </a>
                                                                    </li>
                                                                    <li class="u-has-submenu u-header-collapse__submenu">
                                                                        <a class="u-header-collapse__nav-link " href="/wishlist" >
                                                                            Wishlist
                                                                        </a>
                                                                    </li>
                                                                    <li class="u-has-submenu u-header-collapse__submenu">
                                                                        <a class="u-header-collapse__nav-link " href="/wallet" >
                                                                            wallet
                                                                        </a>
                                                                    </li>
                                                                  
                                                                   


                                                               @else

                                                               @endauth
                                                                <ul id="headerSidebarList" class="u-header-collapse__nav">
                                                                   
                                                                    <li class="u-has-submenu u-header-collapse__submenu">
                                                                        <a class="u-header-collapse__nav-link " href="/offer">
                                                                            Coupon 
                                                                        </a>
                                                                    </li>

                                                                    <li class="u-has-submenu u-header-collapse__submenu">
                                                                        
                                                                        <a class="u-header-collapse__nav-link " href="javascript:;">
                                                                            Sell On Aliya
                                                                        </a>
        
                                                                     
                                                                    </li>
                                                                    
                                                                    <li class="u-has-submenu u-header-collapse__submenu">
                                                                        <a class="u-header-collapse__nav-link" href="javascript:;" data-target="#headerSidebarPagesCollapse" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="headerSidebarPagesCollapse">
                                                                            About Us
                                                                        </a>
        
                                                                    </li>
                                                                  
                                                                    <li class="u-has-submenu u-header-collapse__submenu">
                                                                        <a class="u-header-collapse__nav-link" href="javascript:;" data-target="#headerSidebarBlogCollapse" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="headerSidebarBlogCollapse">
                                                                           Contact Us
                                                                        </a>
        
                                                                       
                                                                    </li>
                                                            
                                                                    <li class="u-has-submenu u-header-collapse__submenu">
                                                                        <a class="u-header-collapse__nav-link" href="javascript:;" data-target="#headerSidebarShopCollapse" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="headerSidebarShopCollapse">
                                                                            Download App
                                                                        </a>
        
                                                                      
                                                                    </li>
                                                                    @auth
                                                                    <li class="u-has-submenu u-header-collapse__submenu">
                                                                    
                                                                            <a class="u-header-collapse__nav-link " href="javascript:;" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="headerSidebarHomeCollapse" data-target="#headerSidebarHomeCollapse" href="{{ route('logout') }}"
                                                                            onclick="event.preventDefault();
                                                                                            document.getElementById('logout-form').submit();">
                                                                             {{ __('Logout') }}
                                                                              </a>
                                                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                                              @csrf
                                                                           </form>
                                                                        
                                                                    </li>
                                                                    @else
                                                                            <li class="u-has-submenu u-header-collapse__submenu">
                                                                                <a class="u-header-collapse__nav-link" href="/login" >
                                                                                        Login
                                                                                </a>
                                                                            </li>

                                                                            <li class="u-has-submenu u-header-collapse__submenu">
                                                                                <a class="u-header-collapse__nav-link" href="/register" >
                                                                                        Register
                                                                                </a>
                
                                                                              
                                                                            </li>
                                                                        
                                                                    @endauth
                                                                 
        
                                                                  
                                                                </ul>
                                                                <!-- End List -->
                                                            </div>
                                                        </div>
                                                        <!-- End Content -->
                                                    </div>
                                                </div>
                                            </div>
                                        </aside>

                                        </li>

                                     


                                    </ul>
                                </div>
                            </div>
                            <!-- End Header Icons -->
                        </div>
                    </div>
                </div>
                <!-- End Logo-Search-header-icons -->

                <div class="d-none d-xl-block bg-primary">
                    <div class="container">
                        <div class="min-height-45">
                            <!-- Nav -->
                            <nav class="js-mega-menu navbar navbar-expand-md u-header__navbar u-header__navbar--wide u-header__navbar--no-space">
                                <!-- Navigation -->
                                <div id="navBar" class="collapse navbar-collapse u-header__navbar-collapse">
                                   @php($category1=DB::table('cats')->get())
                                   @php($sub_category1=DB::table('sub_cats')->get())
                                   @php($sub_sub_category1=DB::table('sub_sub_cats')->get())
                                   
                                    <ul class="navbar-nav u-header__navbar-nav">
                                        <!-- Home -->
                                        @foreach($category1 as $key => $category)

                                        <li class="nav-item hs-has-mega-menu u-header__nav-item"
                                            data-event="hover"
                                            data-animation-in="slideInUP"
                                            data-animation-out="fadeOut"
                                            data-position="left">
                                            <a id="homeMegaMenu" class="nav-link u-header__nav-link u-header__nav-link-toggle" href="/shop?cat_id={{$category->cat_id}}&name={{$category->cat_name}}" aria-haspopup="true" aria-expanded="false">{{$category->cat_name}}</a>
                                         
                                            <!-- Home - Mega Menu -->
                                            <div class="hs-mega-menu w-100 u-header__sub-menu" aria-labelledby="homeMegaMenu">
                                                <div class="row u-header__mega-menu-wrapper">
                                                    @foreach($sub_category1 as $key1=> $sub_category)
                                                    @if($category->cat_id == $sub_category->cat_id)
                                                    <div class="col-md-3">
                                                       
                                                   <a href="/shop?cat_id={{$category->cat_id}}&sub_cat_id={{$sub_category->sub_cat_id}}&name={{$sub_category->sub_cat_name}}"> <span class="u-header__sub-menu-title">{{$sub_category->sub_cat_name}}</span></a>
                                                    @if($key1%2==0)   
                                                    <ul class="u-header__sub-menu-nav-group ">
                                                            @foreach($sub_sub_category1 as $sub_sub_category)
                                                                @if($sub_sub_category->sub_cat_id==$sub_category->sub_cat_id)
                                                                     <li><a href="/shop?cat_id={{$category->cat_id}}&sub_cat_id={{$sub_category->sub_cat_id}}&sub_sub_cat_id={{$sub_sub_category->sub_sub_cat_id}}&name={{$sub_sub_category->sub_sub_cat_name}}" class="nav-link u-header__sub-menu-nav-link">{{$sub_sub_category->sub_sub_cat_name}}</a></li>
                                                                @endif                                                
                                                            @endforeach
                                                        </ul>
                                                      @else
                                                        <ul class="u-header__sub-menu-nav-group mb-3">
                                                            @foreach($sub_sub_category1 as $sub_sub_category)
                                                                @if($sub_sub_category->sub_cat_id==$sub_category->sub_cat_id)
                                                                     <li><a href="/shop?cat_id={{$category->cat_id}}&sub_cat_id={{$sub_category->sub_cat_id}}&sub_sub_cat_id={{$sub_sub_category->sub_sub_cat_id}}&name={{$sub_sub_category->sub_sub_cat_name}}" class="nav-link u-header__sub-menu-nav-link">{{$sub_sub_category->sub_sub_cat_name}}</a></li>
                                                                @endif                                                
                                                            @endforeach
                                                        </ul>
                                                      @endif
 
                                                    </div>
                                                    @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                            <!-- End Home - Mega Menu -->
                                         
                                        </li>
                                        
                                        @endforeach
                                        <!-- End Home -->
                                        <!-- Cameras & Accessories -->
                                        <li class="nav-item hs-has-mega-menu u-header__nav-item"
                                            data-event="click"
                                            data-animation-in="slideInUp"
                                            data-animation-out="fadeOut">
                                            <a id="CamerasAccessoriesMegaMenu" class="nav-link u-header__nav-link u-header__nav-link-toggle" href="javascript:;" aria-haspopup="true" aria-expanded="false">Cameras & Accessories</a>

                                            <!-- Cameras & Accessories - Mega Menu -->
                                            <div class="hs-mega-menu w-100 u-header__sub-menu" aria-labelledby="CamerasAccessoriesMegaMenu">
                                                <div class="row u-header__mega-menu-wrapper">
                                                    <div class="col-md-8">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <span class="u-header__sub-menu-title">Cameras</span>
                                                                <ul class="u-header__sub-menu-nav-group mb-3">
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">DSLR Cameras</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Digital Cameras</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Security &amp; Surveillance</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Camcorders</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Consoles</a></li>
                                                                </ul>
                                                                <span class="u-header__sub-menu-title">Shop By Price</span>
                                                                <ul class="u-header__sub-menu-nav-group">
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Below Rs. 100$</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">101$ - 199$</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">200$ - 299$</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">300$ - 399$</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">400$ and Above</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <span class="u-header__sub-menu-title">Shop By Focal Length</span>
                                                                <ul class="u-header__sub-menu-nav-group mb-3">
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">8mm - 24mm</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">24mm - 35mm</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">35mm - 85mm</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">85mm - 135mm</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">135mm+</a></li>
                                                                </ul>
                                                                <span class="u-header__sub-menu-title">#Trending</span>
                                                                <ul class="u-header__sub-menu-nav-group">
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Sony</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Nikon</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Canon</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Sanyo</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Samsung</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <span class="u-header__sub-menu-title">Accessories</span>
                                                                <ul class="u-header__sub-menu-nav-group mb-3">
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Headphones</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Mouses</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Hardrives</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Headphones</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Speakers</a></li>
                                                                </ul>
                                                                <span class="u-header__sub-menu-title">Add-ons</span>
                                                                <ul class="u-header__sub-menu-nav-group">
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Data Cables</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Keypads</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Earphones</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Lenses</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Camera Accessories</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <span class="u-header__sub-menu-title">Shop By Brands</span>
                                                                <ul class="u-header__sub-menu-nav-group">
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Canon</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Nikon</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Pentax</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Sony</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Apple</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Leica</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Samsung</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Panasonic</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">LG</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Oppo</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Olympus</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Sanyo</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <a href="#" class="d-block">
                                                            <img class="img-fluid" src={{ asset('assets/img/1024X1024/img4.png')}} alt="Image Description">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- End Cameras & Accessories - Mega Menu -->
                                        </li>
                                        <!-- End Cameras & Accessories -->

                                        <!-- Movies & Games -->
                                        <li class="nav-item hs-has-mega-menu u-header__nav-item"
                                            data-event="hover"
                                            data-animation-in="slideInUp"
                                            data-animation-out="fadeOut">
                                            <a id="MoviesGamesMegaMenu" class="nav-link u-header__nav-link u-header__nav-link-toggle" href="javascript:;" aria-haspopup="true" aria-expanded="false">Movies & Games</a>

                                            <!-- Movies & Games - Mega Menu -->
                                            <div class="hs-mega-menu w-100 u-header__sub-menu" aria-labelledby="MoviesGamesMegaMenu">
                                                <div class="row u-header__mega-menu-wrapper">
                                                    <div class="col-md-4">
                                                        <div class="row">
                                                            <div class="col">
                                                                <a href="#" class="d-block">
                                                                    <img class="img-fluid" src={{ asset('assets/img/300X275/img9.jpg')}} alt="Image Description">
                                                                </a>
                                                            </div>
                                                            <div class="col">
                                                                <span class="u-header__sub-menu-title">Movies &amp; TV Shows</span>
                                                                <ul class="u-header__sub-menu-nav-group">
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">All Movies &amp; TV Shows</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Blu-ray</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Latest Movies</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">All English</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">All Hindi</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="row">
                                                            <div class="col">
                                                                <a href="#" class="d-block">
                                                                    <img class="img-fluid" src={{ asset('assets/img/300X275/img10.jpg')}} alt="Image Description">
                                                                </a>
                                                            </div>
                                                            <div class="col">
                                                                <span class="u-header__sub-menu-title">Video Games</span>
                                                                <ul class="u-header__sub-menu-nav-group">
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Games &amp; Accessories</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">PC Games</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">New Releases</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Consoles</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Accessories</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="row">
                                                            <div class="col">
                                                                <a href="#" class="d-block">
                                                                    <img class="img-fluid" src={{ asset('assets/img/300X275/img11.jpg')}} alt="Image Description">
                                                                </a>
                                                            </div>
                                                            <div class="col">
                                                                <span class="u-header__sub-menu-title">Music</span>
                                                                <ul class="u-header__sub-menu-nav-group">
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">5.1 Speaker</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Home Theatres</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Soundbars</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Accessories</a></li>
                                                                    <li><a href="#" class="nav-link u-header__sub-menu-nav-link">Consoles</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- End Movies & Games - Mega Menu -->
                                        </li>
                                        <!-- End Movies & Games -->
                                    </ul>
                                </div>
                                <!-- End Navigation -->
                            </nav>
                            <!-- End Nav -->
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- ========== END HEADER ========== -->






        @yield('content')
















           <!-- ========== FOOTER ========== -->
           <footer>
           
            <!-- Footer-newsletter -->
            <div class="bg-primary py-3">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-7 mb-md-3 mb-lg-0">
                            <div class="row align-items-center">
                                <div class="col-auto flex-horizontal-center">
                                    <i class="ec ec-newsletter font-size-40"></i>
                                    <h2 class="font-size-20 mb-0 ml-3">Sign up to Newsletter</h2>
                                </div>
                                <div class="col my-4 my-md-0">
                                    <h5 class="font-size-15 ml-4 mb-0">...and receive <strong>$20 coupon for first shopping.</strong></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <!-- Subscribe Form -->
                            <form class="js-validate js-form-message">
                                <label class="sr-only" for="subscribeSrEmail">Email address</label>
                                <div class="input-group input-group-pill">
                                    <input type="email" class="form-control border-0 height-40" name="email" id="subscribeSrEmail" placeholder="Email address" aria-label="Email address" aria-describedby="subscribeButton" required
                                    data-msg="Please enter a valid email address.">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-dark btn-sm-wide height-40 py-2" id="subscribeButton">Sign Up</button>
                                    </div>
                                </div>
                            </form>
                            <!-- End Subscribe Form -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Footer-newsletter -->

            <!-- Footer-bottom-widgets -->
            <div class="pt-8 pb-4 bg-gray-13">
                <div class="container mt-1">
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="mb-6">
                                <a href="#" class="d-inline-block">
                                    <svg version="1.1" x="0px" y="0px" width="156px" height="37px" viewBox="0 0 175.748 42.52" enable-background="new 0 0 175.748 42.52">
                                        <ellipse fill-rule="evenodd" clip-rule="evenodd" fill="#FDD700" cx="170.05" cy="36.341" rx="5.32" ry="5.367"></ellipse>
                                        <path fill-rule="evenodd" clip-rule="evenodd" fill="#333E48" d="M30.514,0.71c-0.034,0.003-0.066,0.008-0.056,0.056
                                            C30.263,0.995,29.876,1.181,29.79,1.5c-0.148,0.548,0,1.568,0,2.427v36.459c0.265,0.221,0.506,0.465,0.725,0.734h6.187
                                            c0.2-0.25,0.423-0.477,0.669-0.678V1.387C37.124,1.185,36.9,0.959,36.701,0.71H30.514z M117.517,12.731
                                            c-0.232-0.189-0.439-0.64-0.781-0.734c-0.754-0.209-2.039,0-3.121,0h-3.176V4.435c-0.232-0.189-0.439-0.639-0.781-0.733
                                            c-0.719-0.2-1.969,0-3.01,0h-3.01c-0.238,0.273-0.625,0.431-0.725,0.847c-0.203,0.852,0,2.399,0,3.725
                                            c0,1.393,0.045,2.748-0.055,3.725h-6.41c-0.184,0.237-0.629,0.434-0.725,0.791c-0.178,0.654,0,1.813,0,2.765v2.766
                                            c0.232,0.188,0.439,0.64,0.779,0.733c0.777,0.216,2.109,0,3.234,0c1.154,0,2.291-0.045,3.176,0.057v21.277
                                            c0.232,0.189,0.439,0.639,0.781,0.734c0.719,0.199,1.969,0,3.01,0h3.01c1.008-0.451,0.725-1.889,0.725-3.443
                                            c-0.002-6.164-0.047-12.867,0.055-18.625h6.299c0.182-0.236,0.627-0.434,0.725-0.79c0.176-0.653,0-1.813,0-2.765V12.731z
                                            M135.851,18.262c0.201-0.746,0-2.029,0-3.104v-3.104c-0.287-0.245-0.434-0.637-0.781-0.733c-0.824-0.229-1.992-0.044-2.898,0
                                            c-2.158,0.104-4.506,0.675-5.74,1.411c-0.146-0.362-0.451-0.853-0.893-0.96c-0.693-0.169-1.859,0-2.842,0h-2.842
                                            c-0.258,0.319-0.625,0.42-0.725,0.79c-0.223,0.82,0,2.338,0,3.443c0,8.109-0.002,16.635,0,24.381
                                            c0.232,0.189,0.439,0.639,0.779,0.734c0.707,0.195,1.93,0,2.955,0h3.01c0.918-0.463,0.725-1.352,0.725-2.822V36.21
                                            c-0.002-3.902-0.242-9.117,0-12.473c0.297-4.142,3.836-4.877,8.527-4.686C135.312,18.816,135.757,18.606,135.851,18.262z
                                            M14.796,11.376c-5.472,0.262-9.443,3.178-11.76,7.056c-2.435,4.075-2.789,10.62-0.501,15.126c2.043,4.023,5.91,7.115,10.701,7.9
                                            c6.051,0.992,10.992-1.219,14.324-3.838c-0.687-1.1-1.419-2.664-2.118-3.951c-0.398-0.734-0.652-1.486-1.616-1.467
                                            c-1.942,0.787-4.272,2.262-7.134,2.145c-3.791-0.154-6.659-1.842-7.524-4.91h19.452c0.146-2.793,0.22-5.338-0.279-7.563
                                            C26.961,15.728,22.503,11.008,14.796,11.376z M9,23.284c0.921-2.508,3.033-4.514,6.298-4.627c3.083-0.107,4.994,1.976,5.685,4.627
                                            C17.119,23.38,12.865,23.38,9,23.284z M52.418,11.376c-5.551,0.266-9.395,3.142-11.76,7.056
                                            c-2.476,4.097-2.829,10.493-0.557,15.069c1.997,4.021,5.895,7.156,10.646,7.957c6.068,1.023,11-1.227,14.379-3.781
                                            c-0.479-0.896-0.875-1.742-1.393-2.709c-0.312-0.582-1.024-2.234-1.561-2.539c-0.912-0.52-1.428,0.135-2.23,0.508
                                            c-0.564,0.262-1.223,0.523-1.672,0.676c-4.768,1.621-10.372,0.268-11.537-4.176h19.451c0.668-5.443-0.419-9.953-2.73-13.037
                                            C61.197,13.388,57.774,11.12,52.418,11.376z M46.622,23.343c0.708-2.553,3.161-4.578,6.242-4.686
                                            c3.08-0.107,5.08,1.953,5.686,4.686H46.622z M160.371,15.497c-2.455-2.453-6.143-4.291-10.869-4.064
                                            c-2.268,0.109-4.297,0.65-6.02,1.524c-1.719,0.873-3.092,1.957-4.234,3.217c-2.287,2.519-4.164,6.004-3.902,11.007
                                            c0.248,4.736,1.979,7.813,4.627,10.326c2.568,2.439,6.148,4.254,10.867,4.064c4.457-0.18,7.889-2.115,10.199-4.684
                                            c2.469-2.746,4.012-5.971,3.959-11.063C164.949,21.134,162.732,17.854,160.371,15.497z M149.558,33.952
                                            c-3.246-0.221-5.701-2.615-6.41-5.418c-0.174-0.689-0.26-1.25-0.4-2.166c-0.035-0.234,0.072-0.523-0.045-0.77
                                            c0.682-3.698,2.912-6.257,6.799-6.547c2.543-0.189,4.258,0.735,5.52,1.863c1.322,1.182,2.303,2.715,2.451,4.967
                                            C157.789,30.669,154.185,34.267,149.558,33.952z M88.812,29.55c-1.232,2.363-2.9,4.307-6.13,4.402
                                            c-4.729,0.141-8.038-3.16-8.025-7.563c0.004-1.412,0.324-2.65,0.947-3.726c1.197-2.061,3.507-3.688,6.633-3.612
                                            c3.222,0.079,4.966,1.708,6.632,3.668c1.328-1.059,2.529-1.948,3.9-2.99c0.416-0.315,1.076-0.688,1.227-1.072
                                            c0.404-1.031-0.365-1.502-0.891-2.088c-2.543-2.835-6.66-5.377-11.704-5.137c-6.02,0.288-10.218,3.697-12.484,7.846
                                            c-1.293,2.365-1.951,5.158-1.729,8.408c0.209,3.053,1.191,5.496,2.619,7.508c2.842,4.004,7.385,6.973,13.656,6.377
                                            c5.976-0.568,9.574-3.936,11.816-8.354c-0.141-0.271-0.221-0.604-0.336-0.902C92.929,31.364,90.843,30.485,88.812,29.55z">
                                        </path>
                                    </svg>
                                </a>
                            </div>
                            <div class="mb-4">
                                <div class="row no-gutters">
                                    <div class="col-auto">
                                        <i class="ec ec-support text-primary font-size-56"></i>
                                    </div>
                                    <div class="col pl-3">
                                        <div class="font-size-13 font-weight-light">Got questions? Call us 24/7!</div>
                                        <a href="tel:+80080018588" class="font-size-20 text-gray-90">(800) 8001-8588, </a><a href="tel:+0600874548" class="font-size-20 text-gray-90">(0600) 874 548</a>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-4">
                                <h6 class="mb-1 font-weight-bold">Contact info</h6>
                                <address class="">
                                    17 Princess Road, London, Greater London NW1 8JR, UK
                                </address>
                            </div>
                            <div class="my-4 my-md-4">
                                <ul class="list-inline mb-0 opacity-7">
                                    <li class="list-inline-item mr-0">
                                        <a class="btn font-size-20 btn-icon btn-soft-dark btn-bg-transparent rounded-circle" href="#">
                                            <span class="fab fa-facebook-f btn-icon__inner"></span>
                                        </a>
                                    </li>
                                    <li class="list-inline-item mr-0">
                                        <a class="btn font-size-20 btn-icon btn-soft-dark btn-bg-transparent rounded-circle" href="#">
                                            <span class="fab fa-google btn-icon__inner"></span>
                                        </a>
                                    </li>
                                    <li class="list-inline-item mr-0">
                                        <a class="btn font-size-20 btn-icon btn-soft-dark btn-bg-transparent rounded-circle" href="#">
                                            <span class="fab fa-twitter btn-icon__inner"></span>
                                        </a>
                                    </li>
                                    <li class="list-inline-item mr-0">
                                        <a class="btn font-size-20 btn-icon btn-soft-dark btn-bg-transparent rounded-circle" href="#">
                                            <span class="fab fa-github btn-icon__inner"></span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="row">
                                <div class="col-12 col-md mb-4 mb-md-0">
                                    <h6 class="mb-3 font-weight-bold">Find it Fast</h6>
                                    <!-- List Group -->
                                    <ul class="list-group list-group-flush list-group-borderless mb-0 list-group-transparent">
                                        <li><a class="list-group-item list-group-item-action" href="../shop/product-categories-5-column-sidebar.html">Laptops & Computers</a></li>
                                        <li><a class="list-group-item list-group-item-action" href="../shop/product-categories-5-column-sidebar.html">Cameras & Photography</a></li>
                                        <li><a class="list-group-item list-group-item-action" href="../shop/product-categories-5-column-sidebar.html">Smart Phones & Tablets</a></li>
                                        <li><a class="list-group-item list-group-item-action" href="../shop/product-categories-5-column-sidebar.html">Video Games & Consoles</a></li>
                                        <li><a class="list-group-item list-group-item-action" href="../shop/product-categories-5-column-sidebar.html">TV & Audio</a></li>
                                        <li><a class="list-group-item list-group-item-action" href="../shop/product-categories-5-column-sidebar.html">Gadgets</a></li>
                                        <li><a class="list-group-item list-group-item-action" href="../shop/product-categories-5-column-sidebar.html">Car Electronic & GPS</a></li>
                                    </ul>
                                    <!-- End List Group -->
                                </div>

                                <div class="col-12 col-md mb-4 mb-md-0">
                                    <!-- List Group -->
                                    <ul class="list-group list-group-flush list-group-borderless mb-0 list-group-transparent mt-md-6">
                                        <li><a class="list-group-item list-group-item-action" href="../shop/product-categories-5-column-sidebar.html">Printers & Ink</a></li>
                                        <li><a class="list-group-item list-group-item-action" href="../shop/product-categories-5-column-sidebar.html">Software</a></li>
                                        <li><a class="list-group-item list-group-item-action" href="../shop/product-categories-5-column-sidebar.html">Office Supplies</a></li>
                                        <li><a class="list-group-item list-group-item-action" href="../shop/product-categories-5-column-sidebar.html">Computer Components</a></li>
                                        <li><a class="list-group-item list-group-item-action" href="../shop/product-categories-5-column-sidebar.html">Accesories</a></li>
                                    </ul>
                                    <!-- End List Group -->
                                </div>

                                <div class="col-12 col-md mb-4 mb-md-0">
                                    <h6 class="mb-3 font-weight-bold">Customer Care</h6>
                                    <!-- List Group -->
                                    <ul class="list-group list-group-flush list-group-borderless mb-0 list-group-transparent">
                                        <li><a class="list-group-item list-group-item-action" href="../shop/my-account.html">My Account</a></li>
                                        <li><a class="list-group-item list-group-item-action" href="../shop/track-your-order.html">Order Tracking</a></li>
                                        <li><a class="list-group-item list-group-item-action" href="../shop/wishlist.html">Wish List</a></li>
                                        <li><a class="list-group-item list-group-item-action" href="../home/terms-and-conditions.html">Customer Service</a></li>
                                        <li><a class="list-group-item list-group-item-action" href="../home/terms-and-conditions.html">Returns / Exchange</a></li>
                                        <li><a class="list-group-item list-group-item-action" href="../home/faq.html">FAQs</a></li>
                                        <li><a class="list-group-item list-group-item-action" href="../home/terms-and-conditions.html">Product Support</a></li>
                                    </ul>
                                    <!-- End List Group -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Footer-bottom-widgets -->
            <!-- Footer-copy-right -->
            <div class="bg-gray-14 py-2">
                <div class="container">
                    <div class="flex-center-between d-block d-md-flex">
                        <div class="mb-3 mb-md-0">© <a href="#" class="font-weight-bold text-gray-90">Electro</a> - All rights Reserved</div>
                        <div class="text-md-right">
                            <span class="d-inline-block bg-white border rounded p-1">
                                <img class="max-width-5" src={{ asset('assets/img/100X60/img1.jpg')}} alt="Image Description">
                            </span>
                            <span class="d-inline-block bg-white border rounded p-1">
                                <img class="max-width-5" src={{ asset('assets/img/100X60/img2.jpg')}} alt="Image Description">
                            </span>
                            <span class="d-inline-block bg-white border rounded p-1">
                                <img class="max-width-5" src={{ asset('assets/img/100X60/img3.jpg')}} alt="Image Description">
                            </span>
                            <span class="d-inline-block bg-white border rounded p-1">
                                <img class="max-width-5" src={{ asset('assets/img/100X60/img4.jpg')}} alt="Image Description">
                            </span>
                            <span class="d-inline-block bg-white border rounded p-1">
                                <img class="max-width-5" src={{ asset('assets/img/100X60/img5.jpg')}} alt="Image Description">
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Footer-copy-right -->
        </footer>
        <!-- ========== END FOOTER ========== -->

        <!-- ========== SECONDARY CONTENTS ========== -->
        <!-- Account Sidebar Navigation -->
        <aside id="sidebarContent" class="u-sidebar u-sidebar__lg" aria-labelledby="sidebarNavToggler">
            <div class="u-sidebar__scroller">
                <div class="u-sidebar__container">
                    <div class="js-scrollbar u-header-sidebar__footer-offset pb-3">
                        <!-- Toggle Button -->
                        <div class="d-flex align-items-center pt-4 px-7">
                            <button type="button" class="close ml-auto"
                                aria-controls="sidebarContent"
                                aria-haspopup="true"
                                aria-expanded="false"
                                data-unfold-event="click"
                                data-unfold-hide-on-scroll="false"
                                data-unfold-target="#sidebarContent"
                                data-unfold-type="css-animation"
                                data-unfold-animation-in="fadeInRight"
                                data-unfold-animation-out="fadeOutRight"
                                data-unfold-duration="500">
                                <i class="ec ec-close-remove"></i>
                            </button>
                        </div>
                        <!-- End Toggle Button -->

                        <!-- Content -->
                        <div class="js-scrollbar u-sidebar__body">
                            <div class="u-sidebar__content u-header-sidebar__content">
                                <form class="js-validate">
                                    <!-- Login -->
                                    <div id="login" data-target-group="idForm">
                                        <!-- Title -->
                                        <header class="text-center mb-7">
                                        <h2 class="h4 mb-0">Welcome Back!</h2>
                                        <p>Login to manage your account.</p>
                                        </header>
                                        <!-- End Title -->

                                        <!-- Form Group -->
                                        <div class="form-group">
                                            <div class="js-form-message js-focus-state">
                                                <label class="sr-only" for="signinEmail">Email</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="signinEmailLabel">
                                                            <span class="fas fa-user"></span>
                                                        </span>
                                                    </div>
                                                    <input type="email" class="form-control" name="email" id="signinEmail" placeholder="Email" aria-label="Email" aria-describedby="signinEmailLabel" required
                                                    data-msg="Please enter a valid email address."
                                                    data-error-class="u-has-error"
                                                    data-success-class="u-has-success">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Form Group -->

                                        <!-- Form Group -->
                                        <div class="form-group">
                                            <div class="js-form-message js-focus-state">
                                              <label class="sr-only" for="signinPassword">Password</label>
                                              <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="signinPasswordLabel">
                                                        <span class="fas fa-lock"></span>
                                                    </span>
                                                </div>
                                                <input type="password" class="form-control" name="password" id="signinPassword" placeholder="Password" aria-label="Password" aria-describedby="signinPasswordLabel" required
                                                   data-msg="Your password is invalid. Please try again."
                                                   data-error-class="u-has-error"
                                                   data-success-class="u-has-success">
                                              </div>
                                            </div>
                                        </div>
                                        <!-- End Form Group -->

                                        <div class="d-flex justify-content-end mb-4">
                                            <a class="js-animation-link small link-muted" href="javascript:;"
                                               data-target="#forgotPassword"
                                               data-link-group="idForm"
                                               data-animation-in="slideInUp">Forgot Password?</a>
                                        </div>

                                        <div class="mb-2">
                                            <button type="submit" class="btn btn-block btn-sm btn-primary transition-3d-hover">Login</button>
                                        </div>

                                        <div class="text-center mb-4">
                                            <span class="small text-muted">Do not have an account?</span>
                                            <a class="js-animation-link small text-dark" href="javascript:;"
                                               data-target="#signup"
                                               data-link-group="idForm"
                                               data-animation-in="slideInUp">Signup
                                            </a>
                                        </div>

                                        <div class="text-center">
                                            <span class="u-divider u-divider--xs u-divider--text mb-4">OR</span>
                                        </div>

                                        <!-- Login Buttons -->
                                        <div class="d-flex">
                                            <a class="btn btn-block btn-sm btn-soft-facebook transition-3d-hover mr-1" href="#">
                                              <span class="fab fa-facebook-square mr-1"></span>
                                              Facebook
                                            </a>
                                            <a class="btn btn-block btn-sm btn-soft-google transition-3d-hover ml-1 mt-0" href="#">
                                              <span class="fab fa-google mr-1"></span>
                                              Google
                                            </a>
                                        </div>
                                        <!-- End Login Buttons -->
                                    </div>

                                    <!-- Signup -->
                                    <div id="signup" style="display: none; opacity: 0;" data-target-group="idForm">
                                        <!-- Title -->
                                        <header class="text-center mb-7">
                                        <h2 class="h4 mb-0">Welcome to Electro.</h2>
                                        <p>Fill out the form to get started.</p>
                                        </header>
                                        <!-- End Title -->

                                        <!-- Form Group -->
                                        <div class="form-group">
                                            <div class="js-form-message js-focus-state">
                                                <label class="sr-only" for="signupEmail">Email</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="signupEmailLabel">
                                                            <span class="fas fa-user"></span>
                                                        </span>
                                                    </div>
                                                    <input type="email" class="form-control" name="email" id="signupEmail" placeholder="Email" aria-label="Email" aria-describedby="signupEmailLabel" required
                                                    data-msg="Please enter a valid email address."
                                                    data-error-class="u-has-error"
                                                    data-success-class="u-has-success">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Input -->

                                        <!-- Form Group -->
                                        <div class="form-group">
                                            <div class="js-form-message js-focus-state">
                                                <label class="sr-only" for="signupPassword">Password</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="signupPasswordLabel">
                                                            <span class="fas fa-lock"></span>
                                                        </span>
                                                    </div>
                                                    <input type="password" class="form-control" name="password" id="signupPassword" placeholder="Password" aria-label="Password" aria-describedby="signupPasswordLabel" required
                                                    data-msg="Your password is invalid. Please try again."
                                                    data-error-class="u-has-error"
                                                    data-success-class="u-has-success">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Input -->

                                        <!-- Form Group -->
                                        <div class="form-group">
                                            <div class="js-form-message js-focus-state">
                                            <label class="sr-only" for="signupConfirmPassword">Confirm Password</label>
                                                <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="signupConfirmPasswordLabel">
                                                        <span class="fas fa-key"></span>
                                                    </span>
                                                </div>
                                                <input type="password" class="form-control" name="confirmPassword" id="signupConfirmPassword" placeholder="Confirm Password" aria-label="Confirm Password" aria-describedby="signupConfirmPasswordLabel" required
                                                data-msg="Password does not match the confirm password."
                                                data-error-class="u-has-error"
                                                data-success-class="u-has-success">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Input -->

                                        <div class="mb-2">
                                            <button type="submit" class="btn btn-block btn-sm btn-primary transition-3d-hover">Get Started</button>
                                        </div>

                                        <div class="text-center mb-4">
                                            <span class="small text-muted">Already have an account?</span>
                                            <a class="js-animation-link small text-dark" href="javascript:;"
                                                data-target="#login"
                                                data-link-group="idForm"
                                                data-animation-in="slideInUp">Login
                                            </a>
                                        </div>

                                        <div class="text-center">
                                            <span class="u-divider u-divider--xs u-divider--text mb-4">OR</span>
                                        </div>

                                        <!-- Login Buttons -->
                                        <div class="d-flex">
                                            <a class="btn btn-block btn-sm btn-soft-facebook transition-3d-hover mr-1" href="#">
                                                <span class="fab fa-facebook-square mr-1"></span>
                                                Facebook
                                            </a>
                                            <a class="btn btn-block btn-sm btn-soft-google transition-3d-hover ml-1 mt-0" href="#">
                                                <span class="fab fa-google mr-1"></span>
                                                Google
                                            </a>
                                        </div>
                                        <!-- End Login Buttons -->
                                    </div>
                                    <!-- End Signup -->

                                    <!-- Forgot Password -->
                                    <div id="forgotPassword" style="display: none; opacity: 0;" data-target-group="idForm">
                                        <!-- Title -->
                                        <header class="text-center mb-7">
                                            <h2 class="h4 mb-0">Recover Password.</h2>
                                            <p>Enter your email address and an email with instructions will be sent to you.</p>
                                        </header>
                                        <!-- End Title -->

                                        <!-- Form Group -->
                                        <div class="form-group">
                                            <div class="js-form-message js-focus-state">
                                                <label class="sr-only" for="recoverEmail">Your email</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="recoverEmailLabel">
                                                            <span class="fas fa-user"></span>
                                                        </span>
                                                    </div>
                                                    <input type="email" class="form-control" name="email" id="recoverEmail" placeholder="Your email" aria-label="Your email" aria-describedby="recoverEmailLabel" required
                                                    data-msg="Please enter a valid email address."
                                                    data-error-class="u-has-error"
                                                    data-success-class="u-has-success">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Form Group -->

                                        <div class="mb-2">
                                            <button type="submit" class="btn btn-block btn-sm btn-primary transition-3d-hover">Recover Password</button>
                                        </div>

                                        <div class="text-center mb-4">
                                            <span class="small text-muted">Remember your password?</span>
                                            <a class="js-animation-link small" href="javascript:;"
                                               data-target="#login"
                                               data-link-group="idForm"
                                               data-animation-in="slideInUp">Login
                                            </a>
                                        </div>
                                    </div>
                                    <!-- End Forgot Password -->
                                </form>
                            </div>
                        </div>
                        <!-- End Content -->
                    </div>
                </div>
            </div>
        </aside>
        <input type="hidden" id="_token" value="<?php echo csrf_token();?>">
        <!-- End Account Sidebar Navigation -->
        <!-- ========== END SECONDARY CONTENTS ========== -->

        <!-- Go to Top -->
        <a class="js-go-to u-go-to" href="#"
            data-position='{"bottom": 15, "right": 15 }'
            data-type="fixed"
            data-offset-top="400"
            data-compensation="#header"
            data-show-effect="slideInUp"
            data-hide-effect="slideOutDown">
            <span class="fas fa-arrow-up u-go-to__inner"></span>
        </a>
        <!-- End Go to Top -->

        <!-- JS Global Compulsory -->
        <script src={{ asset('assets/vendor/jquery/dist/jquery.min.js')}}></script>
        <script src={{ asset('assets/vendor/jquery-migrate/dist/jquery-migrate.min.js')}}></script>
        <script src={{ asset('assets/vendor/popper.js/dist/umd/popper.min.js')}}></script>
        <script src={{ asset('assets/vendor/bootstrap/bootstrap.min.js')}}></script>

        <!-- JS Implementing Plugins -->
        <script src={{ asset('assets/vendor/appear.js')}}></script>
        <script src={{ asset('assets/vendor/jquery.countdown.min.js')}}></script>
        <script src={{ asset('assets/vendor/hs-megamenu/src/hs.megamenu.js')}}></script>
        <script src={{ asset('assets/vendor/svg-injector/dist/svg-injector.min.js')}}></script>
        <script src={{ asset('assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js')}}></script>
        <script src={{ asset('assets/vendor/jquery-validation/dist/jquery.validate.min.js')}}></script>
        <script src={{ asset('assets/vendor/fancybox/jquery.fancybox.min.js')}}></script>
        <script src={{ asset('assets/vendor/typed.js/lib/typed.min.js')}}></script>
        <script src={{ asset('assets/vendor/slick-carousel/slick/slick.js')}}></script>
        <script src={{ asset('assets/vendor/bootstrap-select/dist/js/bootstrap-select.min.js')}}></script>

        <!-- JS Electro -->
        <script src={{ asset('assets/js/hs.core.js')}}></script>
        <script src={{ asset('assets/js/components/hs.countdown.js')}}></script>
        <script src={{ asset('assets/js/components/hs.header.js')}}></script>
        <script src={{ asset('assets/js/components/hs.hamburgers.js')}}></script>
        <script src={{ asset('assets/js/components/hs.unfold.js')}}></script>
        <script src={{ asset('assets/js/components/hs.focus-state.js')}}></script>
        <script src={{ asset('assets/js/components/hs.malihu-scrollbar.js')}}></script>
        <script src={{ asset('assets/js/components/hs.validation.js')}}></script>
        <script src={{ asset('assets/js/components/hs.fancybox.js')}}></script>
        <script src={{ asset('assets/js/components/hs.onscroll-animation.js')}}></script>
        <script src={{ asset('assets/js/components/hs.slick-carousel.js')}}></script>
        <script src={{ asset('assets/js/components/hs.show-animation.js')}}></script>
        <script src={{ asset('assets/js/components/hs.svg-injector.js')}}></script>
        <script src={{ asset('assets/js/components/hs.go-to.js')}}></script>
        <script src={{ asset('assets/js/components/hs.selectpicker.js')}}></script>

        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

        <script src={{ asset('Material-Toast-master\mdtoast.min.js')}}></script>
        <!-- JS Plugins Init. -->
        <script>
            $(window).on('load', function () {
                // initialization of HSMegaMenu component
                $('.js-mega-menu').HSMegaMenu({
                    event: 'hover',
                    direction: 'horizontal',
                    pageContainer: $('.container'),
                    breakpoint: 767.98,
                    hideTimeOut: 0
                });
            });

            $(document).on('ready', function () {
                // initialization of header
                $.HSCore.components.HSHeader.init($('#header'));

                // initialization of animation
                $.HSCore.components.HSOnScrollAnimation.init('[data-animation]');

                // initialization of unfold component
                $.HSCore.components.HSUnfold.init($('[data-unfold-target]'), {
                    afterOpen: function () {
                        $(this).find('input[type="search"]').focus();
                    }
                });

                // initialization of popups
                $.HSCore.components.HSFancyBox.init('.js-fancybox');

                // initialization of countdowns
                var countdowns = $.HSCore.components.HSCountdown.init('.js-countdown', {
                    yearsElSelector: '.js-cd-years',
                    monthsElSelector: '.js-cd-months',
                    daysElSelector: '.js-cd-days',
                    hoursElSelector: '.js-cd-hours',
                    minutesElSelector: '.js-cd-minutes',
                    secondsElSelector: '.js-cd-seconds'
                });

                // initialization of malihu scrollbar
                $.HSCore.components.HSMalihuScrollBar.init($('.js-scrollbar'));

                // initialization of forms
                $.HSCore.components.HSFocusState.init();

                // initialization of form validation
                $.HSCore.components.HSValidation.init('.js-validate', {
                    rules: {
                        confirmPassword: {
                            equalTo: '#signupPassword'
                        }
                    }
                });

                // initialization of show animations
                $.HSCore.components.HSShowAnimation.init('.js-animation-link');

                // initialization of fancybox
                $.HSCore.components.HSFancyBox.init('.js-fancybox');

                // initialization of slick carousel
                $.HSCore.components.HSSlickCarousel.init('.js-slick-carousel');

                // initialization of go to
                $.HSCore.components.HSGoTo.init('.js-go-to');

                // initialization of hamburgers
                $.HSCore.components.HSHamburgers.init('#hamburgerTrigger');

                // initialization of unfold component
                $.HSCore.components.HSUnfold.init($('[data-unfold-target]'), {
                    beforeClose: function () {
                        $('#hamburgerTrigger').removeClass('is-active');
                    },
                    afterClose: function() {
                        $('#headerSidebarList .collapse.show').collapse('hide');
                    }
                });

                $('#headerSidebarList [data-toggle="collapse"]').on('click', function (e) {
                    e.preventDefault();

                    var target = $(this).data('target');

                    if($(this).attr('aria-expanded') === "true") {
                        $(target).collapse('hide');
                    } else {
                        $(target).collapse('show');
                    }
                });

                // initialization of unfold component
                $.HSCore.components.HSUnfold.init($('[data-unfold-target]'));

                // initialization of select picker
                $.HSCore.components.HSSelectPicker.init('.js-select');
            });
        </script>


<script>
    $(document).ready(function() {
        wishlist_ajax();
        cart_ajax();
    });

function wishlist_ajax(){
    var token = $("#_token").val();
    $.ajax({
    url:'/wishlist_ajax',
    type:'POST',
    data:{_token:token},
    success:function(response)
    {
        $("#wish_value").html(response);
    }
    });  
}

function cart_ajax(){
   var token = $("#_token").val();
   $.ajax({
    url:'/cart_ajax',
    type:'POST',
    data:{_token:token},
    success:function(response)
    {
        var response1=response.split('|aubdghbjncartview|');
        $("#cart-total-res").html(response1[0]);
        $("#cart_qty").html(response1[0]);
        $("#cart-dropdown").html(response1[1]);
    }
    });
}

function cart(id){
    var user=$("#login_data").val();
    if(user!=0){
    var token = $("#_token").val();
        $.ajax({
            url:'/addtocart_ajax',
            type:'POST',
            data:{_token:token,product_id:id},
            success:function(response)
            {
                if(response==2){
                            mdtoast('Item Already Present In The Cart.', { 
                            type: 'info',
                            duration: 1000
                            });
                }else{
                    cart_ajax();
                    mdtoast('Add to Cart successfully.', { 
                    interaction: true, 
                    actionText: 'View Cart',
                    action: function(){
                    this.hide(); 
                    window.location.href="{{route('cart')}}"
                    },
                    });
                }
            }   
        });
    }else{
        mdtoast('Please login first then item added to Cart', { 
        type: 'warning',
        duration: 3000
        });
    }
}
function remove_cart(id){
    var token = $("#_token").val();
    $.ajax({
        url:'/removetocart_ajax',
        type:'POST',
        data:{_token:token,product_id:id},
        success:function(response)
        {
            mdtoast('Remove to cart sucessfully.', { 
            type: 'info',
            duration: 3000
            });
            cart_ajax();
            carttoggle(id);
        }   
    });
}

function wishlist(id){
   var user=$("#login_data").val();
   if(user!=0){
   var token = $("#_token").val();
    $.ajax({
            url:'/addtowishlist_ajax',
            type:'POST',
            data:{_token:token,product_id:id},
            success:function(response)
            {
                if(response==2){
                    mdtoast('Item Already Present In The Wishlist.', { 
                    type: 'info',
                    duration: 1000
                    });
                }else{
                    cart_ajax();
                    wishlist_ajax();
                    mdtoast('Add to wishlist sucessfully.', { 
                    interaction: true, 
                    actionText: 'View Wishlist',
                    action: function(){
                    this.hide(); 
                    window.location.href="/wishlist"
                },
                });
            }
        }   
    });
    }else{
        mdtoast('Please login first then item added to Wishlist', { 
        type: 'warning',
        duration: 3000
        });
    }
}
function remove_wishlist(id){
    var token = $("#_token").val();
    $.ajax({
        url:'/removetowishlist_ajax',
        type:'POST',
        data:{_token:token,product_id:id},
        success:function(response)
        {
            mdtoast('Remove  to Wishlist sucessfully.', { 
            type: 'info',
            duration: 3000
            });
            wishlist_table();
            cart_ajax();
            wishtoggle(id);    
        }   
    });
}
function remove_wishlist1(id){
    var token = $("#_token").val();
    $.ajax({
        url:'/removetowishlist_ajax',
        type:'POST',
        data:{_token:token,product_id:id},
        success:function(response)
        {
            mdtoast('Remove  to Wishlist sucessfully.', { 
            type: 'info',
            duration: 3000
            });
            wishlist_ajax();
            wishlist_table();
        }   
    });
}

$(document).ready(function() {
    $('#subscribee').click(function () {
        var token=$("#_token").val();
        var email=$("#subscribee_email").val();
        $.ajax({ 
            url:'/subscribe',
            type:'POST',
            data:{email:email,_token:token},
            success:function(response)
            {
                if(response==1)
                {
                mdtoast('You are not Registered User', { 
                type: 'warning',
                duration: 3000
                });
            }
            if(response==2)
            {
                $('.popup_off').click();
                mdtoast('You are now Prime Member of E-Kirana', { 
                type: 'success',
                duration: 5000
                });
            }
            if(response==3)
            {
                mdtoast('Please login', { 
                type: 'warning',
                duration: 3000
                });
            }
            $("#subscribee").css("display", "none");
            $("#un_subscribee").css("display", "block");
        }
    })
});

$('#un_subscribee').click(function () {
    var token=$("#_token").val();
    var email=$("#subscribee_email").val();
        $.ajax({
            url:'/unsubscribe',
            type:'POST',
            data:{email:email,_token:token},
            success:function(response)
            {
            mdtoast('Unsubcribe Sucessfully', { 
            type: 'warning',
            duration: 3000
            });
            $("#un_subscribee").css("display", "none");
            $("#subscribee").css("display", "block");
            }
        })
    });
});


</script>
<script>
  $(document).ready(function(){
    $(".custom-menu").hover(function(){
      $(".custom-menu").removeClass('active');
      $(this).addClass('active');
    },
    function(){
    });
  });
  </script>

    </body>
</html>
