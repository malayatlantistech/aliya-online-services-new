<!doctype html>
<html class="no-js" lang="en">
<head>

  <!-- =====  BASIC PAGE NEEDS  ===== -->
  <meta charset="utf-8">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>@yield('title')</title>

  <!-- =====  SEO MATE  ===== -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="distribution" content="global">
  <meta name="revisit-after" content="2 Days">
  <meta name="robots" content="ALL">
  <meta name="rating mt-5 mb-5" content="8 YEARS">
  <meta name="GOOGLEBOT" content="NOARCHIVE">

  <!-- =====  MOBILE SPECIFICATION  ===== -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- =====  CSS  ===== -->
  <link rel="stylesheet" href="/use_assets/css/bootstrap.css" />
  <link rel="stylesheet" href="/use_assets/css/font-awesome.min.css" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <link rel="stylesheet" href="/use_assets/css/meanmenu.css">
  <link rel="stylesheet" href="/use_assets/css/style.css">  
  <link rel="stylesheet" href="/use_assets/css/magnific-popup.css">
  <link rel="stylesheet" href="/use_assets/css/owl.carousel.css">
  <link rel="shortcut icon" href="/use_assets/images/favicon.png">
  <link rel="apple-touch-icon" href="/use_assets/images/apple-touch-icon.png">
  <link rel="apple-touch-icon" sizes="72x72" href="/use_assets/images/apple-touch-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="114x114" href="/use_assets/images/apple-touch-icon-114x114.png">

  <link rel="stylesheet" href="/Material-Toast-master\mdtoast.min.css" />
</head>
<style>
.main_cat:hover{
  height: auto !important;
  visibility: visible !important;
  opacity: 1 !important;
  overflow: visible !important;
}

#overlay{	
	position: fixed;
	top: 0;
	z-index: 100;
	width: 100%;
	height:100%;
	display: none;
	background: rgba(0,0,0,0.6);
}
.cv-spinner {
	height: 100%;
	display: flex;
	justify-content: center;
	align-items: center;  
}
.spinner {
	width: 80px;
	height: 80px;
	border: 4px #ddd solid;
	border-top: 4px #2e93e6 solid;
	border-radius: 50%;
	animation: sp-anime 0.8s infinite linear;

}
@keyframes sp-anime {
	100% { 
		transform: rotate(360deg); 
	}
}
#text{
  position: absolute;
  top: 35%;
  left: 50%;
  font-size: 30px;
  color: white;
  transform: translate(-50%,-50%);
  -ms-transform: translate(-50%,-50%);	
 
}
#text1{
  position: absolute;
  top: 96%;
  left: 50%;
  font-size: 17px;
  color: #ffffffb8;
  
  transform: translate(-50%,-50%);
  -ms-transform: translate(-50%,-50%);
}
.is-hide{
	display:none;
}
.dropdown-item {
   
   padding: 0.25rem 0;
 
}

 @media only screen and (max-width: 600px) {
  .aa
  {
    display:none;
  }
  .bb
  {
    display:block;
  }
  .pb-15 {
   padding: 11px 0px 10px 0px;
   
    margin-top: 10px;

}
#top .responsive-bar .fa-bars::before {
    content: "";
    background: url(/use_assets/images/sprite.png) no-repeat -1px -65px #333 !important;
    top: 25px;
    left: 0px;
    width: 43px;
    height: 41px !important;
    border-radius: 5px;
    position: absolute;
    z-index: 9;
}
#responsive-menu{
    padding-top: 30px !important;
}
.apps-hide{
    display:none;
}
#brand_carouse{
        padding-bottom: 80px;
}
.header-static-block{
        border-top: solid 1px #bebebe;
}
.header{
    border-bottom: solid 1px #bebebe;
}
.breadcrumb {
    display:none;
}

 }
  @media only screen and (min-width: 600px) {
  .aa
  {
    display:block;
  }
  .bb
  {
    display:none;
  }

}

@media (max-width: 767px){
  .icon-block .search_icon .fa:before {
    background: none !important;
    content: "\f004";
    font-size: x-large;
}
.icon-block .login_icon .fa:before {
    background: url('https://static.thenounproject.com/png/52864-200.png') !important;
    content: "";
    font-size: x-large;
}
.icon-block .telephone_icon .fa:before {
    background: none !important;
    content: "\f022";
    font-size: x-large;
}

}

</style>
<body>
  <div id="overlay" style="    z-index: 1002;">
    <div id="text" style="display:none">Do not press back button or refresh the page.</div>
      <div class="cv-spinner">
        <span class="spinner"></span>
      </div>
        <div id="text1" style=""><center><font style="font-size:14px">Design and develop by</font><br><span style="line-height: 0.8;">Quantex Consulting Services Pvt. Ltd.</span><center></div>
    </div>
  <!-- =====  LODER  ===== -->
  <div class="loder"></div>
  @guest
<input type="hidden" id="login_data" value="0">

@else

<input type="hidden" id="login_data" value="{{Auth::user()->id}}">
@endguest
  <div class="wrapper">

    <!-- Modal -->
    <div id="subscribe-me-" class="modal animated" role="dialog" data-keyboard="true" tabindex="-1">
      <div class="newsletter-popup row align-items-center py-4  px-2"> 
        <img src="/use_assets/images/newsbg.jpg" alt="offer" class="offer col d-none d-sm-block">
        <div class="col-auto newsletter-popup-static newsletter-popup-top">
          <div class="popup-text">
            <div class="popup-title">50% <span>off</span></div>
            <div class="popup-desc mb-30">
              <div>Sign up and get 50% off your next Order</div>
            </div>
          </div>
          <form onsubmit="return  validatpopupemail();" method="post">
            <div class="form-group required">
              <input type="email" name="email-popup" id="email-popup" placeholder="Enter Your Email" class="form-control input-lg" required="">
            </div>
            <div class="form-group required">
              <button type="submit" class="btn" id="email-popup-submit">Subscribe</button>
            </div>
            <div class="form-check">
              <input type="checkbox" class="form-check-input" id="checkme">
              <label class="form-check-label" for="checkme">Dont show again</label>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- Modal End -->

    <!-- =====  Nav START  ===== -->
    <nav id="top">
      <div class="container">
        <div class="row"> <span class="responsive-bar"><i class="fa fa-bars"></i></span>
          <div class="header-middle-outer closetoggle">
            <div id="responsive-menu" class="nav-container1 nav-responsive navbar">
              <div class="navbar-collapse navbar-ex1-collapse collapse">
                <ul class="nav navbar-nav">
                  @php($cat=DB::table('cats')->get())
                  @foreach($cat as $key=>$cats)
                  <li class="collapsed" data-toggle="collapse" data-target="#GroceryampStaples{{$cats->cat_id}}"><a href="/shop?cat_id={{$cats->cat_id}}">{{$cats->cat_name}}</a> <span><i class="fa fa-plus"></i></span>
                    @php($sub_cat_count=DB::table('sub_cats')->where('cat_id',$cats->cat_id)->count())
                    @if($sub_cat_count!=0)
                    @php($sub_cat=DB::table('sub_cats')->where('cat_id',$cats->cat_id)->get())
                    <ul class="menu-dropdown collapse" id="GroceryampStaples{{$cats->cat_id}}">
                      @foreach($sub_cat as $a=>$sub_cats)
                      <li class="dropdown"><a href="/shop?cat_id={{$cats->cat_id}}&sub_cat_id={{$sub_cats->sub_cat_id}}">{{$sub_cats->sub_cat_name}}</a>

                        @php($sub_sub_cat_count=DB::table('sub_sub_cats')->where('sub_cat_id',$sub_cats->sub_cat_id)->count())
                        @if($sub_sub_cat_count!=0)
                        <ul class="list-unstyled childs_2">
                          @php($sub_sub_cat=DB::table('sub_sub_cats')->where('sub_cat_id',$sub_cats->sub_cat_id)->get())
                          @foreach($sub_sub_cat as $sub_sub_cats)
                          <li><a href="/shop?cat_id={{$cats->cat_id}}&sub_cat_id={{$sub_cats->sub_cat_id}}&sub_sub_cat_id={{$sub_sub_cats->sub_sub_cat_id}}">{{$sub_sub_cats->sub_sub_cat_name}}</a></li>
                          @endforeach
                         
                        </ul>
                        @endif

                      </li>
                    @endforeach
                    </ul>

                    @endif
                  </li>
                  @endforeach

                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </nav>
    <!-- =====  Nav END  ===== -->

    <!-- =====  HEADER START  ===== -->
    <header id="header" class="section" style="background-color: #f2f2f2;">
      <div class="container">
        <div class="header-top py-1">
          <div class="row align-items-center">
            <div class="col-md-6">
              {{-- <ul class="header-top-left pull-left">              
                <li class="language dropdown px-2"> <span class="dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">Language <span class="caret"></span> </span>
                  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    <li><a href="#">English</a></li>
                    <li><a href="#">French</a></li>
                    <li><a href="#">German</a></li>
                  </ul>
                </li>
                <li class="currency dropdown pr-2"> <span class="dropdown-toggle" id="dropdownMenu12" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">Currency <span class="caret"></span> </span>
                  <ul class="dropdown-menu" aria-labelledby="dropdownMenu12">
                    <li><a href="#">€ Euro</a></li>
                    <li><a href="#">£ Pound Sterling</a></li>
                    <li><a href="#">$ US Dollar</a></li>
                  </ul>
                </li>
              </ul>  --}}
            </div>
            <div class="col-md-6" >
              <ul class="header-top-left pull-left" style="float: right;">    
                <li class="telephone">
                  <a href="tel:918521340430"><i class="fa fa-phone"></i> +91 8521340430</a> 
                </li>
                <li class="telephone">
                </li>
                <li class="telephone">
                </li>
                <li class="telephone">
                </li>
                @auth
                
              <li class="language dropdown px-2 login"> <span class="dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">{{Auth::user()->name}} <span class="caret"></span> </span>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                  <li><a href="/profile">My Profile</a></li>
                  <li><a href="/order">My Order</a></li>
                  <li><a href="/wishlist">My Wishlist</a></li>
                  <li><a href="/offer">My Offer</a></li>
                  <li><a href="/wallet">My Wallet</a></li>
                  <li class="">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                      </a>
                    </li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                   </form>
                </ul>
              </li>
               
           


                @else
                <li class="login">
                  <a href="/login"><i class="fa fa-user"></i>Login</a>
                </li>

                <li class="register">
                  <a href="/register">Signup</a>
                </li>

               
                @endauth
               


              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="header section pt-15 pb-15">
        <div class="container">
          <div class="row">
            <div class="navbar-header col-2 header-bottom-left"> <a class="navbar-brand" href="/" style="    font-size: 30px;"> 
           <img alt="Bigmarket" src="/use_assets/images/logo.png">
            </a> </div>
            <div class="col-10 header-bottom-right">
              <div class="header-menu"  style="background-color: #c1c3c54a;">
                <div class="responsive-menubar-block">
                  <span>Shop By<br> Category</span>
                  <span class="menu-bar collapsed" data-target=".navbar-ex1-collapse" data-toggle="collapse"><i class="fa fa-bars"></i></span>
                </div>
                <nav id="menu" class="navbar">
                  <div class="collapse navbar-collapse navbar-ex1-collapse">
                    
                    <ul class="nav navbar-nav main-navigation">


                      @php($cat=DB::table('cats')->get())
                      @foreach($cat as $key=>$cats)
                      <li class="main_cat dropdown custom-menu @if($key==0)active @endif"> <a href="/shop?cat_id={{$cats->cat_id}}">{{$cats->cat_name}}</a>

                        @php($sub_cat_count=DB::table('sub_cats')->where('cat_id',$cats->cat_id)->count())
                        @if($sub_cat_count!=0)
                        <div class="dropdown-menu megamenu column4" style="background-color: #f2f2f2;">
                     
                          <div class="dropdown-inner">
                          
                          
                              @php($sub_cat_count=DB::table('sub_cats')->where('cat_id',$cats->cat_id)->count())
                              @if($sub_cat_count!=0)
                              @php($sub_cat=DB::table('sub_cats')->where('cat_id',$cats->cat_id)->get())
                              @foreach($sub_cat as $a=>$sub_cats)
                              <!-- 2 Level Sub Categories START -->
                              @if($a==5 || $a==10 || $a==15)
                              <br>
                              @endif
                            <ul class="list-unstyled childs_1">

                            <li class="dropdown active"><a href="/shop?cat_id={{$cats->cat_id}}&sub_cat_id={{$sub_cats->sub_cat_id}}">{{$sub_cats->sub_cat_name}}</a>
                                <div class="dropdown-menu">
                                  <div class="dropdown-inner">
                                    @php($sub_sub_cat_count=DB::table('sub_sub_cats')->where('sub_cat_id',$sub_cats->sub_cat_id)->count())
                                    @if($sub_sub_cat_count!=0)
                                    <ul class="list-unstyled childs_2">
                                      @php($sub_sub_cat=DB::table('sub_sub_cats')->where('sub_cat_id',$sub_cats->sub_cat_id)->get())
                                      @foreach($sub_sub_cat as $sub_sub_cats)

                                      <li class="active"><a href="/shop?cat_id={{$cats->cat_id}}&sub_cat_id={{$sub_cats->sub_cat_id}}&sub_sub_cat_id={{$sub_sub_cats->sub_sub_cat_id}}">{{$sub_sub_cats->sub_sub_cat_name}}</a></li>
                               
                                      @endforeach
                                    </ul>
                                    @endif
                                 
                                  </div>
                                </div>
                              </li>
                              </ul>
                            
                              @endforeach

                             
                              @endif
                           
                              
                              <!-- 2 Level Sub Categories END -->
                            
              
                          </div>
                       
                        </div>
                        @endif
                      </li>
                      @endforeach



                    </ul>
                  </div>
                </nav>
              </div>
              <div class="header-link-search">
                <div class="header-search">
                  <div class="actions">
                      <button type="submit" title="Search" class="action search" id="head-search"></button>
                  </div>
                  <form action="/shop" method="get">
                  <div id="search" class="input-group">
                    <input type="text" id="search-input"  name="Product_name" value="" placeholder="Search" class="form-control input-lg" autocomplete="off" >
                    <span class="input-group-btn">
                      <button type="button" class="btn btn-default btn-lg bb" >Search</button>
                      
                      <input type="submit" value="Search" class="btn btn-default btn-lg aa"> 
                     
                    </span>
                  </div>
                  </form>



                </div>
                <div class="header-link">
                 <ul class="list-unstyled">
                  <li><a href="/">Home</a></li>
                  <li><a href="/about">About Us</a></li>
                  <li><a href="/shop">Shop</a></li>
                  <li><a href="/contact">Contact Us</a></li>
                </ul>
              </div>
              </div>
              <div class="shopcart">
                <div id="cart" class="btn-block mt-40 mb-30 ">
                  @if(basename($_SERVER['REQUEST_URI'])!="cart" && basename($_SERVER['REQUEST_URI'])!="checkout")
                  <button type="button" class="btn" data-target="#cart-dropdown" data-toggle="collapse" aria-expanded="true"><span id="shippingcart">My basket</span><span id="cart-total">Item <i id="cart_qty">0</i></span></button>
                @else
                <button type="button" class="btn"><span id="shippingcart">My basket</span><span id="cart-total">Item <i id="cart_qty">0</i></span></button>
                
                  @endif
                  <a href="/cart" class="cart_responsive btn"><span id="cart-text">My basket</span><span id="cart-total-res">0</span> </a>
                </div>
                <div id="cart-dropdown" class="cart-menu collapse">
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="header-static-block">
        <div class="container">
          <div class="row">
            <div class="icon-block">
              <div class="home_icon">
              <a href="/"><i class="fa fa-home"></i>Home</a>
              </div>
              
              <div class="cart_icon">
              </div>
              <div class="search_icon">
                <a href="/wishlist"><i class="fa fa-heart " style="color:red"></i>Wishlist</a>
                </div>
              <div class="login_icon">
                <a href="/wallet" style="padding-top:0px"><img src="/use_assets/images/52864-200.png" style="height: 29px;padding: 3px;"><br>Wallet</a>
              </div>
              <div class="telephone_icon">
                <a href="/profile" style="padding-top:0px"><img src="/use_assets/images/menu-bar-order-my-order-icon-11563381244rqqi52g1re.png" style="height: 29px;padding: 3px;"><br>My profile</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
    <!-- =====  HEADER END  ===== -->



    @yield('content')



    <!-- =====  FOOTER START  ===== -->
    <div class="footer section pt-40 apps-hide" style="background-color: #f2f2f2;">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 footer-block">
            <h4 class="footer-title py-2">Information</h4>
            <ul>
              <li><a href="/about">About Us</a></li>
              <li><a href="privacy">Privacy Policy</a></li>
              <li><a href="/trams">Terms & Conditions</a></li>
              <li><a href="/return">Return and Cancelation Policy</a></li>
              <li><a href="/contact">Contact Us</a></li>
            </ul>
          </div>
          <div class="col-lg-3 footer-block">
            <h4 class="footer-title py-2">Services</h4>
            <ul>
           
              <li><a href="/cart">Busket</a></li>
              <li><a href="/wishlist">Wish List</a></li>
              <li><a href="/profile">My Account</a></li>
              <li><a href="/order">Order History</a></li>
              <li><a href="/offer">My Offer</a></li>
            </ul>
          </div>
          <div class="col-lg-3 footer-block">
            <h4 class="footer-title py-2">Extras</h4>
            <ul>
              <li><a href="/">Home</a></li>
              <li><a href="/">Shop</a></li>
           
              <li><a href="/faq">Frequently Asked Questions</a></li>
           
            
            </ul>
          </div>
          <div class="col-lg-3 footer-block">
            <h4 class="footer-title py-2">Contacts</h4>
            <ul>
              <li class="add">Abdullapur, Nimaithi, Darbhanga, <br>Bihar - 847101</li>
              <li class="phone">+91 8521340430
            
              <li class="email">Support@ekirana.co.in</li>
            </ul>
          </div>
        </div>
        
        <!-- =====  Newslatter ===== -->
        <div class="newsletters mt-30"  style="background-color: #c1c3c54a;">
          <div class="news-head pull-left">
            <h2>Subscribe for our offer news</h2>
          </div>
          <div class="news-form pull-right">

       
            @guest
              <div class="form-group required">
                <input name="email" id="subscribeSrEmail" placeholder="Enter Your Email" class="form-control input-lg" required="" type="email">
                <button type="button" id="subscribee" class="btn btn-default btn-lg">Subscribe</button>
              </div>
            @else
            @php($id= Auth::user()->id )
            @php($Subscribe=DB::table('subscribers')->where('user_id',$id)->count())
            <div class="form-group required">
              <input name="email" id="subscribeSrEmail" value="{{Auth::user()->email}}" placeholder="Enter Your Email" class="form-control input-lg" required="" type="email">
              @if($Subscribe=='0')
              <button type="button" id="subscribee" class="btn btn-default btn-lg">Subscribe</button>
              <button type="button" id="un_subscribee" class="btn btn-default btn-lg" style="display:none">Unsubscribe</button>
              @else
              <button type="button" id="subscribee" class="btn btn-default btn-lg" style="display:none">Subscribe</button>
              <button type="button" id="un_subscribee" class="btn btn-default btn-lg">Unsubscribe</button>
              @endif
            </div>
                
            @endguest
            
        
          </div>
        </div>
        <!-- =====  Newslatter End ===== -->
      </div>

      <div class="footer-bottom" style="background-color: #f2f2f2;">
        <div class="container">
          <div class="row">
            <div class="col-12 col-lg-4 mt-20">
              <div class="section_title">payment option </div>
              <div class="payment-icon text-center">
                <ul>
                  <li><i class="fab fa-cc-paypal"></i></li>
                  <li><i class="fab fa-cc-visa"></i></li>
                  <li><i class="fab fa-cc-discover"></i></li>
                  <li><i class="fab fa-cc-mastercard"></i></li>
                  <li><i class="fab fa-cc-amex"></i></li>
                </ul>
              </div>
            </div>

            <div class="col-12 col-lg-4 mt-20">
              <div class="section_title">download app</div>
              <div class="app-download text-center">
                <ul class="app-icon">
                  <li><a href="#" title="playstore"><img src="/use_assets/images/play-store.png" alt="playstore" class="img-responsive"></a></li>
                  <li><a href="#" title="appstore"><img src="/use_assets/images/app-store.png" alt="appstore" class="img-responsive"></a></li>
                </ul>
              </div>
            </div>
            <div class="col-12 col-lg-4 mt-20">
              <div class="section_title">Social media</div>
              <div class="social_icon text-center">
                <ul>
                  <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
                  <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                  <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                  <li><a href="#"><i class="fas fa-rss"></i></a></li>
                </ul>
              </div>
            </div>
           
          </div>
        </div>
         <div class="col-12 " style="    background-color: #e4e5e5;">
              <div class="copyright-part text-center pt-10 pb-10 mt-30">Copyright 2020 © All Rights Reserved. E-Kirana. <p class=" text-sm font-weight-500" >Design and developed by <a data-toggle="" title="" href="https://quantex.co.in" target="_blank" data-original-title="Quantex Consulting Services Pvt. Ltd."><img src="/qcs_logo/qcs_logo.png" style="height:20px" ></a></p></div>
            </div>
      </div>
    </div>
    <input type="hidden" id="_token" value="<?php echo csrf_token();?>">
    <!-- =====  FOOTER END  ===== -->
  </div>
  <a id="scrollup"></a>
  <script src="/use_assets/js/modernizr.js"></script>
  <script src="/use_assets/js/jQuery_v3.1.1.min.js"></script>
  <script src="/use_assets/js/owl.carousel.min.js"></script>
  <script src="/use_assets/js/popper.min.js"></script>
  <script src="/use_assets/js/bootstrap.min.js"></script>
  <script src="/use_assets/js/jquery.magnific-popup.js"></script>
  <script src="/use_assets/js/jquery.firstVisitPopup.js"></script>
  <script src="/use_assets/js/custom.js"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script src="/Material-Toast-master\mdtoast.min.js"></script>
</body>

<script>
    $(document).ready(function() {
    wishlist_ajax();
    cart_ajax();
   
});


function wishlist_ajax(){
//   alert();
   var token = $("#_token").val();
$.ajax({

    url:'wishlist_ajax',

    type:'POST',

    data:{_token:token},
   
  success:function(response)
    {
    // alert(response);
   
        $("#wish_value").html(response);


  
    }

    });  
}



   function cart_ajax(){
//   alert();
   var token = $("#_token").val();
$.ajax({

    url:'/cart_ajax',

    type:'POST',

    data:{_token:token},
   
  success:function(response)
    {
   //  alert(response);
    
   var response1=response.split('|aubdghbjncartview|');
    
    $("#cart-total-res").html(response1[0]);
    $("#cart_qty").html(response1[0]);
   
    $("#cart-dropdown").html(response1[1]);
   // $("#cart_value1").html(response1[0]);
   // $("#cart-mobile").html(response1[1]);
       
  
    }

    });
}


function wishlist(id,price_id){
  //  alert(id);
    var user=$("#login_data").val();
   if(user!=0){
//   alert();
   var token = $("#_token").val();
$.ajax({

    url:'addtowishlist_ajax',


    type:'POST',

    data:{_token:token,product_id:id,product_price_id:price_id},
   
  success:function(response)
    {
     //alert(response);
   if(response==2){
    mdtoast('Iteam alredy in Wishlist.', { 
    type: 'info',
    duration: 1000
    });
   }else{
        
        cart_ajax();
        wishlist_ajax();
        
        mdtoast('Add to wishlist sucessfully.', { 
    interaction: true, 
    actionText: 'View Wishlist',
    
    action: function(){
        this.hide(); 
        window.location.href="wishlist"
    },
    

    });
}
}   
 });
  
  
}else{
   
   // swal('Please Login');
    mdtoast('Please Login', { 
  type: 'warning',
  duration: 3000
});
}
}


function cart(id){
    
    //alert(id);
var user=$("#login_data").val();
if(user!=0){

var token = $("#_token").val();
$.ajax({

    url:'/addtocart_ajax',

    type:'POST',

    data:{_token:token,product_id:id},
   
  success:function(response)
    {
       
     //alert(response);
   if(response==2){
      // swal('Already Exits');
      mdtoast('Item Already Present In The Basket.', { 
        type: 'info',
        duration: 1000
        });
   }else{

    cart_ajax();

    mdtoast('Add to Basket successfully.', { 
        interaction: true, 
        actionText: 'View Basket',
        
        action: function(){
            this.hide(); 
            window.location.href="{{route('cart')}}"
        },
 

        });

        }
    }   
    });
    
    
    }else{
    
        mdtoast('Please login first then item added to Basket', { 
    type: 'error',
    duration: 3000
    });
    }
    
}
function remove_cart(id){
    //alert(id);
   
   var token = $("#_token").val();
 //  alert();
$.ajax({

    url:'/removetocart_ajax',

    type:'POST',

    data:{_token:token,product_id:id},
   
  success:function(response)
    {
      
 mdtoast('Remove to Basket sucessfully.', { 
  type: 'info',
  duration: 3000
});
        cart_ajax();
        carttoggle(id);
       // cart_ajax();
    }   
 });
  
  

   
}

function wishlist(id){
  //  alert(id);
    var user=$("#login_data").val();
   if(user!=0){
//   alert();
   var token = $("#_token").val();
$.ajax({

    url:'/addtowishlist_ajax',


    type:'POST',

    data:{_token:token,product_id:id},
   
  success:function(response)
    {
     //alert(response);
   if(response==2){
    mdtoast('Item Already Present In The Wishlist.', { 
    type: 'info',
    duration: 1000
    });
   }else{
        
        cart_ajax();
       // wishlist_ajax();
        
        mdtoast('Add to wishlist sucessfully.', { 
    interaction: true, 
    actionText: 'View Wishlist',
    
    action: function(){
        this.hide(); 
        window.location.href="/wishlist"
    },
    

    });
}
}   
 });
  
  
}else{
   
   // swal('Please Login');
    mdtoast('Please login first then item added to Wishlist', { 
  type: 'warning',
  duration: 3000
});
}
}
function remove_wishlist(id){
  //  alert(id);
   
   var token = $("#_token").val();
 //  alert();
$.ajax({

    url:'/removetowishlist_ajax',
    type:'POST',
    data:{_token:token,product_id:id},
   
  success:function(response)
    {
mdtoast('Remove  to Wishlist sucessfully.', { 
  type: 'info',
  duration: 3000
});
wishtoggle(id);
        cart_ajax();
       // wishlist_ajax();
        wishlist_table();
        
        
    }   
 });
  
  

   
}
function remove_wishlist1(id){
  //  alert(id);
   
   var token = $("#_token").val();
 //  alert();
$.ajax({

    url:'/removetowishlist_ajax',
    type:'POST',
    data:{_token:token,product_id:id},
   
  success:function(response)
    {
mdtoast('Remove  to Wishlist sucessfully.', { 
  type: 'info',
  duration: 3000
});
//wishtoggle(id);
     //   cart_ajax();
       // wishlist_ajax();
        wishlist_table();
        
        
    }   
 });
  
  

   
}

$(document).ready(function() {
$('#subscribee').click(function () {

  var token=$("#_token").val();
  var email=$("#subscribee_email").val();
 
$.ajax({
  
 url:'/subscribe',

 type:'POST',

 data:{email:email,_token:token},


success:function(response)
 {

if(response==1)
{
  mdtoast('You are not Registered User', { 
  type: 'warning',
  duration: 3000
});


}

if(response==2)
{
    $('.popup_off').click();
  mdtoast('You are now Prime Member of E-Kirana', { 
  type: 'success',
  duration: 5000
});
}
if(response==3)
{
  
  mdtoast('Please login', { 
  type: 'warning',
  duration: 3000
});
}
   // swal('You are now Prime Member of Auveraa Life Style');
    //location.reload();

    $("#subscribee").css("display", "none");
$("#un_subscribee").css("display", "block");
 }

 })
});

$('#un_subscribee').click(function () {

var token=$("#_token").val();
var email=$("#subscribee_email").val();

$.ajax({

url:'/unsubscribe',

type:'POST',

data:{email:email,_token:token},


success:function(response)
{
  mdtoast('Unsubcribe Sucessfully', { 
  type: 'warning',
  duration: 3000
});
$("#un_subscribee").css("display", "none");
$("#subscribee").css("display", "block");
   // location.reload();

}

})
});
});


</script>
<script>
  $(document).ready(function(){
    $(".custom-menu").hover(function(){
    //  alert(this.class);
      $(".custom-menu").removeClass('active');
      $(this).addClass('active');
    },
    function(){
      //$(this).removeClass('active');
    });
  });
  </script>



</html>