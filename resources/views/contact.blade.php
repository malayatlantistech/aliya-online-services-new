@extends('layouts.menu')

@section('title')
Contact Us : E-Kirana
@endsection

@section('content')

    <!-- =====  BREADCRUMB STRAT  ===== -->
    <div class="breadcrumb section pt-60 pb-60">
        <div class="container">
          <h1 class="uppercase">Contact Us</h1>
          <ul>
            <li><a href="/"><i class="fa fa-home"></i></a></li>
            <li class="active">Contact Us</li>
          </ul>
        </div>
      </div>
      <!-- =====  BREADCRUMB END===== -->
      <div class="page-contact section">
      <!-- =====  CONTAINER START  ===== -->
      <div class="container">
        <div class="row ">
       
          <div class="col-lg-12 col-xl-12">
            <!-- contact  -->
            <div class="row">
              <div class="col-md-4 col-xs-12 contact">
                <div class="location mb-50">
                  <h4 class="capitalize mb-20"><strong>Our Location</strong></h4>
                  <div class="address">Office address
                    <br> Abdullapur, Nimaithi, Darbhanga, <br>Bihar - 847101
                   </div>
                  <div class="call mt-10"><i class="fas fa-mobile-alt"></i> +91 8521340430</div>
                </div>
                <div class="Career mb-50">
                  <h4 class="capitalize mb-20"><strong>Careers</strong></h4>
                  <div class="address">dummy text ever since the 1500s, simply dummy text of the typesetting industry. </div>
                  <div class="email mt-10"><i class="far fa-envelope"></i> <a href="mailto:careers@yourdomain.com" target="_top"> Support@ekirana.co.in</a></div>
                </div>
                <div class="Hello mb-50">
                  <h4 class="capitalize mb-20"><strong>Say Hello</strong></h4>
                  <div class="address">simply dummy text of the printing and typesetting industry.</div>
                  <div class="email mt-10"><i class="far fa-envelope"></i> <a href="mailto:info@yourdomailname.com" target="_top"> Support@ekirana.co.in</a></div>
                </div>
              </div>
              <div class="col-md-8 col-xs-12 contact-form mb_50">
                <!-- Contact FORM -->
                <div id="contact_form">
              
                  <div class="form-group required">
                    <input type="text" name="name" class="form-control" id="con_name" data-required="true" placeholder="Enter Name">
                  </div>
                  <div class="form-group required mt-30">
                    <input type="email" name="email" class="form-control" id="con_email" data-required="true" aria-describedby="exampleInputEmail1" placeholder="Email Address">
                  </div>
                 
                  <div class="form-group required mt-30">
                    <input type="text" name="subject" class="form-control" id="con_subject" data-required="true" placeholder="Subject">
                  </div>
                  <div class="form-group required mt-30">
                    <textarea class="form-control" id="con_message" name="message" placeholder="Message" data-required="true"></textarea>
                  </div>
                  
                  <button type="submit" id="submit" class="btn mt-30 mb-20">Send Message</button>
         
  
  
                  <div id="contact_results"></div>
                </div>
                <!-- END Contact FORM -->
              </div>
            </div>
            <!--Google map-->
            <div id="map-container-google-1" class="z-depth-1-half map-container mb-40">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7169.792125880557!2d85.97084617688768!3d26.036956584050202!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39edc1013e137185%3A0x5ca82528ed17ea2e!2sNimaithi%2C%20Bihar%20847101!5e0!3m2!1sen!2sin!4v1598610423919!5m2!1sen!2sin" allowfullscreen></iframe>
            </div>
  
            <!--Google Maps-->
  
          </div>
        </div>
      </div>
      <hr>
    </div>
    <!-- =====  CONTAINER END  ===== -->
  </div>

        <script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous">
</script>
<script>
 $(document).ready(function() {
   
    $('#submit').click(function () {
       
       var token=$("#_token").val();
       var name=$("#con_name").val();
       var email=$("#con_email").val();
       var subject=$("#con_subject").val();
       var message=$("#con_message").val();
        if(name.length==0){
            mdtoast('Please Enter Your Name', { 
                        type: 'warning',
                        duration: 3000
                        });
     
        }else if(email.length==0){
            mdtoast('Please Enter Your email', { 
                        type: 'warning',
                        duration: 3000
                        });
        }else if(subject.length==0){
            mdtoast('Please Enter Subject', { 
                        type: 'warning',
                        duration: 3000
                        });
        }else if(message.length==0){
            mdtoast('Please Enter Message', { 
                        type: 'warning',
                        duration: 3000
                        });
        }else{
            $.ajax({

url:'/contactmail',

type:'POST',

data:{name:name,email:email,subject:subject,message:message,_token:token},


success:function(response)
{
    if(response==1){
        mdtoast('Mail send successfully', { 
        type: 'success',
        duration: 3000
        });

    }else{
        mdtoast('Mail cannot send. Please try again later', { 
        type: 'error',
        duration: 3000
        });
    }
    
}

})
        }



      




    });
});
</script>
        @endsection