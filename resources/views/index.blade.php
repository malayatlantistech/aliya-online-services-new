@extends('layouts.menu')

@section('title')
Online Shopping Site for Fashion : ALIYA Online Services
@endsection
@section('content')

  <!-- ========== MAIN CONTENT ========== -->
  <main id="content" role="main">
    <!-- Slider Section -->
   
    
    <div class="mb-4">
        <div class="bg-img-hero" style="background-image: url(../../assets/img/1920X422/img2.jpg);">
            <div class=" overflow-hidden">
                <div class="js-slick-carousel u-slick"
                    data-pagi-classes="text-center position-absolute right-0 bottom-0 left-0 u-slick__pagination u-slick__pagination--long justify-content-center mb-3 mb-md-4">
                
                            @foreach($banner as $banner1)
                                    <div class="js-slide ">
                                        <div class="row pt-7 py-md-0">
                                            <div class="d-none d-wd-block offset-1"></div>
                                            {{-- <div class="col-xl col col-md-6 mt-md-8 mt-lg-10">
                                                <div class="ml-xl-4">
                                                    <h6 class="font-size-15 font-weight-bold mb-2 text-cyan"
                                                        data-scs-animation-in="fadeInUp">
                                                        SHOP TO GET WHAT YOU LOVE
                                                    </h6>
                                                    <h1 class="font-size-46 text-lh-50 font-weight-light mb-8"
                                                        data-scs-animation-in="fadeInUp"
                                                        data-scs-animation-delay="200">
                                                        TIMEPIECES THAT MAKE A STATEMENT UP TO <stong class="font-weight-bold">40% OFF</stong>
                                                    </h1>
                                                    <a href="../shop/single-product-fullwidth.html" class="btn btn-primary transition-3d-hover rounded-lg font-weight-normal py-2 px-md-7 px-3 font-size-16"
                                                        data-scs-animation-in="fadeInUp"
                                                        data-scs-animation-delay="300">
                                                        Start Buying
                                                    </a>
                                                </div>
                                            </div> --}}
                                            <div class="col-xl-12 col-12 d-flex align-items-end ml-auto ml-md-0"
                                                {{-- data-scs-animation-in="fadeInLeft" --}}
                                                data-scs-animation-delay="500"
                                                data-scs-autoplay="true"
                                                data-scs-autoplaySpeed="1000"

                                                >
                                                <img class="img-fluid ml-auto mr-10 mr-wd-auto" src="banner/{{$banner1->display_banner}}" alt="Image Description">
                                            </div>
                                        </div>
                                    </div>
                             @endforeach

                            



                </div>
            </div>
        </div>
    </div>
  
  
  

    <!-- End Slider Section -->
    <div class="container">
        <!-- Banner -->
        <div class="row mb-6 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
            
        @foreach($coupon_banner as $coupon_banner1)
            <div class="col-md-6 mb-6 mb-xl-0 col-xl-6 col-wd-6 flex-shrink-0 flex-xl-shrink-1">
                <a href="../shop/shop.html" class="min-height-146 py-1 py-xl-2 py-wd-1 banner-bg d-flex align-items-center text-gray-90">
                    <div class="col-6 col-xl-7 col-wd-6 pr-0">
                        <img class="img-fluid" src="coupon_image/{{$coupon_banner1->coupon_image}}" alt="Image Description">
                    </div>
                    <div class="col-6 col-xl-5 col-wd-6 pr-xl-4 pr-wd-3">
                        <div class="mb-2 pb-1 font-size-18 font-weight-light text-ls-n1 text-lh-23">
                            Get {{$coupon_banner1->coupon_value}} @if($coupon_banner1->coupon_type=='FLAT') Flat  @else  %  @endif off
                        </div>
                        <div class="link text-gray-90 font-weight-bold font-size-15" href="#">
                            Coupon code : {{$coupon_banner1->coupon_code}}
                        </div>
                        <div class="link text-gray-90 font-weight-bold font-size-15" href="#">
                            Min Order Value ₹{{$coupon_banner1->coupon_value}} 
                          
                        </div>
                    </div>
                </a>
            </div>
        @endforeach
        </div>
        <!-- End Banner -->
    </div>
    
    <!-- End Products-8-1 -->
    <div class="container">
      
        <!-- New Ariaval -->
        <div class="position-relative">
            <div class="border-bottom border-color-1 mb-2">
                <h3 class="section-title section-title__full d-inline-block mb-0 pb-2 font-size-22">New Arrival</h3>
            </div>
            <div class="js-slick-carousel u-slick position-static overflow-hidden u-slick-overflow-visble pb-7 pt-2 px-1"
                data-pagi-classes="text-center right-0 bottom-1 left-0 u-slick__pagination u-slick__pagination--long mb-0 z-index-n1 mt-3 mt-md-0"
                data-slides-show="7"
                data-slides-scroll="1"
                data-arrows-classes="position-absolute top-0 font-size-17 u-slick__arrow-normal top-10"
                data-arrow-left-classes="fa fa-angle-left right-1"
                data-arrow-right-classes="fa fa-angle-right right-0"
                data-responsive='[{
                  "breakpoint": 1400,
                  "settings": {
                    "slidesToShow": 6
                  }
                }, {
                    "breakpoint": 1200,
                    "settings": {
                      "slidesToShow": 3
                    }
                }, {
                  "breakpoint": 992,
                  "settings": {
                    "slidesToShow": 3
                  }
                }, {
                  "breakpoint": 768,
                  "settings": {
                    "slidesToShow": 2
                  }
                }, {
                  "breakpoint": 554,
                  "settings": {
                    "slidesToShow": 2
                  }
                }]'>

                @foreach($new_arival as $new_arival1)
                @php($product_id=$new_arival1->product_id)
                @php($image1=DB::table('product_images')->where('product_id',$product_id)->limit(1)->first())
                @php($image2=DB::table('product_images')->where('product_id',$product_id)->limit(1)->orderby('product_image_id','desc')->first())
                @php($brand=DB::table('brands')->where('brand_id',$new_arival1->brand_id)->first())
                @php($sub_cat=DB::table('sub_cats')->where('sub_cat_id',$new_arival1->sub_cat_id)->first())
                @php($img_new=DB::table('product_images')->where('product_id',$new_arival1->product_id)->get())
                <div class="js-slide products-group">
                    <div class="product-item">
                        <div class="product-item__outer h-100 w-100">
                            <div class="product-item__inner px-wd-4 p-2 p-md-3">
                                <div class="product-item__body pb-xl-2">
                                    <div class="mb-2"><a href="/shop?brand={{$brand->brand_id}}&name={{$brand->brand_name}}" class="font-size-12 text-gray-5">{{$brand->brand_name}}</a></div>
                                    <h5 class="mb-1 product-item__title"><a href="/product/{{$product_id}}" class="text-blue font-weight-bold">{{$new_arival1->product_name}}</a></h5>
                                    <div class="mb-2">
                                        <a href="/product/{{$product_id}}" class="d-block text-center"><img class="img-fluid" src="/product_image/{{$image1->image}}" alt="Image Description"></a>
                                    </div>
                                    <div class="flex-center-between mb-1">
                                        <div class="prodcut-price">
                                            @php($progst=$new_arival1->selling_price*$sub_cat->gst/100)
                                            <div class="text-gray-100">₹{{round($new_arival1->selling_price+$progst)}}</div>
                                        </div>
                                        <div class="d-none d-xl-block prodcut-add-cart">
                                          
                                            <span style="font-size: 10px;">
                                                @php($a=(int)$new_arival1->review)
                                                @for($i=0;$i<$a;$i++)
                                                <span class="" style="    width:  max-contant;"><i class="fa fa-star" style="color:#fa4251"></i></span>
                                                @endfor
                                                @for($i=0;$i<5-$a;$i++)
                                                <span class="" style="    width: max-contant;"><i class="fa fa-star"     style="color: #79797975;"></i></span> 
                                                @endfor
                                                ({{$new_arival1->review}})
                                                </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-item__footer">
                                    <div class="border-top pt-2 flex-center-between flex-wrap">
                                        <a href="javascript:void(0)" onclick="cart({{$product_id}});" class="text-gray-6 font-size-13"><i class="ec ec-add-to-cart mr-1 font-size-15"></i>Cart</a>
                                        <a href="javascript:void(0)" onclick="wishlist({{$product_id}});" class="text-gray-6 font-size-13"><i class="ec ec-favorites mr-1 font-size-15"></i> Wishlist</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach

                
               
            </div>
        </div>
        <!-- End New Arrival-->

         <!-- Most View Product -->
         <div class="position-relative">
            <div class="border-bottom border-color-1 mb-2">
                <h3 class="section-title section-title__full d-inline-block mb-0 pb-2 font-size-22">Most View Product</h3>
            </div>
            <div class="js-slick-carousel u-slick position-static overflow-hidden u-slick-overflow-visble pb-7 pt-2 px-1"
                data-pagi-classes="text-center right-0 bottom-1 left-0 u-slick__pagination u-slick__pagination--long mb-0 z-index-n1 mt-3 mt-md-0"
                data-slides-show="7"
                data-slides-scroll="1"
                data-arrows-classes="position-absolute top-0 font-size-17 u-slick__arrow-normal top-10"
                data-arrow-left-classes="fa fa-angle-left right-1"
                data-arrow-right-classes="fa fa-angle-right right-0"
                data-responsive='[{
                  "breakpoint": 1400,
                  "settings": {
                    "slidesToShow": 6
                  }
                }, {
                    "breakpoint": 1200,
                    "settings": {
                      "slidesToShow": 3
                    }
                }, {
                  "breakpoint": 992,
                  "settings": {
                    "slidesToShow": 3
                  }
                }, {
                  "breakpoint": 768,
                  "settings": {
                    "slidesToShow": 2
                  }
                }, {
                  "breakpoint": 554,
                  "settings": {
                    "slidesToShow": 2
                  }
                }]'>
                            @foreach($most_view_product as $most_view_product1)
                            @php($product_id=$most_view_product1->product_id)
                            @php($image1=DB::table('product_images')->where('product_id',$product_id)->limit(1)->first())
                            @php($image2=DB::table('product_images')->where('product_id',$product_id)->limit(1)->orderby('product_image_id','desc')->first())
                            @php($brand=DB::table('brands')->where('brand_id',$most_view_product1->brand_id)->first())
                            @php($sub_cat=DB::table('sub_cats')->where('sub_cat_id',$most_view_product1->sub_cat_id)->first())
                            @php($img_new=DB::table('product_images')->where('product_id',$product_id)->get())
                <div class="js-slide products-group">
                    <div class="product-item">
                        <div class="product-item__outer h-100 w-100">
                            <div class="product-item__inner px-wd-4 p-2 p-md-3">
                                <div class="product-item__body pb-xl-2">
                                    <div class="mb-2"><a href="/shop?brand={{$brand->brand_id}}&name={{$brand->brand_name}}" class="font-size-12 text-gray-5">{{$brand->brand_name}}</a></div>
                                    <h5 class="mb-1 product-item__title"><a href="/product/{{$product_id}}" class="text-blue font-weight-bold">{{$new_arival1->product_name}}</a></h5>
                                    <div class="mb-2">
                                        <a href="/product/{{$product_id}}" class="d-block text-center"><img class="img-fluid" src="/product_image/{{$image1->image}}" alt="Image Description"></a>
                                    </div>
                                    <div class="flex-center-between mb-1">
                                        <div class="prodcut-price">
                                            @php($progst=$new_arival1->selling_price*$sub_cat->gst/100)
                                            <div class="text-gray-100">₹{{round($new_arival1->selling_price+$progst)}}</div>
                                        </div>
                                        <div class="d-none d-xl-block prodcut-add-cart">
                                          
                                            <span style="font-size: 10px;">
                                                @php($a=(int)$new_arival1->review)
                                                @for($i=0;$i<$a;$i++)
                                                <span class="" style="    width:  max-contant;"><i class="fa fa-star" style="color:#fa4251"></i></span>
                                                @endfor
                                                @for($i=0;$i<5-$a;$i++)
                                                <span class="" style="    width: max-contant;"><i class="fa fa-star"     style="color: #79797975;"></i></span> 
                                                @endfor
                                                ({{$new_arival1->review}})
                                                </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-item__footer">
                                    <div class="border-top pt-2 flex-center-between flex-wrap">
                                        <a href="javascript:void(0)"  onclick="cart({{$product_id}});" class="text-gray-6 font-size-13"><i class="ec ec-add-to-cart mr-1 font-size-15"></i>Cart</a>
                                        <a href="javascript:void(0)"  onclick="wishlist({{$product_id}});" class="text-gray-6 font-size-13"><i class="ec ec-favorites mr-1 font-size-15"></i> Wishlist</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach

                
               
            </div>
         </div>
        <!-- End Most View Product -->

        
        <div class="container">
         
            <!-- Recently viewed -->
            <div class="mb-6">
                <div class="position-relative">
                    <div class="border-bottom border-color-1 mb-2">
                        <h3 class="section-title mb-0 pb-2 font-size-22">Best Seller</h3>
                    </div>
                    <div class="js-slick-carousel u-slick position-static overflow-hidden u-slick-overflow-visble pb-7 pt-2 px-1"
                        data-pagi-classes="text-center right-0 bottom-1 left-0 u-slick__pagination u-slick__pagination--long mb-0 z-index-n1 mt-3 mt-md-0"
                        data-slides-show="7"
                        data-slides-scroll="1"
                        data-arrows-classes="position-absolute top-0 font-size-17 u-slick__arrow-normal top-10"
                        data-arrow-left-classes="fa fa-angle-left right-1"
                        data-arrow-right-classes="fa fa-angle-right right-0"
                        data-responsive='[{
                          "breakpoint": 1400,
                          "settings": {
                            "slidesToShow": 6
                          }
                        }, {
                            "breakpoint": 1200,
                            "settings": {
                              "slidesToShow": 4
                            }
                        }, {
                          "breakpoint": 992,
                          "settings": {
                            "slidesToShow": 3
                          }
                        }, {
                          "breakpoint": 768,
                          "settings": {
                            "slidesToShow": 2
                          }
                        }, {
                          "breakpoint": 554,
                          "settings": {
                            "slidesToShow": 2
                          }
                        }]'>
   
                        @php($newarrivel=DB::table('products')
                        ->orderby('review','desc')
                        ->where('products.active_status','YES')
                        ->limit(10)
                        ->get())
   
                                           @foreach($newarrivel as $most_view_product1)
                                           @php($product_id=$most_view_product1->product_id)
                                           @php($image1=DB::table('product_images')->where('product_id',$product_id)->limit(1)->first())
                                           @php($image2=DB::table('product_images')->where('product_id',$product_id)->limit(1)->orderby('product_image_id','desc')->first())
                                           @php($brand=DB::table('brands')->where('brand_id',$most_view_product1->brand_id)->first())
                                           @php($sub_cat=DB::table('sub_cats')->where('sub_cat_id',$most_view_product1->sub_cat_id)->first())
                                           @php($img_new=DB::table('product_images')->where('product_id',$product_id)->get())
                                    
                                                   <div class="js-slide products-group">
                                                    <div class="product-item">
                                                        <div class="product-item__outer h-100 w-100">
                                                            <div class="product-item__inner px-wd-4 p-2 p-md-3">
                                                                <div class="product-item__body pb-xl-2">
                                                                    <div class="mb-2"><a href="/shop?brand={{$brand->brand_id}}&name={{$brand->brand_name}}" class="font-size-12 text-gray-5">{{$brand->brand_name}}</a></div>
                                                                    <h5 class="mb-1 product-item__title"><a href="/product/{{$product_id}}" class="text-blue font-weight-bold">{{$new_arival1->product_name}}</a></h5>
                                                                    <div class="mb-2">
                                                                        <a href="/product/{{$product_id}}" class="d-block text-center"><img class="img-fluid" src="/product_image/{{$image1->image}}" alt="Image Description"></a>
                                                                    </div>
                                                                    <div class="flex-center-between mb-1">
                                                                        <div class="prodcut-price">
                                                                            @php($progst=$new_arival1->selling_price*$sub_cat->gst/100)
                                                                            <div class="text-gray-100">₹{{round($new_arival1->selling_price+$progst)}}</div>
                                                                        </div>
                                                                        <div class="d-none d-xl-block prodcut-add-cart">
                                                                          
                                                                            <span style="font-size: 10px;">
                                                                                @php($a=(int)$new_arival1->review)
                                                                                @for($i=0;$i<$a;$i++)
                                                                                <span class="" style="    width:  max-contant;"><i class="fa fa-star" style="color:#fa4251"></i></span>
                                                                                @endfor
                                                                                @for($i=0;$i<5-$a;$i++)
                                                                                <span class="" style="    width: max-contant;"><i class="fa fa-star"     style="color: #79797975;"></i></span> 
                                                                                @endfor
                                                                                ({{$new_arival1->review}})
                                                                                </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="product-item__footer">
                                                                    <div class="border-top pt-2 flex-center-between flex-wrap">
                                                                        <a href="javascript:void(0)"  onclick="cart({{$product_id}});" class="text-gray-6 font-size-13"><i class="ec ec-add-to-cart mr-1 font-size-15"></i>Cart</a>
                                                                        <a href="javascript:void(0)"  onclick="wishlist({{$product_id}});" class="text-gray-6 font-size-13"><i class="ec ec-favorites mr-1 font-size-15"></i> Wishlist</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                    @endforeach
                    </div>
                </div>
            </div>
         
        </div>
    
   
   
            
                <div class="container">
                
                    <!-- Recently viewed -->
                    <div class="mb-6">
                        <div class="position-relative">
                            <div class="border-bottom border-color-1 mb-2">
                                <h3 class="section-title mb-0 pb-2 font-size-22">Best Seller</h3>
                            </div>
                            <div class="js-slick-carousel u-slick position-static overflow-hidden u-slick-overflow-visble pb-7 pt-2 px-1"
                                data-pagi-classes="text-center right-0 bottom-1 left-0 u-slick__pagination u-slick__pagination--long mb-0 z-index-n1 mt-3 mt-md-0"
                                data-slides-show="7"
                                data-slides-scroll="1"
                                data-arrows-classes="position-absolute top-0 font-size-17 u-slick__arrow-normal top-10"
                                data-arrow-left-classes="fa fa-angle-left right-1"
                                data-arrow-right-classes="fa fa-angle-right right-0"
                                data-responsive='[{
                                "breakpoint": 1400,
                                "settings": {
                                    "slidesToShow": 6
                                }
                                }, {
                                    "breakpoint": 1200,
                                    "settings": {
                                    "slidesToShow": 4
                                    }
                                }, {
                                "breakpoint": 992,
                                "settings": {
                                    "slidesToShow": 3
                                }
                                }, {
                                "breakpoint": 768,
                                "settings": {
                                    "slidesToShow": 2
                                }
                                }, {
                                "breakpoint": 554,
                                "settings": {
                                    "slidesToShow": 2
                                }
                                }]'>
        
                                @php($newarrivel=DB::table('products')
                                ->orderby('product_id','desc')
                                ->where('products.active_status','YES')
                                ->limit(10)
                                ->get())
                                                @foreach($newarrivel as $most_view_product1)
                                           @php($product_id=$most_view_product1->product_id)
                                           @php($image1=DB::table('product_images')->where('product_id',$product_id)->limit(1)->first())
                                           @php($image2=DB::table('product_images')->where('product_id',$product_id)->limit(1)->orderby('product_image_id','desc')->first())
                                           @php($brand=DB::table('brands')->where('brand_id',$most_view_product1->brand_id)->first())
                                           @php($sub_cat=DB::table('sub_cats')->where('sub_cat_id',$most_view_product1->sub_cat_id)->first())
                                           @php($img_new=DB::table('product_images')->where('product_id',$product_id)->get())
                                    
                                                   <div class="js-slide products-group">
                                                    <div class="product-item">
                                                        <div class="product-item__outer h-100 w-100">
                                                            <div class="product-item__inner px-wd-4 p-2 p-md-3">
                                                                <div class="product-item__body pb-xl-2">
                                                                    <div class="mb-2"><a href="/shop?brand={{$brand->brand_id}}&name={{$brand->brand_name}}" class="font-size-12 text-gray-5">{{$brand->brand_name}}</a></div>
                                                                    <h5 class="mb-1 product-item__title"><a href="/product/{{$product_id}}" class="text-blue font-weight-bold">{{$new_arival1->product_name}}</a></h5>
                                                                    <div class="mb-2">
                                                                        <a href="/product/{{$product_id}}" class="d-block text-center"><img class="img-fluid" src="/product_image/{{$image1->image}}" alt="Image Description"></a>
                                                                    </div>
                                                                    <div class="flex-center-between mb-1">
                                                                        <div class="prodcut-price">
                                                                            @php($progst=$new_arival1->selling_price*$sub_cat->gst/100)
                                                                            <div class="text-gray-100">₹{{round($new_arival1->selling_price+$progst)}}</div>
                                                                        </div>
                                                                        <div class="d-none d-xl-block prodcut-add-cart">
                                                                          
                                                                            <span style="font-size: 10px;">
                                                                                @php($a=(int)$new_arival1->review)
                                                                                @for($i=0;$i<$a;$i++)
                                                                                <span class="" style="    width:  max-contant;"><i class="fa fa-star" style="color:#fa4251"></i></span>
                                                                                @endfor
                                                                                @for($i=0;$i<5-$a;$i++)
                                                                                <span class="" style="    width: max-contant;"><i class="fa fa-star"     style="color: #79797975;"></i></span> 
                                                                                @endfor
                                                                                ({{$new_arival1->review}})
                                                                                </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="product-item__footer">
                                                                    <div class="border-top pt-2 flex-center-between flex-wrap">
                                                                        <a href="javascript:void(0)"  onclick="cart({{$product_id}});" class="text-gray-6 font-size-13"><i class="ec ec-add-to-cart mr-1 font-size-15"></i>Cart</a>
                                                                        <a href="javascript:void(0)"  onclick="wishlist({{$product_id}});" class="text-gray-6 font-size-13"><i class="ec ec-favorites mr-1 font-size-15"></i> Wishlist</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        @endforeach
                                        </div>
                                    </div>
                                </div>
                            
                            </div>
                        

         

 




      
    </div>
    <!-- Brand Carousel -->
    <div class="container mb-8">
        <div class="py-2 border-top border-bottom">
            <div class="js-slick-carousel u-slick my-1"
                data-slides-show="5"
                data-slides-scroll="1"
                data-arrows-classes="d-none d-lg-inline-block u-slick__arrow-normal u-slick__arrow-centered--y"
                data-arrow-left-classes="fa fa-angle-left u-slick__arrow-classic-inner--left z-index-9"
                data-arrow-right-classes="fa fa-angle-right u-slick__arrow-classic-inner--right"
                data-responsive='[{
                    "breakpoint": 992,
                    "settings": {
                        "slidesToShow": 2
                    }
                }, {
                    "breakpoint": 768,
                    "settings": {
                        "slidesToShow": 1
                    }
                }, {
                    "breakpoint": 554,
                    "settings": {
                        "slidesToShow": 1
                    }
                }]'>
                @php($brands=DB::table('brands')
                ->get())
                 @foreach($brands as $brands)
                <div class="js-slide">
                    <a href="/shop?brand={{$brands->brand_id}}&name={{$brands->brand_name}}" class="link-hover__brand">
                        <img class="img-fluid m-auto max-height-50" src="/brand_logo/{{$brands->brand_image}}" title="{{$brands->brand_name}}" alt="{{$brands->brand_name}}">
                    </a>
                </div>
                @endforeach       
            </div>
        </div>
    </div>
    <!-- End Brand Carousel -->
</main>
<!-- ========== END MAIN CONTENT ========== -->


@endsection
<script
      
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous">
</script>
<script>
 var item_length = $('.slide > div').length - 1;
var slider = $('.slide').slick({
    autoplay: true,
    autoplaySpeed: 1000,
    dots: false,
    infinite: false,
    speed: 1000,
    fade: true,
    slide: 'div',
    cssEase: 'linear',
    onAfterChange: function(){
        //check the length of total items in .slide container
        //if that number is the same with the number of the last slide
        //Then pause the slider
        if( item_length == slider.slickCurrentSlide() ){
            //this should do the same thing -> slider.slickPause();
            slider.slickSetOption("autoplay",false,false)
        };
    }
});
</script>