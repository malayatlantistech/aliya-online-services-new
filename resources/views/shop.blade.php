@extends('layouts.menu')

@section('title')
@if(isset($_GET['name']))
{{$_GET['name']}} : ALIYA Online Services
@else
India's Most Popular fashion Shopping Website : ALIYA Online Services
@endif
@endsection

@section('content')
<style>
    
    .price-slider {
        padding-top: 00px;
    
      margin: auto;
      text-align: center;
      position: relative;
    
    }
    .price-slider svg,
    .price-slider input[type=range] {
      position: absolute;
      left: 0;
      bottom: 0;
      background: transparent;
    }
    input[type=number] {
        text-align: center;
        font-size: 14px;
        width: max-content;
        border: 0;
        padding: 20px 0px;
        background: transparent;
      -moz-appearance: textfield;
    }
    input[type=number]::-webkit-outer-spin-button,
    input[type=number]::-webkit-inner-spin-button {
      -webkit-appearance: none;
    }
    input[type=number]:invalid,
    input[type=number]:out-of-range {
      border: 2px solid #e60023;
    }
    input[type=range] {
      -webkit-appearance: none;
      width: 100%;
    }
    input[type=range]:focus {
      outline: none;
    }
    input[type=range]:focus::-webkit-slider-runnable-track {
      background: #1da1f2;
    }
    input[type=range]:focus::-ms-fill-lower {
      background: #1da1f2;
    }
    input[type=range]:focus::-ms-fill-upper {
      background: #1da1f2;
    }
    input[type=range]::-webkit-slider-runnable-track {
      width: 100%;
      height: 5px;
      cursor: pointer;
      animate: 0.2s;
      background: #1da1f2;
      border-radius: 1px;
      box-shadow: none;
      border: 0;
    }
    input[type=range]::-webkit-slider-thumb {
      z-index: 2;
      position: relative;
      box-shadow: 0px 0px 0px #000;
      border: 1px solid #1da1f2;
      height: 18px;
      width: 18px;
      border-radius: 25px;
      background: #a1d0ff;
      cursor: pointer;
      -webkit-appearance: none;
      margin-top: -7px;
    }
    input[type=range]::-moz-range-track {
      width: 100%;
      height: 5px;
      cursor: pointer;
      animate: 0.2s;
      background: #1da1f2;
      border-radius: 1px;
      box-shadow: none;
      border: 0;
    }
    input[type=range]::-moz-range-thumb {
      z-index: 2;
      position: relative;
      box-shadow: 0px 0px 0px #000;
      border: 1px solid #1da1f2;
      height: 18px;
      width: 18px;
      border-radius: 25px;
      background: #a1d0ff;
      cursor: pointer;
    }
    input[type=range]::-ms-track {
      width: 100%;
      height: 5px;
      cursor: pointer;
      animate: 0.2s;
      background: transparent;
      border-color: transparent;
      color: transparent;
    }
    input[type=range]::-ms-fill-lower,
    input[type=range]::-ms-fill-upper {
      background: #1da1f2;
      border-radius: 1px;
      box-shadow: none;
      border: 0;
    }
    input[type=range]::-ms-thumb {
      z-index: 2;
      position: relative;
      box-shadow: 0px 0px 0px #000;
      border: 1px solid #1da1f2;
      height: 18px;
      width: 18px;
      border-radius: 25px;
      background: #a1d0ff;
      cursor: pointer;
    }
    .codes{
        bottom: 5%;
        left: 5%;
        position: fixed;
      }
      .codes div {
        border: 2px solid black;
        font-size: 20px;
        padding: 10px;
        background-color: red;
      }
      .codes div a{
        text-decoration: none;
        color: white;
        font-weight: 800;
      }
     
    .pagination {
        max-width: fit-content;
    }
    .custom-control-label {
        
        margin-left: 6px;
    }
    .custom-control-input {
        position: initial;
        z-index: -1;
        opacity: 0;
    }
    .price-filter {
        padding-top: 10px;
        padding-bottom: 10px;
    }
    input[type='range']::-webkit-slider-runnable-track {
        
          -webkit-appearance: none;
          color: #13bba4;
          margin-top: -1px;
        }
        
      .nav-responsive{
          padding-bottom: 30px !important;
          color: black;
      }
      .addtocartbtn {

    line-height: 2 !important;
 
}
.section_title {

    border-bottom: 1px solid #cecdcd !important;
    
}
    </style>
     
@if(isset($_GET['sub_sub_id']))
   <input type="hidden" value="{{$_GET['sub_sub_id']}}"  id="sub_sub_id">
   @else
   <input type="hidden" value="0"  id="sub_sub_id">
    @endif

    @if(isset($_GET['sub_id']))
   <input type="hidden" value="{{$_GET['sub_id']}}"  id="sub_idd">
   @else
   <input type="hidden" value="0"  id="sub_idd">
    @endif

    
    @if(isset($_GET['cat_id']))
   <input type="hidden" value="{{$_GET['cat_id']}}"  id="cat_id">
   @else
   <input type="hidden" value="0"  id="cat_id">
    @endif

    @if(isset($_GET['p_name']))
   <input type="hidden" value="{{$_GET['p_name']}}"  id="p_name">
   @else
   <input type="hidden" value="0"  id="p_name">
    @endif
    @if(isset($_GET['Product_name']))
   <input type="hidden" value="{{$_GET['Product_name']}}"  id="Product_name">
   @else
   <input type="hidden" value="0"  id="Product_name">
    @endif

    @guest
    <input type="hidden" value="USER"  id="user_type">
    @else
    <input type="hidden" value="{{Auth::user()->usertype}}"  id="user_type">
    @endguest

   <!-- ========== MAIN CONTENT ========== -->
   <main id="content" role="main">
    <!-- breadcrumb -->
    <div class="bg-gray-13 bg-md-transparent">
        <div class="container">
            <!-- breadcrumb -->
            <div class="my-md-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="/">Home</a></li>
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">Shop</li>
                    </ol>
                </nav>
            </div>
            <!-- End breadcrumb -->
        </div>
    </div>

    <!-- End breadcrumb -->

    <div class="container">
        <div class="row mb-8">
            <div class="d-none d-xl-block col-xl-3 col-wd-2gdot5">
{{--<div class="mb-6" style="position: sticky;top: 0;bottom: 0;"> --}}
                    <div class="mb-6" style="">
                    <div class="flex-center-between mb-3">
                        <h3 class="font-size-25 mb-0">Filters</h3>
                    </div>

                   
                   <div style="    background-color: whitesmoke;padding: 20px;border-radius: 11px;">

                    <div class=" pb-4 mb-4">
                        <div class="border-bottom border-color-1 mb-5">
                            <h4 class="section-title section-title__sm mb-0 pb-2 font-size-18">Brands</h4>
                        </div>

                        <!-- Checkboxes -->
                        @foreach($brand as $key=>$brand1)
                        @if($key<4)
                        <div class="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input common_selector brand" value="{{$brand1->brand_id}}" id="brand-{{$key}}" @if(isset($_GET['brand'])) @if($_GET['brand']==$brand1->brand_id) {{'checked'}} @endif @endif>
                                <label class="custom-control-label" for="brand-{{$key}}">{{$brand1->brand_name}}
                                </label>
                            </div>
                        </div>
                        @endif
                        @endforeach
                        <!-- End Checkboxes -->

                        <!-- View More - Collapse -->
                        <div class="collapse" id="collapseBrand">
                               <!-- Checkboxes -->
                        @foreach($brand as $key=>$brand1)
                        @if($key>3)
                            <div class="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input common_selector brand" value="{{$brand1->brand_id}}" id="brand-{{$key}}" @if(isset($_GET['brand'])) @if($_GET['brand']==$brand1->brand_id) {{'checked'}} @endif @endif>
                                    <label class="custom-control-label" for="brand-{{$key}}">{{$brand1->brand_name}}
                                      
                                    </label>
                                </div>
                            </div>
                        @endif
                        @endforeach
                          
                        </div>
                        <!-- End View More - Collapse -->

                        <!-- Link -->
                        <a class="link link-collapse small font-size-13 text-gray-27 d-inline-flex mt-2" data-toggle="collapse" href="#collapseBrand" role="button" aria-expanded="false" aria-controls="collapseBrand">
                            <span class="link__icon text-gray-27 bg-white">
                                <span class="link__icon-inner">+</span>
                            </span>
                            <span class="link-collapse__default">Show more</span>
                            <span class="link-collapse__active">Show less</span>
                        </a>
                        <!-- End Link -->
                    </div>
                  
  
                    @if($product_size_count!=0)
                    <div class=" pb-4 mb-4">
                        <div class="border-bottom border-color-1 mb-5">
                            <h4 class="section-title section-title__sm mb-0 pb-2 font-size-18">Size</h4>
                        </div>
                        <!-- Checkboxes -->
                        @foreach($product_size as $key1=>$product_size1)
                         @if($product_size1->size!='NO')
                            @if($key1<4)
                            <div class="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input  size common_selector" value="{{$product_size1->size}}" id="size-{{$key1}}">
                                <label class="custom-control-label" for="size-{{$key1}}">{{$product_size1->size}}</label>
                                </div>
                            </div>
                            @endif
                            @else
                            {{-- <div class="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" value="{{$product_size1->weight}}" id="size-{{$key1}}">
                                    <label class="custom-control-label" ffor="size-{{$key1}}">No Size</label>
                                </div>
                            </div> --}}

                        @endif
                        @endforeach
                      
                        <!-- End Checkboxes -->

                        <!-- View More - Collapse -->
                        <div class="collapse" id="collapseColor">
                            @foreach($product_size as $key1=>$product_size1)
                            @if($product_size1->size!='NO')
                            @if($key1>3)
                            
                            <div class="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input common_selector size" value="{{$product_size1->size}}" id="size-{{$key1}}">
                                    <label class="custom-control-label" for="size-{{$key1}}">{{$product_size1->size}}</label>
                                </div>
                            </div>
                            @endif
                            @else
                            {{-- <div class="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" value="{{$product_size1->weight}}" id="size-{{$key1}}">
                                    <label class="custom-control-label" ffor="size-{{$key1}}">No Size</label>
                                </div>
                            </div> --}}

                            @endif
                            @endforeach
                          
                           
                        </div>
                        <!-- End View More - Collapse -->

                        <!-- Link -->
                    @if($product_size_count>4)
                        <a class="link link-collapse small font-size-13 text-gray-27 d-inline-flex mt-2" data-toggle="collapse" href="#collapseColor" role="button" aria-expanded="false" aria-controls="collapseColor">
                            <span class="link__icon text-gray-27 bg-white">
                                <span class="link__icon-inner">+</span>
                            </span>
                            <span class="link-collapse__default">Show more</span>
                            <span class="link-collapse__active">Show less</span>
                        </a>
                    @endif
                        <!-- End Link -->
                    </div>
                    @endif


                    

                    @if($product_color_count!=0)
                    <div class=" pb-4 mb-4">
                        <div class="border-bottom border-color-1 mb-5">
                            <h4 class="section-title section-title__sm mb-0 pb-2 font-size-18">Colors</h4>
                        </div>
                       
                        <!-- Checkboxes -->
                        @foreach($product_color as $key1=>$product_color1)

                 
                         @if($product_color1->color!='NO')
                            @if($key1<4)
                            <div class="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input  color common_selector" value="{{$product_color1->color}}" id="color-{{$key1}}">
                                <label class="custom-control-label" for="color-{{$key1}}">{{$product_color1->color}}</label>
                                </div>
                            </div>
                            @endif
                            @else
                            {{-- <div class="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" value="{{$product_size1->weight}}" id="size-{{$key1}}">
                                    <label class="custom-control-label" ffor="size-{{$key1}}">No Size</label>
                                </div>
                            </div> --}}

                        @endif
                        @endforeach
                      
                        <!-- End Checkboxes -->

                        <!-- View More - Collapse -->
                        <div class="collapse" id="collapseColor1">
                            @foreach($product_color as $key1=>$product_color1)
                            @if($product_color1->color!='NO')
                            @if($key1>3)
                            
                            <div class="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input  color common_selector" value="{{$product_color1->color}}" id="color-{{$key1}}">
                                    <label class="custom-control-label" for="color-{{$key1}}">{{$product_color1->color}}</label>
                                </div>
                            </div>
                            @endif
                            @else
                            {{-- <div class="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" value="{{$product_size1->weight}}" id="size-{{$key1}}">
                                    <label class="custom-control-label" ffor="size-{{$key1}}">No Size</label>
                                </div>
                            </div> --}}

                            @endif
                            @endforeach
                          
                           
                        </div>
                        <!-- End View More - Collapse -->

                        <!-- Link -->
                    @if($product_color_count>4)
                        <a class="link link-collapse small font-size-13 text-gray-27 d-inline-flex mt-2" data-toggle="collapse" href="#collapseColor1" role="button" aria-expanded="false" aria-controls="collapseColor1">
                            <span class="link__icon text-gray-27 bg-white">
                                <span class="link__icon-inner">+</span>
                            </span>
                            <span class="link-collapse__default">Show more</span>
                            <span class="link-collapse__active">Show less</span>
                        </a>
                    @endif
                        <!-- End Link -->
                    </div>
                    @endif
                 

                        <div class="nav-responsive" >
                            <div class="border-bottom border-color-1 mb-5">
                                <h4 class="section-title section-title__sm mb-0 pb-2 font-size-18">Price Filter</h4>
                            </div>
                        <div id="filter-group1" style="padding: 0 15px 0 0;">
                           
                        
                            <div class="collection-collapse-block-content">
                                <input type="hidden" id="hidden_minimum_price" value="0" />
                                <input type="hidden" id="hidden_maximum_price" value="0" />
                                <div class="price-slider">
                                <input value="0" min="0" class="price_range" max="50000" step="500" type="range" />
                                <input value="50000" class="price_range" min="0" max="50000" step="500" type="range" />
                                    <span>
                                    <input type="number"  id="min" value="0" min="0" max="50000"/>  -  
                                    <input type="number"  id="max" value="50000" min="0" max="50000"/>
                                    </span>
                                </div>
            
                            </div>
                      
                          </div>
                      </div>


                      <div class=" pb-4 mb-4">
                        <div class="border-bottom border-color-1 mb-5">
                            <h4 class="section-title section-title__sm mb-0 pb-2 font-size-18">Review and Ratting</h4>
                        </div>
                            <div class="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="review" class="custom-control-input common_selector review" value="4" id="review-5">
                                    <label class="custom-control-label" for="review-5" style="font-weight: 100;">4★ & above</label>
                                </div>
                            </div>

                            <div class="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="review" class="custom-control-input common_selector review" value="3" id="review-4">
                                    <label class="custom-control-label" for="review-4" style="font-weight: 100;">3★ & above</label>
                                </div>
                            </div>

                            <div class="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="review" class="custom-control-input common_selector review" value="2" id="review-2">
                                    <label class="custom-control-label" for="review-2" style="font-weight: 100;">2★ & above</label>
                                </div>
                            </div>

                            <div class="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="review" class="custom-control-input common_selector review" value="1" id="review-1">
                                    <label class="custom-control-label" for="review-1" style="font-weight: 100;">1★ & above</label>
                                </div>
                            </div>
                    </div>
                </div>


                </div>
            </div>

            <div class="col-xl-9 col-wd-9gdot5">
                <!-- Shop-control-bar Title -->
                <div class="flex-center-between mb-3">
                    <h3 class="font-size-25 mb-0">
                        @if(isset($_GET['name']))
                            {{$_GET['name']}}
                            
                        
                        @else
                            Shop
                 
                        @endif
                        
                        </h3>
                    <p class="font-size-14 text-gray-90 mb-0"><span id="view_total"></span> Products Found</p>
                </div>
                <!-- End shop-control-bar Title -->
                <!-- Shop-control-bar -->
                <div class="bg-gray-1 flex-center-between borders-radius-9 py-1">
                    <div class="d-xl-none">
                        <!-- Account Sidebar Toggle Button -->
                        <a id="sidebarNavToggler1" class="btn btn-sm py-1 font-weight-normal" href="javascript:;" role="button"
                            aria-controls="sidebarContent1"
                            aria-haspopup="true"
                            aria-expanded="false"
                            data-unfold-event="click"
                            data-unfold-hide-on-scroll="false"
                            data-unfold-target="#sidebarContent1"
                            data-unfold-type="css-animation"
                            data-unfold-animation-in="fadeInLeft"
                            data-unfold-animation-out="fadeOutLeft"
                            data-unfold-duration="500">
                            <i class="fas fa-sliders-h"></i> <span class="ml-1">Filters</span>
                        </a>
                        <!-- End Account Sidebar Toggle Button -->
                    </div>
                    <div class="px-3 d-none d-xl-block">
                        <ul class="nav nav-tab-shop" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-one-example1-tab" data-toggle="pill" href="#pills-one-example1" role="tab" aria-controls="pills-one-example1" aria-selected="false">
                                    <div class="d-md-flex justify-content-md-center align-items-md-center">
                                        <i class="fa fa-th"></i>
                                    </div>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-two-example1-tab" data-toggle="pill" href="#pills-two-example1" role="tab" aria-controls="pills-two-example1" aria-selected="false">
                                    <div class="d-md-flex justify-content-md-center align-items-md-center">
                                        <i class="fa fa-align-justify"></i>
                                    </div>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-three-example1-tab" data-toggle="pill" href="#pills-three-example1" role="tab" aria-controls="pills-three-example1" aria-selected="true">
                                    <div class="d-md-flex justify-content-md-center align-items-md-center">
                                        <i class="fa fa-list"></i>
                                    </div>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-four-example1-tab" data-toggle="pill" href="#pills-four-example1" role="tab" aria-controls="pills-four-example1" aria-selected="true">
                                    <div class="d-md-flex justify-content-md-center align-items-md-center">
                                        <i class="fa fa-th-list"></i>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="d-flex">
                        <form method="get">
                            <!-- Select -->
                            <select class="js-select selectpicker dropdown-select max-width-200 max-width-160-sm right-dropdown-0 px-2 px-xl-0"
                                data-style="btn-sm bg-white font-weight-normal py-2 border text-gray-20 bg-lg-down-transparent border-lg-down-0" id="status">
                                <option value="0" selected>Default sorting</option>
                                <option value="popular">Sort by popularity</option>
                            
                                <option value="latest">Sort by latest</option>
                
                                <option value="highretting">Rating(low to high)</option>
                                <option value="lowretting">Rating(high to low)</option>
                
                                <option value="low">Sort by price: low to high</option>
                                <option value="high">Sort by price: high to low</option>
                            </select>
                            <!-- End Select -->
                        </form>
                        
                    </div>
 
                </div>
                <!-- End Shop-control-bar -->
                <!-- Shop Body -->
                <!-- Tab Content -->
               
                    <input type="hidden" name="hidden_page" id="hidden_page" value="1" />
                    <input type="hidden" name="hidden_column_name" id="hidden_column_name" value="id" />
                    <input type="hidden" name="hidden_sort_type" id="hidden_sort_type" value="asc" />
                    <div class="" id="pills-tabContent">


                <!-- End Tab Content -->
                <!-- End Shop Body -->
                <!-- Shop Pagination -->
                <nav class="d-md-flex justify-content-between align-items-center border-top pt-3" aria-label="Page navigation example">
                    <div class="text-center text-md-left mb-3 mb-md-0">Showing 1–25 of 56 results</div>
                    <ul class="pagination mb-0 pagination-shop justify-content-center justify-content-md-start">
                        <li class="page-item"><a class="page-link current" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                    </ul>
                </nav>
                <!-- End Shop Pagination -->
            </div>
        </div>
      
    </div>

     <!-- Sidebar Navigation -->
     <aside id="sidebarContent1" class="u-sidebar u-sidebar--left" aria-labelledby="sidebarNavToggler1">
        <div class="u-sidebar__scroller">
            <div class="u-sidebar__container">
                <div class="">
                    <!-- Toggle Button -->
                    <div class="d-flex align-items-center pt-3 px-4 bg-white">
                        <button type="button" class="close ml-auto"
                            aria-controls="sidebarContent1"
                            aria-haspopup="true"
                            aria-expanded="false"
                            data-unfold-event="click"
                            data-unfold-hide-on-scroll="false"
                            data-unfold-target="#sidebarContent1"
                            data-unfold-type="css-animation"
                            data-unfold-animation-in="fadeInLeft"
                            data-unfold-animation-out="fadeOutLeft"
                            data-unfold-duration="500">
                            <span aria-hidden="true"><i class="ec ec-close-remove"></i></span>
                        </button>
                    </div>
                    <!-- End Toggle Button -->

                    <!-- Content -->
                    <div class="js-scrollbar u-sidebar__body">
                        <div class="u-sidebar__content u-header-sidebar__content px-4">
                           
                            <div class="mb-6">
                                <div class="border-bottom border-color-1 mb-5">
                                    <h3 class="section-title section-title__sm mb-0 pb-2 font-size-18">Filters</h3>
                                </div>
                                <div class="border-bottom pb-4 mb-4">
                                    <h4 class="font-size-14 mb-3 font-weight-bold">Brands</h4>
            
                                    <!-- Checkboxes -->
                                    @foreach($brand as $key=>$brand1)
                                    @if($key<4)
                                    <div class="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input common_selector brand" value="{{$brand1->brand_id}}" id="brand1-{{$key}}" @if(isset($_GET['brand'])) @if($_GET['brand']==$brand1->brand_id) {{'checked'}} @endif @endif>
                                            <label class="custom-control-label" for="brand1-{{$key}}">{{$brand1->brand_name}}
                                            </label>
                                        </div>
                                    </div>
                                    @endif
                                    @endforeach
                                    <!-- End Checkboxes -->
            
                                    <!-- View More - Collapse -->
                                    <div class="collapse" id="collapseBrand">
                                           <!-- Checkboxes -->
                                    @foreach($brand as $key=>$brand1)
                                    @if($key>3)
                                        <div class="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input common_selector brand" value="{{$brand1->brand_id}}" id="brand1-{{$key}}" @if(isset($_GET['brand'])) @if($_GET['brand']==$brand1->brand_id) {{'checked'}} @endif @endif>
                                                <label class="custom-control-label" for="brand1-{{$key}}">{{$brand1->brand_name}}
                                                  
                                                </label>
                                            </div>
                                        </div>
                                    @endif
                                    @endforeach
                                      
                                    </div>
                                    <!-- End View More - Collapse -->
            
                                    <!-- Link -->
                                    <a class="link link-collapse small font-size-13 text-gray-27 d-inline-flex mt-2" data-toggle="collapse" href="#collapseBrand" role="button" aria-expanded="false" aria-controls="collapseBrand">
                                        <span class="link__icon text-gray-27 bg-white">
                                            <span class="link__icon-inner">+</span>
                                        </span>
                                        <span class="link-collapse__default">Show more</span>
                                        <span class="link-collapse__active">Show less</span>
                                    </a>
                                    <!-- End Link -->
                                </div>

                              
                    @if($product_size_count!=0)
                    <div class="border-bottom pb-4 mb-4">
                        <h4 class="font-size-14 mb-3 font-weight-bold">Size</h4>
                        <!-- Checkboxes -->
                        @foreach($product_size as $key1=>$product_size1)
                         @if($product_size1->size!='NO')
                            @if($key1<4)
                            <div class="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input  size common_selector" value="{{$product_size1->size}}" id="size1-{{$key1}}">
                                <label class="custom-control-label" for="size1-{{$key1}}">{{$product_size1->size}}</label>
                                </div>
                            </div>
                            @endif
                            @else
                            {{-- <div class="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" value="{{$product_size1->weight}}" id="size-{{$key1}}">
                                    <label class="custom-control-label" ffor="size-{{$key1}}">No Size</label>
                                </div>
                            </div> --}}

                        @endif
                        @endforeach
                      
                        <!-- End Checkboxes -->

                        <!-- View More - Collapse -->
                        <div class="collapse" id="collapseColor">
                            @foreach($product_size as $key1=>$product_size1)
                            @if($product_size1->size!='NO')
                            @if($key1>3)
                            
                            <div class="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input common_selector size" value="{{$product_size1->size}}" id="size1-{{$key1}}">
                                    <label class="custom-control-label" for="size1-{{$key1}}">{{$product_size1->size}}</label>
                                </div>
                            </div>
                            @endif
                            @else
                            {{-- <div class="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" value="{{$product_size1->weight}}" id="size-{{$key1}}">
                                    <label class="custom-control-label" ffor="size-{{$key1}}">No Size</label>
                                </div>
                            </div> --}}

                            @endif
                            @endforeach
                          
                           
                        </div>
                        <!-- End View More - Collapse -->

                        <!-- Link -->
                    @if($product_size_count>4)
                        <a class="link link-collapse small font-size-13 text-gray-27 d-inline-flex mt-2" data-toggle="collapse" href="#collapseColor" role="button" aria-expanded="false" aria-controls="collapseColor">
                            <span class="link__icon text-gray-27 bg-white">
                                <span class="link__icon-inner">+</span>
                            </span>
                            <span class="link-collapse__default">Show more</span>
                            <span class="link-collapse__active">Show less</span>
                        </a>
                    @endif
                        <!-- End Link -->
                    </div>
                    @endif


                    

                    @if($product_color_count!=0)
                    <div class="border-bottom pb-4 mb-4">
                        <h4 class="font-size-14 mb-3 font-weight-bold">Color</h4>
                       
                        <!-- Checkboxes -->
                        @foreach($product_color as $key1=>$product_color1)

                 
                         @if($product_color1->color!='NO')
                            @if($key1<4)
                            <div class="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input  color common_selector" value="{{$product_color1->color}}" id="color1-{{$key1}}">
                                <label class="custom-control-label" for="color1-{{$key1}}">{{$product_color1->color}}</label>
                                </div>
                            </div>
                            @endif
                            @else
                            {{-- <div class="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" value="{{$product_size1->weight}}" id="size-{{$key1}}">
                                    <label class="custom-control-label" ffor="size-{{$key1}}">No Size</label>
                                </div>
                            </div> --}}

                        @endif
                        @endforeach
                      
                        <!-- End Checkboxes -->

                        <!-- View More - Collapse -->
                        <div class="collapse" id="collapseColor1">
                            @foreach($product_color as $key1=>$product_color1)
                            @if($product_color1->color!='NO')
                            @if($key1>3)
                            
                            <div class="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input  color common_selector" value="{{$product_color1->color}}" id="color1-{{$key1}}">
                                    <label class="custom-control-label" for="color1-{{$key1}}">{{$product_color1->color}}</label>
                                </div>
                            </div>
                            @endif
                            @else
                            {{-- <div class="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" value="{{$product_size1->weight}}" id="size-{{$key1}}">
                                    <label class="custom-control-label" ffor="size-{{$key1}}">No Size</label>
                                </div>
                            </div> --}}

                            @endif
                            @endforeach
                          
                           
                        </div>
                        <!-- End View More - Collapse -->

                        <!-- Link -->
                    @if($product_color_count>4)
                        <a class="link link-collapse small font-size-13 text-gray-27 d-inline-flex mt-2" data-toggle="collapse" href="#collapseColor1" role="button" aria-expanded="false" aria-controls="collapseColor1">
                            <span class="link__icon text-gray-27 bg-white">
                                <span class="link__icon-inner">+</span>
                            </span>
                            <span class="link-collapse__default">Show more</span>
                            <span class="link-collapse__active">Show less</span>
                        </a>
                    @endif
                        <!-- End Link -->
                    </div>
                    @endif
                 
            
                                <div class="border-bottom pb-4 mb-4">
                                    <h4 class="font-size-14 mb-3 font-weight-bold">Review and Ratting</h4>
                                        <div class="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" name="review" class="custom-control-input common_selector review" value="4" id="review1-5">
                                                <label class="custom-control-label" for="review1-5" style="font-weight: 100;">4★ & above</label>
                                            </div>
                                        </div>
            
                                        <div class="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" name="review" class="custom-control-input common_selector review" value="3" id="review1-4">
                                                <label class="custom-control-label" for="review1-4" style="font-weight: 100;">3★ & above</label>
                                            </div>
                                        </div>
            
                                        <div class="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" name="review" class="custom-control-input common_selector review" value="2" id="review1-2">
                                                <label class="custom-control-label" for="review1-2" style="font-weight: 100;">2★ & above</label>
                                            </div>
                                        </div>
            
                                        <div class="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" name="review" class="custom-control-input common_selector review" value="1" id="review1-1">
                                                <label class="custom-control-label" for="review1-1" style="font-weight: 100;">1★ & above</label>
                                            </div>
                                        </div>
                                </div>


                            </div>
                     
                        </div>
                    </div>
                    <!-- End Content -->
                </div>
            </div>
        </div>
    </aside>
    <!-- End Sidebar Navigation -->



</main>
<!-- ========== END MAIN CONTENT ========== -->
  
<script
src="https://code.jquery.com/jquery-3.4.1.js"
integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
crossorigin="anonymous">



</script>



<script>
$(document).ready(function(){

  var page = $('#hidden_page').val();
      filter_data(page);
       $(document).on('click', '.pagination a', function(event){
       event.preventDefault();
       var page = $(this).attr('href').split('page=')[1];
       $('#hidden_page').val(page);
       $('li').removeClass('active');
        $(this).parent().addClass('active');
        filter_data(page);
 });





function filter_data(page)
{
$("#overlay").fadeIn(300);
var token = $("#_token").val();
var page=$("#hidden_page").val();
var type = $('#type').val();
var cat_id= $('#cat_id').val();
var sub_cat_id= $('#sub_cat_id').val();
var sub_sub_cat_id= $('#sub_sub_cat_id').val();
var minimum_price = $('#hidden_minimum_price').val();
var maximum_price = $('#hidden_maximum_price').val();
var brand = get_filter('brand');
var Product_name =$('#Product_name').val();
var review = get_filter('review');
var size = get_filter('size');
var color = get_filter('color');
var user_type=$('#user_type').val();
var status = $('#status').val();
$.ajax({
    url:"fetch_data",
    method:"GET",
    data:{page:page,_token:token,type:type,cat_id:cat_id,sub_cat_id:sub_cat_id,sub_sub_cat_id:sub_sub_cat_id, minimum_price:minimum_price, maximum_price:maximum_price, brand:brand, review:review,size:size,status:status,Product_name:Product_name,color:color},
    success:function(response){
        $('#grid-view').addClass("active");
        $('#list-view').removeClass("active");
        var abcd=response.split("malayssj");
        $("#view_total").html(abcd[0]);
$("#pills-tabContent").html(abcd[1]);
$("#overlay").fadeOut(300);
    }
});
}


function get_filter(class_name)
{
  $('#hidden_page').val(1);
var filter = [];
$('.'+class_name+':checked').each(function(){
    filter.push($(this).val());
});
return filter;
}
$('.common_selector').click(function(){
  
  var page = $('#hidden_page').val();
filter_data(page);
});
$('#status').change(function(){
  var page = $('#hidden_page').val();
filter_data(page);
});


$('.price_range').change(function(){
var minimum_price = $('#min').val();
var maximum_price = $('#max').val();


$('#hidden_minimum_price').val(minimum_price);
    $('#hidden_maximum_price').val(maximum_price);
    var page = $('#hidden_page').val();
filter_data(page);

});

// $('#slider-range').click(function(){
    
//     var view=$("#amount").val();
//     var price=view.split("-");
//     var minimum_price=price[0];
//     var maximum_price=price[1];
//     var page = $('#hidden_page').val();
//     $('#hidden_minimum_price').val(minimum_price);
//     $('#hidden_maximum_price').val(maximum_price);

// filter_data(page);
// });





});
</script>


@endsection
