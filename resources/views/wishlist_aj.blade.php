@php($user_id=Auth::user()->id)
@php($for_cart_view=DB::table('wishlists')->where('user_id',$user_id)->count())
 
 @if($for_cart_view!=0)
 
<table class="table table-bordered">
<thead>
<tr>
<td class="text-center">Image</td>
<td class="text-left">Product Name</td>
<th class="text-right">Unit Price</th>
<th class="product-Stock w-lg-15">Stock Status</th>

<td class="text-left">Action</td>
</tr>
</thead>
<tbody>



@foreach($item as $item)  
@php($view_product_id=$item->product_id)
@php($view_product_id=$item->product_id)
@php($product=DB::table('products')->where('products.product_id',$view_product_id)->first())
@php($product_image=DB::table('product_images')->where('product_id',$view_product_id)->limit(1)->get())
@php($sub_cat=DB::table('sub_cats')->where('sub_cat_id',$product->sub_cat_id)->first())
    <tr>

        <td class="text-center">
            @foreach($product_image as $product_image)
            <a href="/product/{{$view_product_id}}"><img class="img-fluid max-width-100 p-1 border border-color-1" src="product_image/{{$product_image->image}}" style="    width: 80px;" alt="{{$product->product_name}}"></a>
       @endforeach
        </td>
        <td class="text-left"><a href="/product/{{$view_product_id}}">{{$product->product_name}}@if($product->weight>0) @if($product->weight>999){{$product->weight/1000}}kg @else {{$product->weight}}gm @endif @endif</a></td>
        <td class="text-right">         @php($progst=$product->selling_price*$sub_cat->gst/100)
               
            <p class="price">₹{{$product->selling_price+$progst}}</p>
        
        </td>
        <td class="">
            @if($product->available_stock>0)
            <span style="color:green"><b>In Stock</b></span>

            @else
            <span style="color:red"><b>Out of Stock</b></span>

            @endif


        </td>
        <td class="text-left">
        <div style="max-width: 200px;" class="input-group btn-block">
          
            <span class="input-group-btn">
                <button  class="btn btn-success" title="Add to Basket" data-toggle="tooltip" type="button" data-original-title="Add" onclick="wishlisttoaddtocat({{$view_product_id}});" style="border: none;background: #3f8dda;" @if($product->available_stock==0) dasabled @endif> <i class="fa fa-shopping-cart  "  ></i></button>
        <button  class="btn btn-danger" title="Remove to Wishlist" data-toggle="tooltip" type="button" data-original-title="Remove" onclick="remove_wishlist1({{$view_product_id}});"><i class="fa fa-trash "  ></i></button>
        </span></div>
        </td>

    </tr>
@endforeach



</tbody>
</table>

@else
<br>
<center> <h2> No Item In Your Wishlist </h2>
    <a  href="/shop" class="btn btn-success" type="button" data-original-title="Add" style="border: none;background: #3f8dda;color:white"> Continue Shopping</a>
</center><br>
@endif
                    <script
src="https://code.jquery.com/jquery-3.4.1.js"
integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
crossorigin="anonymous">
</script>
<script type="text/javascript">

$(document).ready(function() {

//  wishlist_table();
});

function wishlisttoaddtocat(id,price_id){
//   alert();
var token = $("#_token").val();


remove_wishlist(id,price_id);
cart(id,price_id);
wishlist_table();
wishlist_ajax();
$("[data-toggle='tooltip']").tooltip('hide');
}


</script>
<script>
$(document).ready(function(){
$('[data-toggle="tooltip"]').tooltip();   
});
</script>
