@extends('layouts.menu')

@section('title')
Your Basket : E-Kirana
@endsection

@section('content')


  <!-- =====  BREADCRUMB STRAT  ===== -->
  <div class="breadcrumb section pt-60 pb-60">
    <div class="container">
      <h1 class="uppercase">Shopping Cart</h1>
      <ul>
        <li><a href="index.html"><i class="fa fa-home"></i></a></li>
        <li class="active">Shopping Cart</li>
      </ul>
    </div>
  </div>
  <!-- =====  BREADCRUMB END===== -->
  <!-- =====  HEADER END  ===== -->
  <div class="page-cart section">
  <!-- =====  CONTAINER START  ===== -->
    <div class="container">
      <div class="row ">
        <div class="col-12 mt-20 mb-20" id="cart_view">
         
          
         
        </div>
      </div>
    <!-- =====  Brand start ===== -->
    <div id="brand_carouse" class="section text-center mt-30 pb-15">
        <div class="row">
          <div class="col-12">
            <div class="section_title">Our Popular Brands</div>
          </div>
          <div class="col-sm-12">
            <div class="brand owl-carousel">
              @php($brands=DB::table('brands')
              ->get())
            @foreach($brands as $brands)
              <div class="product-thumb"><div class="item text-center"> <a href="/shop?brand={{$brands->brand_id}}"><img src="/brand_logo/{{$brands->brand_image}}" title="{{$brands->brand_name}}" alt="Disney" class="img-responsive" /></a> </div></div>
            
              @endforeach         
            </div>
          </div>
        </div>
      </div>
    <!-- =====  Brand end ===== -->
    </div>
  <!-- =====  CONTAINER END  ===== -->
  </div>

<script
src="https://code.jquery.com/jquery-3.4.1.js"
integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
crossorigin="anonymous">
</script>
<script>
$(document).ready(function(){


show_cart();

});

function show_cart(){
    $("#overlay").fadeIn(300);　

var token=$("#_token").val();

$.ajax({

url:'/cart_page_ajax',

type:'POST',

data:{_token:token},

success:function(response)
{

  $("[data-toggle='tooltip']").tooltip('hide');


$('#cart_view').html(response);

sub_total();
$("#overlay").fadeOut(300);　

}



});
}

function remove_item(id){


remove_cart(id);

show_cart() ;



}

function up(product_id){
// alert(product_id);


var value=Number($('#val-'+product_id).val())+1;
//alert(value);

$('#val-'+product_id).val(value);


if($('#original_qty-'+product_id).val()!=value){
    $('#update_btn-'+product_id).prop( "disabled", false );
}else{
    $('#update_btn-'+product_id).prop( "disabled", true );
}

var price= Number($('#database_price-'+product_id).val());
var total_price=price*value;

// alert(price);
// alert(total_price);
// $('#total_product_price-'+product_id).val(total_price);
// $('#product_price-'+product_id).val(price);
// sub_total();
// update(product_id);
$("[data-toggle='tooltip']").tooltip('hide');
}
function down(product_id){

//alert(value);

var value=Number($('#val-'+product_id).val())-1;
//alert(value);
if(value>0){
$('#val-'+product_id).val(value);

if($('#original_qty-'+product_id).val()!=value){
    $('#update_btn-'+product_id).prop( "disabled", false );
}else{
    $('#update_btn-'+product_id).prop( "disabled", true );
}



var price= Number($('#database_price-'+product_id).val());
var total_price=price*value;

// alert(price);
// alert(total_price);
// $('#total_product_price-'+product_id).val(total_price);
// $('#product_price-'+product_id).val(price);
// sub_total();
// update(product_id);
$("[data-toggle='tooltip']").tooltip('hide');
}
}


function update(id){
    //alert(id);
var value=$('#val-'+id).val();
//alert(value);

var token=$("#_token").val();



$.ajax({

url:'/update_qty',

type:'POST',

data:{_token:token,id:id,value:value},

success:function(response)
{

    show_cart() ;
    cart_ajax();
}

});
}

function sub_total(){

var total_product=$('#total_product').val();
//alert(total_product);
var sub_total=0;
var i;
//alert($('.tp-0').val());
//var p=$('.tp-0').val();
//alert(p);
for(i=0;i<=total_product;i++){
var p=$('.tp-'+i).val();
// alert(p);
sub_total=sub_total+Number(p);
// alert(sub_total);
}
$('#sub_total').val(sub_total);
}



</script>
@endsection