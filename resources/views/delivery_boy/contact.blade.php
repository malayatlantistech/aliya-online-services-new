
@extends('delivery_boy.layouts.menu')
@section('title','Dashboard | E-Kirana')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
 

    <!-- Main content -->
    <section class="content"  style="
    padding-top: 14px;
">

      <!-- Default box -->
      <div class="card card-solid">
        <div class="card-body pb-0">
          <div class="row d-flex align-items-stretch">
             
            <div class="col-12 col-sm-6 col-md-4">
              <div class="card bg-light">
                <div class="card-header text-muted border-bottom-0">
                        Contact Us
                </div>
                <div class="card-body pt-0">
                  <div class="row">
                    <div class="col-12">
                   <center> <h4 class=""><b>Name : {{Auth::user()->name}}</b></h4></center>
                   <center><h5 class=""><b>Email : {{Auth::user()->email}}</b></h5></center>
                   <center><h5 class=""><b>Phone : {{Auth::user()->mobile}}</b></h5></center>
                    </div>
                  </div>
                </div>
                <div class="card-footer">
                  <div class="text-right">
                   
                    <a href="/delivery_profile" class="btn btn-sm btn-primary">
                      <i class="fas fa-user"></i> View Profile
                    </a>
                  </div>
                </div>
              </div>
            </div>
           
          </div>
        </div>
        <!-- /.card-body -->
     
        <!-- /.card-footer -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
 
@endsection
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5dbd5c25154bf74666b7253a/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
    <!--End of Tawk.to Script-->