@extends('delivery_boy.layouts.menu')
@section('title','Profile | E-Kirana')
@section('content')
<style>
    .info-box {
        font-family: inherit !important;
    margin-bottom: 5px !important;

}
.info-box .info-box-text, .info-box .progress-description {
    font-family: inherit !important;
    font-size: 14px !important;
}
.info-box-name{
    font-family: inherit !important;
    font-weight: 400;
    padding-top: 6px;
    font-weight: 500;
    font-size:15px !important
}
body {
   
    font-family: inherit !important;
}
.info-box-text{
   color: #7b7d80; 
}


</style>

<div class="content-wrapper">

    <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Pending Booking</h1>
            </div><!-- /.col -->
           
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
    <section class="content" >
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">


          @if($booking_details_count!=0)
          @foreach($booking as $booking)
          @php($image=DB::table('product_images')->where('product_id',$booking->product_id)->first())

            <a href="/booking_details?booking_id={{$booking->booking_id}}&book_multi_id={{$booking->multi_id}}" style="color:black;width: 100%;">
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                  <span class="info-box-icon elevation-1"><img src="/product_image/{{$image->image}}"></span>
    
                  <div class="info-box-content">
                  
                    <div class="row" style="padding-left: 10px;">
                      <div style="width:75%">
                          <span class="info-box-number" style="font-size: 14px;">Order No {{$booking->order_id}}-{{$booking->multi_id}}</span>
                      <span class="info-box-text">{{date('h:i A, d M Y',strtotime($booking->created_at))}}</span>
                      </div>
                      <div style="float:right;width:25%;font-size: 20px !important;">
                          <span class="info-box-number" style="float:right;font-size: 17px;">&#8377; {{$booking->product_price+$booking->gst}}/-</span>
                      </div> 
                  </div>
  
  
                    <span class="info-box-name">{{$booking->product_name}}</span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
        </a>
        @endforeach
        @else
        <div class="col-12 col-sm-6 col-md-3">
        <center>No Order Found</center>
        </div>
        @endif



  
   
            <!-- /.col -->
          </div>
      </div>
    </section>
</div>


@endsection