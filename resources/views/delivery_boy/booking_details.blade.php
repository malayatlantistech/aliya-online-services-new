@extends('delivery_boy.layouts.menu')
@section('title','Profile | E-Kirana')
@section('content')
<style>
    .info-box {
  
    margin-bottom: 5px !important;

}
.info-box .info-box-text, .info-box .progress-description {
 
    font-size: 14px !important;
}
.info-box-name{
    font-weight: 400;
    padding-top: 6px;
    font-weight: 500;
}
body {
   
   font-family: inherit !important;
}

</style>

<div class="content-wrapper">

    <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Booking Details</h1>
            </div><!-- /.col -->
           
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
    <section class="content" >
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">
            @php($image=DB::table('product_images')->where('product_id',$booking->product_id)->first())
            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="row">
                <div class="text-center col-4" >
               
                  <img class="info-box-icon elevation-1" style="max-height: 100px;" src="/product_image/{{$image->image}}" >
                </div>
                <div class="col-8">
                  
              

                <p class="text-muted">
                  <span style="font-size:12px">{{date('h:i A, d M Y',strtotime($booking->created_at))}}</span><br>
                <span style="color:#212529;font-weight:700">Order No {{$booking->order_id}}-{{$booking->multi_id}}</span><br>
                
                <span style="color:#212529">{{$booking->product_name}}</span>
              
              </p>
                </div>
              </div>
<hr style="margin-top: 0rem;">


               
                <b>Sold By : E-Kirana</b> <br>
                <span style="font-size: 14px;">
               Webel IT Park , Module-104, <br>
                Beside Mosjit Devog,Haldia,Purba Medinipur<br>
                West Bengal,Pin Code - 721657
              </span>

                <hr>
                @php($address=DB::table('addres')->join('users','users.id','=','addres.user_id')->where('addres.id',$booking->address_id)->select(['users.email','addres.*'])->first())
                  <b>Shipped To : {{$address->name}} </b><br>
                  <span style="font-size: 14px;">
                  Phone no.-{{$address->phone_no}}/{{$address->optional_phone}} <br>
                  Email ID-{{$address->email}} <br>
                  {{$address->flat}},{{$address->address}}, 
                  {{$address->landmark}},{{$address->city}},{{$address->district}},
                  {{$address->state}},Pin Code - {{$address->pincode}} 
                </span>
                <hr>
                <b>Order Status : </b><br><br>


                <div class="tab-pane active" id="timeline">
                  <!-- The timeline -->
                  <div class="timeline timeline-inverse">
                    <!-- timeline time label -->
                    
                    <!-- /.timeline-label -->
                    <!-- timeline item -->
                    <div>
                      <i class="fas fa-clock bg-primary"></i>

                      <div class="timeline-item">
                        @php($multi=DB::table('book_multi_items')->where('multi_id',$_GET['book_multi_id'])->first())

                        <h3 class="timeline-header"><a href="#">Order Placed</a> 
                          <br><span style="font-size: 14px;">{{date('h:i A, d M Y',strtotime($multi->created_at))}}</span>
                        
                        </h3>

                       
                        {{-- <div class="timeline-footer">
                          <a href="#" class="btn btn-primary btn-sm">Read more</a>
                          <a href="#" class="btn btn-danger btn-sm">Delete</a>
                        </div> --}}
                      </div>
                    </div>
                    <!-- END timeline item -->
                    <!-- timeline item -->
                    <div>
                      <i class="fas fa-clock bg-info"></i>

                      <div class="timeline-item">
                    

                        <h3 class="timeline-header"><a href="#">Order Under Process</a> 
                          {{-- <br><span style="font-size: 14px;">{{date('h:i A, d M Y',strtotime($multi->created_at))}}</span> --}}
                        
                        </h3>
                      </div>
                    </div>
                    <!-- END timeline item -->
                     <!-- timeline item -->
                     <div>
                      <i class="fas fa-clock bg-purple"></i>

                      <div class="timeline-item">
                      

                        <h3 class="timeline-header"><a href="#">Order In Transit</a> 
                          {{-- <br><span style="font-size: 14px;">{{date('h:i A, d M Y',strtotime($multi->created_at))}}</span> --}}
                        
                        </h3>
                      </div>
                    </div>
                    <!-- END timeline item -->
                     <!-- timeline item -->
                     <div>
                      <i class="fas fa-clock bg-warning"></i>

                      @if($multi->order_status==2)
                      <div class="timeline-item">
                      
                        <span class="time"><i class="far fa-clock"></i> Pending</span>
                        <h3 class="timeline-header"><a href="#">Out For Delivery</a> 
                          <br><span style="font-size: 14px;">{{date('h:i A, d M Y',strtotime($multi->updated_at))}}</span>
                        
                        </h3>
                        <div class="timeline-footer">
                          <a href="javascript:void()" onclick="tracking('{{$multi->multi_id}}',{{$multi->order_status}});" class="btn btn-primary btn-sm">Order Process</a>

                        </div>
                      </div>
                      @else
                      <div class="timeline-item">
                      
                     
                        <h3 class="timeline-header"><a href="#">Out For Delivery</a> 
                       
                        
                        </h3>
                       
                      </div>
                      @endif


                    </div>
                    <!-- END timeline item -->
                     <!-- timeline item -->
                     <div>
                      <i class="fas fa-clock bg-success"></i>
                      @if($multi->order_status==3)
                        <div class="timeline-item">
                          <span class="time"><i class="far fa-clock"></i> Pending</span>
                          <h3 class="timeline-header"><a href="#">Delivered</a> 
                            <br><span style="font-size: 14px;">Pending</span>
                          
                          </h3>
                          <div class="timeline-footer">
                            <a href="javascript:void()" onclick="tracking('{{$multi->multi_id}}',{{$multi->order_status}});" class="btn btn-success btn-sm">Successfully Delivered</a>
                          
                          </div>
                        </div>
                      @elseif($multi->order_status<4)
                      <div class="timeline-item">
                        <span class="time"><i class="far fa-clock"></i> Pending</span>
                        <h3 class="timeline-header"><a href="#">Delivered</a> 
                          <br><span style="font-size: 14px;">Pending</span>
                        
                        </h3>
                       
                      </div>
                    @else
                        <div class="timeline-item">
                          
                          <h3 class="timeline-header"><a href="#">Delivered</a> 
                            <br><span style="font-size: 14px;">{{date('h:i A, d M Y',strtotime($multi->delivery_date))}}</span>
                          
                          </h3>
                        
                        </div>
                      @endif
                    </div>
                    
                 @if(isset($_GET['return']))
          
                <div>
                  <i class="fas fa-clock bg-danger"></i>

                  <div class="timeline-item">
                  
                    
                    <h3 class="timeline-header"><a href="#">Return Placed</a> 
           
                    
                    </h3>
                   
                  </div>
                </div>
                <div>
                  <i class="fas fa-clock bg-warning"></i>

                  <div class="timeline-item">
             
                    <h3 class="timeline-header"><a href="#">Return Under Process</a> 
                     
                    
                    </h3>
                   
                  </div>
                </div>

                
                <div>
                  <i class="fas fa-clock bg-info"></i>
                  @if($multi->order_status==5)
                  <div class="timeline-item">
                    <span class="time"><i class="far fa-clock"></i> Pending</span>
                    <h3 class="timeline-header"><a href="#">Out For Pickup</a> 
                      <br><span style="font-size: 14px;">Pending</span>
                    
                    </h3>
                    <div class="timeline-footer">
                      <a href="javascript:void()" onclick="tracking('{{$multi->multi_id}}',{{$multi->order_status}});" class="btn btn-primary btn-sm">Return Process</a>
                     
                    </div>
                  </div>
                  @else
                  <div class="timeline-item">
                  
                    <h3 class="timeline-header"><a href="#">Out For Pickup</a> 
                     
                    
                    </h3>
                   
                  </div>
                  
                  @endif
                


                </div>



                <div>
                  <i class="fas fa-clock bg-success"></i>
                  @if($multi->order_status==6)
                  <div class="timeline-item">
                    <span class="time"><i class="far fa-clock"></i> Pending</span>
                    <h3 class="timeline-header"><a href="#">Returned</a> 
                      <br><span style="font-size: 14px;">Pending</span>
                    
                    </h3>
                    <div class="timeline-footer">
                      <a href="javascript:void()" onclick="tracking('{{$multi->multi_id}}',{{$multi->order_status}});" class="btn btn-success btn-sm">Successfully Returned</a>
                     
                    </div>
                  </div>
                  @elseif($multi->order_status<6)
                  <div class="timeline-item">
                    <span class="time"><i class="far fa-clock"></i> Pending</span>
                    <h3 class="timeline-header"><a href="#">Returned</a> 
                      <br><span style="font-size: 14px;">Pending</span>
                    
                    </h3>
                  
                  </div>
                  @else
                  <div class="timeline-item">
           
                    <h3 class="timeline-header"><a href="#">Returned</a> 
                      <br><span style="font-size: 14px;">{{date('h:i A, d M Y',strtotime($multi->return_date))}}</span>
                    
                    </h3>
                   
                  </div>
                  @endif

                </div>



                 @endif


               
                   
                    <div>
                      <i class="far fa-clock bg-gray"></i>
                    </div>
                    
                  </div>
                </div>



              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
          
            <!-- /.card -->
          </div>
          <!-- /.col -->
        
          <!-- /.col -->
        </div>
      
      </div>
    </section>
</div>
<input type="hidden" id="aa" name="aa" value="{{$booking->customer_id}}">
<input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>"/>	
<input type="hidden" id="booking_id" value="{{$booking->booking_id}}">

<script
src="https://code.jquery.com/jquery-3.4.1.js"
integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
crossorigin="anonymous">
</script>
<script>
	function tracking(book_multi_id,status){
		var token = $("#_token").val();
		var idd= $("#aa").val();
		var booking_id= $("#booking_id").val();

	$("#overlay").fadeIn(300);　
		$.ajax({
		
			url:'driver_order_status',
		
			type:'POST',
		
			data:{_token:token,book_multi_id:book_multi_id,idd:idd,booking_id:booking_id},
			
			success:function(response)
			{
				// alert(response);
			
			
					　
				location.reload(true); 
			
				$("#overlay").fadeOut(300);
		
		
       }
    });
   
 
}	
</script>

@endsection