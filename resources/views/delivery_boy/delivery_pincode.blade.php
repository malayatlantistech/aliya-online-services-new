
@extends('delivery_boy.layouts.menu')
@section('title','Dashboard | E-Kirana')
@section('content')



<div class="content-wrapper">

    <!-- Main content -->
    <section class="content" style="padding-top:14px;background-color: #f4f6f9;min-height: 75vh;">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
      

        <div class="card card-danger">
            <div class="card-header">
              <h3 class="card-title">Delivered Pincode</h3>
              
            </div>
            <div class="card-body">
            @foreach($pin as $key=>$pin)
            @if($key!=0)
            <hr style="">
            @endif
              <div class="row">
              
                <div class="col-7">
                   <span style="font-size: 27px">{{$pin->pincode}}</span>
                    
                    <br> 
                    @if($pin->active_status=='NO')

                    <span class="badge badge-danger"><b>Pincode is Deactivate</b></span>
@else
<span class="badge badge-primary"><b>Pincode is Activate</b></span>
@endif
                 

                </div>
                <div class="col-5" style="padding-top: 5%;">
                    @if($pin->active_status=='NO')


                    <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                      <input type="checkbox" class="custom-control-input" id="customSwitch3{{$pin->id}}"  onchange="pin_status_change(this.checked,{{$pin->id}})">
                      <label class="custom-control-label" for="customSwitch3{{$pin->id}}">Active / Deactive</label>
                    </div>
                          {{-- <input type="checkbox" name="my-checkbox" data-bootstrap-switch onchange="pin_status_change1(this.checked,{{$pin->id}})"> --}}

                        @else
                        <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                          <input type="checkbox" class="custom-control-input" id="customSwitch3{{$pin->id}}" checked onchange="pin_status_change(this.checked,{{$pin->id}})">
                          <label class="custom-control-label" for="customSwitch3{{$pin->id}}">Active / Deactive</label>
                        </div>
                          {{-- <input type="checkbox" name="my-checkbox" checked data-bootstrap-switch onchange="pin_status_change1(this.checked,{{$pin->id}})"> --}}
                        @endif
                </div>
                
              </div>

            @endforeach
            </div>
            <!-- /.card-body -->
          </div>

          <input type="hidden" name="_token" id="_token" value="<?php echo csrf_token(); ?>"/>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script
  src="https://code.jquery.com/jquery-3.4.1.js"
  integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
  crossorigin="anonymous">
  </script>
  <script>

        function pin_status_change(status,id){
            var token=$('#_token').val();

        
                  $.ajax({
                        url: '/delivery_pin_status_change',
                        type: 'POST',
                        data: {status:status,id:id,_token:token},
                        success: function (response) {  
                          
                                mdtoast(response.message, { 
                                type: response.type,
                                duration: 3000
                              });
                          
                        },error: function (jqXHR) {
                                mdtoast('Please try again later.', { 
                                      type: 'error',
                                      duration: 3000
                            
                                });
                        }
                    
                    });
          }

 
  </script>



<!-- jQuery -->
<script src="/delivery_boy_assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="/delivery_boy_assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Select2 -->
<script src="/delivery_boy_assets/plugins/select2/js/select2.full.min.js"></script>


<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservationdate').datetimepicker({
        format: 'L'
    });
    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })
    
    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

   
    $('.my-colorpicker1').colorpicker()
 
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  })
</script>

@endsection