@extends('delivery_boy.layouts.menu')
@section('title','cash withdrawl | E-Kirana')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="info-box mb-3 bg-info">
            <span class="info-box-icon"><i class="fas fa-wallet"></i></span>
    
            <div class="info-box-content">
              <span class="info-box-text" style="font-size: 25px;">Wallet Amount </span>
            <span class="info-box-number" style="font-size: 20px;">₹ {{$wallet_amount}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Withdradal Request</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="myForm"  action="{{route('withdrawal_request_code')}}" enctype="multipart/form-data">
               @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Enter Amount</label>
                    <input type="number" class="form-control" id="withdrawal_amount" name="withdrawal_amount" placeholder="Enter Withdrawal Amount">
                  </div>
                  <div class="text-danger"><strong id="a_h_name_error"></strong></div>
                <!-- /.card-body -->
                <div class="">
                  <button type="submit" class="btn btn-primary form-control">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

            <!-- Form Element sizes -->
         
            <!-- /.card -->

           
            <!-- /.card -->

            <!-- Input addon -->
       
            <!-- /.card -->
            <!-- Horizontal Form -->
        
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
        
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
 


@endsection



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    
$(document).ready(function(){
   $('#myForm').submit(function(event){
        event.preventDefault();
        var formdata = new FormData($(this)[0]);
            $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            dataType: 'json',
            processData: false,
            contentType: false,
            cache:false,
            data: formdata,
            success: function (response) {  
                $('.error').html('');
                if(response.success == true){
                    mdtoast(response.msg, { 
                        type: 'ewrty',
                        duration: 3000
                        });
                }

            },error: function (jqXHR) {
                $("#update_bttn").prop('disabled', false);
              
                var errormsg = jQuery.parseJSON(jqXHR.responseText);
                $.each(errormsg.errors,function(key,value) {

                    $('#'+key+'_error').html(value);
                });
            }
        });
    });
});

    </script>



  