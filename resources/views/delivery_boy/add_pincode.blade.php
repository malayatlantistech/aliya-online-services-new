
@extends('delivery_boy.layouts.menu')
@section('title','Dashboard | E-Kirana')
@section('content')

<section class="content" style="padding-top:14px;background-color: #f4f6f9;min-height: 75vh;">
    <div class="container-fluid">
      <div class="row">

        <div class="col-md-6">

          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Your Delivered Pincode</h3>
            </div>

         <form method="POST" action="/add_pincode_action" id="from-submit" enctype="multipart/form-data">
                @csrf
              <div class="card-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Select Pincodes</label>
                  <select class="form-control  select2" multiple="multiple" data-placeholder="Select a Pincode"   id="pincode" name="pincode[]">

                    @foreach($pin as $pin)
                    <option value="{{$pin->pincode}}">{{$pin->pincode}}</option>
                    @endforeach


                  </select>
                  <div class="text-danger"><strong class="error" id="pincode_error"></strong></div>
                </div>
            </form>
  

              <div class="card-footer" style="background-color:white;">
               <center> <button type="submit" id="submit" class="btn btn-primary">Save Pincodes</button></center>
              </div>
           
          </div>
         


        </div>
        
      </div>
    
    </div>
  </section>



  <script
  src="https://code.jquery.com/jquery-3.4.1.js"
  integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
  crossorigin="anonymous">
</script>

<script>
    $(document).ready(function(){
       $('#from-submit').submit(function(event){
            event.preventDefault();
            $("#submit").prop('disabled', true);
            var formdata = new FormData($(this)[0]);
                $.ajax({
                url: $(this).attr('action'),
                type: 'POST',
                dataType: 'json',
                processData: false,
                contentType: false,
                cache:false,
                data: formdata,
                success: function (response) {  
                    $('.error').html('');
                    if(response.a==1){
                        mdtoast(response.message, { 
                        type: 'ewrty',
                        duration: 3000
                        });
                    }else if(response.a==2){
                        mdtoast(response.message, { 
                        type: 'warning',
                        duration: 3000
                        });
                    }else{
                        mdtoast(response.message, { 
                        type: 'error',
                        duration: 3000
                        });
                    }
                    
                    
                    $("#submit").prop('disabled', false);
                },error: function (jqXHR) {
                    $("#submit").prop('disabled', false);
              
              var errormsg = jQuery.parseJSON(jqXHR.responseText);
              $.each(errormsg.errors,function(key,value) {

                  $('#'+key+'_error').html(value);
              });
                }
                    });
                });
            });
  
        </script>

<!-- jQuery -->
<script src="/delivery_boy_assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="/delivery_boy_assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Select2 -->
<script src="/delivery_boy_assets/plugins/select2/js/select2.full.min.js"></script>

<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservationdate').datetimepicker({
        format: 'L'
    });
    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })
    
    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

   
    $('.my-colorpicker1').colorpicker()
 
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  })
</script>
@endsection
