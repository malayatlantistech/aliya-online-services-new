<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@yield('title')</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="\delivery_boy_assets/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="\delivery_boy_assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="\delivery_boy_assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="\delivery_boy_assets/plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="\delivery_boy_assets/dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="\delivery_boy_assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="\delivery_boy_assets/plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="\delivery_boy_assets/plugins/summernote/summernote-bs4.css">

  <link rel="stylesheet" href="\delivery_boy_assets/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="\delivery_boy_assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

  <link rel="stylesheet" href="\delivery_boy_assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
  <link rel="stylesheet" href="\delivery_boy_assets/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">

  <link rel="stylesheet" href="\Material-Toast-master\mdtoast.min.css">





  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script src="http://cdn.bootcss.com/jquery/2.2.4/jquery.min.js"></script>
  <script src="http://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>
  <link rel="stylesheet" href="http://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">


<!---------   loader js  ---------->
<script src="\pace_loder\pace.js"></script>


 <style>
#snackbar {
 
    min-width: 100%;
  
    background-color: #333;
    color: #fff;
    text-align: center;
    border-radius: 2px;
    padding: 16px;
    position: fixed;
    z-index: 1;
    left: 0;
    bottom: 0px;
    -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
  animation: fadein 0.5s, fadeout 0.5s 2.5s;
}
.back-to-top {
  display:none !important;

}


/* Animations to fade the snackbar in and out */
@-webkit-keyframes fadein {
  from {bottom: 0; opacity: 0;}
  to {bottom: 30px; opacity: 1;}
}

@keyframes fadein {
  from {bottom: 0; opacity: 0;}
  to {bottom: 30px; opacity: 1;}
}

@-webkit-keyframes fadeout {
  from {bottom: 30px; opacity: 1;}
  to {bottom: 0; opacity: 0;}
}

@keyframes fadeout {
  from {bottom: 30px; opacity: 1;}
  to {bottom: 0; opacity: 0;}
}
.isDisabled {
  color: currentColor;
  cursor: not-allowed;
  opacity: 0.5;
  text-decoration: none;

}

.select2-container--default .select2-selection--multiple .select2-selection__choice {
    background-color: #007bff !important;
    border: 1px solid #006fe6 !important;
    border-radius: 4px;
    cursor: default;
    float: left;
    margin-right: 5px;
    margin-top: 5px;
    padding: 0 5px;
}
.select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
    color: rgb(245, 240, 240) !important;
    cursor: pointer;
    display: inline-block;
    font-weight: bold;
    margin-right: 2px;
}




.pace {
  -webkit-pointer-events: none;
  pointer-events: none;

  -webkit-user-select: none;
  -moz-user-select: none;
  user-select: none;
}

.pace-inactive {
  display: none;
}

.pace .pace-progress {
  background: #29d;
  position: fixed;
  z-index: 2000;
  top: 0;
  right: 100%;
  width: 100%;
  height: 2px;
}






  </style>
  	<style>
      #overlay{	
        position: fixed;
        top: 0;
        z-index: 100;
        width: 100%;
        height:100%;
        display: none;
        background: rgba(0,0,0,0.6);
      }
      .cv-spinner {
        height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;  
      }
      .spinner {
        width: 40px;
        height: 40px;
        border: 4px #ddd solid;
        border-top: 4px #2e93e6 solid;
        border-radius: 50%;
        animation: sp-anime 0.8s infinite linear;
      }
      @keyframes sp-anime {
        100% { 
          transform: rotate(360deg); 
        }
      }
      .is-hide{
        display:none;
      }
      


.float{
	position:fixed;
	width:60px;
	height:60px;
	bottom:40px;
	right:40px;
  background-color: #21252999;
	color:#FFF;
	border-radius:50px;
	text-align:center;
  z-index: 999;
	/* box-shadow: 2px 2px 3px #999; */
}

.my-float{
	margin: 16px;
    font-size: 27px;
    

}
      </style>
</head>



<body class="hold-transition sidebar-mini layout-fixed">
  

@php($pincode=DB::table('delivery_boy_pincodes')->where('delivery_boy_id',Auth::user()->id)->where('active_status','YES')->count())
@php($status=DB::table('users')->where('id',Auth::user()->id)->where('status','YES')->count())
@php($wallet_amount=DB::table('delivery_wallets')->where('delivery_id',Auth::user()->id)->first())
@php($pending_delivery=DB::table('bookings')
->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')
->join('users','users.id','=','bookings.customer_id')
->join('products','products.product_id','=','book_multi_items.product_id')
->where('book_multi_items.delivery_boy_id',Auth::user()->id)
->where('book_multi_items.order_status',1)
->orwhere('book_multi_items.order_status',2)
->orwhere('book_multi_items.order_status',3)
->count())
@php($complele_delivery=DB::table('bookings')
->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')
->join('users','users.id','=','bookings.customer_id')
->join('products','products.product_id','=','book_multi_items.product_id')
->where('book_multi_items.delivery_boy_id',Auth::user()->id)
->where('book_multi_items.order_status','>=',4)

->count())
@php($pending_return=DB::table('bookings')
->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')
->join('users','users.id','=','bookings.customer_id')
->join('products','products.product_id','=','book_multi_items.product_id')
->where('book_multi_items.return_delivery_boy_id',Auth::user()->id)
->where('book_multi_items.order_status',5)
->orwhere('book_multi_items.order_status',6)

->count())
@php($complete_return=DB::table('bookings')
->join('book_multi_items','bookings.booking_id','=','book_multi_items.booking_id')
->join('users','users.id','=','bookings.customer_id')
->join('products','products.product_id','=','book_multi_items.product_id')
->where('book_multi_items.return_delivery_boy_id',Auth::user()->id)
->where('book_multi_items.order_status',7)

->count())
<div id="overlay">
	<div class="cv-spinner">
		<span class="spinner"></span>
	</div>
</div>

<div class="wrapper">
@if(basename($_SERVER['REQUEST_URI'])!="contact" && basename($_SERVER['REQUEST_URI'])!="qr_code_scan") 
  <a href="/qr_code_scan" class="float">
    <i class="fa fa-qrcode my-float"></i>
    </a>
    @endif
 <!-- Navbar -->
 <nav class="main-header navbar navbar-expand navbar-white navbar-light" style="    position: fixed;width: 100%;">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
     
    </ul>

   
    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->
      
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#" style="padding-top: 0px;">
        <img src="profile_image/{{Auth::user()->profile_image}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8;height:40px">
         
        </a>
        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right">
   
          <a href="/delivery_profile" class="dropdown-item" >
            <i class="fas fa-users mr-2"></i> My Profile
            
          </a>
          <div class="dropdown-divider"></div>
          <a href="{{route('delivery_logout')}}" class="dropdown-item">
            <i class="fas fa-envelope mr-2"></i>Logout
         
          </a>

		  
        
        </div>
      </li>
      
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/" class="brand-link">
    <center>  <img src="http://admin.localhost:8000/logo/OHMAGO%20Delivery%20Services%20Logo%20PNG.png" alt="AdminLTE Logo" style="height:50px">
</center>
  
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="profile_image/{{Auth::user()->profile_image}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
        <a href="/delivery_profile" class="d-block">{{Auth::user()->name}}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview menu-open">
            <a href="/" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                
              </p>
            </a>
          
          </li>
          <li class="nav-item">
            <a href="/pincode" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                View Pincode
                <span class="right badge badge-danger">{{$pincode}}</span>
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/pincode_add" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Add new Pincode
              
              </p>
            </a>
          </li>
          <li class="nav-item ">
            <a @if($status!=0 && $pincode!=0) href="/driver_wallet" @endif class="nav-link @if($status==0 || $pincode==0) {{'isDisabled'}} @endif">
              <i class="nav-icon fas fa-tree"></i>
              <p>
               My Wallet
               <span class="right badge badge-info">&#8377;{{$wallet_amount->wallet_ammount}}</span>
              
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a @if($status!=0 && $pincode!=0) href="/delivery_pending" @endif class="nav-link @if($status==0 || $pincode==0) {{'isDisabled'}} @endif" >
              <i class="nav-icon fas fa-calendar-alt"></i>
              <p>
                Pending Delivery
                <span class="right badge badge-primary">{{$pending_delivery}}</span>
              
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a @if($status!=0 && $pincode!=0) href="/delivery_complete" @endif class="nav-link @if($status==0 || $pincode==0) {{'isDisabled'}} @endif">
              <i class="nav-icon fas fa-table"></i>
              <p>
                Completed Delivery
                <span class="right badge badge-success">{{$complele_delivery}}</span>
              
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a @if($status!=0 && $pincode!=0) href="/delivery_pending_return" @endif class="nav-link @if($status==0 || $pincode==0) {{'isDisabled'}} @endif" >
              <i class="nav-icon fas fa-calendar-alt"></i>
              <p>
                Pending Return
              <span class="right badge badge-primary">{{$pending_return}}</span>
              
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a @if($status!=0 && $pincode!=0) href="/delivery_complete_return" @endif class="nav-link @if($status==0 || $pincode==0) {{'isDisabled'}} @endif">
              <i class="nav-icon fas fa-table"></i>
              <p>
                Complete Return
                <span class="right badge badge-success">{{$complete_return}}</span>
              
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="bank_details" class="nav-link">
              <i class="nav-icon fas fa-university "></i>
              <p>
                Bank Details
               
              
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/delivery_profile" class="nav-link">
              <i class="nav-icon fas fa-user "></i>
              <p>
               My Profile
               
              
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('change_password')}}" class="nav-link">
              <i class="nav-icon fas fa-cog "></i>
             
              <p>
            Settings
              </p>
            </a>
          </li>
          
          <li class="nav-item">
            <a href="/contact_us" class="nav-link">
              <i class="nav-icon fas fa-cog "></i>
             
              <p>
            Contact Us
              </p>
            </a>
          </li>
         
          <li class="nav-item" style="margin-bottom: 50px;">
            <a href="{{route('delivery_logout')}}" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt "></i>
             
              <p>
                Logout
               
              
              </p>
            </a>
          </li>
    
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <script>
$('.isDisabled').attr('href').remove();
  </script>











  <div class="row" style="height:57px;    width: 100%;"></div>

@if($pincode==0 && $status==0)
  <div id="snackbar" class="show">Please wetting for admin approval.</div>
@elseif($pincode==0 && $status!=0)
<div id="snackbar" class="show">Please add Your Delivered Pincodes.</div>
@elseif($pincode!=0 && $status==0)
  <div id="snackbar" class="show">Please wetting for admin approval.</div>

@endif





  @yield('content')











   <!-- Control Sidebar -->
   <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->





@yield('add-js')




<!-- jQuery -->
<script src="/delivery_boy_assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="/delivery_boy_assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Select2 -->
<script src="/delivery_boy_assets/plugins/select2/js/select2.full.min.js"></script>


<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservationdate').datetimepicker({
        format: 'L'
    });
    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })
    
    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

   
    $('.my-colorpicker1').colorpicker()
 
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  })
</script>



{{-- 
<script src="\delivery_boy_assets/plugins/select2/js/select2.full.min.js"></script>

<script src="\delivery_boy_assets/plugins/jquery/jquery.min.js"></script>
  --}}



<!-- jQuery UI 1.11.4 -->
<script src="\delivery_boy_assets/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>

<!-- Bootstrap 4 -->
<script src="\delivery_boy_assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="\delivery_boy_assets/plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="\delivery_boy_assets/plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="\delivery_boy_assets/plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="\delivery_boy_assets/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="\delivery_boy_assets/plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="\delivery_boy_assets/plugins/moment/moment.min.js"></script>
<script src="\delivery_boy_assets/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="\delivery_boy_assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="\delivery_boy_assets/plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="\delivery_boy_assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="\delivery_boy_assets/dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="\delivery_boy_assets/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="\delivery_boy_assets/dist/js/demo.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="\delivery_boy_assets/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- InputMask -->

<script src="\delivery_boy_assets/plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>

<!-- bootstrap color picker -->
<script src="\delivery_boy_assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>

<!-- Bootstrap Switch -->
<script src="\delivery_boy_assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<!-- AdminLTE App -->
{{-- <script src="\delivery_boy_assets/dist/js/adminlte.min.js')}}"></script> --}}
<script src="\toast\toast.js"></script>
<script src="\Material-Toast-master\mdtoast.min.js"></script>





 
</body>
</html>
