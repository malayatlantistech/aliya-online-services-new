@extends('layouts.menu')

@section('title')
India's Most Popular Grocery Shopping Website : E-Kirana
@endsection

@section('content')
    <!-- =====  BANNER STRAT  ===== -->
    <div class="banner section">
        <div class="main-banner owl-carousel">

          @foreach($banner as $banner1)
          <div class="item"><a href="#"><img src="banner/{{$banner1->display_banner}}" alt="Main Banner" class="img-responsive" /></a></div>
         @endforeach
        </div>
      </div>
      <!-- =====  BANNER END  ===== -->
  
      <!-- =====  CONTAINER START  ===== -->
     
      <div class="container">
        <!-- =====  SUB BANNER  STRAT ===== -->
        @if(isset($coupon_banner))
        <div class="subbanner-section section mt-20">
          <div class="owl-carousel banner-carousel">
            @foreach($coupon_banner as $coupon_banner1)
            <div class="home-subbanner">
              <div class="home-img"><a href="#"><img class="leftbanner" src="coupon_image/{{$coupon_banner1->coupon_image}}" alt="sub-banner1"></a></div>
              <div class="cms-desc">
                <div class="cms-text1"><b>Get {{$coupon_banner1->coupon_value}} @if($coupon_banner1->coupon_type=='FLAT') Flat  @else  %  @endif off</b></div>
                <div class="cms-text1"><b>Coupon code : {{$coupon_banner1->coupon_code}}</b></div>
                <div class="cms-text3">Minimum Order Value ₹{{$coupon_banner1->coupon_value}}</div>
              </div>
            </div>
            @endforeach
          </div>
          @endif
        </div>
        <!-- =====  SUB BANNER END  ===== --> 
  
        <!-- =====  SEARVICES START  ===== -->
        <div class="shipping-outer section">
          <div class="shipping-inner row">
            <div class="heading col-lg-3 col-12 text-center text-lg-left">
              <h2>Why choose us?</h2>
            </div>
            <div class="subtitle-part subtitle-part1 col-lg-3 col-4 text-center text-lg-left">
              <div class="subtitle-part-inner">
                <div class="subtitile">
                  <div class="subtitle-part-image"></div>
                  <div class="subtitile1">On time delivery</div>
                  <div class="subtitile2">All over India</div>
                </div>
              </div>
            </div>
            <div class="subtitle-part subtitle-part2 col-lg-3 col-4 text-center text-lg-left">
              <div class="subtitle-part-inner">
                <div class="subtitile">
                  <div class="subtitle-part-image"></div>
                  <div class="subtitile1">Free delivery</div>
                  <div class="subtitile2">Order over for all Order</div>
                </div>
              </div>
            </div>
            <div class="subtitle-part subtitle-part3 col-lg-3 col-4 text-center text-lg-left">
              <div class="subtitle-part-inner">
                <div class="subtitile">
                  <div class="subtitle-part-image"></div>
                  <div class="subtitile1">Quality assurance</div>
                  <div class="subtitile2">You can trust us</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- =====  SEARVICES END  ===== -->
  
        <!-- =====  PRODUCT CATEGORY START  ===== -->
        {{-- <div class="category-banner-block">
          <div class="section_title">top categories</div>
          <div class="row">
            <div class="product-layout col-lg-2 col-md-3 col-sm-4 col-6">
              <div class="product-thumb transition text-center">
                <div class="caption categoryname">
                  <h4><a href="#">Del Monte Beets</a></h4>
                </div>
                <div class="image"><a href="category_page.html"><img src="use_assets/images/cat1.png" alt="Del Monte Beets" title="Del Monte Beets" class="img-responsive"></a></div>
              </div>
            </div>
            <div class="product-layout col-lg-2 col-md-3 col-sm-4 col-6">
              <div class="product-thumb transition text-center">
                <div class="caption categoryname">
                  <h4><a href="category_page.html">Veggies</a></h4>
                </div>
                <div class="image"><a href="category_page.html"><img src="use_assets/images/cat2.png" alt="Veggies" title="Veggies" class="img-responsive"></a></div>
              </div>
            </div>
            <div class="product-layout col-lg-2 col-md-3 col-sm-4 col-6">
              <div class="product-thumb transition text-center">
                <div class="caption categoryname">
                  <h4><a href="category_page.html">Del Monte Corn</a></h4>
                </div>
                <div class="image"><a href="category_page.html"><img src="use_assets/images/cat3.png" alt="Del Monte Corn" title="Del Monte Corn" class="img-responsive"></a></div>
              </div>
            </div>
            <div class="product-layout col-lg-2 col-md-3 col-sm-4 col-6">
              <div class="product-thumb transition text-center">
                <div class="caption categoryname">
                  <h4><a href="category_page.html">Riced cauliflower</a></h4>
                </div>
                <div class="image"><a href="category_page.html"><img src="use_assets/images/cat4.png" alt="Riced cauliflower" title="Riced cauliflower" class="img-responsive"></a></div>
              </div>
            </div>
            <div class="product-layout col-lg-2 col-md-3 col-sm-4 col-6">
              <div class="product-thumb transition text-center">
                <div class="caption categoryname">
                  <h4><a href="category_page.html">Veggies Bowl</a></h4>
                </div>
                <div class="image"><a href="category_page.html"><img src="use_assets/images/cat5.png" alt="Veggies Bowl" title="Veggies Bowl" class="img-responsive"></a></div>
              </div>
            </div>
            <div class="product-layout col-lg-2 col-md-3 col-sm-4 col-6">
              <div class="product-thumb transition text-center">
                <div class="caption categoryname">
                  <h4><a href="category_page.html">Green peas | Carrots</a></h4>
                </div>
                <div class="image"><a href="category_page.html"><img src="use_assets/images/cat6.png" alt="Green peas | Carrots" title="Green peas | Carrots" class="img-responsive"></a></div>
              </div>
            </div>
          </div>
        </div> --}}
        <!-- =====  PRODUCT CATEGORY END  ===== -->
  
        <!-- =====  PRODUCT section  ===== -->
        {{-- <div class="category_product section">
          <div class="row">
            <div class="col-12">
              <div class="section_title">Fruits store</div>
            </div>
            <div class="col-sm-3 productcategory_thumb text-center mb-15"> <img src="use_assets/images/14.jpg" alt="" title="" class="img-thumbnail"> </div>
            <div class="col-sm-9 right_block">
              <div class="row">
                <div class="product-layout col-lg-4 col-md-4 col-sm-4 col-6">
                  <div class="product-thumb transition">
                    <p class="tag">11<br>
                      % <br>
                      <i>off</i></p>
                    <h4><a href="category_page.html">Apple</a></h4>
                    <div class="image"><a href="category_page.html"><img src="use_assets/images/12.jpg" alt="Apple" title="Apple" class="img-thumbnail"></a></div>
                  </div>
                </div>
                <div class="product-layout col-lg-4 col-md-4 col-sm-4 col-6">
                  <div class="product-thumb transition">
                    <p class="tag">20<br>
                      % <br>
                      <i>off</i></p>
                    <h4><a href="category_page.html">Strawberry</a></h4>
                    <div class="image"><a href="category_page.html"><img src="use_assets/images/11.jpg" alt="Strawberry" title="Strawberry" class="img-thumbnail"></a></div>
                  </div>
                </div>
                <div class="product-layout col-lg-4 col-md-4 col-sm-4 col-6">
                  <div class="product-thumb transition">
                    <h4><a href="category_page.html">Pineapple</a></h4>
                    <div class="image"><a href="category_page.html"><img src="use_assets/images/10.jpg" alt="Pineapple" title="Pineapple" class="img-thumbnail"></a></div>
                  </div>
                </div>
                <div class="product-layout col-lg-4 col-md-4 col-sm-4 col-6">
                  <div class="product-thumb transition">
                    <h4><a href="category_page.html">Banana</a></h4>
                    <div class="image"><a href="category_page.html"><img src="use_assets/images/09.jpg" alt="Banana" title="Banana" class="img-thumbnail"></a></div>
                  </div>
                </div>
                <div class="product-layout col-lg-4 col-md-4 col-sm-4 col-6">
                  <div class="product-thumb transition">
                    <h4><a href="category_page.html">Orange</a></h4>
                    <div class="image"><a href="category_page.html"><img src="use_assets/images/08.jpg" alt="Orange" title="Orange" class="img-thumbnail"></a></div>
                  </div>
                </div>
                <div class="product-layout col-lg-4 col-md-4 col-sm-4 col-6">
                  <div class="product-thumb transition">
                    <p class="tag">10<br>
                      % <br>
                      <i>off</i></p>
                    <h4><a href="category_page.html">Greps</a></h4>
                    <div class="image"><a href="category_page.html"><img src="use_assets/images/07.jpg" alt="Greps" title="Greps" class="img-thumbnail"></a></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-12 text-center">
              <div class="btn btn-primary viewall"><a href="category_page.html">View All</a></div>
            </div>
          </div>
        </div>
   --}}
        <!-- =====  featured section  ===== -->


        
        {{-- <div class="row">
          <div class="col-md-12">
            <div class="heading-part text-center mb-10">
              <h3 class="section_title mt-50">Related Products</h3>
            </div>
            <div class="related_pro">
              <div class="product-layout related-pro  owl-carousel mb-50 ">

                @foreach($new_arival as $related_product_details1) 
                @php($product_id=$related_product_details1->product_id)
                @php($image1=DB::table('product_images')->where('product_id',$product_id)->limit(1)->first())
                @php($image2=DB::table('product_images')->where('product_id',$product_id)->limit(1)->orderby('product_image_id','desc')->first())
                @php($brand=DB::table('brands')->where('brand_id',$related_product_details1->brand_id)->first())
                @php($sub_cat=DB::table('sub_cats')->where('sub_cat_id',$related_product_details1->sub_cat_id)->first())

                <div class="product-grid">
                  <div class="item">
                     <div class="product-thumb transition">
                        <div class="image">

                          <div class="first_image"> <a href="/product/{{$product_id}}"> <img src="/product_image/{{$image1->image}}" alt="pure-spice-3" title="{{$related_product_details1->product_name}}" class="img-responsive"> </a> </div>
                          <div class="swap_image"> <a href="/product/{{$product_id}}"> <img src="/product_image/{{$image2->image}}" alt="pure-spice-3" title="{{$related_product_details1->product_name}}" class="img-responsive"> </a></div>
                       
                        </div>
                        <div class="product-details">
                          <div class="caption">
                            <h4><a href="/shop/brand={{$brand->brand_id}}">{{$brand->brand_name}}</a></h4>
                            @php($progst=$related_product_details1->selling_price*$sub_cat->gst/100)
                 
                            <p class="price">₹{{$related_product_details1->selling_price+$progst}}</p>
                         
                            <div class="product_option">
                              <div class="form-group required ">
                                <a href="/product/{{$product_id}}"><?php echo substr($related_product_details1->product_name,0,20); ?>@if(strlen($related_product_details1->product_name)>20){{'...'}} @endif</a>
                              </div>
                              <div class="input-group button-group">
                                <span style="font-size: 11px;">
                                @php($a=(int)$related_product_details1->review)
                                @for($i=0;$i<$a;$i++)
                                <span class="" style="    width:  max-contant;"><i class="fa fa-star" style="color:#fa4251"></i></span>
                                @endfor
                                @for($i=0;$i<5-$a;$i++)
                                <span class="" style="    width: max-contant;"><i class="fa fa-star"     style="color: #79797975;"></i></span> 
                                @endfor
                                
        
                                ({{$related_product_details1->review}})
                                </span>
                                
                                <button type="button" class="addtocart pull-right" onclick="cart({{$product_id}});">Add</button>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                  </div>
                </div>

                @endforeach

             
              </div>
            </div>
          </div>
        </div>


        <div class="row">
          <div class="col-md-12">
            <div class="heading-part text-center mb-10">
              <h3 class="section_title mt-50">Related Products</h3>
            </div>
            <div class="related_pro">
              <div class="product-layout related-pro  owl-carousel mb-50 ">

                @foreach($new_arival as $related_product_details1) 
                @php($product_id=$related_product_details1->product_id)
                @php($image1=DB::table('product_images')->where('product_id',$product_id)->limit(1)->first())
                @php($image2=DB::table('product_images')->where('product_id',$product_id)->limit(1)->orderby('product_image_id','desc')->first())
                @php($brand=DB::table('brands')->where('brand_id',$related_product_details1->brand_id)->first())
                @php($sub_cat=DB::table('sub_cats')->where('sub_cat_id',$related_product_details1->sub_cat_id)->first())

                <div class="product-grid">
                  <div class="item">
                     <div class="product-thumb transition">
                        <div class="image">

                          <div class="first_image"> <a href="/product/{{$product_id}}"> <img src="/product_image/{{$image1->image}}" alt="pure-spice-3" title="{{$related_product_details1->product_name}}" class="img-responsive"> </a> </div>
                          <div class="swap_image"> <a href="/product/{{$product_id}}"> <img src="/product_image/{{$image2->image}}" alt="pure-spice-3" title="{{$related_product_details1->product_name}}" class="img-responsive"> </a></div>
                       
                        </div>
                        <div class="product-details">
                          <div class="caption">
                            <h4><a href="/shop/brand={{$brand->brand_id}}">{{$brand->brand_name}}</a></h4>
                            @php($progst=$related_product_details1->selling_price*$sub_cat->gst/100)
                 
                            <p class="price">₹{{$related_product_details1->selling_price+$progst}}</p>
                         
                            <div class="product_option">
                              <div class="form-group required ">
                                <a href="/product/{{$product_id}}"><?php echo substr($related_product_details1->product_name,0,20); ?>@if(strlen($related_product_details1->product_name)>20){{'...'}} @endif</a>
                              </div>
                              <div class="input-group button-group">
                                <span style="font-size: 11px;">
                                @php($a=(int)$related_product_details1->review)
                                @for($i=0;$i<$a;$i++)
                                <span class="" style="    width:  max-contant;"><i class="fa fa-star" style="color:#fa4251"></i></span>
                                @endfor
                                @for($i=0;$i<5-$a;$i++)
                                <span class="" style="    width: max-contant;"><i class="fa fa-star"     style="color: #79797975;"></i></span> 
                                @endfor
                                
        
                                ({{$related_product_details1->review}})
                                </span>
                                
                                <button type="button" class="addtocart pull-right" onclick="cart({{$product_id}});">Add</button>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                  </div>
                </div>

                @endforeach

             
              </div>
            </div>
          </div>
        </div> --}}




        <div class="featured_product section mt-30">
          <div class="row">
            <div class="col-12">
              <div class="section_title">New Arrival</div>
            </div>
            <div class="section-product grid section">

              @foreach($new_arival as $new_arival1)
                            @php($product_id=$new_arival1->product_id)
                            @php($image1=DB::table('product_images')->where('product_id',$product_id)->limit(1)->first())
                            @php($image2=DB::table('product_images')->where('product_id',$product_id)->limit(1)->orderby('product_image_id','desc')->first())
                            @php($brand=DB::table('brands')->where('brand_id',$new_arival1->brand_id)->first())
                            @php($sub_cat=DB::table('sub_cats')->where('sub_cat_id',$new_arival1->sub_cat_id)->first())

                                  <div class=" product-items col-6 col-sm-4 col-md-4 col-lg-3">
                                    <div class="product-thumb transition">

                                      @php($img_new=DB::table('product_images')->where('product_id',$new_arival1->product_id)->get())
                                    <div class="image">
                                      <div class="first_image"> <a href="/product/{{$product_id}}"> <img src="/product_image/{{$image1->image}}" alt="pure-spice-3" title="{{$new_arival1->product_name}}" class="img-responsive"> </a> </div>
                                      <div class="swap_image"> <a href="/product/{{$product_id}}"> <img src="/product_image/{{$image2->image}}" alt="pure-spice-3" title="{{$new_arival1->product_name}}" class="img-responsive"> </a></div>
                                      </div>
                                      <div class="product-details">
                                        <div class="caption">
                                          <h4><a href="/shop?brand={{$brand->brand_id}}">{{$brand->brand_name}}</a></h4>

                                          @php($progst=$new_arival1->selling_price*$sub_cat->gst/100)
                                          <p class="price">₹{{round($new_arival1->selling_price+$progst)}}</p>
                                          <div class="product_option">
                                            <div class="form-group required ">
                                              <a href="/product/{{$product_id}}"><?php echo substr($new_arival1->product_name,0,18); ?>@if(strlen($new_arival1->product_name)>18){{'...'}} @endif</a>
                                                      
                                            </div>
                                            <div class="input-group button-group">
                                              <span style="font-size: 10px;">
                                                @php($a=(int)$new_arival1->review)
                                                @for($i=0;$i<$a;$i++)
                                                <span class="" style="    width:  max-contant;"><i class="fa fa-star" style="color:#fa4251"></i></span>
                                                @endfor
                                                @for($i=0;$i<5-$a;$i++)
                                                <span class="" style="    width: max-contant;"><i class="fa fa-star"     style="color: #79797975;"></i></span> 
                                                @endfor
                                                ({{$new_arival1->review}})
                                                </span>
                                                
                                                <button type="button" class="addtocart pull-right addtocartbtn" onclick="cart({{$product_id}});">Add</button>
                                      
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                              @endforeach
                                </div>
                      
                              </div>
                            </div>
                          


    
        <!-- =====  featured section end ===== -->
  
  
        {{-- <div class="category_product section category_product2 mt-30">
          <div class="row">
            <div class="col-12">
              <div class="section_title">Grocery & Staples</div>
            </div>
            <div class="col-sm-9 left_block">
              <div class="row">
                <div class="product-layout col-lg-4 col-md-4 col-sm-4 col-6">
                  <div class="product-thumb transition">
                    <p class="tag">11<br>
                      % <br>
                      <i>off</i></p>
                    <h4><a href="category_page.html">Clasic cioclato</a></h4>
                    <div class="image"><a href="category_page.html"><img src="use_assets/images/06.jpg" alt="Apple" title="Apple" class="img-thumbnail"></a></div>
                  </div>
                </div>
                <div class="product-layout col-lg-4 col-md-4 col-sm-4 col-6">
                  <div class="product-thumb transition">
                    <p class="tag">20<br>
                      % <br>
                      <i>off</i></p>
                    <h4><a href="category_page.html">Lentilles</a></h4>
                    <div class="image"><a href="category_page.html"><img src="use_assets/images/05.jpg" alt="Strawberry" title="Strawberry" class="img-thumbnail"></a></div>
                  </div>
                </div>
                <div class="product-layout col-lg-4 col-md-4 col-sm-4 col-6">
                  <div class="product-thumb transition">
                    <h4><a href="category_page.html">Bikano shahi</a></h4>
                    <div class="image"><a href="category_page.html"><img src="use_assets/images/04.jpg" alt="Pineapple" title="Pineapple" class="img-thumbnail"></a></div>
                  </div>
                </div>
                <div class="product-layout col-lg-4 col-md-4 col-sm-4 col-6">
                  <div class="product-thumb transition">
                    <h4><a href="category_page.html">pure-spice-3</a></h4>
                    <div class="image"><a href="category_page.html"><img src="use_assets/images/03.jpg" alt="Banana" title="Banana" class="img-thumbnail"></a></div>
                  </div>
                </div>
                <div class="product-layout col-lg-4 col-md-4 col-sm-4 col-6">
                  <div class="product-thumb transition">
                    <h4><a href="category_page.html">il-tuo-espresso</a></h4>
                    <div class="image"><a href="category_page.html"><img src="use_assets/images/02.jpg" alt="Orange" title="Orange" class="img-thumbnail"></a></div>
                  </div>
                </div>
                <div class="product-layout col-lg-4 col-md-4 col-sm-4 col-6">
                  <div class="product-thumb transition">
                    <p class="tag">10<br>
                      % <br>
                      <i>off</i></p>
                    <h4><a href="category_page.html">Bladen</a></h4>
                    <div class="image"><a href="category_page.html"><img src="use_assets/images/01.jpg" alt="Greps" title="Greps" class="img-thumbnail"></a></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-3 productcategory_thumb right_block text-center mb-15"> <img src="use_assets/images/13.jpg" alt="" title="" class="img-thumbnail"> </div>
  
            <div class="col-sm-12 text-center">
              <div class="btn btn-primary viewall"><a href="category_page.html">View All</a></div>
            </div>
          </div>
        </div> --}}
  
        <!-- =====  PRODUCT section  END ===== -->
  
        <!-- =====  Brand start1 ===== -->

        <div class="featured_product section mt-30">
          <div class="row">
            <div class="col-12">
              <div class="section_title">Most View Product</div>
            </div>
            <div class="section-product grid section">

              @foreach($most_view_product as $most_view_product1)
                            @php($product_id=$most_view_product1->product_id)
                            @php($image1=DB::table('product_images')->where('product_id',$product_id)->limit(1)->first())
                            @php($image2=DB::table('product_images')->where('product_id',$product_id)->limit(1)->orderby('product_image_id','desc')->first())
                            @php($brand=DB::table('brands')->where('brand_id',$most_view_product1->brand_id)->first())
                            @php($sub_cat=DB::table('sub_cats')->where('sub_cat_id',$most_view_product1->sub_cat_id)->first())

              <div class=" product-items col-6 col-sm-4 col-md-4 col-lg-3">
                <div class="product-thumb transition">

                  @php($img_new=DB::table('product_images')->where('product_id',$new_arival1->product_id)->get())
                 <div class="image">
                  <div class="first_image"> <a href="/product/{{$product_id}}"> <img src="/product_image/{{$image1->image}}" alt="pure-spice-3" title="{{$new_arival1->product_name}}" class="img-responsive"> </a> </div>
                  <div class="swap_image"> <a href="/product/{{$product_id}}"> <img src="/product_image/{{$image2->image}}" alt="pure-spice-3" title="{{$new_arival1->product_name}}" class="img-responsive"> </a></div>
                  </div>
                  <div class="product-details">
                    <div class="caption">
                      <h4><a href="/shop?brand={{$brand->brand_id}}">{{$brand->brand_name}}</a></h4>

                      @php($progst=$most_view_product1->selling_price*$sub_cat->gst/100)
                      <p class="price">₹{{round($most_view_product1->selling_price+$progst)}}</p>
                      <div class="product_option">
                        <div class="form-group required ">
                          <a href="/product/{{$product_id}}"><?php echo substr($most_view_product1->product_name,0,18); ?>@if(strlen($most_view_product1->product_name)>18){{'...'}} @endif</a>
                                  
                        </div>
                        <div class="input-group button-group">
                          <span style="font-size: 10px;">
                            @php($a=(int)$most_view_product1->review)
                            @for($i=0;$i<$a;$i++)
                            <span class="" style="    width:  max-contant;"><i class="fa fa-star" style="color:#fa4251"></i></span>
                            @endfor
                            @for($i=0;$i<5-$a;$i++)
                            <span class="" style="    width: max-contant;"><i class="fa fa-star"     style="color: #79797975;"></i></span> 
                            @endfor
                            ({{$most_view_product1->review}})
                            </span>
                            
                            <button type="button" class="addtocart pull-right addtocartbtn" onclick="cart({{$product_id}});">Add</button>
                   
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
           @endforeach
            </div>
  
          </div>
        </div>




         <!-- =====  Brand start2 ===== -->


        <div id="brand_carouse" class="section text-center mt-30 pb-15">
          <div class="row">
            <div class="col-12">
              <div class="section_title">Our Popular Brands</div>
            </div>
            <div class="col-sm-12">
              <div class="brand owl-carousel">
                
              
                @php($brands=DB::table('brands')
                ->get())
                 @foreach($brands as $brands)
                <div class="product-thumb"><div class="item text-center"> <a href="/shop?brand={{$brands->brand_id}}"><img src="/brand_logo/{{$brands->brand_image}}" title="{{$brands->brand_name}}" alt="Disney" class="img-responsive" /></a> </div></div>
               
                @endforeach       
              </div>
            </div>
          </div>
        </div>
        <!-- =====  Brand end ===== -->
  
  
  
      </div>
      <!-- =====  CONTAINER END  ===== -->

@endsection