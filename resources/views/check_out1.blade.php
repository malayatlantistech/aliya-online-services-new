@extends('layouts.menu') @section('title') Checkout Item : E-Kirana @endsection @section('content')

<!-- ========== MAIN CONTENT ========== -->
<main id="content" role="main" class="checkout-page">
    <!-- breadcrumb -->
    <div class="bg-gray-13 bg-md-transparent">
        <div class="container">
            <!-- breadcrumb -->
            <div class="my-md-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="/">Home</a></li>
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">Checkout</li>
                    </ol>
                </nav>
            </div>
            <!-- End breadcrumb -->
        </div>
    </div>
    <!-- End breadcrumb -->

    <div class="container">
        <div class="mb-5">
            <h1 class="text-center">Checkout</h1>
        </div>
        <!-- Accordion -->

        <!-- End Accordion -->

        <!-- Accordion -->

        <!-- End Accordion -->
        <form class="js-validate" novalidate="novalidate">
            <div class="row">
                <div class="col-lg-5 order-lg-2 mb-7 mb-lg-0">
                    <div class="pl-lg-3">
                        <div class="bg-gray-1 rounded-lg">
                            <div class="p-4 mb-4 checkout-table">
                                <div class="input-group input-group-pill max-width-660-xl">
                                    <input class="form-control" type="text" id="coupon_code" name="coupon_code" placeholder="Apply Coupon code" />
                                    <div class="input-group-append">
                                        <a type="submit" class="btn btn-block btn-dark font-weight-normal btn-pill px-4">
                                            <i class="fas fa-tags d-md-none"></i>
                                            <span class="d-none d-md-inline" style="color: white;" onclick="coupon()">Apply coupon</span>
                                        </a>
                                    </div>
                                </div>
                                <span class="as"></span>
                                <br />
                                <!-- Title -->
                                <div class="border-bottom border-color-1 mb-5">
                                    <h3 class="section-title mb-0 pb-2 font-size-25">Your order</h3>
                                </div>
                                <!-- End Title -->

                                <!-- Product Content -->
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="product-name">Product</th>
                                            <th class="product-total" style="min-width: 92px;">
                                                <input type="text" style="width: 80px; border: 0; background: transparent; text-align: center; font-size: 15px; color: black;" value="Total" readonly />
                                            </th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @php($i=0) @foreach($cart as $carts) @php($product_price=DB::table('products')->where('product_id',$carts->product_id)->first())
                                        <tr class="cart_item">
                                            <td>{{$carts->product_name}} &nbsp; <strong class="product-quantity">× {{$carts->quantity}}</strong></td>
                                            <input type="hidden" id="product_id" name="product_id" value="{{$product_id1}}" />

                                            @php($price=$product_price->selling_price)

                                            <td>₹ <input type="text" style="width: 80px; border: 0; background: transparent; text-align: center; font-size: 15px; color: black;" value="{{$price}}" readonly /></td>
                                        </tr>
                                        @php($i=$i+$price) @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Subtotal</th>
                                            <td>₹<input type="text" style="width: 80px; border: 0; background: transparent; text-align: center; font-size: 15px; color: black;" id="sub_total" name="sub_total" value="{{$i}}" readonly /></td>
                                        </tr>
                                        <tr>
                                            <th>Shipping</th>
                                            <td>
                                                ₹
                                                <input type="text" style="width: 80px; border: 0; background: transparent; text-align: center; font-size: 17px; color: black;" id="shipping_charge" name="shipping_charge" value="0" readonly />
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Coupon Discount</th>
                                            <td>
                                                ₹
                                                <input type="text" style="width: 80px; border: 0; background: transparent; text-align: center; font-size: 17px; color: black;" id="coupon_discount" name="coupon_discount" value="0" readonly />
                                            </td>
                                        </tr>
                                        <tr>
                                            <th><input type="checkbox" id="wa_am" onchange="wa_amount(this.checked);" style="font-size: 10px;" /> Wallet Amount</th>
                                            <td>
                                                ₹ <input type="text" style="width: 80px; border: 0; background: transparent; text-align: center; font-size: 17px; color: black;" id="wallet_amount" name="wallet_amount" value="0" readonly />
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Total</th>
                                            <td>
                                                <strong>
                                                    ₹
                                                    <input
                                                        type="text"
                                                        style="width: 80px; border: 0; background: transparent; text-align: center; font-size: 17px; color: black;"
                                                        id="total_payble_value"
                                                        name="total_payble_value"
                                                        value="0"
                                                        readonly
                                                    />
                                                </strong>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>

                                <!-- End Product Content -->
                                <div class="border-top border-width-3 border-color-1 pt-3 mb-3">
                                    <!-- Basics Accordion -->
                                    <div id="basicsAccordion1">
                                        <!-- Card -->
                                        <input type="hidden" value="{{$cod_count}}" id="cod" name="cod" />
                                        <center style="font-size: 20px;">
                                            <input type="radio" name="payment-group" style="width: 15px;" value="cash" id="payment-2" />&nbsp; <label for="payment-2" id="ddd">Cash On Delivery</label> &nbsp; &nbsp;
                                            <input type="radio" name="payment-group" value="online" style="width: 15px;" id="payment-1" checked="checked" /> &nbsp;<label for="payment-1">Online Payment</label>
                                        </center>
                                        <center>
                                            <span id="ccc"></span>
                                        </center>

                                        <input class="form-control" id="pincode_avalable" type="hidden" placeholder="" />
                                    </div>
                                    <!-- End Basics Accordion -->
                                </div>

                                <a type="submit" class="btn btn-primary-dark-w btn-block btn-pill font-size-20 mb-3 py-3" onclick="bookingpayment()" style="color: white;">Place order</a>
                            </div>

                            <!-- End Order Summary -->
                        </div>
                    </div>
                </div>

                <div class="col-lg-7 order-lg-1">
                    <div class="pb-7 mb-7">
                        <!-- Title -->
                        <div class="border-bottom border-color-1 mb-5">
                            <h3 class="section-title mb-0 pb-2 font-size-25">Shipping Details details</h3>
                        </div>
                        <!-- End Title -->
                        <!-- Accordion -->

                        <div class="col-lg-12 col-12">
                            <div id="accordion">
                                @if($address2>0)

                                <input type="radio" name="address" value="gg" id="address" checked style="display: none;" />

                                @foreach($address as $address1)
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h5 class="mb-0">
                                            @php($pin_code=$address1->pincode) @php($available=DB::table('pincodes')->where('pincode',$pin_code)->first())

                                            <table>
                                                @if($available->active_status=='NO')
                                                <tr width="100%">
                                                    <td style="padding: 10px;" width="7%">
                                                        <input type="radio" id="address" name="address" disabled class="form-control" style="width: 20px; height: 20px;" />
                                                    </td>

                                                    <td width="83%">
                                                        <label for="female">
                                                            <span style="font-size: 17px;"><b>{{$address1->name}}</b>, {{$address1->phone_no}}</span><br />
                                                            <font style="font-size: 13px; line-height: 24px;"> {{$address1->address}},{{$address1->landmark}},{{$address1->city}},{{$address1->state}}-{{$address1->pincode}}</font><br />
                                                            <span style="color: red; font-size: 15px;">Delivery Not Available In This Pin Code</span>
                                                        </label>
                                                    </td>

                                                    <td align="right" width="10%">
                                                        <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne{{$address1->id}}" aria-expanded="true" aria-controls="collapseOne{{$address1->id}}">
                                                            <h6 align="right" style="color: black;">Edit</h6>
                                                        </a>
                                                    </td>
                                                </tr>
                                                @else
                                                <tr width="100%">
                                                    <td style="padding: 10px;" width="7%">
                                                        <input type="radio" id="address" name="address" value="{{$address1->id}}" onclick="shiping({{$address1->id}})" class="form-control" style="width: 20px; height: 20px;" />
                                                    </td>

                                                    <td width="83%">
                                                        <label for="female">
                                                            <span style="font-size: 17px;"><b>{{$address1->name}}</b>, {{$address1->phone_no}}</span><br />
                                                            <font style="font-size: 13px; line-height: 24px;"> {{$address1->address}},{{$address1->landmark}},{{$address1->city}},{{$address1->state}}-{{$address1->pincode}}</font>
                                                        </label>
                                                    </td>

                                                    <td align="right" width="10%">
                                                        <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne{{$address1->id}}" aria-expanded="true" aria-controls="collapseOne{{$address1->id}}">
                                                            <h6 align="right" style="color: black;">Edit</h6>
                                                        </a>
                                                    </td>
                                                </tr>

                                                @endif
                                            </table>
                                        </h5>
                                    </div>

                                    <div id="collapseOne{{$address1->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <!-- Input -->
                                                    <div class="js-form-message mb-6">
                                                        <label class="form-label">
                                                            Postcode/Zip
                                                            <span class="text-danger">*</span>
                                                        </label>
                                                        <input class="form-control" type="pin" name="pin_code" id="pin_code" readonly value="{{$address1->pincode}}" />
                                                    </div>
                                                    <!-- End Input -->
                                                </div>

                                                <div class="col-md-6">
                                                    <!-- Input -->
                                                    <div class="js-form-message mb-6">
                                                        <label class="form-label">
                                                            GST No (optional)
                                                        </label>
                                                        <input class="form-control" type="text" name="gst" id="gst" value="{{$address1->gst_no}}" />
                                                    </div>
                                                    <!-- End Input -->
                                                </div>

                                                <div class="w-100"></div>

                                                <div class="col-md-12">
                                                    <!-- Input -->
                                                    <div class="js-form-message mb-6">
                                                        <label class="form-label">
                                                            State
                                                            <span class="text-danger">*</span>
                                                        </label>
                                                        <input class="form-control" type="text" name="district" id="district" value="{{$address1->state}}" />
                                                    </div>
                                                    <!-- End Input -->
                                                </div>

                                                <div class="col-md-6">
                                                    <!-- Input -->
                                                    <div class="js-form-message mb-6">
                                                        <label class="form-label">
                                                            Area
                                                            <span class="text-danger">*</span>
                                                        </label>
                                                        <input class="form-control" type="text" name="area" id="area" value="{{$address1->location}}" />
                                                    </div>
                                                    <!-- End Input -->
                                                </div>

                                                <div class="col-md-6">
                                                    <!-- Input -->
                                                    <div class="js-form-message mb-6">
                                                        <label class="form-label">
                                                            City
                                                            <span class="text-danger">*</span>
                                                        </label>
                                                        <input class="form-control" type="text" name="city" id="city" value="{{$address1->city}}" />
                                                    </div>
                                                    <!-- End Input -->
                                                </div>

                                                <div class="col-md-12">
                                                    <!-- Input -->
                                                    <div class="js-form-message mb-6">
                                                        <label class="form-label">
                                                            District
                                                            <span class="text-danger">*</span>
                                                        </label>
                                                        <input class="form-control" type="text" name="district" id="district" value="{{$address1->district}}" />
                                                    </div>
                                                    <!-- End Input -->
                                                </div>

                                                <div class="w-100"></div>

                                                <div class="col-md-6">
                                                    <!-- Input -->
                                                    <div class="js-form-message mb-6">
                                                        <label class="form-label">
                                                            House No(Optioal)
                                                        </label>
                                                        <input class="form-control" type="text" name="house_no" id="house_no" value="{{$address1->flat}}" />
                                                    </div>
                                                    <!-- End Input -->
                                                </div>

                                                <div class="col-md-6">
                                                    <!-- Input -->
                                                    <div class="js-form-message mb-6">
                                                        <label class="form-label">
                                                            Landmark(Optional)
                                                        </label>
                                                        <input class="form-control" type="text" name="landmark" id="landmark" value="{{$address1->landmark}}" />
                                                    </div>

                                                    <!-- End Input -->
                                                </div>

                                                <div class="col-md-12">
                                                    <!-- Input -->
                                                    <div class="js-form-message mb-6">
                                                        <label class="form-label">
                                                            Address
                                                            <span class="text-danger">*</span>
                                                        </label>
                                                        <textarea class="form-control" rows="4" cols="50" name="add" id="add">{{$address1->address}}</textarea>
                                                    </div>
                                                    <!-- End Input -->
                                                </div>

                                                <div class="col-md-12">
                                                    <!-- Input -->
                                                    <div class="js-form-message mb-6">
                                                        <label class="form-label">
                                                            Name
                                                            <span class="text-danger">*</span>
                                                        </label>
                                                        <input class="form-control" type="text" name="name" id="name" value="{{$address1->name}}" />
                                                    </div>
                                                    <!-- End Input -->
                                                </div>

                                                <div class="col-md-6">
                                                    <!-- Input -->
                                                    <div class="js-form-message mb-6">
                                                        <label class="form-label">
                                                            Phone No
                                                            <span class="text-danger">*</span>
                                                        </label>
                                                        <input class="form-control" type="number" name="phone1" id="phone1" value="{{$address1->phone_no}}" pattern="^\d{10}$" required />
                                                    </div>
                                                    <!-- End Input -->
                                                </div>

                                                <div class="col-md-6">
                                                    <!-- Input -->
                                                    <div class="js-form-message mb-6">
                                                        <label class="form-label">
                                                            Optional Phone No (Optional)
                                                        </label>
                                                        <input class="form-control" type="number" name="phone2" id="phone2" value="{{$address1->optional_phone}}" pattern="^\d{10}$" />
                                                    </div>

                                                    <!-- End Input -->
                                                </div>

                                                <input type="hidden" value="{{$address1->id}}" name="address_id" id="address_id" />

                                                <div class="w-100">
                                                    <center><button type="button" class="ps-btn ps-btn btn btn-secondary" onclick="address_update()">Submit</button></center>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach @else
                                    <input type="radio" name="address" value="gg" id="address" checked style="display: none;" />
                                    @endif

                                    <div class="card">
                                        <div class="card-header" id="headingTwo">
                                            <h2 class="mb-0">
                                                <center>
                                                    <a style="color: white;" class="btn btn-secondary" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                        Add New Address
                                                    </a>
                                                </center>
                                            </h2>
                                        </div>
                                        <div id="collapseTwo" class="collapse @if($address2==0) show @endif" aria-labelledby="headingTwo" data-parent="#accordion">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <!-- Input -->
                                                        <div class="js-form-message mb-6">
                                                            <label class="form-label">
                                                                Postcode/Zip
                                                                <span class="text-danger">*</span>
                                                            </label>
                                                            <input class="form-control" type="pin" name="pin_code1" id="pin_code1" onkeyup="fetch_address(this.value,'{{csrf_token()}}')" />
                                                            <p style="color: red;" id="pincode_error"></p>
                                                        </div>
                                                        <!-- End Input -->
                                                    </div>

                                                    <div class="col-md-6">
                                                        <!-- Input -->
                                                        <div class="js-form-message mb-6">
                                                            <label class="form-label">
                                                                GST No (optional)
                                                            </label>
                                                            <input class="form-control" type="text" name="gst1" id="gst1" />
                                                        </div>
                                                        <!-- End Input -->
                                                    </div>

                                                    <div class="w-100"></div>

                                                    <div class="col-md-12">
                                                        <!-- Input -->
                                                        <div class="js-form-message mb-6">
                                                            <label class="form-label">
                                                                State
                                                                <span class="text-danger">*</span>
                                                            </label>
                                                            <input class="form-control" type="text" name="state1" id="state1" />
                                                        </div>
                                                        <!-- End Input -->
                                                    </div>

                                                    <div class="col-md-6">
                                                        <!-- Input -->
                                                        <div class="js-form-message mb-6">
                                                            <label class="form-label">
                                                                Area
                                                                <span class="text-danger">*</span>
                                                            </label>
                                                            <input class="form-control" type="text" name="area1" id="area1" />
                                                        </div>
                                                        <!-- End Input -->
                                                    </div>

                                                    <div class="col-md-6">
                                                        <!-- Input -->
                                                        <div class="js-form-message mb-6">
                                                            <label class="form-label">
                                                                City
                                                                <span class="text-danger">*</span>
                                                            </label>
                                                            <input class="form-control" type="text" name="city1" id="city1" />
                                                        </div>
                                                        <!-- End Input -->
                                                    </div>

                                                    <div class="w-100"></div>

                                                    <div class="col-md-12">
                                                        <!-- Input -->
                                                        <div class="js-form-message mb-6">
                                                            <label class="form-label">
                                                                District
                                                                <span class="text-danger">*</span>
                                                            </label>
                                                            <input class="form-control" type="text" name="district1" id="district1" />
                                                        </div>
                                                        <!-- End Input -->
                                                    </div>

                                                    <div class="col-md-6">
                                                        <!-- Input -->
                                                        <div class="js-form-message mb-6">
                                                            <label class="form-label">
                                                                House No(Optioal)
                                                            </label>
                                                            <input class="form-control" type="text" name="house_no1" id="house_no1" />
                                                        </div>
                                                        <!-- End Input -->
                                                    </div>

                                                    <div class="col-md-6">
                                                        <!-- Input -->
                                                        <div class="js-form-message mb-6">
                                                            <label class="form-label">
                                                                Landmark(Optional)
                                                            </label>
                                                            <input class="form-control" type="text" name="landmark1" id="landmark1" />
                                                        </div>

                                                        <!-- End Input -->
                                                    </div>

                                                    <div class="col-md-12">
                                                        <!-- Input -->
                                                        <div class="js-form-message mb-6">
                                                            <label class="form-label">
                                                                Address
                                                                <span class="text-danger">*</span>
                                                            </label>
                                                            <textarea class="form-control" rows="4" cols="50" name="address1" id="address1"> </textarea>
                                                        </div>
                                                        <!-- End Input -->
                                                    </div>

                                                    <div class="col-md-12">
                                                        <!-- Input -->
                                                        <div class="js-form-message mb-6">
                                                            <label class="form-label">
                                                                Name
                                                                <span class="text-danger">*</span>
                                                            </label>
                                                            <input class="form-control" type="text" name="name1" id="name1" />
                                                        </div>
                                                        <!-- End Input -->
                                                    </div>

                                                    <div class="col-md-6">
                                                        <!-- Input -->
                                                        <div class="js-form-message mb-6">
                                                            <label class="form-label">
                                                                Phone No
                                                                <span class="text-danger">*</span>
                                                            </label>
                                                            <input class="form-control" type="number" name="phone11" id="phone11" pattern="^\d{10}$" required />
                                                        </div>
                                                        <!-- End Input -->
                                                    </div>

                                                    <div class="col-md-6">
                                                        <!-- Input -->
                                                        <div class="js-form-message mb-6">
                                                            <label class="form-label">
                                                                Optional Phone No (Optional)
                                                            </label>
                                                            <input class="form-control" type="number" name="phone21" id="phone21" pattern="^\d{10}$" />
                                                        </div>

                                                        <!-- End Input -->
                                                    </div>
                                                    <input type="hidden" value="" name="address_id" id="address_id" />

                                                    <div class="w-100">
                                                        <center><button type="button" id="address_su" onclick="address_submit()" class="btn btn-secondary">Submit</button></center>
                                                        <br />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Accordion -->

                                    <!-- End Input -->
                                </div>

                                <div id="shopCartAccordion2" class="accordion rounded mb-6">
                                    <!-- Card -->

                                    <div id="shopCartThree" class="collapse" aria-labelledby="shopCartHeadingThree" data-parent="#shopCartAccordion2" style="">
                                        <!-- Form Group -->
                                        <div class="js-form-message form-group py-5">
                                            <label class="form-label" for="signinSrPasswordExample1">
                                                Create account password
                                                <span class="text-danger">*</span>
                                            </label>
                                            <input
                                                type="password"
                                                class="form-control"
                                                name="password"
                                                id="signinSrPasswordExample1"
                                                placeholder="********"
                                                aria-label="********"
                                                required
                                                data-msg="Enter password."
                                                data-error-class="u-has-error"
                                                data-success-class="u-has-success"
                                            />
                                        </div>
                                        <!-- End Form Group -->
                                    </div>
                                </div>
                                <!-- End Card -->
                            </div>
                            <!-- End Accordion -->
                            <!-- Title -->
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</main>
<!-- ========== END MAIN CONTENT ========== -->
<input type="hidden" id="product_id" name="product_id" value="{{$product_id1}}" />
<input type="hidden" id="name55" placeholder="name" />
<input type="hidden" id="phone55" />
<input type="hidden" id="email55" />
<input type="hidden" id="add55" />

<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<!-- Address Submited -->
<script>
    function address_submit() {
        $("#overlay").fadeIn(300);
        var pin_code = $("#pin_code1").val();
        var gst = $("#gst1").val();
        var house_no = $("#house_no1").val();
        var area = $("#area1").val();
        var city = $("#city1").val();
        var district = $("#district1").val();
        var state = $("#state1").val();
        var landmark = $("#landmark1").val();
        var address = $("#address1").val();
        var name = $("#name1").val();
        var phone1 = $("#phone11").val();
        var phone2 = $("#phone21").val();
        var token = $("#_token").val();
        if (phone1 != "" && name != "" && address != "" && state != "" && district != "" && city != "" && area != "" && pin_code != "") {
            $.ajax({
                url: "address_submit",

                type: "POST",

                data: { _token: token, phone2: phone2, phone1: phone1, name: name, address: address, state: state, district: district, city: city, area: area, pin_code: pin_code, landmark: landmark, house_no: house_no, gst: gst },

                success: function (response) {
                    $("#overlay").fadeOut(300);
                    mdtoast("address has been submited.", {
                        type: "success",
                        duration: 4000,
                    });

                    location.reload(true);
                },
            });
        } else {
            swal("Please fill up proper address");
            $("#overlay").fadeOut(300);
        }
    }
</script>

<!-- featching address -->
<script>
    function fetch_address(pin, token) {
        if (pin.length == 6) {
            $("#overlay").fadeIn(300);
            $.ajax({
                url: "fetch_address_checkout",
                type: "POST",
                data: { _token: token, pin: pin },
                success: function (response) {
                    var data = response.split("|checkoutfatchaddredd|");
                    //  alert(data[4]);
                    if (data[4] == 1) {
                        $("#address_su").prop("disabled", false);
                        $("#area1").val(data[0]);
                        $("#city1").val(data[1]);
                        $("#district1").val(data[2]);
                        $("#state1").val(data[3]);
                        $("#pincode_error").html("");
                        $("#overlay").fadeOut(300);
                    } else if (data[4] == 2) {
                        $("#address_su").prop("disabled", true);
                        $("#area1").val("");
                        $("#city1").val("");
                        $("#district1").val("");
                        $("#state1").val("");
                        $("#pincode_error").html("Delivery not available");
                        document.getElementById("area1").placeholder = "Enter Location";
                        document.getElementById("city1").placeholder = "Enter City";
                        document.getElementById("district1").placeholder = "Enter District";
                        document.getElementById("state1").placeholder = "Enter State";
                        $("#overlay").fadeOut(300);
                    } else {
                        $("#address_su").prop("disabled", true);
                        $("#area1").val("");
                        $("#city1").val("");
                        $("#district1").val("");
                        $("#state1").val("");
                        $("#pincode_error").html("Please enter valid pincode");
                        document.getElementById("area1").placeholder = "Enter Location";
                        document.getElementById("city1").placeholder = "Enter City";
                        document.getElementById("district1").placeholder = "Enter District";
                        document.getElementById("state1").placeholder = "Enter State";
                        $("#overlay").fadeOut(300);
                    }
                },
            });
        }
    }
</script>

<script>
    function address_update() {
        $("#overlay").fadeIn(300);
        var pin_code = $("#pin_code").val();
        var gst = $("#gst").val();
        var house_no = $("#house_no").val();
        var area = $("#area").val();
        var city = $("#city").val();
        var district = $("#district").val();
        var state = $("#state").val();
        var landmark = $("#landmark").val();
        var address = $("#add").val();
        var name = $("#name").val();
        var phone1 = $("#phone1").val();
        var phone2 = $("#phone2").val();
        var token = $("#_token").val();
        var address_id = $("#address_id").val();
        if (phone1 != "" && name != "" && address != "" && state != "" && district != "" && city != "" && area != "" && pin_code != "") {
            $.ajax({
                url: "address_update",
                type: "POST",
                data: {
                    _token: token,
                    phone2: phone2,
                    phone1: phone1,
                    name: name,
                    address: address,
                    state: state,
                    district: district,
                    city: city,
                    area: area,
                    pin_code: pin_code,
                    landmark: landmark,
                    house_no: house_no,
                    gst: gst,
                    address_id: address_id,
                },
                success: function (response) {
                    $("#overlay").fadeOut(300);
                    mdtoast("address has been updated.", {
                        type: "success",
                        duration: 4000,
                    });
                    location.reload(true);
                },
            });
        } else {
            swal("Please fill up proper address");
        }
    }
</script>

<!-- wallet balance check or uncheck -->
<script>
    function wa_amount(aa) {
        var cod = $("#cod").val();

        if (aa.toString() == "true") {
            var id = $("#shipping_charge").val();
            wallet_calculation(id);
            final_value();
            // $('#payment-1').prop('checked',true);
            // $('#payment-2').prop('disabled',true);
        } else {
            var wallet = $(".wallet_amount").val();
            $("#wallet_amount").val(0);
            final_value();
        }
    }
</script>

<!-- Shipingcharge -->
<script>
    function shiping(id) {
        var price1 = $("#sub_total").val();
        var token = $("#_token").val();

        $.ajax({
            url: "/shipping_calculation",
            type: "POST",
            data: { _token: token, address_id: id, price1: price1 },
            success: function (response1) {
                if (response1 == 5) {
                    swal("Delivary Unavailable!", "Please Change your address!", "warning");
                    $("#pincode_avalable").val(0);
                } else {
                    $("#pincode_avalable").val(1);
                    var response = response1.split("quantexfggjfjftry");
                    $("#name55").val(response[0]);
                    $("#phone55").val(response[1]);
                    $("#email55").val(response[2]);
                    $("#add55").val(response[3]);

                    $("#shipping_charge").val(response[5]);

                    wallet_calculation(response[5]);

                    if (price1 > 399) {
                        $("#wa_am").prop("disabled", false);
                        $("#wa_am").prop("checked", true);
                    } else {
                        $("#wa_am").prop("disabled", true);
                        $("#wa_am").prop("checked", false);
                    }

                    if (response[4] == "YES") {
                        $("#payment-2").prop("disabled", false);
                    } else {
                        $("#payment-2").css({ display: "none" });
                        $("#ddd").css({ display: "none" });

                        $("#ccc").text("COD Not Available In this Pin Code");
                        $("#ccc").css({ "font-size": "15px", color: "red" });
                    }
                }
            },
        });
    }
</script>

<!-- wallet Calculation   -->
<script>
    function wallet_calculation(id) {
        var coupon_discount = $("#coupon_discount").val();
        var price1 = $("#sub_total").val();
        var token = $("#_token").val();
        $.ajax({
            url: "/wallet_calculation",
            type: "POST",
            data: { _token: token, price1: price1, s_charge: id, coupon_discount: coupon_discount },
            success: function (response) {
                $("#wallet_amount").val(response);
                $("#coupon_discount").val(coupon_discount);
                final_value();
            },
        });
    }
</script>

<!-- coupon calculation -->
<script>
    function coupon() {
        var s = $("#coupon_code").val();
        var token = $("#_token").val();
        var price1 = $("#sub_total").val();
        var shipping_charge = $("#shipping_charge").val();
        var wallet_amount = $("#wallet_amount").val();
        var product_id = $("#product_id").val();

        $.ajax({
            url: "/coupon_calculation",
            type: "POST",
            data: { _token: token, coupon_code: s, price1: price1, shipping_charge: shipping_charge, product_id: product_id },
            success: function (response3) {
                var response2 = response3.split("|");
                if (response2[0] == 3) {
                    $(".as").text("Invalid Coupon Code!");
                    $(".as").css("color", "red");
                    $("#coupon_discount").val(0);
                    $("#shipping_charge").val(shipping_charge);
                    $("#wallet_amount").val(wallet_amount);
                } else if (response2[0] == 2) {
                    $(".as").text("Coupon Expair!");
                    $(".as").css("color", "#3498DB");
                    $("#coupon_discount").val(0);
                    $("#shipping_charge").val(shipping_charge);
                    $("#wallet_amount").val(wallet_amount);
                } else if (response2[0] == 4) {
                    $(".as").text("Must be order value gratter then " + response2[1]);
                    $(".as").css("color", "#3498DB");
                    $("#coupon_discount").val(0);
                    $("#shipping_charge").val(shipping_charge);
                    $("#wallet_amount").val(wallet_amount);
                } else if (response2[0] == 8) {
                    $(".as").text("Coupon not applicable for this category");
                    $(".as").css("color", "#3498DB");
                    $("#coupon_discount").val(0);
                    $("#shipping_charge").val(shipping_charge);
                    $("#wallet_amount").val(wallet_amount);
                } else if (response2[0] == 9) {
                    $(".as").text("Coupon not applicable only " + response2[1] + " category");
                    $(".as").css("color", "#3498DB");
                    $("#coupon_discount").val(0);
                    $("#shipping_charge").val(shipping_charge);
                    $("#wallet_amount").val(wallet_amount);
                } else if (response2[0] == 5) {
                    $(".as").text("You already used this coupon");
                    $(".as").css("color", "#3498DB");
                    $("#coupon_discount").val(0);
                    $("#shipping_charge").val(shipping_charge);
                    $("#wallet_amount").val(wallet_amount);
                } else {
                    $(".as").text("Coupon Applied");
                    $(".as").css("color", "#45CE30");
                    var response1 = response3.split("|");
                    var wallet_ammount = response1[0];
                    var coupon_price = response1[1];
                    var s_charge = response1[2];
                    $("#coupon_discount").val(coupon_price);
                    $("#shipping_charge").val(s_charge);
                    $("#wallet_amount").val(wallet_ammount);
                }
                final_value();
            },
        });
    }
</script>

<!-- Final Value -->
<script>
    function final_value() {
        var coupon_discount = $("#coupon_discount").val();
        var shipping_charge = $("#shipping_charge").val();
        var wallet_amount = $("#wallet_amount").val();
        var price1 = $("#sub_total").val();
        var token = $("#_token").val();
        var total_payble_value = Number(price1) + Number(shipping_charge) - Number(coupon_discount) - Number(wallet_amount);
        $("#total_payble_value").val(total_payble_value);
    }
</script>

<!--  payment getway -->
<script>
    function bookingpayment() {
        var pincode_avalable = $("#pincode_avalable").val();
        var token = $("#_token").val();
        var coupon_discount = $("#coupon_discount").val();
        var shipping_charge = $("#shipping_charge").val();
        var wallet_amount = $("#wallet_amount").val();
        var price1 = $("#sub_total").val();
        var total_payble_value = $("#total_payble_value").val();
        var radioValue = $("input[name='address']:checked").val();
        var product_id = $("#product_id").val();
        var type = $("input[name='payment-group']:checked").val();

        if (radioValue != "gg") {
            if (pincode_avalable != 0) {
                $("#overlay").fadeIn(300);
                if (total_payble_value != 0) {
                    if (type == "online") {
                        $.ajax({
                            type: "POST",
                            url: "/pay",
                            headers: {
                                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                            },
                            data: { name_val: $("#name55").val(), amt_val: $("#total_payble_value").val(), phone_val: $("#phone55").val() },
                            success: function (data) {
                                var options2 = {
                                    key: "rzp_test_s8waJxmKNm05sl",
                                    amount: data["amount"],
                                    currency: "INR",
                                    name: "Siddharth Shah Jewellers",
                                    order_id: data["order_id"],
                                    handler: function (response) {
                                        $("#text").css("display", "block");
                                        order_submit("online", response.razorpay_payment_id);
                                    },
                                    prefill: {
                                        name: $("#name55").val(),
                                        email: $("#email55").val(),
                                        contact: $("#phone55").val(),
                                    },
                                    notes: {
                                        address: $("#add55").val(),
                                    },
                                    theme: {
                                        color: "#dd2400",
                                    },
                                    modal: {
                                        ondismiss: function () {
                                            $("#overlay").fadeOut(300);
                                            mdtoast("You cancel the payment.", {
                                                type: "warning",
                                                duration: 10000,
                                            });
                                            //alert();
                                        },
                                    },
                                };
                                var rzp2 = new Razorpay(options2);
                                rzp2.open();
                                e.preventDefault();
                            },
                            error: function (data, textStatus, errorThrown) {},
                        });
                    } else {
                        order_submit("cash", 0);
                    }
                } else {
                    order_submit("wallet", 0);
                }
            } else {
                swal("Delivary Unavailable!", "Please Change your address!", "warning");
            }
        } else {
            swal("Please Select Address");
        }
    }
</script>
<!-- order submited -->
<script>
    function order_submit(payment_type, razorpay_payment_id) {
        var token = $("#_token").val();
        var coupon_discount = $("#coupon_discount").val();
        var shipping_charge = $("#shipping_charge").val();
        var wallet_amount = $("#wallet_amount").val();
        var price1 = $("#sub_total").val();
        var total_payble_value = $("#total_payble_value").val();
        var radioValue = $("input[name='address']:checked").val();
        var product_id = $("#product_id").val();
        var coupon_code = $("#coupon_code").val();
        if (radioValue != "gg") {
            $.ajax({
                url: "/order_submit",
                type: "POST",
                data: {
                    _token: token,
                    coupon_discount: coupon_discount,
                    shipping_charge: shipping_charge,
                    wallet_amount: wallet_amount,
                    price1: price1,
                    radioValue: radioValue,
                    total_payble_value: total_payble_value,
                    product_id: product_id,
                    razorpay_payment_id: razorpay_payment_id,
                    payment_type: payment_type,
                    coupon_code: coupon_code,
                },
                success: function (response) {
                    window.location.href = "successfull";
                    $("#overlay").fadeOut(300);
                    // alert('booking successfully');
                    //  window.location.replace('home');
                },
            });
        } else {
            swal("Please Select Address");
        }
    }
</script>

@endsection
