      
@extends('layouts.menu')

@section('title')
India's Most Popular Jewellery Shopping Website : E-Kirana
@endsection

@section('content')
<style>
    
    .price-slider {
        padding-top: 00px;
    
      margin: auto;
      text-align: center;
      position: relative;
    
    }
    .price-slider svg,
    .price-slider input[type=range] {
      position: absolute;
      left: 0;
      bottom: 0;
      background: transparent;
    }
    input[type=number] {
        text-align: center;
        font-size: 14px;
        width: max-content;
        border: 0;
        padding: 20px 0px;
        background: transparent;
      -moz-appearance: textfield;
    }
    input[type=number]::-webkit-outer-spin-button,
    input[type=number]::-webkit-inner-spin-button {
      -webkit-appearance: none;
    }
    input[type=number]:invalid,
    input[type=number]:out-of-range {
      border: 2px solid #e60023;
    }
    input[type=range] {
      -webkit-appearance: none;
      width: 100%;
    }
    input[type=range]:focus {
      outline: none;
    }
    input[type=range]:focus::-webkit-slider-runnable-track {
      background: #1da1f2;
    }
    input[type=range]:focus::-ms-fill-lower {
      background: #1da1f2;
    }
    input[type=range]:focus::-ms-fill-upper {
      background: #1da1f2;
    }
    input[type=range]::-webkit-slider-runnable-track {
      width: 100%;
      height: 5px;
      cursor: pointer;
      animate: 0.2s;
      background: #1da1f2;
      border-radius: 1px;
      box-shadow: none;
      border: 0;
    }
    input[type=range]::-webkit-slider-thumb {
      z-index: 2;
      position: relative;
      box-shadow: 0px 0px 0px #000;
      border: 1px solid #1da1f2;
      height: 18px;
      width: 18px;
      border-radius: 25px;
      background: #a1d0ff;
      cursor: pointer;
      -webkit-appearance: none;
      margin-top: -7px;
    }
    input[type=range]::-moz-range-track {
      width: 100%;
      height: 5px;
      cursor: pointer;
      animate: 0.2s;
      background: #1da1f2;
      border-radius: 1px;
      box-shadow: none;
      border: 0;
    }
    input[type=range]::-moz-range-thumb {
      z-index: 2;
      position: relative;
      box-shadow: 0px 0px 0px #000;
      border: 1px solid #1da1f2;
      height: 18px;
      width: 18px;
      border-radius: 25px;
      background: #a1d0ff;
      cursor: pointer;
    }
    input[type=range]::-ms-track {
      width: 100%;
      height: 5px;
      cursor: pointer;
      animate: 0.2s;
      background: transparent;
      border-color: transparent;
      color: transparent;
    }
    input[type=range]::-ms-fill-lower,
    input[type=range]::-ms-fill-upper {
      background: #1da1f2;
      border-radius: 1px;
      box-shadow: none;
      border: 0;
    }
    input[type=range]::-ms-thumb {
      z-index: 2;
      position: relative;
      box-shadow: 0px 0px 0px #000;
      border: 1px solid #1da1f2;
      height: 18px;
      width: 18px;
      border-radius: 25px;
      background: #a1d0ff;
      cursor: pointer;
    }
    .codes{
        bottom: 5%;
        left: 5%;
        position: fixed;
      }
      .codes div {
        border: 2px solid black;
        font-size: 20px;
        padding: 10px;
        background-color: red;
      }
      .codes div a{
        text-decoration: none;
        color: white;
        font-weight: 800;
      }
     
    .pagination {
        max-width: fit-content;
    }
    .custom-control-label {
        
        margin-left: 6px;
    }
    .custom-control-input {
        position: initial;
        z-index: -1;
        opacity: 0;
    }
    .price-filter {
        padding-top: 10px;
        padding-bottom: 10px;
    }
    input[type='range']::-webkit-slider-runnable-track {
        
          -webkit-appearance: none;
          color: #13bba4;
          margin-top: -1px;
        }
        
      .nav-responsive{
          padding-bottom: 30px !important;
          color: black;
      }
      .addtocartbtn {

    line-height: 2 !important;
 
}
.section_title {

    border-bottom: 1px solid #cecdcd !important;
    
}
    </style>
     <script async src="/assets/price_slider/price.js"></script>
     <link rel="stylesheet" href="/assets/price_slider/price.css">
@if(isset($_GET['sub_sub_id']))
   <input type="hidden" value="{{$_GET['sub_sub_id']}}"  id="sub_sub_id">
   @else
   <input type="hidden" value="0"  id="sub_sub_id">
    @endif

    @if(isset($_GET['sub_id']))
   <input type="hidden" value="{{$_GET['sub_id']}}"  id="sub_idd">
   @else
   <input type="hidden" value="0"  id="sub_idd">
    @endif

    
    @if(isset($_GET['cat_id']))
   <input type="hidden" value="{{$_GET['cat_id']}}"  id="cat_id">
   @else
   <input type="hidden" value="0"  id="cat_id">
    @endif

    @if(isset($_GET['p_name']))
   <input type="hidden" value="{{$_GET['p_name']}}"  id="p_name">
   @else
   <input type="hidden" value="0"  id="p_name">
    @endif
    @if(isset($_GET['Product_name']))
   <input type="hidden" value="{{$_GET['Product_name']}}"  id="Product_name">
   @else
   <input type="hidden" value="0"  id="Product_name">
    @endif

@guest
<input type="hidden" value="USER"  id="user_type">
@else
<input type="hidden" value="{{Auth::user()->usertype}}"  id="user_type">
@endguest
   <!-- =====  BREADCRUMB STRAT  ===== -->
   <div class="breadcrumb section pt-60 pb-60">
    <div class="container">
      <h1 class="uppercase">Shop</h1>
      <ul>
        <li><a href="/"><i class="fa fa-home"></i></a></li>
        <li class="active">Shop</li>
      </ul>
    </div>
  </div>
  <!-- =====  BREADCRUMB END===== -->
  <div class="product-section section mt-30">
  <!-- =====  CONTAINER START  ===== -->
  <div class="container">
    <div class="row">
      <div id="column-left" class="col-lg-3 col-xl-3 col-sm-4" style="background-color:#f5f5f5;padding: 20px;">
     
          <div class="nav-responsive">
            <div class="heading-part">
              <h3 class="section_title">Brand</h3>
            </div>
            <div id="filter-group1" style="padding-left: 0;">
                @foreach($brand as $key=>$brand)
                <div class="checkbox">
                    <input type="checkbox" class="custom-control-input common_selector brand"  value="{{$brand->brand_id}}" id="brand-{{$key}}" @if(isset($_GET['brand'])) @if($_GET['brand']==$brand->brand_id) {{'checked'}} @endif @endif>
                    <label class="custom-control-label" for="brand-{{$key}}">{{$brand->brand_name}} </label>
                </div>
                @endforeach

          
              </div>
          </div>

          <div class="nav-responsive" >
            <div class="heading-part">
              <h3 class="section_title">Size</h3>
            </div>
            <div id="filter-group1" style="padding-left: 0;">
                @foreach($product_size as $key1=>$product_size)
                @if($product_size->weight!=0)
                <div class="checkbox">
                    <input type="checkbox" class="custom-control-input common_selector size" value="{{$product_size->weight}}" id="size-{{$key1}}">
                    <label class="custom-control-label" for="size-{{$key1}}">@if($product_size->weight>999) {{$product_size->weight/1000}}Kg @else {{$product_size->weight}}gm @endif
                </div>

                @else
                <div class="checkbox">
                    <input type="checkbox" class="custom-control-input common_selector size" value="{{$product_size->weight}}" id="size-{{$key1}}">
                    <label class="custom-control-label" for="size-{{$key1}}">No Size
                </div>
                @endif
              

                @endforeach

          
              </div>
          </div>


          <div class="nav-responsive" >
            <div class="heading-part">
              <h3 class="section_title">Price Filter</h3>
            </div>
            <div id="filter-group1" style="padding: 0 15px 0 0;">
               
            
                <div class="collection-collapse-block-content">
                    <input type="hidden" id="hidden_minimum_price" value="0" />
                    <input type="hidden" id="hidden_maximum_price" value="0" />
            
                    <div class="price-slider">
                    <input value="0" min="0" class="price_range" max="50000" step="500" type="range" />
                    <input value="50000" class="price_range" min="0" max="50000" step="500" type="range" />
                        <span>
                        <input type="number"  id="min" value="0" min="0" max="50000"/>  -  
                        <input type="number"  id="max" value="50000" min="0" max="50000"/>
                        </span>
                    </div>

                </div>
          
              </div>
          </div>


          <div class="nav-responsive" >
            <div class="heading-part">
              <h3 class="section_title">Review and Ratting</h3>
            </div>
            <div id="filter-group1" style="padding-left: 0;">
               
                <div class="checkbox">
                    <input type="checkbox" name="review" class="custom-control-input common_selector review" value="4" id="review-5">
                    <label class="custom-control-label" for="review-5" style="font-weight: 100;">4★ & above</label>
                </div>
                <div class="checkbox">
                    <input type="checkbox" name="review" class="custom-control-input common_selector review" value="3" id="review-4">
                        <label class="custom-control-label" for="review-4" style="font-weight: 100;">3★ & above</label>
                </div>
                <div class="checkbox">
                    <input type="checkbox" name="review" class="custom-control-input common_selector review" value="2" id="review-3">
                    <label class="custom-control-label" for="review-3" style="font-weight: 100;">2★ & above</label>
                </div>
                <div class="checkbox">
                    <input type="checkbox" name="review" class="custom-control-input common_selector review" value="1" id="review-2">
                    <label class="custom-control-label" for="review-2" style="font-weight: 100;">1★ & above</label>
                </div>
                

          
              </div>
          </div>
         



        



          <div id="category-menu" class="mb-30 mt-10">
            <div class="nav-responsive">
              <div class="heading-part">
                <h3 class="section_title">Top category</h3>
              </div>
              <ul>
                @php($cat=DB::table('cats')->get())
                @foreach($cat as $key=>$cats)
                <li><a href="/shop?cat_id={{$cats->cat_id}}">{{$cats->cat_name}}</a></li>
                @endforeach
            
              </ul>
            </div>
          </div>

      </div>

      
      <div class="col-lg-9 col-xl-9 col-sm-8">
        <div class="category-page-wrapper mb-15 pb-10" style="background-color:#f5f5f5;    padding: 10px;">
          <div class="row">
          <div class="col list-grid-wrapper Order-1">
            <div class="btn-group btn-list-grid">
              <button type="button" id="grid-view" class="btn btn-default grid-view active"></button>
              <button type="button" id="list-view" class="btn btn-default list-view"></button>
            </div>
          </div>
        
          <div class="col-md-auto sort-wrapper order-11">
            <label class="control-label" for="input-sort">Sort By :</label>
            <div class="sort-inner">
                <select class="form-control"  id="status">
                <option value="0" selected>Default sorting</option>
                <option value="popular">Sort by popularity</option>
            
                <option value="latest">Sort by latest</option>

                <option value="highretting">Rating(low to high)</option>
                <option value="lowretting">Rating(high to low)</option>

                <option value="low">Sort by price: low to high</option>
                <option value="high">Sort by price: high to low</option>

            </select>
            </div>
          </div>
        </div>
        </div>
        <input type="hidden" name="hidden_page" id="hidden_page" value="1" />
        <input type="hidden" name="hidden_column_name" id="hidden_column_name" value="id" />
        <input type="hidden" name="hidden_sort_type" id="hidden_sort_type" value="asc" />
        <div class="" id="pills-tabContent">
     
       

        </div>
           
        
      

       
      </div>
    </div>
  </div>
  <!-- =====  CONTAINER END  ===== -->
  <hr>
  </div>
@endsection

<script
src="https://code.jquery.com/jquery-3.4.1.js"
integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
crossorigin="anonymous">



</script>



<script>
$(document).ready(function(){

  var page = $('#hidden_page').val();
      filter_data(page);
       $(document).on('click', '.pagination a', function(event){
       event.preventDefault();
       var page = $(this).attr('href').split('page=')[1];
       $('#hidden_page').val(page);
       $('li').removeClass('active');
        $(this).parent().addClass('active');
        filter_data(page);
 });





function filter_data(page)
{
$("#overlay").fadeIn(300);
var token = $("#_token").val();
var page=$("#hidden_page").val();
var type = $('#type').val();
var cat_id= $('#cat_id').val();
var sub_cat_id= $('#sub_cat_id').val();
var sub_sub_cat_id= $('#sub_sub_cat_id').val();
var minimum_price = $('#hidden_minimum_price').val();
var maximum_price = $('#hidden_maximum_price').val();
var brand = get_filter('brand');
var Product_name =$('#Product_name').val();
var review = get_filter('review');
var size = get_filter('size');
var user_type=$('#user_type').val();
var status = $('#status').val();
$.ajax({
    url:"fetch_data",
    method:"GET",
    data:{page:page,_token:token,type:type,cat_id:cat_id,sub_cat_id:sub_cat_id,sub_sub_cat_id:sub_sub_cat_id, minimum_price:minimum_price, maximum_price:maximum_price, brand:brand, review:review,size:size,status:status,Product_name:Product_name},
    success:function(response){
        $('#grid-view').addClass("active");
        $('#list-view').removeClass("active");
        var abcd=response.split("malayssj");
        $("#view_total").html(abcd[0]);
$("#pills-tabContent").html(abcd[1]);
$("#overlay").fadeOut(300);
    }
});
}


function get_filter(class_name)
{
  $('#hidden_page').val(1);
var filter = [];
$('.'+class_name+':checked').each(function(){
    filter.push($(this).val());
});
return filter;
}
$('.common_selector').click(function(){
  var page = $('#hidden_page').val();
filter_data(page);
});
$('#status').change(function(){
  var page = $('#hidden_page').val();
filter_data(page);
});
$('.price_range').change(function(){
var minimum_price = $('#min').val();
var maximum_price = $('#max').val();
$('#hidden_minimum_price').val(minimum_price);
    $('#hidden_maximum_price').val(maximum_price);
    var page = $('#hidden_page').val();
filter_data(page);

});
$('#slider-range').click(function(){

    var view=$("#amount").val();
    var price=view.split("-");
    var minimum_price=price[0];
    var maximum_price=price[1];
    var page = $('#hidden_page').val();
$('#hidden_minimum_price').val(minimum_price);
    $('#hidden_maximum_price').val(maximum_price);
filter_data(page);
});
});
</script>


