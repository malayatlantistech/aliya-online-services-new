<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/abc', 'HomeController@abc')->name('/b');
Auth::routes();
Route::group(['domain' =>  env("USER_APP_URL")], function () {
Route::get('/', 'HomeController@index')->name('/');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/shop', 'HomeController@shop')->name('shop');
Route::get('/fetch_data', 'HomeController@fetch_data')->name('fetch_data');

//Route::get('/product_details', 'HomeController@product_details')->name('product_details');

Route::get('/product/{id}', 'HomeController@product_details')->name('product_details');
Route::get('/productDetails/{group}/{size}/{color}', 'HomeController@product_details1')->name('product_details1');


Route::post('login1', 'UserController@login1')->name('login1');
Route::post('loginWithOtp', 'UserController@loginWithOtp');
Route::get('loginWithOtp', 'UserController@loginWithOtp1')->name('loginWithOtp');
Route::get('smsRequest', 'UserController@smsRequest');
Route::post('sendOtp', 'UserController@sendOtp');
Route::post('user_register_otp', 'UserController@user_register_otp')->name('user_register_otp');
Route::post('sendOtp1', 'HomeController@sendOtp1');
Route::post('sendOtp11', 'HomeController@sendOtp11');
Route::get('mediator_register', 'UserController@register1')->name('mediator_register');
Route::post('register11', 'UserController@register')->name('register11');


Route::post('/wishlist_ajax', 'HomeController@wishlist_ajax');
Route::post('/addtowishlist_ajax', 'HomeController@addtowishlist_ajax')->name('addtowishlist_ajax');
Route::post('/addtowishlist_ajax1', 'HomeController@addtowishlist_ajax1')->name('addtowishlist_ajax1');

Route::post('/removetowishlist_ajax', 'HomeController@removetowishlist_ajax')->name('removetowishlist_ajax');


Route::post('/cart_ajax', 'HomeController@cart_ajax');
Route::post('/addtocart_ajax', 'HomeController@addtocart_ajax')->name('addtocart_ajax');
Route::post('/add_to_noe_cart', 'HomeController@add_to_noe_cart')->name('add_to_noe_cart');
Route::post('/add_to_cart_ajax1', 'HomeController@add_to_cart_ajax1')->name('add_to_cart_ajax1');
Route::post('/removetocart_ajax', 'HomeController@removetocart_ajax')->name('removetocart_ajax');
Route::post('/toggle', 'HomeController@toggle')->name('toggle');
Route::post('/toggle_wish', 'HomeController@toggle_wish')->name('toggle_wish');



Route::post('/price', 'HomeController@price')->name('price');

// check out section 
Route::get('/aa', 'HomeController@aa')->name('aa');
Route::post('/wallet', 'HomeController@wallet')->name('wallet');

Route::get('/checkout', 'AfterLoginController@checkout')->name('checkout');
Route::post('/address_submit', 'AfterLoginController@address_submit')->name('address_submit');
Route::post('/fetch_address_checkout', 'AfterLoginController@fetch_address_checkout')->name('fetch_address_checkout');
Route::post('/address_update', 'AfterLoginController@address_update')->name('address_update');
Route::post('/shipping_calculation', 'AfterLoginController@shipping_calculation')->name('shipping_calculation');
Route::post('/wallet_calculation', 'AfterLoginController@wallet_calculation')->name('wallet_calculation');
Route::post('/coupon_calculation', 'AfterLoginController@coupon_calculation')->name('coupon_calculation');
Route::post('/pay', 'AfterLoginController@pay')->name('pay');
Route::post('/order_submit', 'AfterLoginController@order_submit')->name('order_submit');
Route::get('/successfull', 'AfterLoginController@successfull')->name('successfull');

Route::post('/return_status', 'AfterLoginController@return_status')->name('return_status');
Route::post('/cencel_status', 'AfterLoginController@cencel_status')->name('cencel_status');



//check_out End
Route::get('/wishlist', 'AfterLoginController@wishlist')->name('wishlist');
Route::post('/wishlist_page_ajax', 'AfterLoginController@wishlist_page_ajax')->name('wishlist_page_ajax');

Route::get('/cart', 'AfterLoginController@cart')->name('cart');
Route::post('/cart_page_ajax', 'AfterLoginController@cart_page_ajax')->name('cart_page_ajax');
Route::post('/update_qty', 'AfterLoginController@update_qty')->name('update_qty');


Route::get('/profile', 'AfterLoginController@profile')->name('profile');
Route::post('/update_profile', 'AfterLoginController@update_profile')->name('update_profile');
Route::post('/change_password','AfterLoginController@change_password');


Route::post('billing_address', 'AfterLoginController@billing_address')->name('billing_address');
Route::post('fetch_address', 'AfterLoginController@fetch_address')->name('fetch_address');

Route::get('order', 'AfterLoginController@order')->name('order');

Route::post('/subscribe', 'HomeController@subscribe');
Route::post('/unsubscribe', 'HomeController@unsubscribe');
Route::post('/pinckeck', 'HomeController@pinckeck')->name('pinckeck');

Route::get('/refer', 'AfterLoginController@refer')->name('refer');
Route::post('/refersend', 'AfterLoginController@refersend');
Route::post('/sendReview', 'AfterLoginController@sendReview');
Route::post('/fafasend','HomeController@fafasend');

Route::get('/contact', 'HomeController@contact')->name('contact');
Route::post('/contactmail','HomeController@contactmail');

Route::get('/about', 'HomeController@about')->name('about');
Route::get('/faq', 'HomeController@faq')->name('faq');
Route::get('/privacy', 'HomeController@privacy')->name('privacy');
Route::get('/trams', 'HomeController@trams')->name('trams');
Route::get('/return', 'HomeController@return')->name('return');

Route::get('/offer', 'HomeController@offer')->name('offer');
Route::get('/wallet', 'AfterLoginController@my_wallet')->name('my_wallet');
Route::post('/account', 'AfterLoginController@account')->name('account');

Route::get('/reset_link', 'AccountsController@reset_link')->name('reset_link');
Route::get('/reset1', 'AccountsController@reset1')->name('reset1');
Route::post('/resetPassword1', 'AccountsController@resetPassword1')->name('resetPassword1');
Route::post('reset_password_without_token', 'AccountsController@validatePasswordRequest')->name('reset_password_without_token');

Route::get('/invoice', 'HomeController@htmlPDF58')->name('user_invoice');

});

Route::group(['domain' =>  env("ADMIN_APP_URL")], function () {
    Route::get('admin-login', 'UserController@admin_login')->name('admin_login');
    Route::get('/admin_logout', 'AdminController@admin_logout')->name('admin_logout');
    Route::get('/', 'AdminController@index')->name('admin_home');
    Route::get('/developer_setting', 'AdminController@developer_setting')->name('developer_setting');

    Route::get('/display_banner','AdminController@display_banner')->name('display_banner');
    Route::post('/add_display_banner','AdminController@add_display_banner');
    Route::get('/delete_display_banner','AdminController@delete_display_banner')->name('delete_display_banner');

     //coupon
     Route::get('/view_coupon','AdminController@view_coupon')->name('view_coupon');
     Route::get('/add_coupon','AdminController@add_coupon')->name('add_coupon');
     Route::post('/add_coupon_action','AdminController@add_coupon_action');
     Route::get('/update_coupon','AdminController@update_coupon')->name('update_coupon');
     Route::post('/update_coupon_action','AdminController@update_coupon_action');
     Route::get('/delete_coupon','AdminController@delete_coupon')->name('delete_coupon');
    
    

    //pincode
    Route::get('/pincode','AdminController@admin_pincode')->name('pincode');
    Route::get('/add_pincode','AdminController@admin_add_pincode');
    Route::post('/add_pincode_code','AdminController@admin_add_pincode_code');
    Route::get('/update_pincode','AdminController@admin_update_pincode');
    Route::post('/update_pincode_code','AdminController@admin_update_pincode_code');
    Route::get('/pincode_status','AdminController@pincode_status');
    Route::get('/delete_pincode','AdminController@delete_pincode');


    //user
    Route::get('/normal_user','AdminController@normal_user')->name('normal_user');
    Route::get('/mediator_user','AdminController@mediator_user')->name('mediator_user');
    Route::get('/subscribe_user','AdminController@subscribe_user')->name('subscribe_user');
    Route::post('/user_block','AdminController@user_block');

    Route::get('/inventory_report','AdminController@inventory_report')->name('inventory_report');
    Route::post('/sub_cat_ajax','AdminController@sub_cat_ajax');
    Route::post('/sub_sub_cat_ajax','AdminController@sub_sub_cat_ajax');
    Route::get('/view_product','AdminController@view_product')->name('view_product');

    Route::get('/view_category','AdminController@category')->name('view_category');
    Route::get('/add_category','AdminController@add_category')->name('add_category');
    Route::post('/category_action','AdminController@category_action');
    Route::get('/delete_category','AdminController@cat_delete');
    Route::post('/cat_update_action','AdminController@cat_update_action');
    Route::get('/update_category','AdminController@cat_update');

    Route::post('/sub_cat_update_action','AdminController@sub_cat_update_action');
    Route::get('/update_sub_category','AdminController@sub_cat_update');
    Route::get('/delete_sub_category','AdminController@sub_cat_delete');
    Route::post('/sub_category_action','AdminController@sub_category_action');
    Route::get('/add_sub_category','AdminController@add_sub_category');
    Route::get('/view_sub_category','AdminController@view_sub_category')->name('view_sub_category');
    Route::get('/all_cats_details', 'AdminController@all_cats_deail')->name('all_cats_details');

    Route::get('/view_sub_sub_category','AdminController@view_sub_sub_category')->name('view_sub_sub_category');
    Route::get('/add_sub_sub_category','AdminController@add_sub_sub_category');
    Route::post('/sub_sub_category_action','AdminController@sub_sub_category_action');
    Route::get('/update_sub_sub_category','AdminController@sub_sub_cat_update');
    Route::post('/sub_sub_cat_update_action','AdminController@sub_sub_cat_update_action');
    Route::get('/delete_sub_sub_category','AdminController@sub_sub_cat_delete');

    Route::get('/brand','AdminController@brand')->name('brand');
    Route::get('/add_brand','AdminController@add_brand');
    Route::post('/add_brand_action','AdminController@add_brand_action');
    Route::get('/update_brand','AdminController@update_brand');
    Route::post('/update_brand_action','AdminController@update_brand_action');
    Route::get('/delete_brand','AdminController@delete_brand');

    Route::get('/add_product','AdminController@add_product')->name('add_product');
    Route::post('/product_action','add_product@product_action');
    Route::get('/view_details','AdminController@view_details');

    Route::get('/approve1','AdminController@approve1');

    Route::get('/add_specification','AdminController@add_specification');
    Route::get('/add_features','AdminController@add_features');
    Route::get('/update_product_details','AdminController@update_product_details');
    Route::get('/update_product_description','AdminController@update_product_description');
    Route::post('/update_product_description_code','AdminController@update_product_description_code');
    Route::post('/update_product_details_code','AdminController@update_product_details_code');


    Route::post('/add_product_image','AdminController@add_product_image');
    Route::get('/delete_product_image','AdminController@delete_product_image');

    Route::post('/add_features_code','AdminController@add_features_code');
    Route::post('/add_specification_code','AdminController@add_specification_code');
    Route::get('/delete_feature','AdminController@delete_feature');
    Route::get('/delete_specification','AdminController@delete_specification');
    Route::get('/update_price','AdminController@update_price');
    Route::post('/update_price','AdminController@update_price_code');

    Route::get('/inventory_details','AdminController@inventory_details')->name('inventory_details');
    Route::get('/add_stock','AdminController@add_stock');
    Route::post('/add_stock_code','AdminController@add_stock_code');

    Route::get('/promotion_banner','AdminController@view_promotion_banner');
    Route::post('/add_promotion_banner','AdminController@add_promotion_banner');

    Route::get('/mediator_approve','AdminController@mediator_approve');

    Route::get('/pending_order','AdminController@pending_order');
    Route::get('/pending_order_admin','AdminController@pending_order_admin');
    Route::get('/view_booking_details','AdminController@view_booking_details');
    Route::get('/view_booking_details_admin','AdminController@view_booking_details_admin');
    Route::post('/order_status','AdminController@order_status');
    Route::post('/refund','AdminController@refund');
    Route::post('/refund1','AdminController@refund1');
    //review

    Route::get('/complete_order','AdminController@complete_order');
    Route::get('/complete_order_admin','AdminController@complete_order_admin');

    Route::get('/cancel_order','AdminController@cancel_order');
    Route::get('/cancel_order_admin','AdminController@cancel_order_admin');

    Route::get('/return_order','AdminController@return_order');
    Route::get('/return_order_admin','AdminController@return_order_admin');

    
    Route::get('/pending_return_order','AdminController@pending_return_order');
    Route::get('/complete_return_order','AdminController@complete_return_order');
    Route::get('/complete_return_order_admin','AdminController@complete_return_order_admin');


    Route::get('/invoice','AdminController@htmlPDF58');
    Route::get('generatePDF58','AdminController@generatePDF58');

    //tax_report
    Route::get('/tax_report','AdminController@tax_report')->name('tax_report');
    Route::post('/tax_report_ajax','AdminController@tax_report_ajax')->name('tax_report_ajax');

    //sales_report
    Route::get('/sales_report','AdminController@sales_report')->name('sales_report');
    Route::post('/sales_report_ajax','AdminController@sales_report_ajax')->name('sales_report_ajax');

    Route::get('/db_backup','AdminController@db_backup')->name('db_backup');

    Route::get('/group','AdminController@group')->name('group');
    Route::get('/add_group','AdminController@add_group');
    Route::post('/add_group_action','AdminController@add_group_action');
    Route::get('/update_group','AdminController@update_group');
    Route::post('/update_group_action','AdminController@update_group_action');
    Route::get('/delete_group','AdminController@delete_group');

    //delivery boy
    Route::get('/delivery_boy','AdminController@delivery_boy')->name('delivery_boy');
    Route::get('/add_delivery_boy','AdminController@add_delivery_boy')->name('add_delivery_boy');
    Route::post('/add_delivery_boy_action','AdminController@add_delivery_boy_action')->name('add_delivery_boy_action');
    Route::get('/delivered_pincodes','AdminController@delivered_pincodes')->name('delivered_pincodes');
    Route::get('/pin_approve','AdminController@pin_approve')->name('pin_approve');


    Route::get('/delivery_report','AdminController@delivery_report')->name('delivery_report');
    Route::get('/delivery_details','AdminController@delivery_details')->name('delivery_details');
    Route::get('/return_details','AdminController@return_details')->name('return_details');

    Route::get('/withdrawal','AdminController@withdrawal')->name('withdrawal');

    Route::get('/withdrawl_pay', 'AdminController@withdrawl_pay')->name('withdrawl_pay');

    Route::get('/delivery_slip', 'AdminController@delivery_slip')->name('delivery_slip');

    Route::get('delivery_slip_download','AdminController@delivery_slip_download');


    Route::get('/view_vendor','AdminController@view_vendor');
    Route::get('/updateVendorBlockStatus','AdminController@updateVendorBlockStatus');
    Route::get('/approveStatus','AdminController@approveStatus');
    Route::get('/viewDetails','AdminController@viewDetails');

    

    Route::get('/withdrawal_request','AdminController@withdrawal_request');
    Route::get('/updatepaymentStatus','AdminController@updatepaymentStatus');

    

});

// Route::group(['domain' =>  env("DELIVERY_BOY_APP_URL")], function () {
//     Route::get('/','DeliveryBoyController@dashboard')->name('/delivery_index')->middleware('delivery_middleware');
//     Route::get('/dashboard','DeliveryBoyController@dashboard')->name('dashboard')->middleware('delivery_middleware');
    
//     Route::get('delivery_login', 'DeliveryBoyController@delivery_login')->name('delivery_login')->middleware('redirectdeliverymiddleware');
//     Route::get('delivery_login_mobile', 'DeliveryBoyController@delivery_login_mobile')->name('delivery_login_mobile')->middleware('redirectdeliverymiddleware');
//     Route::get('delivery_register', 'DeliveryBoyController@delivery_register')->name('delivery_register')->middleware('redirectdeliverymiddleware');
//     Route::get('/delivery_logout', 'DeliveryBoyController@delivery_logout')->name('delivery_logout')->middleware('delivery_middleware');
//     Route::post('/delivery_sendOtp', 'DeliveryBoyController@delivery_sendOtp')->name('delivery_sendOtp');
//     Route::post('/loginWithOtp_driver','DeliveryBoyController@loginWithOtp_driver')->name('loginWithOtp_driver');
//     Route::get('/delivery_profile','DeliveryBoyController@delivery_profile')->name('delivery_profile');
//     Route::get('/change_password','DeliveryBoyController@delivery_change_password')->name('change_password');
//     Route::post('/edit_profile','DeliveryBoyController@edit_profile')->name('edit_profile');
//     Route::post('/delivery_change_password_password','DeliveryBoyController@delivery_change_password_password')->name('delivery_change_password_password');
//     Route::get('/bank_details', 'DeliveryBoyController@bank_details')->name('bank_details');
//     Route::post('/add_bank_details_code', 'DeliveryBoyController@add_bank_details_code')->name('add_bank_details_code');

//     Route::get('/pincode_add', 'DeliveryBoyController@add_pincode')->name('delivery_pincode_app')->middleware('delivery_middleware');
//     Route::post('/add_pincode_action', 'DeliveryBoyController@add_pincode_action')->name('add_pincode_action')->middleware('delivery_middleware');

//     Route::get('/pincode', 'DeliveryBoyController@delivery_pincode')->name('delivery_pincode')->middleware('delivery_middleware');

//     Route::post('/delivery_pin_status_change', 'DeliveryBoyController@delivery_pin_status_change')->name('delivery_pin_status_change')->middleware('delivery_middleware');

//     Route::get('/delivery_pending', 'DeliveryBoyController@pending_delivery')->name('pending_delivery')->middleware('delivery_middleware');
    
//     Route::get('/delivery_complete', 'DeliveryBoyController@complete_delivery')->name('complete_delivery')->middleware('delivery_middleware');
//     Route::get('/delivery_pending_return', 'DeliveryBoyController@pending_return_delivery')->name('pending_return_delivery')->middleware('delivery_middleware');
    
//     Route::get('/delivery_complete_return', 'DeliveryBoyController@complete_return_delivery')->name('complete_return_delivery')->middleware('delivery_middleware');

//     Route::get('/booking_details', 'DeliveryBoyController@booking_details')->name('booking_details')->middleware('delivery_middleware');

//     Route::get('/driver_wallet', 'DeliveryBoyController@driver_wallet')->name('driver_wallet')->middleware('delivery_middleware');
//     Route::get('/amount_withdrawal', 'DeliveryBoyController@amount_withdrawal')->name('amount_withdrawal')->middleware('delivery_middleware');
//     Route::post('/withdrawal_request_code', 'DeliveryBoyController@withdrawal_request_code')->name('withdrawal_request_code')->middleware('delivery_middleware');
   
//     Route::get('/qr_code_scan', 'DeliveryBoyController@qr_code_scan')->name('qr_code_scan')->middleware('delivery_middleware');
//     Route::post('/qr_scan_result', 'DeliveryBoyController@qr_scan_result')->name('qr_scan_result')->middleware('delivery_middleware');


//     Route::get('/experiment', 'DeliveryBoyController@experiment')->name('experiment')->middleware('delivery_middleware');
//     Route::post('/experiment_code', 'DeliveryBoyController@experiment_code')->name('experiment_code')->middleware('delivery_middleware');
   
//     Route::post('/register_otp', 'DeliveryBoyController@register_otp')->name('register_otp');
//     Route::post('/driver_order_status','DeliveryBoyController@driver_order_status');
//     Route::get('/contact_us','DeliveryBoyController@contact_us');
// });



Route::group(['domain' =>  env("VENDOR_APP_URL")], function () {
    Route::get('/','VendorController@homepage')->name('/delivery_index')->middleware('redirectdeliverymiddleware');
    Route::get('/dashboard','VendorController@dashboard')->name('dashboard')->middleware('vendor_middleware');

    Route::get('/welcome','VendorController@welcome')->name('welcome');
    Route::get('/block','VendorController@block')->name('block');
    Route::get('/approve','VendorController@approve')->name('approve');

    Route::get('/profile','VendorController@profile')->name('vendorProfile');
    Route::get('/store_address','VendorController@store_address')->name('store_address');
    Route::post('/store_address','VendorController@store_address_action')->name('store_address_action');
    Route::get('/change_password','VendorController@change_password')->name('change_password');
    Route::post('/change_password1','VendorController@change_password1')->name('change_password1');
    Route::get('/bank','VendorController@bank')->name('bank');
    Route::post('/add_bank','VendorController@add_bank');
    Route::post('/update_bank','VendorController@update_bank');


    Route::get('/vendor_login', 'VendorController@vendor_login')->name('vendor_login')->middleware('redirectdeliverymiddleware');
    Route::get('/vendor_register', 'VendorController@vendor_register')->name('vendor_register')->middleware('redirectdeliverymiddleware');
    Route::post('/vendor_register_action', 'VendorController@vendor_register_action')->name('vendor_register_action');
    Route::get('/vendor_logout', 'VendorController@vendor_logout')->name('vendor_logout')->middleware('vendor_middleware');



    Route::get('/view_category','VendorController@category');
    Route::get('/view_sub_category','VendorController@view_sub_category');
    Route::get('/all_cats_details', 'VendorController@all_cats_deail');
    Route::get('/view_sub_sub_category','VendorController@view_sub_sub_category');



    Route::get('/group','VendorController@group')->name('vendorgroup');
    Route::get('/add_group','VendorController@add_group');
    Route::post('/add_group_action','VendorController@add_group_action');
    Route::get('/update_group','VendorController@update_group');
    Route::post('/update_group_action','VendorController@update_group_action');
    Route::get('/delete_group','VendorController@delete_group');

    Route::get('/redeem','VendorController@redeem')->name('redeem');



    Route::get('/brand','VendorController@brand');
    Route::get('/add_brand','VendorController@add_brand');
    Route::post('/add_brand_action','VendorController@add_brand_action');
    Route::get('/update_brand','VendorController@update_brand');
    Route::post('/update_brand_action','VendorController@update_brand_action');
    Route::get('/delete_brand','VendorController@delete_brand');

    Route::get('/add_product','VendorController@add_product');
    Route::post('/product_action','add_product@product_action');
    Route::get('/view_details','VendorController@view_details');
    Route::get('/view_product','VendorController@view_product');
    Route::post('/sub_cat_ajax','VendorController@sub_cat_ajax');
    Route::post('/sub_sub_cat_ajax','VendorController@sub_sub_cat_ajax');



    Route::get('/add_specification','VendorController@add_specification');
    Route::get('/add_features','VendorController@add_features');
    Route::get('/update_product_details','VendorController@update_product_details');
    Route::get('/update_product_description','VendorController@update_product_description');
    Route::post('/update_product_description_code','VendorController@update_product_description_code');
    Route::post('/update_product_details_code','VendorController@update_product_details_code');


    Route::post('/add_product_image','VendorController@add_product_image');
    Route::get('/delete_product_image','VendorController@delete_product_image');

    Route::post('/add_features_code','VendorController@add_features_code');
    Route::post('/add_specification_code','VendorController@add_specification_code');
    Route::get('/delete_feature','VendorController@delete_feature');
    Route::get('/delete_specification','VendorController@delete_specification');
    Route::get('/update_price','VendorController@update_price');
    Route::post('/update_price','VendorController@update_price_code');

    Route::get('/approve1','VendorController@approve1');
    
    
    Route::get('/pending_order','VendorController@pending_order');
    Route::get('/view_booking_details','VendorController@view_booking_details');
    Route::get('/complete_order','VendorController@complete_order');
    Route::get('/complete_order_admin','VendorController@complete_order_admin');
    Route::get('/cancel_order','VendorController@cancel_order');
    Route::get('/return_order','VendorController@return_order');
    Route::get('/pending_return_order','VendorController@pending_return_order');
    Route::get('/complete_return_order','VendorController@complete_return_order');


    Route::get('/inventory_report','VendorController@inventory_report')->name('inventory_report1');
    Route::get('/inventory_details','VendorController@inventory_details')->name('inventory_details1');
    Route::get('/add_stock','VendorController@add_stock');
    Route::post('/add_stock_code','VendorController@add_stock_code');

    // Route::get('/mediator_approve','AdminController@mediator_approve');


    Route::post('/order_status','VendorController@order_status');

    // Route::post('/refund','AdminController@refund');
    // Route::post('/refund1','AdminController@refund1');
    // //review



    // Route::get('/invoice','AdminController@htmlPDF58');
    // Route::get('generatePDF58','AdminController@generatePDF58');

    // //tax_report
    // Route::get('/tax_report','AdminController@tax_report')->name('tax_report');
    // Route::post('/tax_report_ajax','AdminController@tax_report_ajax')->name('tax_report_ajax');

    // //sales_report
    // Route::get('/sales_report','AdminController@sales_report')->name('sales_report');
    // Route::post('/sales_report_ajax','AdminController@sales_report_ajax')->name('sales_report_ajax');

    // Route::get('/db_backup','AdminController@db_backup')->name('db_backup');




    // Route::get('/delivery_report','AdminController@delivery_report')->name('delivery_report');
    // Route::get('/delivery_details','AdminController@delivery_details')->name('delivery_details');
    // Route::get('/return_details','AdminController@return_details')->name('return_details');
    // Route::get('/withdrawal','AdminController@withdrawal')->name('withdrawal');
    // Route::get('/withdrawl_pay', 'AdminController@withdrawl_pay')->name('withdrawl_pay');
    // Route::get('/delivery_slip', 'AdminController@delivery_slip')->name('delivery_slip');
    // Route::get('delivery_slip_download','AdminController@delivery_slip_download');

    Route::get('/edit_profile','VendorController@edit_profile');
    Route::post('/edit_profile_code','VendorController@edit_profile_code');
    Route::get('/wallet','VendorController@wallet');

    Route::post('/vendor_withdrawal','VendorController@vendor_withdrawal');
    


});