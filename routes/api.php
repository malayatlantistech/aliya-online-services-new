<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::get('/', 'API\UserController@index');

Route::post('phone_no_check', 'API\UserController@phone_no_check');
Route::post('login', 'API\UserController@login');
Route::post('userregisters', 'API\UserController@register');
Route::group(['middleware' => 'auth:api'], function(){
Route::post('details', 'API\UserController@details');
Route::post('user_wallet', 'API\UserController@user_wallet');
Route::post('cart', 'API\UserController@cart');
Route::post('addtocart', 'API\ApiController@addtocart');
Route::post('removetocart', 'API\ApiController@removetocart');
Route::post('addtowishlist', 'API\ApiController@addtowishlist');
Route::post('removetowishlist', 'API\ApiController@removetowishlist');
Route::post('address', 'API\ApiController@address');
Route::post('add_update_address', 'API\ApiController@add_update_address');
Route::post('checkout_item', 'API\ApiController@checkout_item');
Route::post('update_profile', 'API\UserController@update_profile');
Route::post('wish_list', 'API\UserController@wish_list');
Route::post('coupon_calculation', 'API\UserController@coupon_calculation');

Route::post('pay', 'API\ApiController@pay');
Route::post('order_submit', 'API\ApiController@order_submit');
Route::post('wishlisttocart', 'API\ApiController@wishlisttocart');

Route::get('booking_history', 'API\ApiController@booking_history');
Route::post('cancel_order', 'API\ApiController@cancel_order');
Route::post('return_order', 'API\ApiController@return_order');
Route::post('sendReview', 'API\ApiController@sendReview');





});



Route::get('cat', 'API\ApiController@cat');
Route::get('sub_cat', 'API\ApiController@sub_cat');
Route::get('sub_sub_cat', 'API\ApiController@sub_sub_cat');
Route::get('product', 'API\ApiController@product');
Route::get('product_details', 'API\ApiController@product_details');
Route::get('new_arrival', 'API\ApiController@new_arrival');
Route::get('best_seller', 'API\ApiController@best_seller');
Route::get('mostViewedProduct', 'API\ApiController@mostViewedProduct');
Route::post('pincheck', 'API\ApiController@pincheck');
Route::post('find_delivery_charge', 'API\ApiController@find_delivery_charge');
Route::get('coupon_banner', 'API\ApiController@coupon_banner');
Route::get('display_banner', 'API\ApiController@display_banner');
Route::get('option', 'API\ApiController@option');
Route::get('search', 'API\UserController@search');
Route::post('filter_data', 'API\ApiController@shop');
Route::post('fetch_data', 'API\ApiController@fetch_data');

